diff --git a/examples/20_nested_variant/src/main.c b/examples/20_nested_variant/src/main.c
index b9e16e8df..cac835a47 100644
--- a/examples/20_nested_variant/src/main.c
+++ b/examples/20_nested_variant/src/main.c
@@ -60,4 +60,4 @@ int main(int argc, char *argv[]) {
 
     /* Cleanup */
     return ecs_fini(world);
-}
\ No newline at end of file
+}
diff --git a/src/column_system.c b/src/column_system.c
index 548e58363..910de4745 100644
--- a/src/column_system.c
+++ b/src/column_system.c
@@ -86,14 +86,25 @@ void add_table(
     ecs_table_t *table)
 {
     ecs_matched_table_t *table_data;
-    ecs_type_t table_type = table->type;
+    ecs_type_t table_type = table ? table->type : NULL;
     uint32_t column_count = ecs_vector_count(system_data->base.columns);
 
     /* Initially always add table to inactive group. If the system is registered
      * with the table and the table is not empty, the table will send an
      * activate signal to the system. */
-    table_data = ecs_vector_add(
-        &system_data->inactive_tables, &matched_table_params);
+    if (table) {
+        table_data = ecs_vector_add(
+            &system_data->inactive_tables, &matched_table_params);
+    } else {
+        /* If no table is provided to function, this is a system that contains
+         * no columns that require table matching. In this case, the system will
+         * only have one "dummy" table that caches data from the system columns.
+         * Always add this dummy table to the list of active tables, since it
+         * would never get activated otherwise. */
+        table_data = ecs_vector_add(
+            &system_data->tables, &matched_table_params);
+    }
+
     table_data->table = table;
     table_data->references = NULL;
 
@@ -171,7 +182,7 @@ void add_table(
         if (!entity && kind != EcsFromEmpty) {
             if (component) {
                 /* Retrieve offset for component */
-                table_data->columns[c] = ecs_type_index_of(table->type, component);
+                table_data->columns[c] = ecs_type_index_of(table_type, component);
 
                 /* If column is found, add one to the index, as column zero in
                  * a table is reserved for entity id's */
@@ -236,6 +247,8 @@ void add_table(
                         e = entity;
                     } else if (kind == EcsCascade) {
                         e = entity;
+                    } else if (kind == EcsFromSystem) {
+                        e = entity;
                     } else {
                         e = ecs_get_entity_for_component(
                             world, entity, table_type, component);
@@ -275,7 +288,9 @@ void add_table(
         table_data->components[c] = component;
     }
 
-    ecs_table_register_system(world, table, system);
+    if (table) {
+        ecs_table_register_system(world, table, system);
+    }
 }
 
 /* Remove table */
@@ -299,6 +314,10 @@ bool match_table(
 {
     (void)system; /* useful for debugging */
 
+    if (!system_data->base.needs_tables) {
+        return false;
+    }
+
     ecs_type_t type, table_type = table->type;
 
     if (!system_data->base.match_disabled && ecs_type_has_entity_intern(
@@ -746,6 +765,7 @@ ecs_entity_t ecs_new_col_system(
     system_data->base.kind = kind;
     system_data->base.cascade_by = 0;
     system_data->base.has_refs = false;
+    system_data->base.needs_tables = ecs_needs_tables(world, sig);
 
     system_data->column_params.element_size = sizeof(int32_t) * (count);
     system_data->ref_params.element_size = sizeof(ecs_reference_t) * count;
@@ -765,7 +785,15 @@ ecs_entity_t ecs_new_col_system(
 
     ecs_system_init_base(world, &system_data->base);
 
-    match_tables(world, result, system_data);
+    if (system_data->base.needs_tables) {
+        match_tables(world, result, system_data);
+    } else {
+        /* If this system does not match with tables, for example, because it
+         * does not have any SELF columns, add a single "matched" table that
+         * caches the data for the columns, but does not have a reference to an
+         * actual table. */
+        add_table(world, result, system_data, NULL /* table is NULL */);
+    }
 
     ecs_entity_t *elem = NULL;
 
@@ -897,45 +925,53 @@ ecs_entity_t _ecs_run_w_filter(
 
     for (i = 0; i < table_count; i ++) {
         ecs_matched_table_t *table = &tables[i];
-
         ecs_table_t *world_table = table->table;
-        ecs_table_column_t *table_data = world_table->columns;
-        uint32_t first = 0, count = ecs_table_count(world_table);
+        ecs_table_column_t *table_data = NULL;
+        uint32_t first = 0, count = 0;
 
-        if (filter) {
-            if (!ecs_type_contains(
-                real_world, world_table->type, filter, true, true))
-            {
-                continue;
-            }
-        }
+        if (world_table) {
+            table_data = world_table->columns;
+            count = ecs_table_count(world_table);
 
-        if (offset_limit) {
-            if (offset) {
-                if (offset > count) {
-                    offset -= count;
+            if (filter) {
+                if (!ecs_type_contains(
+                    real_world, world_table->type, filter, true, true))
+                {
                     continue;
-                } else {
-                    first += offset;
-                    count -= offset;
-                    offset = 0;
                 }
             }
 
-            if (limit) {
-                if (limit > count) {
-                    limit -= count;
-                } else {
-                    count = limit;
-                    limit = 0;
+            if (offset_limit) {
+                if (offset) {
+                    if (offset > count) {
+                        offset -= count;
+                        continue;
+                    } else {
+                        first += offset;
+                        count -= offset;
+                        offset = 0;
+                    }
+                }
+
+                if (limit) {
+                    if (limit > count) {
+                        limit -= count;
+                    } else {
+                        count = limit;
+                        limit = 0;
+                    }
+                } else if (limit_set) {
+                    break;
                 }
-            } else if (limit_set) {
-                break;
             }
-        }
 
-        if (!count) {
-            continue;
+            if (!count) {
+                continue;
+            }
+
+            ecs_entity_t *entity_buffer = 
+                    ecs_vector_first(table_data[0].data);
+            info.entities = &entity_buffer[first];            
         }
 
         if (table->references) {
@@ -944,16 +980,12 @@ ecs_entity_t _ecs_run_w_filter(
             info.references = NULL;
         }
 
-        info.columns =  table->columns;
+        info.columns = table->columns;
         info.table = world_table;
         info.table_columns = table_data;
         info.components = table->components;
         info.offset = first;
         info.count = count;
-
-        ecs_entity_t *entity_buffer = 
-                ecs_vector_first(((ecs_table_column_t*)info.table_columns)[0].data);
-        info.entities = &entity_buffer[first];
         
         action(&info);
 
diff --git a/src/flecs_private.h b/src/flecs_private.h
index 47048d2b3..5ec13049a 100644
--- a/src/flecs_private.h
+++ b/src/flecs_private.h
@@ -88,6 +88,11 @@ void ecs_world_activate_system(
 ecs_stage_t *ecs_get_stage(
     ecs_world_t **world_ptr);
 
+/* Get array for system kind */
+ecs_vector_t** ecs_system_array(
+    ecs_world_t *world,
+    EcsSystemKind kind);
+
 /* -- Stage API -- */
 
 /* Initialize stage data structures */
diff --git a/src/stats.c b/src/stats.c
index 831e038cd..1aca866bb 100644
--- a/src/stats.c
+++ b/src/stats.c
@@ -158,7 +158,6 @@ void get_memory_stats(
     ecs_vector_memory(world->post_update_systems, &handle_arr_params, &memory->systems.allocd, &memory->systems.used);
     ecs_vector_memory(world->pre_store_systems, &handle_arr_params, &memory->systems.allocd, &memory->systems.used);
     ecs_vector_memory(world->on_store_systems, &handle_arr_params, &memory->systems.allocd, &memory->systems.used);
-    ecs_vector_memory(world->tasks, &handle_arr_params, &memory->systems.allocd, &memory->systems.used);
     ecs_vector_memory(world->inactive_systems, &handle_arr_params, &memory->systems.allocd, &memory->systems.used);
     ecs_vector_memory(world->on_demand_systems, &handle_arr_params, &memory->systems.allocd, &memory->systems.used);
 
@@ -170,7 +169,6 @@ void get_memory_stats(
     calculate_systems_stats(world, world->post_update_systems, &memory->systems.allocd, &memory->systems.used);
     calculate_systems_stats(world, world->pre_store_systems, &memory->systems.allocd, &memory->systems.used);
     calculate_systems_stats(world, world->on_store_systems, &memory->systems.allocd, &memory->systems.used);
-    calculate_systems_stats(world, world->tasks, &memory->systems.allocd, &memory->systems.used);
     calculate_systems_stats(world, world->inactive_systems, &memory->systems.allocd, &memory->systems.used);
     calculate_systems_stats(world, world->on_demand_systems, &memory->systems.allocd, &memory->systems.used);
 
@@ -466,8 +464,6 @@ void ecs_get_stats(
         world, &stats->pre_store_systems, world->pre_store_systems);
     stats->system_count += system_stats_arr(
         world, &stats->on_store_systems, world->on_store_systems);
-    stats->system_count += system_stats_arr(
-        world, &stats->task_systems, world->tasks);
     stats->system_count += system_stats_arr(
         world, &stats->on_demand_systems, world->on_demand_systems);
 
diff --git a/src/system.c b/src/system.c
index d19c70105..ba1902f39 100644
--- a/src/system.c
+++ b/src/system.c
@@ -134,12 +134,8 @@ ecs_entity_t new_row_system(
 
     ecs_entity_t *elem = NULL;
 
-    if (!needs_tables) {
-        if (kind == EcsOnUpdate) {
-            elem = ecs_vector_add(&world->tasks, &handle_arr_params);
-        } else if (kind == EcsOnRemove) {
-            elem = ecs_vector_add(&world->fini_tasks, &handle_arr_params);
-        }
+    if (!needs_tables && kind == EcsOnRemove) {
+        elem = ecs_vector_add(&world->fini_tasks, &handle_arr_params);
     } else {
         if (kind == EcsOnAdd) {
             elem = ecs_vector_add(&world->add_systems, &handle_arr_params);
@@ -525,7 +521,7 @@ ecs_entity_t ecs_new_system(
         return result;
     }
 
-    if (needs_tables && (kind == EcsOnLoad || kind == EcsPostLoad ||
+    if ((kind == EcsOnLoad || kind == EcsPostLoad ||
                          kind == EcsPreUpdate || kind == EcsOnUpdate ||
                          kind == EcsOnValidate || kind == EcsPostUpdate ||
                          kind == EcsPreStore || kind == EcsOnStore ||
@@ -817,17 +813,18 @@ EcsSystem* get_system_ptr(
     ecs_world_t *world,
     ecs_entity_t system)
 {
+    EcsSystem *result = NULL;
     EcsColSystem *cs = ecs_get_ptr(world, system, EcsColSystem);
     if (cs) {
-        return (EcsSystem*)cs;
+        result = (EcsSystem*)cs;
     } else {
         EcsRowSystem *rs = ecs_get_ptr(world, system, EcsRowSystem);
         if (rs) {
-            return (EcsSystem*)rs;
+            result = (EcsSystem*)rs;
         }
     }
 
-    return NULL;
+    return result;
 }
 
 void ecs_set_system_context(
diff --git a/src/types.h b/src/types.h
index 0b5cd461e..bae06e59e 100644
--- a/src/types.h
+++ b/src/types.h
@@ -183,6 +183,7 @@ typedef struct EcsSystem {
     float time_spent;              /* Time spent on running system */
     bool enabled;                  /* Is system enabled or not */
     bool has_refs;                 /* Does the system have reference columns */
+    bool needs_tables;             /* Does the system need table matching */
     bool match_prefab;             /* Should this system match prefabs */
     bool match_disabled;           /* Should this system match disabled entities */
 } EcsSystem;
@@ -388,7 +389,6 @@ struct ecs_world {
 
     /* -- Tasks -- */
 
-    ecs_vector_t *tasks;              /* Periodic actions not invoked on entities */
     ecs_vector_t *fini_tasks;         /* Tasks to execute on ecs_fini */
 
 
diff --git a/src/world.c b/src/world.c
index ee2a59da8..9c490952e 100644
--- a/src/world.c
+++ b/src/world.c
@@ -263,8 +263,7 @@ ecs_table_t* ecs_world_get_table(
     return table;
 }
 
-static
-ecs_vector_t** frame_system_array(
+ecs_vector_t** ecs_system_array(
     ecs_world_t *world,
     EcsSystemKind kind)
 {
@@ -321,9 +320,9 @@ void ecs_world_activate_system(
 
     if (active) {
         src_array = world->inactive_systems;
-        dst_array = *frame_system_array(world, kind);
+        dst_array = *ecs_system_array(world, kind);
      } else {
-        src_array = *frame_system_array(world, kind);
+        src_array = *ecs_system_array(world, kind);
         dst_array = world->inactive_systems;
     }
 
@@ -344,7 +343,7 @@ void ecs_world_activate_system(
         &dst_array, src_array, &handle_arr_params, i);
 
     if (active) {
-         *frame_system_array(world, kind) = dst_array;
+         *ecs_system_array(world, kind) = dst_array;
          qsort(dst_array, ecs_vector_count(dst_array) + 1,
           sizeof(ecs_entity_t), compare_handle);
     } else {
@@ -636,7 +635,6 @@ ecs_world_t *ecs_init(void) {
     world->add_systems = ecs_vector_new(&handle_arr_params, 0);
     world->remove_systems = ecs_vector_new(&handle_arr_params, 0);
     world->set_systems = ecs_vector_new(&handle_arr_params, 0);
-    world->tasks = ecs_vector_new(&handle_arr_params, 0);
     world->fini_tasks = ecs_vector_new(&handle_arr_params, 0);
 
     world->type_sys_add_index = ecs_map_new(0, sizeof(ecs_vector_t*));
@@ -810,7 +808,6 @@ int ecs_fini(
     row_systems_deinit(world, world->add_systems);
     row_systems_deinit(world, world->remove_systems);
     row_systems_deinit(world, world->set_systems);
-    row_systems_deinit(world, world->tasks);
 
     row_index_deinit(world->type_sys_add_index);
     row_index_deinit(world->type_sys_remove_index);
@@ -832,7 +829,6 @@ int ecs_fini(
 
     ecs_vector_free(world->inactive_systems);
     ecs_vector_free(world->on_demand_systems);
-    ecs_vector_free(world->tasks);
     ecs_vector_free(world->fini_tasks);
 
     ecs_vector_free(world->add_systems);
@@ -1072,28 +1068,6 @@ void run_multi_thread_stage(
     }
 }
 
-static
-void run_tasks(
-    ecs_world_t *world)
-{
-    /* Run periodic row systems (not matched to any entity) */
-    uint32_t i, system_count = ecs_vector_count(world->tasks);
-    if (system_count) {
-        world->in_progress = true;
-
-        ecs_entity_t *buffer = ecs_vector_first(world->tasks);
-        for (i = 0; i < system_count; i ++) {
-            ecs_run_task(world, buffer[i]);
-        }
-
-        if (world->auto_merge) {
-            world->in_progress = false;
-            ecs_merge(world);
-            world->in_progress = true;
-        }
-    }
-}
-
 static
 float start_measure_frame(
     ecs_world_t *world,
@@ -1195,8 +1169,6 @@ bool ecs_progress(
         run_single_thread_stage(world, world->post_update_systems);
     }
 
-    run_tasks(world);
-
     run_single_thread_stage(world, world->pre_store_systems);
     run_single_thread_stage(world, world->on_store_systems);
 
diff --git a/test/api/project.json b/test/api/project.json
index dddad68b5..8b2cc3699 100644
--- a/test/api/project.json
+++ b/test/api/project.json
@@ -398,7 +398,8 @@
                 "from_system",
                 "on_remove_no_components",
                 "on_remove_one_tag",
-                "on_remove_from_system"
+                "on_remove_from_system",
+                "tasks_in_phases"
             ]
         }, {
             "id": "Container",
diff --git a/test/api/src/Tasks.c b/test/api/src/Tasks.c
index 63f853d82..e0e1c2e1c 100644
--- a/test/api/src/Tasks.c
+++ b/test/api/src/Tasks.c
@@ -122,3 +122,90 @@ void Tasks_on_remove_from_system() {
     test_int(ctx.column_count, 1);
     test_int(ctx.c[0][0], ecs_entity(Position));
 }
+
+static int phase_counter = 0;
+
+static 
+void OnLoadTask(ecs_rows_t *rows) {
+    test_assert(rows->entities == NULL);
+    test_int(rows->count, 0);
+    test_int(phase_counter, 0);
+    phase_counter ++;
+}
+
+static 
+void PostLoadTask(ecs_rows_t *rows) {
+    test_assert(rows->entities == NULL);
+    test_int(rows->count, 0);
+    test_int(phase_counter, 1);
+    phase_counter ++;
+}
+
+static 
+void PreUpdateTask(ecs_rows_t *rows) {
+    test_assert(rows->entities == NULL);
+    test_int(rows->count, 0);
+    test_int(phase_counter, 2);
+    phase_counter ++;
+}
+
+static 
+void OnUpdateTask(ecs_rows_t *rows) {
+    test_assert(rows->entities == NULL);
+    test_int(rows->count, 0);
+    test_int(phase_counter, 3);
+    phase_counter ++;
+}
+
+static 
+void OnValidateTask(ecs_rows_t *rows) {
+    test_assert(rows->entities == NULL);
+    test_int(rows->count, 0);
+    test_int(phase_counter, 4);
+    phase_counter ++;
+}
+
+static 
+void PostUpdateTask(ecs_rows_t *rows) {
+    test_assert(rows->entities == NULL);
+    test_int(rows->count, 0);
+    test_int(phase_counter, 5);
+    phase_counter ++;
+}
+
+static 
+void PreStoreTask(ecs_rows_t *rows) {
+    test_assert(rows->entities == NULL);
+    test_int(rows->count, 0);
+    test_int(phase_counter, 6);
+    phase_counter ++;
+}
+
+static 
+void OnStoreTask(ecs_rows_t *rows) {
+    test_assert(rows->entities == NULL);
+    test_int(rows->count, 0);
+    test_int(phase_counter, 7);
+    phase_counter ++;
+}
+
+void Tasks_tasks_in_phases() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+
+    ECS_SYSTEM(world, OnLoadTask, EcsOnLoad, .Position);
+    ECS_SYSTEM(world, PostLoadTask, EcsPostLoad, .Position);
+    ECS_SYSTEM(world, PreUpdateTask, EcsPreUpdate, .Position);
+    ECS_SYSTEM(world, OnUpdateTask, EcsOnUpdate, .Position);
+    ECS_SYSTEM(world, OnValidateTask, EcsOnValidate, .Position);
+    ECS_SYSTEM(world, PostUpdateTask, EcsPostUpdate, .Position);
+    ECS_SYSTEM(world, PreStoreTask, EcsPreStore, .Position);
+    ECS_SYSTEM(world, OnStoreTask, EcsOnStore, .Position);
+
+    ecs_progress(world, 1);
+
+    test_int(phase_counter, 8);
+
+    ecs_fini(world);
+}
diff --git a/test/api/src/main.c b/test/api/src/main.c
index 8c2964bb4..e8178c22d 100644
--- a/test/api/src/main.c
+++ b/test/api/src/main.c
@@ -356,6 +356,7 @@ void Tasks_from_system(void);
 void Tasks_on_remove_no_components(void);
 void Tasks_on_remove_one_tag(void);
 void Tasks_on_remove_from_system(void);
+void Tasks_tasks_in_phases(void);
 
 // Testsuite 'Container'
 void Container_child(void);
@@ -2053,7 +2054,7 @@ static bake_test_suite suites[] = {
     },
     {
         .id = "Tasks",
-        .testcase_count = 6,
+        .testcase_count = 7,
         .testcases = (bake_test_case[]){
             {
                 .id = "no_components",
@@ -2078,6 +2079,10 @@ static bake_test_suite suites[] = {
             {
                 .id = "on_remove_from_system",
                 .function = Tasks_on_remove_from_system
+            },
+            {
+                .id = "tasks_in_phases",
+                .function = Tasks_tasks_in_phases
             }
         }
     },
