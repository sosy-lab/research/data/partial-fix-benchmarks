diff --git a/opal/mca/pmix/pmix2x/Makefile.am b/opal/mca/pmix/pmix2x/Makefile.am
index 049238c6a40..bd9304e5d13 100644
--- a/opal/mca/pmix/pmix2x/Makefile.am
+++ b/opal/mca/pmix/pmix2x/Makefile.am
@@ -12,6 +12,8 @@
 
 EXTRA_DIST = autogen.subdirs
 
+dist_opaldata_DATA = help-pmix-pmix2x.txt
+
 SUBDIRS = pmix
 
 sources = \
diff --git a/opal/mca/pmix/pmix2x/help-pmix-pmix2x.txt b/opal/mca/pmix/pmix2x/help-pmix-pmix2x.txt
new file mode 100644
index 00000000000..4b88bf55271
--- /dev/null
+++ b/opal/mca/pmix/pmix2x/help-pmix-pmix2x.txt
@@ -0,0 +1,54 @@
+# -*- text -*-
+#
+# Copyright (c) 2004-2007 The Trustees of Indiana University and Indiana
+#                         University Research and Technology
+#                         Corporation.  All rights reserved.
+# Copyright (c) 2004-2005 The University of Tennessee and The University
+#                         of Tennessee Research Foundation.  All rights
+#                         reserved.
+# Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
+#                         University of Stuttgart.  All rights reserved.
+# Copyright (c) 2004-2005 The Regents of the University of California.
+#                         All rights reserved.
+# Copyright (c) 2017      Intel, Inc.  All rights reserved.
+# $COPYRIGHT$
+#
+# Additional copyrights may follow
+#
+# $HEADER$
+#
+# This is the US/English help file for Open MPI MCA error messages.
+#
+[pmix-override-prefix]
+We found that you have set OPAL_PREFIX in your environment, thus indicating
+that OMPI has been relocated. However, PMIX_INSTALL_PREFIX has also been set
+in your environment:
+
+  PMIX_INSTALL_PREFIX = %s
+
+OMPI was configured to use its internal copy of PMIx. Setting PMIX_INSTALL_PREFIX
+can cause us to accidentally use plugins from another PMIx installation on your
+system, leading to unstable/unpredictable behavior.
+
+We are overriding the current envar value with the value of OPAL_PREFIX to
+ensure stable behavior by using the plugins from our internal PMIx library.
+If this is not what you want, you may want to reconfigure OMPI to point to
+your own PMIx installation.
+
+You can set the MCA param pmix_pmix2x_silence_warning=1 to prevent this
+warning from being printed.
+
+[pmix-unset-prefix]
+We found that PMIX_INSTALL_PREFIX has been set in your environment, but
+OMPI was configured to use its internal copy of PMIx. This can cause us
+to accidentally use plugins from another PMIx installation on your system,
+leading to unstable/unpredictable behavior. The value we found is:
+
+  PMIX_INSTALL_PREFIX = %s
+
+We are unsetting this value to ensure stable behavior by using the plugins
+from our internal PMIx library. If this is not what you want, you may want
+to reconfigure OMPI to point to your own PMIx installation.
+
+You can set the MCA param pmix_pmix2x_silence_warning=1 to prevent this
+warning from being printed.
diff --git a/opal/mca/pmix/pmix2x/pmix2x.h b/opal/mca/pmix/pmix2x/pmix2x.h
index f8d93e55f81..0fb0355ea58 100644
--- a/opal/mca/pmix/pmix2x/pmix2x.h
+++ b/opal/mca/pmix/pmix2x/pmix2x.h
@@ -46,6 +46,7 @@ typedef struct {
   int cache_size;
   opal_list_t cache;
   opal_list_t dmdx;
+  bool silence_warning;
 } mca_pmix_pmix2x_component_t;
 
 OPAL_DECLSPEC extern mca_pmix_pmix2x_component_t mca_pmix_pmix2x_component;
diff --git a/opal/mca/pmix/pmix2x/pmix2x_client.c b/opal/mca/pmix/pmix2x/pmix2x_client.c
index 52d77fab99e..d4ec39ebc26 100644
--- a/opal/mca/pmix/pmix2x/pmix2x_client.c
+++ b/opal/mca/pmix/pmix2x/pmix2x_client.c
@@ -31,6 +31,7 @@
 #include "opal/util/argv.h"
 #include "opal/util/opal_environ.h"
 #include "opal/util/proc.h"
+#include "opal/util/show_help.h"
 
 #include "opal/mca/pmix/base/base.h"
 #include "pmix2x.h"
@@ -66,7 +67,7 @@ int pmix2x_client_init(opal_list_t *ilist)
     pmix_info_t *pinfo;
     size_t ninfo, n;
     opal_value_t *ival;
-    char *evar;
+    char *evar, *evar2;
 
     opal_output_verbose(1, opal_pmix_base_framework.framework_output,
                         "PMIx_client init");
@@ -78,9 +79,58 @@ int pmix2x_client_init(opal_list_t *ilist)
             asprintf(&dbgvalue, "PMIX_DEBUG=%d", dbg);
             putenv(dbgvalue);
         }
-        if ((NULL != (evar = getenv("OPAL_PREFIX"))) && 
-            (NULL == getenv("PMIX_INSTALL_PREFIX"))) {
-            opal_setenv("PMIX_INSTALL_PREFIX", evar, false, &environ);
+        /* We have to be a little careful about where the PMIx plugins are located.
+         * There is only one case we care about, and that is the case where the user
+         * built the embedded PMIx library. If they built against an external PMIx
+         * library, then they bear full responsibility for pointing us to the right
+         * location, including the location of the plugins.
+         *
+         * Given that we are using the embedded PMIx library, we have several potential
+         * use-cases to consider:
+         *
+         * (a) if OPAL_PREFIX is set, then we should automatically set PMIX_INSTALL_PREFIX
+         * to the OPAL_PREFIX location and override anything set in the user's
+         * environment. It is quite possible that the system has an installed PMIx
+         * library on it, and that the user may have PMIX_INSTALL_PREFIX set by default
+         * in their environment so they can use it. However, this will totally confuse
+         * the OMPI internal PMIx library as it will still attempt to operate (since
+         * their app was linked against OMPI, and OMPI was configured to use the
+         * internal library), but will mistakenly use the wrong plugins. Thus the
+         * requirement that we OVERRIDE the current environmental param.
+         *
+         * Ordinarily, we would have to propagate the PMIX_INSTALL_PREFIX value. However,
+         * we actually don't because OPAL_PREFIX is already being propagated for us! So
+         * all we need to do is detect that OPAL_PREFIX is set and do the right thing.
+         *
+         * (b) if OPAL_PREFIX is not set and PMIX_INSTALL_DIR is not set, then do nothing.
+         * The plugins will be installed in the correct place, and PMIx will look for them
+         * in that location
+         *
+         * (c) if OPAL_PREFIX is not set and PMIX_INSTALL_DIR is set, then must unset
+         * the PMIX_INSTALL_DIR envar to maintain consistency. Since the user might be
+         * calling PMIx functions directly from their application, and it is possible
+         * that they are relying on some behavior that we don't support (or is different)
+         * in our embedded version, We need to generate a warning (with a param to
+         * silence it) so they know we are redirecting them.\
+         */
+
+        /* Handle Case (a) */
+        if ((NULL != (evar = getenv("OPAL_PREFIX")))) {
+            /* check if they have PMIX_INSTALL_PREFIX set in their environment - if they
+             * do, then we need to warn them that we are overriding it */
+            if (NULL != (evar2 = getenv("PMIX_INSTALL_PREFIX")) &&
+                !mca_pmix_pmix2x_component.silence_warning) {
+                opal_show_help("help-pmix-pmix2x.txt", "pmix-override-prefix",
+                               true, evar2);
+            }
+            opal_setenv("PMIX_INSTALL_PREFIX", evar, true, &environ);
+        } else if (NULL != (evar2 = getenv("PMIX_INSTALL_PREFIX"))) {
+            /* Handle Case (c) */
+            if (!mca_pmix_pmix2x_component.silence_warning) {
+                opal_show_help("help-pmix-pmix2x.txt", "pmix-unset-prefix",
+                               true, evar2);
+            }
+            opal_unsetenv("PMIX_INSTALL_PREFIX", &environ);
         }
     }
 
diff --git a/opal/mca/pmix/pmix2x/pmix2x_component.c b/opal/mca/pmix/pmix2x/pmix2x_component.c
index 21785a7edf7..03246c11801 100644
--- a/opal/mca/pmix/pmix2x/pmix2x_component.c
+++ b/opal/mca/pmix/pmix2x/pmix2x_component.c
@@ -33,6 +33,7 @@ const char *opal_pmix_pmix2x_component_version_string =
 /*
  * Local function
  */
+static int external_register(void);
 static int external_open(void);
 static int external_close(void);
 static int external_component_query(mca_base_module_t **module, int *priority);
@@ -65,6 +66,7 @@ mca_pmix_pmix2x_component_t mca_pmix_pmix2x_component = {
             .mca_open_component = external_open,
             .mca_close_component = external_close,
             .mca_query_component = external_component_query,
+            .mca_register_component_params = external_register
         },
         /* Next the MCA v1.0.0 component meta data */
         .base_data = {
@@ -75,6 +77,21 @@ mca_pmix_pmix2x_component_t mca_pmix_pmix2x_component = {
     .native_launch = false
 };
 
+static int external_register(void)
+{
+    mca_base_component_t *component = &mca_pmix_pmix2x_component.super.base_version;
+
+    mca_pmix_pmix2x_component.silence_warning = false;
+    (void) mca_base_component_var_register (component, "silence_warning",
+                                            "Silence warning about PMIX_INSTALL_PREFIX",
+                                            MCA_BASE_VAR_TYPE_BOOL, NULL, 0, 0,
+                                            OPAL_INFO_LVL_4,
+                                            MCA_BASE_VAR_SCOPE_READONLY,
+                                            &mca_pmix_pmix2x_component.silence_warning);
+
+    return OPAL_SUCCESS;
+}
+
 static int external_open(void)
 {
     mca_pmix_pmix2x_component.evindex = 0;
diff --git a/opal/mca/pmix/pmix2x/pmix2x_server_south.c b/opal/mca/pmix/pmix2x/pmix2x_server_south.c
index 41abb8b4d58..2c418ee9836 100644
--- a/opal/mca/pmix/pmix2x/pmix2x_server_south.c
+++ b/opal/mca/pmix/pmix2x/pmix2x_server_south.c
@@ -100,7 +100,7 @@ int pmix2x_server_init(opal_pmix_server_module_t *module,
     opal_pmix2x_event_t *event;
     opal_pmix2x_jobid_trkr_t *job;
     opal_pmix_lock_t lk;
-    char *evar;
+    char *evar, *evar2;
 
     OPAL_PMIX_ACQUIRE_THREAD(&opal_pmix_base.lock);
 
@@ -109,9 +109,24 @@ int pmix2x_server_init(opal_pmix_server_module_t *module,
             asprintf(&dbgvalue, "PMIX_DEBUG=%d", dbg);
             putenv(dbgvalue);
         }
-        if ((NULL != (evar = getenv("OPAL_PREFIX"))) && 
-            (NULL == getenv("PMIX_INSTALL_PREFIX"))) {
-            opal_setenv("PMIX_INSTALL_PREFIX", evar, false, &environ);
+        /* SEE LENGTHY EXPLANATION IN PMIX2X_CLIENT.C */
+        /* Handle Case (a) */
+        if ((NULL != (evar = getenv("OPAL_PREFIX")))) {
+            /* check if they have PMIX_INSTALL_PREFIX set in their environment - if they
+             * do, then we need to warn them that we are overriding it */
+            if (NULL != (evar2 = getenv("PMIX_INSTALL_PREFIX")) &&
+                !mca_pmix_pmix2x_component.silence_warning) {
+                opal_show_help("help-pmix-pmix2x.txt", "pmix-override-prefix",
+                               true, evar2);
+            }
+            opal_setenv("PMIX_INSTALL_PREFIX", evar, true, &environ);
+        } else if (NULL != (evar2 = getenv("PMIX_INSTALL_PREFIX"))) {
+            /* Handle Case (c) */
+            if (!mca_pmix_pmix2x_component.silence_warning) {
+                opal_show_help("help-pmix-pmix2x.txt", "pmix-unset-prefix",
+                               true, evar2);
+            }
+            opal_unsetenv("PMIX_INSTALL_PREFIX", &environ);
         }
     }
     ++opal_pmix_base.initialized;
diff --git a/orte/orted/orted_main.c b/orte/orted/orted_main.c
index 0bd57a25a92..ff6291d4df4 100644
--- a/orte/orted/orted_main.c
+++ b/orte/orted/orted_main.c
@@ -285,8 +285,9 @@ int orte_daemon(int argc, char *argv[])
      */
     orte_launch_environ = opal_argv_copy(environ);
 
-    /* purge any ess flag set in the environ when we were launched */
+    /* purge any ess/pmix flags set in the environ when we were launched */
     opal_unsetenv(OPAL_MCA_PREFIX"ess", &orte_launch_environ);
+    opal_unsetenv(OPAL_MCA_PREFIX"pmix", &orte_launch_environ);
 
     /* if orte_daemon_debug is set, let someone know we are alive right
      * away just in case we have a problem along the way
diff --git a/orte/orted/orted_submit.c b/orte/orted/orted_submit.c
index b471acf13ba..12cc995397b 100644
--- a/orte/orted/orted_submit.c
+++ b/orte/orted/orted_submit.c
@@ -537,11 +537,6 @@ int orte_submit_init(int argc, char *argv[],
      */
     opal_finalize();
 
-    /* clear params from the environment so our children
-     * don't pick them up */
-    opal_unsetenv(OPAL_MCA_PREFIX"ess", &environ);
-    opal_unsetenv(OPAL_MCA_PREFIX"pmix", &environ);
-
     if (ORTE_PROC_IS_TOOL) {
         opal_value_t val;
         /* extract the name */
@@ -589,6 +584,13 @@ int orte_submit_init(int argc, char *argv[],
          * orterun
          */
         orte_launch_environ = opal_argv_copy(environ);
+        /* clear params from the environment so our children
+         * don't pick them up */
+        opal_unsetenv(OPAL_MCA_PREFIX"ess", &orte_launch_environ);
+        opal_unsetenv(OPAL_MCA_PREFIX"pmix", &orte_launch_environ);
+        /* clear any install_prefix that might have been set so
+         * we don't cause our children to falsely generate warnings */
+        opal_unsetenv("PMIX_INSTALL_PREFIX", &orte_launch_environ);
     }
 
     return ORTE_SUCCESS;
