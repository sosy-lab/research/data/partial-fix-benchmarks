diff --git a/plugins/thunderbolt/fu-plugin-thunderbolt.c b/plugins/thunderbolt/fu-plugin-thunderbolt.c
index 785b6b6768..506c64ee30 100644
--- a/plugins/thunderbolt/fu-plugin-thunderbolt.c
+++ b/plugins/thunderbolt/fu-plugin-thunderbolt.c
@@ -565,183 +565,6 @@ fu_plugin_thunderbolt_write_firmware (FuDevice     *device,
 	return g_output_stream_close (os, NULL, error);
 }
 
-typedef struct UpdateData {
-
-	gboolean   have_device;
-	GMainLoop *mainloop;
-	const gchar *target_uuid;
-	guint      timeout_id;
-
-	GHashTable *changes;
-} UpdateData;
-
-static gboolean
-on_wait_for_device_timeout (gpointer user_data)
-{
-	UpdateData *data = (UpdateData *) user_data;
-	g_main_loop_quit (data->mainloop);
-	data->timeout_id = 0;
-	return FALSE;
-}
-
-static void
-on_wait_for_device_added (FuPlugin    *plugin,
-			  GUdevDevice *device,
-			  UpdateData  *up_data)
-{
-	FuDevice  *dev;
-	const gchar *uuid;
-	const gchar *path;
-	g_autofree gchar *version = NULL;
-	g_autofree gchar *id = NULL;
-
-	uuid = g_udev_device_get_sysfs_attr (device, "unique_id");
-	if (uuid == NULL)
-		return;
-
-	dev = g_hash_table_lookup (up_data->changes, uuid);
-	if (dev == NULL) {
-		/* a previously unknown device, add it via
-		 * the normal way */
-		fu_plugin_thunderbolt_add (plugin, device);
-		return;
-	}
-
-	/* ensure the device path is correct */
-	path = g_udev_device_get_sysfs_path (device);
-	fu_device_set_metadata (dev, "sysfs-path", path);
-
-	/* make sure the version is correct, might have changed
-	 * after update. */
-	version = fu_plugin_thunderbolt_udev_get_version (device);
-	fu_device_set_version (dev, version);
-
-	id = fu_plugin_thunderbolt_gen_id (device);
-	fu_plugin_cache_add (plugin, id, dev);
-
-	g_hash_table_remove (up_data->changes, uuid);
-
-	/* check if this device is the target*/
-	if (g_str_equal (uuid, up_data->target_uuid)) {
-		up_data->have_device = TRUE;
-		g_debug ("target (%s) re-appeared", uuid);
-		g_main_loop_quit (up_data->mainloop);
-	}
-}
-
-static void
-on_wait_for_device_removed (FuPlugin    *plugin,
-			    GUdevDevice *device,
-			    UpdateData *up_data)
-{
-	g_autofree gchar *id = NULL;
-	FuDevice  *dev;
-	const gchar *uuid;
-
-	id = fu_plugin_thunderbolt_gen_id (device);
-	dev = fu_plugin_cache_lookup (plugin, id);
-
-	if (dev == NULL)
-		return;
-
-	fu_plugin_cache_remove (plugin, id);
-	uuid = fu_device_get_physical_id (dev);
-	g_hash_table_insert (up_data->changes,
-			     (gpointer) uuid,
-			     g_object_ref (dev));
-
-	/* check if this device is the target */
-	if (g_str_equal (uuid, up_data->target_uuid)) {
-		up_data->have_device = FALSE;
-		g_debug ("target (%s) disappeared", uuid);
-	}
-}
-
-static void
-on_wait_for_device_notify (FuPlugin    *plugin,
-			   GUdevDevice *device,
-			   const char  *action,
-			   gpointer    user_data)
-{
-	UpdateData *up_data = (UpdateData *) user_data;
-
-	/* nb: action cannot be NULL since we are only called from
-	 *     udev_event_cb, which ensures that */
-	if (g_str_equal (action, "add")) {
-		on_wait_for_device_added (plugin, device, up_data);
-	} else if (g_str_equal (action, "remove")) {
-		on_wait_for_device_removed (plugin, device, up_data);
-	} else if (g_str_equal (action, "change")) {
-		fu_plugin_thunderbolt_change (plugin, device);
-	}
-}
-
-static void
-remove_leftover_devices (gpointer key,
-			 gpointer value,
-			 gpointer user_data)
-{
-	FuPlugin *plugin = FU_PLUGIN (user_data);
-	FuDevice *dev = FU_DEVICE (value);
-	const gchar *syspath = fu_device_get_metadata (dev, "sysfs-path");
-	g_autofree gchar *id = NULL;
-
-	id = fu_plugin_thunderbolt_gen_id_from_syspath (syspath);
-
-	fu_plugin_cache_remove (plugin, id);
-	fu_plugin_device_remove (plugin, dev);
-}
-
-static gboolean
-fu_plugin_thunderbolt_wait_for_device (FuPlugin  *plugin,
-				       FuDevice  *dev,
-				       GError   **error)
-{
-	FuPluginData *data = fu_plugin_get_data (plugin);
-	UpdateData up_data = { TRUE, };
-	g_autoptr(GMainLoop) mainloop = NULL;
-	g_autoptr(GHashTable) changes = NULL;
-
-	up_data.mainloop = mainloop = g_main_loop_new (NULL, FALSE);
-	up_data.target_uuid = fu_device_get_physical_id (dev);
-
-	/* this will limit the maximum amount of time we wait for
-	 * the device (i.e. 'dev') to re-appear. */
-	up_data.timeout_id = g_timeout_add (data->timeout,
-					    on_wait_for_device_timeout,
-					    &up_data);
-
-	/* this will capture the device added, removed, changed
-	 * signals while we are updating.  */
-	data->update_data = &up_data;
-	data->update_notify = on_wait_for_device_notify;
-
-	changes = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, g_object_unref);
-	up_data.changes = changes;
-
-	/* now we wait ... */
-	g_main_loop_run (mainloop);
-
-	/* restore original udev change handler */
-	data->update_data = NULL;
-	data->update_notify = NULL;
-
-	if (up_data.timeout_id > 0)
-		g_source_remove (up_data.timeout_id);
-
-	if (!up_data.have_device) {
-		g_set_error_literal (error,
-				     FWUPD_ERROR,
-				     FWUPD_ERROR_NOT_FOUND,
-				     "timed out while waiting for device");
-		return FALSE;
-	}
-
-	g_hash_table_foreach (changes, remove_leftover_devices, plugin);
-
-	return TRUE;
-}
-
 /* internal interface  */
 
 void
@@ -812,7 +635,6 @@ fu_plugin_update (FuPlugin *plugin,
 {
 	FuPluginData *data = fu_plugin_get_data (plugin);
 	const gchar *devpath;
-	guint64 status;
 	g_autoptr(GUdevDevice) udevice = NULL;
 	g_autoptr(GError) error_local = NULL;
 	gboolean install_force = (flags & FWUPD_INSTALL_FLAG_FORCE) != 0;
@@ -874,11 +696,31 @@ fu_plugin_update (FuPlugin *plugin,
 	}
 
 	fu_device_set_status (dev, FWUPD_STATUS_DEVICE_RESTART);
+	fu_device_set_remove_delay (dev, data->timeout);
+	fu_device_add_flag (dev, FWUPD_DEVICE_FLAG_WAIT_FOR_REPLUG);
+
+	return TRUE;
+}
+
+gboolean
+fu_plugin_update_attach (FuPlugin *plugin,
+			 FuDevice *dev,
+			 GError **error)
+{
+	FuPluginData *data = fu_plugin_get_data (plugin);
+	const gchar *devpath;
+	guint64 status;
+	g_autoptr(GUdevDevice) udevice = NULL;
+	g_autoptr(GError) error_local = NULL;
 
-	/* the device will disappear and we need to wait until it reappears,
-	 * and then check if we find an error */
-	if (!fu_plugin_thunderbolt_wait_for_device (plugin, dev, error)) {
-		g_prefix_error (error, "could not detect device after update: ");
+	devpath = fu_device_get_metadata (dev, "sysfs-path");
+	udevice = g_udev_client_query_by_sysfs_path (data->udev, devpath);
+	if (udevice == NULL) {
+		g_set_error (error,
+			     FWUPD_ERROR,
+			     FWUPD_ERROR_NOT_FOUND,
+			     "could not find thunderbolt device at %s",
+			     devpath);
 		return FALSE;
 	}
 
diff --git a/plugins/thunderbolt/fu-self-test.c b/plugins/thunderbolt/fu-self-test.c
index 8ddf92fbd7..8714e23d9d 100644
--- a/plugins/thunderbolt/fu-self-test.c
+++ b/plugins/thunderbolt/fu-self-test.c
@@ -1136,6 +1136,10 @@ test_update_working (ThunderboltTest *tt, gconstpointer user_data)
 	g_assert_no_error (error);
 	g_assert_true (ret);
 
+	ret = fu_plugin_runner_update_attach (plugin, tree->fu_device, &error);
+	g_assert_no_error (error);
+	g_assert_true (ret);
+
 	version_after = fu_device_get_version (tree->fu_device);
 	g_debug ("version after update: %s", version_after);
 	g_assert_cmpstr (version_after, ==, "42.23");
@@ -1177,6 +1181,10 @@ test_update_fail (ThunderboltTest *tt, gconstpointer user_data)
 	g_assert_error (error, FWUPD_ERROR, FWUPD_ERROR_INTERNAL);
 	g_assert_false (ret);
 
+	ret = fu_plugin_runner_update_attach (plugin, tree->fu_device, &error);
+	g_assert_no_error (error);
+	g_assert_true (ret);
+
 	/* version should *not* have changed (but we get parsed version) */
 	version_after = fu_device_get_version (tree->fu_device);
 	g_debug ("version after update: %s", version_after);
@@ -1217,6 +1225,10 @@ test_update_fail_nowshow (ThunderboltTest *tt, gconstpointer user_data)
 	fu_plugin_thunderbolt_set_timeout (plugin, 1000);
 
 	ret = fu_plugin_runner_update (plugin, tree->fu_device, NULL, fw_data, 0, &error);
+	g_assert_no_error (error);
+	g_assert_true (ret);
+
+	ret = fu_plugin_runner_update_attach (plugin, tree->fu_device, &error);
 	g_assert_error (error, FWUPD_ERROR, FWUPD_ERROR_NOT_FOUND);
 	g_assert_false (ret);
 }
