diff --git a/mutt/buffer.c b/mutt/buffer.c
index e38f840833..bbeb970739 100644
--- a/mutt/buffer.c
+++ b/mutt/buffer.c
@@ -32,7 +32,7 @@
  * | mutt_buffer_addch()  | Add a single character to a Buffer
  * | mutt_buffer_addstr() | Add a string to a Buffer
  * | mutt_buffer_free()   | Release a Buffer and its contents
- * | mutt_buffer_from()   | Create Buffer from an existing string
+ * | mutt_buffer_from()   | Initialize a Buffer from an existing string
  * | mutt_buffer_init()   | Initialise a new Buffer
  * | mutt_buffer_deinit() | Release the memory allocated by a Buffer
  * | mutt_buffer_printf() | Format a string into a Buffer
@@ -81,22 +81,19 @@ void mutt_buffer_reset(struct Buffer *b)
 }
 
 /**
- * mutt_buffer_from - Create Buffer from an existing string
+ * mutt_buffer_from - Initialize a Buffer from an existing string
+ * @param buf Buffer to initialize
  * @param seed String to put in the Buffer
- * @retval ptr New Buffer
  */
-struct Buffer *mutt_buffer_from(char *seed)
+void mutt_buffer_from(struct Buffer *buf, char *seed)
 {
-  struct Buffer *b = NULL;
-
   if (!seed)
-    return NULL;
+    return;
 
-  b = mutt_mem_calloc(1, sizeof(struct Buffer));
-  b->data = mutt_str_strdup(seed);
-  b->dsize = mutt_str_strlen(seed);
-  b->dptr = (char *) b->data + b->dsize;
-  return b;
+  mutt_buffer_init(buf);
+  buf->data = mutt_str_strdup(seed);
+  buf->dsize = mutt_str_strlen(seed);
+  buf->dptr = (char *) buf->data + buf->dsize;
 }
 
 /**
diff --git a/mutt/buffer.h b/mutt/buffer.h
index 8c1dd23d24..8d340ffd90 100644
--- a/mutt/buffer.h
+++ b/mutt/buffer.h
@@ -37,15 +37,15 @@ struct Buffer
   int destroy;  /**< destroy 'data' when done? */
 };
 
-#define MoreArgs(p) (*p->dptr && (*p->dptr != ';') && (*p->dptr != '#'))
+#define MoreArgs(p) (*(p)->dptr && (*(p)->dptr != ';') && (*(p)->dptr != '#'))
 
-void           mutt_buffer_addch(struct Buffer *buf, char c);
-void           mutt_buffer_addstr(struct Buffer *buf, const char *s);
-void           mutt_buffer_free(struct Buffer **p);
-struct Buffer *mutt_buffer_from(char *seed);
-void           mutt_buffer_init(struct Buffer *b);
-void           mutt_buffer_deinit(struct Buffer *b);
-int            mutt_buffer_printf(struct Buffer *buf, const char *fmt, ...);
-void           mutt_buffer_reset(struct Buffer *b);
+void mutt_buffer_addch(struct Buffer *buf, char c);
+void mutt_buffer_addstr(struct Buffer *buf, const char *s);
+void mutt_buffer_free(struct Buffer **p);
+void mutt_buffer_from(struct Buffer *buf, char *seed);
+void mutt_buffer_init(struct Buffer *buf);
+void mutt_buffer_deinit(struct Buffer *buf);
+int  mutt_buffer_printf(struct Buffer *buf, const char *fmt, ...);
+void mutt_buffer_reset(struct Buffer *buf);
 
 #endif /* _MUTT_BUFFER_H */
diff --git a/muttlib.c b/muttlib.c
index 2ac68f2255..b954bae81a 100644
--- a/muttlib.c
+++ b/muttlib.c
@@ -930,7 +930,7 @@ void mutt_expando_format(char *buf, size_t buflen, size_t col, int cols, const c
     {
       struct Buffer word = {0};
       struct Buffer command = {0};
-      struct Buffer *srcbuf = NULL;
+      struct Buffer srcbuf = {0};
       char srccopy[LONG_STRING];
       int i = 0;
 
@@ -940,17 +940,17 @@ void mutt_expando_format(char *buf, size_t buflen, size_t col, int cols, const c
       srccopy[n - 1] = '\0';
 
       /* prepare BUFFERs */
-      srcbuf = mutt_buffer_from(srccopy);
-      srcbuf->dptr = srcbuf->data;
+      mutt_buffer_from(&srcbuf, srccopy);
+      srcbuf.dptr = srcbuf.data;
 
       /* Iterate expansions across successive arguments */
       do
       {
         /* Extract the command name and copy to command line */
-        mutt_debug(3, "fmtpipe +++: %s\n", srcbuf->dptr);
+        mutt_debug(3, "fmtpipe +++: %s\n", srcbuf.dptr);
         if (word.data)
           *word.data = '\0';
-        mutt_extract_token(&word, srcbuf, 0);
+        mutt_extract_token(&word, &srcbuf, 0);
         mutt_debug(3, "fmtpipe %2d: %s\n", i++, word.data);
         mutt_buffer_addch(&command, '\'');
         mutt_expando_format(buf, sizeof(buf), 0, cols, word.data, callback,
@@ -968,7 +968,7 @@ void mutt_expando_format(char *buf, size_t buflen, size_t col, int cols, const c
         }
         mutt_buffer_addch(&command, '\'');
         mutt_buffer_addch(&command, ' ');
-      } while (MoreArgs(srcbuf));
+      } while (MoreArgs(&srcbuf));
 
       mutt_debug(3, "fmtpipe > %s\n", command.data);
 
@@ -1031,7 +1031,7 @@ void mutt_expando_format(char *buf, size_t buflen, size_t col, int cols, const c
         *wptr = '\0';
       }
 
-      mutt_buffer_free(&srcbuf);
+      mutt_buffer_deinit(&srcbuf);
       mutt_buffer_deinit(&word);
       mutt_buffer_deinit(&command);
       return;
diff --git a/parse.c b/parse.c
index 4d83405dd4..bd1542f786 100644
--- a/parse.c
+++ b/parse.c
@@ -1232,13 +1232,15 @@ struct Envelope *mutt_read_rfc822_header(FILE *f, struct Header *hdr,
         /* spam tag is new, and match expr is non-empty; copy */
         else if (!e->spam && *buf)
         {
-          e->spam = mutt_buffer_from(buf);
+          e->spam = mutt_mem_calloc(1, sizeof(struct Buffer));
+          mutt_buffer_from(e->spam, buf);
         }
 
         /* match expr is empty; plug in null string if no existing tag */
         else if (!e->spam)
         {
-          e->spam = mutt_buffer_from("");
+          e->spam = mutt_mem_calloc(1, sizeof(struct Buffer));
+          mutt_buffer_from(e->spam, "");
         }
 
         if (e->spam && e->spam->data)
