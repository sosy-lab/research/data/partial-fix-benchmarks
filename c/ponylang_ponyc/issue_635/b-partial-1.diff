diff --git a/CHANGELOG.md b/CHANGELOG.md
index 654a6176d4..59daa6834a 100644
--- a/CHANGELOG.md
+++ b/CHANGELOG.md
@@ -108,6 +108,7 @@ All notable changes to the Pony compiler and standard library will be documented
 - Only allow single, internal underscores in numeric literals.
 - `ponyc --version` now includes the build type (debug/release) in its output.
 - Strings now grow and shrink geometrically.
+- Embedded fields can now be constructed from complex expressions containing constructors
 
 ## [0.2.1] - 2015-10-06
 
diff --git a/src/libponyc/codegen/gencall.c b/src/libponyc/codegen/gencall.c
index d4d1f4e358..3aa7466991 100644
--- a/src/libponyc/codegen/gencall.c
+++ b/src/libponyc/codegen/gencall.c
@@ -329,13 +329,14 @@ LLVMValueRef gen_call(compile_t* c, ast_t* ast)
       case TK_NEWBEREF:
       {
         ast_t* parent = ast_parent(ast);
-        ast_t* sibling = ast_sibling(ast);
+        while((parent != NULL) && (ast_id(parent) != TK_ASSIGN))
+          parent = ast_parent(parent);
 
         // If we're constructing an embed field, pass a pointer to the field
         // as the receiver. Otherwise, allocate an object.
-        if((ast_id(parent) == TK_ASSIGN) && (ast_id(sibling) == TK_EMBEDREF))
+        if((parent != NULL) && (ast_id(ast_childidx(parent, 1)) == TK_EMBEDREF))
         {
-          args[0] = gen_fieldptr(c, sibling);
+          args[0] = gen_fieldptr(c, ast_childidx(parent, 1));
           set_descriptor(c, t, args[0]);
         } else {
           args[0] = gencall_alloc(c, t);
diff --git a/src/libponyc/expr/operator.c b/src/libponyc/expr/operator.c
index 59c468f30a..e01ea89a63 100644
--- a/src/libponyc/expr/operator.c
+++ b/src/libponyc/expr/operator.c
@@ -382,6 +382,50 @@ static bool infer_locals(pass_opt_t* opt, ast_t* left, ast_t* r_type)
   return true;
 }
 
+static bool is_expr_constructor(ast_t* ast)
+{
+  assert(ast != NULL);
+  switch(ast_id(ast))
+  {
+    case TK_CALL:
+      return ast_id(ast_childidx(ast, 2)) == TK_NEWREF;
+    case TK_SEQ:
+      return is_expr_constructor(ast_childlast(ast));
+    case TK_IF:
+    case TK_WHILE:
+    case TK_REPEAT:
+    {
+      ast_t* body = ast_childidx(ast, 1);
+      ast_t* else_expr = ast_childidx(ast, 2);
+      return is_expr_constructor(body) && is_expr_constructor(else_expr);
+    }
+    case TK_TRY:
+    {
+      ast_t* body = ast_childidx(ast, 0);
+      ast_t* else_expr = ast_childidx(ast, 1);
+      return is_expr_constructor(body) && is_expr_constructor(else_expr);
+    }
+    case TK_MATCH:
+    {
+      ast_t* cases = ast_childidx(ast, 1);
+      ast_t* else_expr = ast_childidx(ast, 2);
+      ast_t* the_case = ast_child(cases);
+
+      while(the_case != NULL)
+      {
+        if(!is_expr_constructor(ast_childidx(the_case, 2)))
+          return false;
+        the_case = ast_sibling(the_case);
+      }
+      return is_expr_constructor(else_expr);
+    }
+    case TK_RECOVER:
+      return is_expr_constructor(ast_childidx(ast, 1));
+    default:
+      return false;
+  }
+}
+
 bool expr_assign(pass_opt_t* opt, ast_t* ast)
 {
   // Left and right are swapped in the AST to make sure we type check the
@@ -499,8 +543,7 @@ bool expr_assign(pass_opt_t* opt, ast_t* ast)
   // If it's an embedded field, check for a constructor result.
   if(ast_id(left) == TK_EMBEDREF)
   {
-    if((ast_id(right) != TK_CALL) ||
-      (ast_id(ast_childidx(right, 2)) != TK_NEWREF))
+    if(!is_expr_constructor(right))
     {
       ast_error(opt->check.errors, ast,
         "an embedded field must be assigned using a constructor");
