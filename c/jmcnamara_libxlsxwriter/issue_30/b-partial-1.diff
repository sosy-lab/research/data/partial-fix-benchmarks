diff --git a/examples/outline.c b/examples/outline.c
new file mode 100644
index 00000000..ab580170
--- /dev/null
+++ b/examples/outline.c
@@ -0,0 +1,255 @@
+/*
+ * Example of how use libxlsxwriter to generate Excel outlines and grouping.
+ *
+ * Excel allows you to group rows or columns so that they can be hidden or
+ * displayed with a single mouse click. This feature is referred to as
+ * outlines.
+ *
+ * Outlines can reduce complex data down to a few salient sub-totals or
+ * summaries.
+ *
+ * Copyright 2014-2018, John McNamara, jmcnamara@cpan.org
+ *
+ */
+
+#include "xlsxwriter.h"
+
+int main() {
+
+    lxw_workbook  *workbook   = workbook_new("outline.xlsx");
+    lxw_worksheet *worksheet1 = workbook_add_worksheet(workbook, "Outlined Rows");
+    lxw_worksheet *worksheet2 = workbook_add_worksheet(workbook, "Collapsed Rows");
+    lxw_worksheet *worksheet3 = workbook_add_worksheet(workbook, "Outline Columns");
+    lxw_worksheet *worksheet4 = workbook_add_worksheet(workbook, "Outline levels");
+
+    lxw_format *bold = workbook_add_format(workbook);
+    format_set_bold(bold);
+
+   /*
+    * Example 1: Create a worksheet with outlined rows. It also includes
+    * SUBTOTAL() functions so that it looks like the type of automatic
+    * outlines that are generated when you use the 'Sub Totals' option.
+    *
+    * For outlines the important parameters are 'hidden' and 'level'. Rows
+    * with the same 'level' are grouped together. The group will be collapsed
+    * if 'hidden' is non-zero.
+    */
+
+    /* The option structs with the outline level set. */
+    lxw_row_col_options options1 = {.hidden = 0, .level = 2, .collapsed = 0};
+    lxw_row_col_options options2 = {.hidden = 0, .level = 1, .collapsed = 0};
+
+    /* Set the column width for clarity. */
+    worksheet_set_column(worksheet1, COLS("A:A"), 20, NULL);
+
+    /* Set the row options with the outline level. */
+    worksheet_set_row_opt(worksheet1, 1,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 2,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 3,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 4,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 5,  LXW_DEF_ROW_HEIGHT, NULL, &options2);
+
+    worksheet_set_row_opt(worksheet1, 6,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 7,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 8,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 9,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 10, LXW_DEF_ROW_HEIGHT, NULL, &options2);
+
+    /* Add data and formulas to the worksheet. */
+    worksheet_write_string(worksheet1, CELL("A1"), "Region", bold);
+    worksheet_write_string(worksheet1, CELL("A2"), "North" , NULL);
+    worksheet_write_string(worksheet1, CELL("A3"), "North" , NULL);
+    worksheet_write_string(worksheet1, CELL("A4"), "North" , NULL);
+    worksheet_write_string(worksheet1, CELL("A5"), "North" , NULL);
+    worksheet_write_string(worksheet1, CELL("A6"), "North Total", bold);
+
+    worksheet_write_string(worksheet1, CELL("B1"), "Sales", bold);
+    worksheet_write_number(worksheet1, CELL("B2"), 1000 , NULL);
+    worksheet_write_number(worksheet1, CELL("B3"), 1200 , NULL);
+    worksheet_write_number(worksheet1, CELL("B4"), 900 , NULL);
+    worksheet_write_number(worksheet1, CELL("B5"), 1200 , NULL);
+    worksheet_write_formula_num(worksheet1, CELL("B6"), "=SUBTOTAL(9,B2:B5)", bold, 4300);
+
+    worksheet_write_string(worksheet1, CELL("A7"), "South" , NULL);
+    worksheet_write_string(worksheet1, CELL("A8"), "South" , NULL);
+    worksheet_write_string(worksheet1, CELL("A9"), "South" , NULL);
+    worksheet_write_string(worksheet1, CELL("A10"), "South" , NULL);
+    worksheet_write_string(worksheet1, CELL("A11"), "South Total", bold);
+
+    worksheet_write_number(worksheet1, CELL("B7"), 400 , NULL);
+    worksheet_write_number(worksheet1, CELL("B8"), 600 , NULL);
+    worksheet_write_number(worksheet1, CELL("B9"), 500 , NULL);
+    worksheet_write_number(worksheet1, CELL("B10"), 600 , NULL);
+    worksheet_write_formula_num(worksheet1, CELL("B11"), "=SUBTOTAL(9,B7:B10)", bold, 2100);
+
+    worksheet_write_string(worksheet1, CELL("A12"), "Grand Total", bold);
+    worksheet_write_formula_num(worksheet1, CELL("B12"), "=SUBTOTAL(9,B2:B10)", bold, 6400);
+
+
+   /*
+    * Example 2: Create a worksheet with outlined rows. This is the same as
+    * the previous example except that the rows are collapsed.  Note: We need
+    * to indicate the row that contains the collapsed symbol '+' with the
+    * optional parameter, 'collapsed'.
+    */
+
+    /* The option structs with the outline level and collapsed property set. */
+    lxw_row_col_options options3 = {.hidden = 1, .level = 2, .collapsed = 0};
+    lxw_row_col_options options4 = {.hidden = 1, .level = 1, .collapsed = 0};
+    lxw_row_col_options options5 = {.hidden = 0, .level = 0, .collapsed = 1};
+
+    /* Set the column width for clarity. */
+    worksheet_set_column(worksheet2, COLS("A:A"), 20, NULL);
+
+    /* Set the row options with the outline level. */
+    worksheet_set_row_opt(worksheet2, 1,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 2,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 3,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 4,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 5,  LXW_DEF_ROW_HEIGHT, NULL, &options4);
+
+    worksheet_set_row_opt(worksheet2, 6,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 7,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 8,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 9,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 10, LXW_DEF_ROW_HEIGHT, NULL, &options4);
+    worksheet_set_row_opt(worksheet2, 11, LXW_DEF_ROW_HEIGHT, NULL, &options5);
+
+    /* Add data and formulas to the worksheet. */
+    worksheet_write_string(worksheet2, CELL("A1"), "Region", bold);
+    worksheet_write_string(worksheet2, CELL("A2"), "North" , NULL);
+    worksheet_write_string(worksheet2, CELL("A3"), "North" , NULL);
+    worksheet_write_string(worksheet2, CELL("A4"), "North" , NULL);
+    worksheet_write_string(worksheet2, CELL("A5"), "North" , NULL);
+    worksheet_write_string(worksheet2, CELL("A6"), "North Total", bold);
+
+    worksheet_write_string(worksheet2, CELL("B1"), "Sales", bold);
+    worksheet_write_number(worksheet2, CELL("B2"), 1000 , NULL);
+    worksheet_write_number(worksheet2, CELL("B3"), 1200 , NULL);
+    worksheet_write_number(worksheet2, CELL("B4"), 900 , NULL);
+    worksheet_write_number(worksheet2, CELL("B5"), 1200 , NULL);
+    worksheet_write_formula_num(worksheet2, CELL("B6"), "=SUBTOTAL(9,B2:B5)", bold, 4300);
+
+    worksheet_write_string(worksheet2, CELL("A7"), "South" , NULL);
+    worksheet_write_string(worksheet2, CELL("A8"), "South" , NULL);
+    worksheet_write_string(worksheet2, CELL("A9"), "South" , NULL);
+    worksheet_write_string(worksheet2, CELL("A10"), "South" , NULL);
+    worksheet_write_string(worksheet2, CELL("A11"), "South Total", bold);
+
+    worksheet_write_number(worksheet2, CELL("B7"), 400 , NULL);
+    worksheet_write_number(worksheet2, CELL("B8"), 600 , NULL);
+    worksheet_write_number(worksheet2, CELL("B9"), 500 , NULL);
+    worksheet_write_number(worksheet2, CELL("B10"), 600 , NULL);
+    worksheet_write_formula_num(worksheet2, CELL("B11"), "=SUBTOTAL(9,B7:B10)", bold, 2100);
+
+    worksheet_write_string(worksheet2, CELL("A12"), "Grand Total", bold);
+    worksheet_write_formula_num(worksheet2, CELL("B12"), "=SUBTOTAL(9,B2:B10)", bold, 6400);
+
+
+    /*
+     * Example 3: Create a worksheet with outlined columns.
+     */
+    lxw_row_col_options options6 = {.hidden = 0, .level = 1, .collapsed = 0};
+
+    /* Add data and formulas to the worksheet. */
+    worksheet_write_string(worksheet3, CELL("A1"), "Month", NULL);
+    worksheet_write_string(worksheet3, CELL("B1"), "Jan",   NULL);
+    worksheet_write_string(worksheet3, CELL("C1"), "Feb",   NULL);
+    worksheet_write_string(worksheet3, CELL("D1"), "Mar",   NULL);
+    worksheet_write_string(worksheet3, CELL("E1"), "Apr",   NULL);
+    worksheet_write_string(worksheet3, CELL("F1"), "May",   NULL);
+    worksheet_write_string(worksheet3, CELL("G1"), "Jun",   NULL);
+    worksheet_write_string(worksheet3, CELL("H1"), "Total", NULL);
+
+    worksheet_write_string(worksheet3, CELL("A2"), "North", NULL);
+    worksheet_write_number(worksheet3, CELL("B2"), 50,      NULL);
+    worksheet_write_number(worksheet3, CELL("C2"), 20,      NULL);
+    worksheet_write_number(worksheet3, CELL("D2"), 15,      NULL);
+    worksheet_write_number(worksheet3, CELL("E2"), 25,      NULL);
+    worksheet_write_number(worksheet3, CELL("F2"), 65,      NULL);
+    worksheet_write_number(worksheet3, CELL("G2"), 80,      NULL);
+    worksheet_write_formula_num(worksheet3, CELL("H2"), "=SUM(B2:G2)", NULL, 255);
+
+    worksheet_write_string(worksheet3, CELL("A3"), "South", NULL);
+    worksheet_write_number(worksheet3, CELL("B3"), 10,      NULL);
+    worksheet_write_number(worksheet3, CELL("C3"), 20,      NULL);
+    worksheet_write_number(worksheet3, CELL("D3"), 30,      NULL);
+    worksheet_write_number(worksheet3, CELL("E3"), 50,      NULL);
+    worksheet_write_number(worksheet3, CELL("F3"), 50,      NULL);
+    worksheet_write_number(worksheet3, CELL("G3"), 50,      NULL);
+    worksheet_write_formula_num(worksheet3, CELL("H3"), "=SUM(B3:G3)", NULL, 210);
+
+    worksheet_write_string(worksheet3, CELL("A4"), "East",  NULL);
+    worksheet_write_number(worksheet3, CELL("B4"), 45,      NULL);
+    worksheet_write_number(worksheet3, CELL("C4"), 75,      NULL);
+    worksheet_write_number(worksheet3, CELL("D4"), 50,      NULL);
+    worksheet_write_number(worksheet3, CELL("E4"), 15,      NULL);
+    worksheet_write_number(worksheet3, CELL("F4"), 75,      NULL);
+    worksheet_write_number(worksheet3, CELL("G4"), 100,     NULL);
+    worksheet_write_formula_num(worksheet3, CELL("H4"), "=SUM(B4:G4)", NULL, 360);
+
+    worksheet_write_string(worksheet3, CELL("A5"), "West",  NULL);
+    worksheet_write_number(worksheet3, CELL("B5"), 15,      NULL);
+    worksheet_write_number(worksheet3, CELL("C5"), 15,      NULL);
+    worksheet_write_number(worksheet3, CELL("D5"), 55,      NULL);
+    worksheet_write_number(worksheet3, CELL("E5"), 35,      NULL);
+    worksheet_write_number(worksheet3, CELL("F5"), 20,      NULL);
+    worksheet_write_number(worksheet3, CELL("G5"), 50,      NULL);
+    worksheet_write_formula_num(worksheet3, CELL("H5"), "=SUM(B5:G5)", NULL, 190);
+
+    worksheet_write_formula_num(worksheet3, CELL("H6"), "=SUM(H2:H5)", bold, 1015);
+
+    /* Add bold format to the first row. */
+    worksheet_set_row(worksheet3, 0, LXW_DEF_ROW_HEIGHT, bold);
+
+    /* Set column formatting and the outline level. */
+    worksheet_set_column(    worksheet3, COLS("A:A"), 10, bold);
+    worksheet_set_column(    worksheet3, COLS("H:H"), 10, NULL);
+    worksheet_set_column_opt(worksheet3, COLS("B:G"),  6, NULL, &options6);
+
+
+
+    /*
+     * Example 4: Show all possible outline levels.
+     */
+    lxw_row_col_options level1 = {.level = 1, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level2 = {.level = 2, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level3 = {.level = 3, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level4 = {.level = 4, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level5 = {.level = 5, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level6 = {.level = 6, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level7 = {.level = 7, .hidden = 0, .collapsed = 0};
+
+    worksheet_write_string(worksheet4, 0,  0, "Level 1", NULL);
+    worksheet_write_string(worksheet4, 1,  0, "Level 2", NULL);
+    worksheet_write_string(worksheet4, 2,  0, "Level 3", NULL);
+    worksheet_write_string(worksheet4, 3,  0, "Level 4", NULL);
+    worksheet_write_string(worksheet4, 4,  0, "Level 5", NULL);
+    worksheet_write_string(worksheet4, 5,  0, "Level 6", NULL);
+    worksheet_write_string(worksheet4, 6,  0, "Level 7", NULL);
+    worksheet_write_string(worksheet4, 7,  0, "Level 6", NULL);
+    worksheet_write_string(worksheet4, 8,  0, "Level 5", NULL);
+    worksheet_write_string(worksheet4, 9,  0, "Level 4", NULL);
+    worksheet_write_string(worksheet4, 10, 0, "Level 3", NULL);
+    worksheet_write_string(worksheet4, 11, 0, "Level 2", NULL);
+    worksheet_write_string(worksheet4, 12, 0, "Level 1", NULL);
+
+    worksheet_set_row_opt(worksheet4, 0,  LXW_DEF_ROW_HEIGHT, NULL, &level1);
+    worksheet_set_row_opt(worksheet4, 1,  LXW_DEF_ROW_HEIGHT, NULL, &level2);
+    worksheet_set_row_opt(worksheet4, 2,  LXW_DEF_ROW_HEIGHT, NULL, &level3);
+    worksheet_set_row_opt(worksheet4, 3,  LXW_DEF_ROW_HEIGHT, NULL, &level4);
+    worksheet_set_row_opt(worksheet4, 4,  LXW_DEF_ROW_HEIGHT, NULL, &level5);
+    worksheet_set_row_opt(worksheet4, 5,  LXW_DEF_ROW_HEIGHT, NULL, &level6);
+    worksheet_set_row_opt(worksheet4, 6,  LXW_DEF_ROW_HEIGHT, NULL, &level7);
+    worksheet_set_row_opt(worksheet4, 7,  LXW_DEF_ROW_HEIGHT, NULL, &level6);
+    worksheet_set_row_opt(worksheet4, 8,  LXW_DEF_ROW_HEIGHT, NULL, &level5);
+    worksheet_set_row_opt(worksheet4, 9,  LXW_DEF_ROW_HEIGHT, NULL, &level4);
+    worksheet_set_row_opt(worksheet4, 10, LXW_DEF_ROW_HEIGHT, NULL, &level3);
+    worksheet_set_row_opt(worksheet4, 11, LXW_DEF_ROW_HEIGHT, NULL, &level2);
+    worksheet_set_row_opt(worksheet4, 12, LXW_DEF_ROW_HEIGHT, NULL, &level1);
+
+
+    workbook_close(workbook);
+
+    return 0;
+}
diff --git a/include/xlsxwriter/worksheet.h b/include/xlsxwriter/worksheet.h
index dc4442bd..ebf1db93 100644
--- a/include/xlsxwriter/worksheet.h
+++ b/include/xlsxwriter/worksheet.h
@@ -725,6 +725,8 @@ typedef struct lxw_worksheet {
     uint32_t default_col_pixels;
     uint8_t default_row_zeroed;
     uint8_t default_row_set;
+    uint8_t outline_row_level;
+    uint8_t outline_col_level;
 
     uint8_t header_footer_changed;
     char header[LXW_HEADER_FOOTER_MAX];
@@ -1377,7 +1379,7 @@ lxw_error worksheet_set_row(lxw_worksheet *worksheet,
  *     lxw_row_col_options options = {.hidden = 1, .level = 0, .collapsed = 0};
  *
  *     // Hide the fourth row.
- *     worksheet_set_row(worksheet, 3, 20, NULL, &options);
+ *     worksheet_set_row_opt(worksheet, 3, 20, NULL, &options);
  * @endcode
  *
  */
diff --git a/src/worksheet.c b/src/worksheet.c
index 20d2c4a1..5934365d 100644
--- a/src/worksheet.c
+++ b/src/worksheet.c
@@ -1464,6 +1464,12 @@ _worksheet_write_sheet_format_pr(lxw_worksheet *self)
     if (self->default_row_zeroed)
         LXW_PUSH_ATTRIBUTES_STR("zeroHeight", "1");
 
+    if (self->outline_row_level)
+        LXW_PUSH_ATTRIBUTES_INT("outlineLevelRow", self->outline_row_level);
+
+    if (self->outline_col_level)
+        LXW_PUSH_ATTRIBUTES_INT("outlineLevelCol", self->outline_col_level);
+
     lxw_xml_empty_tag(self->file, "sheetFormatPr", &attributes);
 
     LXW_FREE_ATTRIBUTES();
@@ -1700,6 +1706,9 @@ _write_row(lxw_worksheet *self, lxw_row *row, char *spans)
     if (height != LXW_DEF_ROW_HEIGHT)
         LXW_PUSH_ATTRIBUTES_STR("customHeight", "1");
 
+    if (row->level)
+        LXW_PUSH_ATTRIBUTES_INT("outlineLevel", row->level);
+
     if (row->collapsed)
         LXW_PUSH_ATTRIBUTES_STR("collapsed", "1");
 
@@ -4327,6 +4336,17 @@ worksheet_set_column_opt(lxw_worksheet *self,
     copied_options = calloc(1, sizeof(lxw_col_options));
     RETURN_ON_MEM_ERROR(copied_options, LXW_ERROR_MEMORY_MALLOC_FAILED);
 
+    /* Set the limits for the outline levels (0 <= x <= 7). */
+    if (level < 0)
+        level = 0;
+
+    if (level > 7)
+        level = 7;
+
+    if (level > self->outline_col_level)
+        self->outline_col_level = level;
+
+    /* Set the column properties. */
     copied_options->firstcol = firstcol;
     copied_options->lastcol = lastcol;
     copied_options->width = width;
@@ -4399,6 +4419,17 @@ worksheet_set_row_opt(lxw_worksheet *self,
         height = self->default_row_height;
     }
 
+    /* Set the limits for the outline levels (0 <= x <= 7). */
+    if (level < 0)
+        level = 0;
+
+    if (level > 7)
+        level = 7;
+
+    if (level > self->outline_row_level)
+        self->outline_row_level = level;
+
+    /* Store the row properties. */
     row = _get_row(self, row_num);
 
     row->height = height;
diff --git a/test/functional/src/test_outline01.c b/test/functional/src/test_outline01.c
new file mode 100644
index 00000000..a93a5b8b
--- /dev/null
+++ b/test/functional/src/test_outline01.c
@@ -0,0 +1,67 @@
+/*****************************************************************************
+ * Test cases for libxlsxwriter.
+ *
+ * Test to compare output against Excel files.
+ *
+ * Copyright 2014-2018, John McNamara, jmcnamara@cpan.org
+ *
+ */
+
+#include "xlsxwriter.h"
+
+int main() {
+
+    lxw_workbook  *workbook  = new_workbook("test_outline01.xlsx");
+    lxw_worksheet *worksheet1 = workbook_add_worksheet(workbook, "Outlined Rows");
+
+    lxw_format *bold = workbook_add_format(workbook);
+    format_set_bold(bold);
+
+    lxw_row_col_options options1 = {.hidden = 0, .level = 2, .collapsed = 0};
+    lxw_row_col_options options2 = {.hidden = 0, .level = 1, .collapsed = 0};
+
+    worksheet_set_column(worksheet1, COLS("A:A"), 20, NULL);
+
+    worksheet_set_row_opt(worksheet1, 1,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 2,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 3,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 4,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 5,  LXW_DEF_ROW_HEIGHT, NULL, &options2);
+
+    worksheet_set_row_opt(worksheet1, 6,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 7,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 8,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 9,  LXW_DEF_ROW_HEIGHT, NULL, &options1);
+    worksheet_set_row_opt(worksheet1, 10, LXW_DEF_ROW_HEIGHT, NULL, &options2);
+
+    worksheet_write_string(worksheet1, CELL("A1"), "Region", bold);
+    worksheet_write_string(worksheet1, CELL("A2"), "North" , NULL);
+    worksheet_write_string(worksheet1, CELL("A3"), "North" , NULL);
+    worksheet_write_string(worksheet1, CELL("A4"), "North" , NULL);
+    worksheet_write_string(worksheet1, CELL("A5"), "North" , NULL);
+    worksheet_write_string(worksheet1, CELL("A6"), "North Total", bold);
+
+    worksheet_write_string(worksheet1, CELL("B1"), "Sales", bold);
+    worksheet_write_number(worksheet1, CELL("B2"), 1000 , NULL);
+    worksheet_write_number(worksheet1, CELL("B3"), 1200 , NULL);
+    worksheet_write_number(worksheet1, CELL("B4"), 900 , NULL);
+    worksheet_write_number(worksheet1, CELL("B5"), 1200 , NULL);
+    worksheet_write_formula_num(worksheet1, CELL("B6"), "=SUBTOTAL(9,B2:B5)", bold, 4300);
+
+    worksheet_write_string(worksheet1, CELL("A7"), "South" , NULL);
+    worksheet_write_string(worksheet1, CELL("A8"), "South" , NULL);
+    worksheet_write_string(worksheet1, CELL("A9"), "South" , NULL);
+    worksheet_write_string(worksheet1, CELL("A10"), "South" , NULL);
+    worksheet_write_string(worksheet1, CELL("A11"), "South Total", bold);
+
+    worksheet_write_number(worksheet1, CELL("B7"), 400 , NULL);
+    worksheet_write_number(worksheet1, CELL("B8"), 600 , NULL);
+    worksheet_write_number(worksheet1, CELL("B9"), 500 , NULL);
+    worksheet_write_number(worksheet1, CELL("B10"), 600 , NULL);
+    worksheet_write_formula_num(worksheet1, CELL("B11"), "=SUBTOTAL(9,B7:B10)", bold, 2100);
+
+    worksheet_write_string(worksheet1, CELL("A12"), "Grand Total", bold);
+    worksheet_write_formula_num(worksheet1, CELL("B12"), "=SUBTOTAL(9,B2:B10)", bold, 6400);
+
+    return workbook_close(workbook);
+}
diff --git a/test/functional/src/test_outline02.c b/test/functional/src/test_outline02.c
new file mode 100644
index 00000000..4d78de62
--- /dev/null
+++ b/test/functional/src/test_outline02.c
@@ -0,0 +1,70 @@
+/*****************************************************************************
+ * Test cases for libxlsxwriter.
+ *
+ * Test to compare output against Excel files.
+ *
+ * Copyright 2014-2018, John McNamara, jmcnamara@cpan.org
+ *
+ */
+
+#include "xlsxwriter.h"
+
+int main() {
+
+    lxw_workbook  *workbook  = new_workbook("test_outline02.xlsx");
+    lxw_worksheet *worksheet2 = workbook_add_worksheet(workbook, "Collapsed Rows");
+
+    lxw_format *bold = workbook_add_format(workbook);
+    format_set_bold(bold);
+
+    lxw_row_col_options options3 = {.hidden = 1, .level = 2, .collapsed = 0};
+    lxw_row_col_options options4 = {.hidden = 1, .level = 1, .collapsed = 0};
+    lxw_row_col_options options5 = {.hidden = 0, .level = 0, .collapsed = 1};
+
+    worksheet_set_column(worksheet2, COLS("A:A"), 20, NULL);
+    worksheet_set_selection(worksheet2, RANGE("A14:A14"));
+
+    worksheet_set_row_opt(worksheet2, 1,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 2,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 3,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 4,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 5,  LXW_DEF_ROW_HEIGHT, NULL, &options4);
+
+    worksheet_set_row_opt(worksheet2, 6,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 7,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 8,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 9,  LXW_DEF_ROW_HEIGHT, NULL, &options3);
+    worksheet_set_row_opt(worksheet2, 10, LXW_DEF_ROW_HEIGHT, NULL, &options4);
+    worksheet_set_row_opt(worksheet2, 11, LXW_DEF_ROW_HEIGHT, NULL, &options5);
+
+    worksheet_write_string(worksheet2, CELL("A1"), "Region", bold);
+    worksheet_write_string(worksheet2, CELL("A2"), "North" , NULL);
+    worksheet_write_string(worksheet2, CELL("A3"), "North" , NULL);
+    worksheet_write_string(worksheet2, CELL("A4"), "North" , NULL);
+    worksheet_write_string(worksheet2, CELL("A5"), "North" , NULL);
+    worksheet_write_string(worksheet2, CELL("A6"), "North Total", bold);
+
+    worksheet_write_string(worksheet2, CELL("B1"), "Sales", bold);
+    worksheet_write_number(worksheet2, CELL("B2"), 1000 , NULL);
+    worksheet_write_number(worksheet2, CELL("B3"), 1200 , NULL);
+    worksheet_write_number(worksheet2, CELL("B4"), 900 , NULL);
+    worksheet_write_number(worksheet2, CELL("B5"), 1200 , NULL);
+    worksheet_write_formula_num(worksheet2, CELL("B6"), "=SUBTOTAL(9,B2:B5)", bold, 4300);
+
+    worksheet_write_string(worksheet2, CELL("A7"), "South" , NULL);
+    worksheet_write_string(worksheet2, CELL("A8"), "South" , NULL);
+    worksheet_write_string(worksheet2, CELL("A9"), "South" , NULL);
+    worksheet_write_string(worksheet2, CELL("A10"), "South" , NULL);
+    worksheet_write_string(worksheet2, CELL("A11"), "South Total", bold);
+
+    worksheet_write_number(worksheet2, CELL("B7"), 400 , NULL);
+    worksheet_write_number(worksheet2, CELL("B8"), 600 , NULL);
+    worksheet_write_number(worksheet2, CELL("B9"), 500 , NULL);
+    worksheet_write_number(worksheet2, CELL("B10"), 600 , NULL);
+    worksheet_write_formula_num(worksheet2, CELL("B11"), "=SUBTOTAL(9,B7:B10)", bold, 2100);
+
+    worksheet_write_string(worksheet2, CELL("A12"), "Grand Total", bold);
+    worksheet_write_formula_num(worksheet2, CELL("B12"), "=SUBTOTAL(9,B2:B10)", bold, 6400);
+
+    return workbook_close(workbook);
+}
diff --git a/test/functional/src/test_outline03.c b/test/functional/src/test_outline03.c
new file mode 100644
index 00000000..1e91b5cc
--- /dev/null
+++ b/test/functional/src/test_outline03.c
@@ -0,0 +1,77 @@
+/*****************************************************************************
+ * Test cases for libxlsxwriter.
+ *
+ * Test to compare output against Excel files.
+ *
+ * Copyright 2014-2018, John McNamara, jmcnamara@cpan.org
+ *
+ */
+
+#include "xlsxwriter.h"
+
+int main() {
+
+    lxw_workbook  *workbook  = new_workbook("test_outline03.xlsx");
+    lxw_worksheet *worksheet3 = workbook_add_worksheet(workbook, "Outline Columns");
+
+    lxw_format *bold = workbook_add_format(workbook);
+    format_set_bold(bold);
+
+    lxw_row_col_options options6 = {.hidden = 0, .level = 1, .collapsed = 0};
+
+    worksheet_write_string(worksheet3, CELL("A1"), "Month", NULL);
+    worksheet_write_string(worksheet3, CELL("B1"), "Jan",   NULL);
+    worksheet_write_string(worksheet3, CELL("C1"), "Feb",   NULL);
+    worksheet_write_string(worksheet3, CELL("D1"), "Mar",   NULL);
+    worksheet_write_string(worksheet3, CELL("E1"), "Apr",   NULL);
+    worksheet_write_string(worksheet3, CELL("F1"), "May",   NULL);
+    worksheet_write_string(worksheet3, CELL("G1"), "Jun",   NULL);
+    worksheet_write_string(worksheet3, CELL("H1"), "Total", NULL);
+
+    worksheet_write_string(worksheet3, CELL("A2"), "North", NULL);
+    worksheet_write_number(worksheet3, CELL("B2"), 50,      NULL);
+    worksheet_write_number(worksheet3, CELL("C2"), 20,      NULL);
+    worksheet_write_number(worksheet3, CELL("D2"), 15,      NULL);
+    worksheet_write_number(worksheet3, CELL("E2"), 25,      NULL);
+    worksheet_write_number(worksheet3, CELL("F2"), 65,      NULL);
+    worksheet_write_number(worksheet3, CELL("G2"), 80,      NULL);
+    worksheet_write_formula_num(worksheet3, CELL("H2"), "=SUM(B2:G2)", NULL, 255);
+
+    worksheet_write_string(worksheet3, CELL("A3"), "South", NULL);
+    worksheet_write_number(worksheet3, CELL("B3"), 10,      NULL);
+    worksheet_write_number(worksheet3, CELL("C3"), 20,      NULL);
+    worksheet_write_number(worksheet3, CELL("D3"), 30,      NULL);
+    worksheet_write_number(worksheet3, CELL("E3"), 50,      NULL);
+    worksheet_write_number(worksheet3, CELL("F3"), 50,      NULL);
+    worksheet_write_number(worksheet3, CELL("G3"), 50,      NULL);
+    worksheet_write_formula_num(worksheet3, CELL("H3"), "=SUM(B3:G3)", NULL, 210);
+
+    worksheet_write_string(worksheet3, CELL("A4"), "East",  NULL);
+    worksheet_write_number(worksheet3, CELL("B4"), 45,      NULL);
+    worksheet_write_number(worksheet3, CELL("C4"), 75,      NULL);
+    worksheet_write_number(worksheet3, CELL("D4"), 50,      NULL);
+    worksheet_write_number(worksheet3, CELL("E4"), 15,      NULL);
+    worksheet_write_number(worksheet3, CELL("F4"), 75,      NULL);
+    worksheet_write_number(worksheet3, CELL("G4"), 100,     NULL);
+    worksheet_write_formula_num(worksheet3, CELL("H4"), "=SUM(B4:G4)", NULL, 360);
+
+    worksheet_write_string(worksheet3, CELL("A5"), "West",  NULL);
+    worksheet_write_number(worksheet3, CELL("B5"), 15,      NULL);
+    worksheet_write_number(worksheet3, CELL("C5"), 15,      NULL);
+    worksheet_write_number(worksheet3, CELL("D5"), 55,      NULL);
+    worksheet_write_number(worksheet3, CELL("E5"), 35,      NULL);
+    worksheet_write_number(worksheet3, CELL("F5"), 20,      NULL);
+    worksheet_write_number(worksheet3, CELL("G5"), 50,      NULL);
+    worksheet_write_formula_num(worksheet3, CELL("H5"), "=SUM(B5:G5)", NULL, 190);
+
+    worksheet_write_formula_num(worksheet3, CELL("H6"), "=SUM(H2:H5)", bold, 1015);
+
+
+    worksheet_set_row(worksheet3, 0, LXW_DEF_ROW_HEIGHT, bold);
+
+    worksheet_set_column(    worksheet3, COLS("A:A"), 10, bold);
+    worksheet_set_column(    worksheet3, COLS("H:H"), 10, NULL);
+    worksheet_set_column_opt(worksheet3, COLS("B:G"),  6, NULL, &options6);
+
+    return workbook_close(workbook);
+}
diff --git a/test/functional/src/test_outline04.c b/test/functional/src/test_outline04.c
new file mode 100644
index 00000000..ec179e39
--- /dev/null
+++ b/test/functional/src/test_outline04.c
@@ -0,0 +1,54 @@
+/*****************************************************************************
+ * Test cases for libxlsxwriter.
+ *
+ * Test to compare output against Excel files.
+ *
+ * Copyright 2014-2018, John McNamara, jmcnamara@cpan.org
+ *
+ */
+
+#include "xlsxwriter.h"
+
+int main() {
+
+    lxw_workbook  *workbook  = new_workbook("test_outline04.xlsx");
+    lxw_worksheet *worksheet4 = workbook_add_worksheet(workbook, "Outline levels");
+
+    lxw_row_col_options level1 = {.level = 1, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level2 = {.level = 2, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level3 = {.level = 3, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level4 = {.level = 4, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level5 = {.level = 5, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level6 = {.level = 6, .hidden = 0, .collapsed = 0};
+    lxw_row_col_options level7 = {.level = 7, .hidden = 0, .collapsed = 0};
+
+    worksheet_write_string(worksheet4, 0,  0, "Level 1", NULL);
+    worksheet_write_string(worksheet4, 1,  0, "Level 2", NULL);
+    worksheet_write_string(worksheet4, 2,  0, "Level 3", NULL);
+    worksheet_write_string(worksheet4, 3,  0, "Level 4", NULL);
+    worksheet_write_string(worksheet4, 4,  0, "Level 5", NULL);
+    worksheet_write_string(worksheet4, 5,  0, "Level 6", NULL);
+    worksheet_write_string(worksheet4, 6,  0, "Level 7", NULL);
+    worksheet_write_string(worksheet4, 7,  0, "Level 6", NULL);
+    worksheet_write_string(worksheet4, 8,  0, "Level 5", NULL);
+    worksheet_write_string(worksheet4, 9,  0, "Level 4", NULL);
+    worksheet_write_string(worksheet4, 10, 0, "Level 3", NULL);
+    worksheet_write_string(worksheet4, 11, 0, "Level 2", NULL);
+    worksheet_write_string(worksheet4, 12, 0, "Level 1", NULL);
+
+    worksheet_set_row_opt(worksheet4, 0,  LXW_DEF_ROW_HEIGHT, NULL, &level1);
+    worksheet_set_row_opt(worksheet4, 1,  LXW_DEF_ROW_HEIGHT, NULL, &level2);
+    worksheet_set_row_opt(worksheet4, 2,  LXW_DEF_ROW_HEIGHT, NULL, &level3);
+    worksheet_set_row_opt(worksheet4, 3,  LXW_DEF_ROW_HEIGHT, NULL, &level4);
+    worksheet_set_row_opt(worksheet4, 4,  LXW_DEF_ROW_HEIGHT, NULL, &level5);
+    worksheet_set_row_opt(worksheet4, 5,  LXW_DEF_ROW_HEIGHT, NULL, &level6);
+    worksheet_set_row_opt(worksheet4, 6,  LXW_DEF_ROW_HEIGHT, NULL, &level7);
+    worksheet_set_row_opt(worksheet4, 7,  LXW_DEF_ROW_HEIGHT, NULL, &level6);
+    worksheet_set_row_opt(worksheet4, 8,  LXW_DEF_ROW_HEIGHT, NULL, &level5);
+    worksheet_set_row_opt(worksheet4, 9,  LXW_DEF_ROW_HEIGHT, NULL, &level4);
+    worksheet_set_row_opt(worksheet4, 10, LXW_DEF_ROW_HEIGHT, NULL, &level3);
+    worksheet_set_row_opt(worksheet4, 11, LXW_DEF_ROW_HEIGHT, NULL, &level2);
+    worksheet_set_row_opt(worksheet4, 12, LXW_DEF_ROW_HEIGHT, NULL, &level1);
+
+    return workbook_close(workbook);
+}
diff --git a/test/functional/test_outline.py b/test/functional/test_outline.py
new file mode 100644
index 00000000..0bca449b
--- /dev/null
+++ b/test/functional/test_outline.py
@@ -0,0 +1,38 @@
+###############################################################################
+#
+# Tests for libxlsxwriter.
+#
+# Copyright 2014-2017, John McNamara, jmcnamara@cpan.org
+#
+
+import base_test_class
+
+class TestCompareXLSXFiles(base_test_class.XLSXBaseTest):
+    """
+    Test file created with libxlsxwriter against a file created by Excel.
+
+    """
+
+    def test_outline01(self):
+        self.ignore_files = ['xl/calcChain.xml',
+                             '[Content_Types].xml',
+                             'xl/_rels/workbook.xml.rels']
+        self.run_exe_test('test_outline01')
+
+    def test_outline02(self):
+        self.ignore_files = ['xl/calcChain.xml',
+                             '[Content_Types].xml',
+                             'xl/_rels/workbook.xml.rels']
+        self.run_exe_test('test_outline02')
+
+    def test_outline03(self):
+        self.ignore_files = ['xl/calcChain.xml',
+                             '[Content_Types].xml',
+                             'xl/_rels/workbook.xml.rels']
+        self.run_exe_test('test_outline03')
+
+    def test_outline04(self):
+        self.ignore_files = ['xl/calcChain.xml',
+                             '[Content_Types].xml',
+                             'xl/_rels/workbook.xml.rels']
+        self.run_exe_test('test_outline04')
diff --git a/test/functional/xlsx_files/outline01.xlsx b/test/functional/xlsx_files/outline01.xlsx
new file mode 100644
index 00000000..926b013d
Binary files /dev/null and b/test/functional/xlsx_files/outline01.xlsx differ
diff --git a/test/functional/xlsx_files/outline02.xlsx b/test/functional/xlsx_files/outline02.xlsx
new file mode 100644
index 00000000..97d8b80c
Binary files /dev/null and b/test/functional/xlsx_files/outline02.xlsx differ
diff --git a/test/functional/xlsx_files/outline03.xlsx b/test/functional/xlsx_files/outline03.xlsx
new file mode 100644
index 00000000..22f26826
Binary files /dev/null and b/test/functional/xlsx_files/outline03.xlsx differ
diff --git a/test/functional/xlsx_files/outline04.xlsx b/test/functional/xlsx_files/outline04.xlsx
new file mode 100644
index 00000000..ca14fabd
Binary files /dev/null and b/test/functional/xlsx_files/outline04.xlsx differ
