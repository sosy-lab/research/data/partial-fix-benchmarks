diff --git a/Makefile.am b/Makefile.am
index ab7f2332de..6f235b0239 100644
--- a/Makefile.am
+++ b/Makefile.am
@@ -34,7 +34,7 @@ mutt_SOURCES = \
 	edit.c enter.c flags.c init.c filter.c from.c \
 	getdomain.c group.c \
 	handler.c hash.c hdrline.c headers.c help.c hook.c keymap.c \
-	main.c mbox.c menu.c mh.c mx.c pager.c parse.c pattern.c \
+	main.c mbox.c menu.c mh.c mutt_sasl_plain.c mx.c pager.c parse.c pattern.c \
 	postpone.c query.c recvattach.c recvcmd.c \
 	rfc822.c rfc1524.c rfc2047.c rfc2231.c rfc3676.c \
 	score.c send.c sendlib.c signal.c sort.c \
diff --git a/imap/auth_plain.c b/imap/auth_plain.c
index 561c3edefd..248bbab7c3 100644
--- a/imap/auth_plain.c
+++ b/imap/auth_plain.c
@@ -24,16 +24,14 @@
 #endif
 
 #include "mutt.h"
+#include "mutt_sasl_plain.h"
 #include "imap_private.h"
 #include "auth.h"
 
 /* imap_auth_plain: SASL PLAIN support */
 imap_auth_res_t imap_auth_plain (IMAP_DATA* idata, const char* method)
 {
-  char auth[STRING];
   char buf[STRING];
-  size_t authlen;
-  size_t buflen;
 
   if (mutt_account_getuser (&idata->conn->account))
     return IMAP_AUTH_FAILURE;
@@ -42,13 +40,9 @@ imap_auth_res_t imap_auth_plain (IMAP_DATA* idata, const char* method)
 
   mutt_message _("Logging in...");
 
-  authlen = snprintf (auth, sizeof (auth), "%s%c%s%c%s",
-      idata->conn->account.user, '\0', idata->conn->account.user, '\0',
-      idata->conn->account.pass);
-
-  buflen = snprintf (buf, sizeof (buf), "AUTHENTICATE PLAIN ");
-  mutt_to_base64 ((unsigned char *)buf+buflen, (unsigned char *)auth,
-      authlen, STRING-buflen);
+  mutt_sasl_plain_msg (buf, STRING, "AUTHENTICATE PLAIN",
+                       idata->conn->account.user, idata->conn->account.user,
+                       idata->conn->account.pass);
 
   if (!imap_exec (idata, buf, IMAP_CMD_FAIL_OK | IMAP_CMD_PASS))
   {
diff --git a/mutt_sasl_plain.c b/mutt_sasl_plain.c
new file mode 100644
index 0000000000..0a895bcbb2
--- /dev/null
+++ b/mutt_sasl_plain.c
@@ -0,0 +1,46 @@
+/*
+ * Copyright (C) 2016 Pietro Cerutti <gahr@gahr.ch>
+ *
+ *     This program is free software; you can redistribute it and/or modify it
+ *     under the terms of the GNU General Public License as published by the
+ *     Free Software Foundation; either version 2 of the License, or (at your
+ *     option) any later version.
+ *
+ *     This program is distributed in the hope that it will be useful, but
+ *     WITHOUT ANY WARRANTY; without even the implied warranty of
+ *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+ *     General Public License for more details.
+ *
+ *     You should have received a copy of the GNU General Public License along
+ *     with this program; if not, write to the Free Software Foundation, Inc.,
+ *     51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
+ */
+
+#ifdef HAVE_CONFIG_H
+#include "config.h"
+#endif
+
+#include "mutt.h"
+#include "mutt_sasl_plain.h"
+
+size_t
+mutt_sasl_plain_msg(char *buf, size_t buflen, const char *cmd,
+                    const char *authz, const char *user, const char *pass)
+{
+  /* authz, user, and pass can each be up to 255 bytes, making up for a 765
+   * bytes string. Add the two NULL bytes in between plus one at the end and we
+   * get 768. */
+  char tmp[768];
+  size_t len;
+  size_t tmplen;
+
+  if (!user || !*user || !pass || !*pass)
+    return 0;
+
+  tmplen = snprintf(tmp, sizeof(tmp), "%s%c%s%c%s",
+      NONULL(authz), '\0', user, '\0', pass);
+
+  len = snprintf(buf, buflen, "%s ", cmd);
+  len += mutt_to_base64(buf+len, tmp, tmplen, buflen-len);
+  return len;
+}
diff --git a/mutt_sasl_plain.h b/mutt_sasl_plain.h
new file mode 100644
index 0000000000..cc6467a210
--- /dev/null
+++ b/mutt_sasl_plain.h
@@ -0,0 +1,49 @@
+/*
+ * Copyright (C) 2016 Pietro Cerutti <gahr@gahr.ch>
+ *
+ *     This program is free software; you can redistribute it and/or modify it
+ *     under the terms of the GNU General Public License as published by the
+ *     Free Software Foundation; either version 2 of the License, or (at your
+ *     option) any later version.
+ *
+ *     This program is distributed in the hope that it will be useful, but
+ *     WITHOUT ANY WARRANTY; without even the implied warranty of
+ *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+ *     General Public License for more details.
+ *
+ *     You should have received a copy of the GNU General Public License along
+ *     with this program; if not, write to the Free Software Foundation, Inc.,
+ *     51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
+ */
+
+#ifndef _MUTT_SASL_PLAIN_H_
+#define _MUTT_SASL_PLAIN_H_
+
+#include <stdlib.h> /* for size_t */
+
+/**
+ * mutt_sasl_plain_msg - construct a base64 encoded SASL PLAIN message
+ *
+ * This function can be used to build a protocol-specific SASL Response message
+ * using the PLAIN mechanism. The protocol specific command is given in the cmd
+ * parameter. The function appends a space, encodes the string derived from
+ * authz\0user\0pass using base64 encoding, and stores the result in buf.
+ *
+ * Example usages for IMAP and SMTP, respectively:
+ * 
+ * mutt_sasl_plain_msg(buf, sizeof(buf), "AUTHENTICATE PLAIN", user, user, pass);
+ * mutt_sasl_plain_msg(buf, sizeof(buf), "AUTH PLAIN", NULL, user, pass);
+ *
+ * \param buf Destination buffer.
+ * \param buflen Available space in the destination buffer.
+ * \param cmd Protocol-specific string the prepend to the PLAIN message.
+ * \param authz Authorization identity.
+ * \param user Authentication identity (username).
+ * \param pass Password.
+ * \return The number of bytes written to buf.
+ */
+size_t mutt_sasl_plain_msg(char *buf, size_t buflen, const char *cmd,
+                           const char *authz, const char *user,
+                           const char *pass);
+
+#endif /* _MUTT_SASL_PLAIN_H_ */
diff --git a/smtp.c b/smtp.c
index 1d147a5af5..072b24a29f 100644
--- a/smtp.c
+++ b/smtp.c
@@ -34,6 +34,8 @@
 
 #include <sasl/sasl.h>
 #include <sasl/saslutil.h>
+#else
+#include "mutt_sasl_plain.h"
 #endif
 
 #include <netdb.h>
@@ -69,6 +71,8 @@ enum {
 #ifdef USE_SASL
 static int smtp_auth (CONNECTION* conn);
 static int smtp_auth_sasl (CONNECTION* conn, const char* mechanisms);
+#else
+static int smtp_auth_plain (CONNECTION* conn);
 #endif
 
 static int smtp_fill_account (ACCOUNT* account);
@@ -498,9 +502,7 @@ static int smtp_open (CONNECTION* conn)
 #ifdef USE_SASL
     return smtp_auth (conn);
 #else
-    mutt_error (_("SMTP authentication requires SASL"));
-    mutt_sleep (1);
-    return -1;
+    return smtp_auth_plain (conn);
 #endif /* USE_SASL */
   }
 
@@ -662,4 +664,33 @@ static int smtp_auth_sasl (CONNECTION* conn, const char* mechlist)
   FREE (&buf);
   return SMTP_AUTH_FAIL;
 }
+#else /* USE_SASL */
+
+static int smtp_auth_plain (CONNECTION* conn)
+{
+  char buf[LONG_STRING];
+  size_t len;
+  int rc = -1;
+
+  if (mutt_account_getuser (&conn->account) ||
+      mutt_account_getpass (&conn->account))
+      goto end;
+
+  len = mutt_sasl_plain_msg (buf, sizeof (buf), "AUTH PLAIN",
+      conn->account.user, conn->account.user, conn->account.pass);
+  if (len > sizeof (buf) - 3)
+      goto end;
+  snprintf (buf + len, sizeof (buf) - len, "\r\n");
+
+  if (mutt_socket_write (conn, buf) < 0)
+    goto end;
+
+  if (smtp_get_resp (conn) == 0)
+      return 0;
+
+end:
+    mutt_error (_("SASL authentication failed"));
+    mutt_sleep (1);
+    return rc;
+}
 #endif /* USE_SASL */
