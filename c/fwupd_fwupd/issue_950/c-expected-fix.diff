diff --git a/libfwupd/fwupd-client.c b/libfwupd/fwupd-client.c
index 311b6f6b1..649c979c8 100644
--- a/libfwupd/fwupd-client.c
+++ b/libfwupd/fwupd-client.c
@@ -37,6 +37,7 @@ static void fwupd_client_finalize	 (GObject *object);
 
 typedef struct {
 	FwupdStatus			 status;
+	gboolean			 tainted;
 	guint				 percentage;
 	gchar				*daemon_version;
 	GDBusConnection			*conn;
@@ -57,6 +58,7 @@ enum {
 	PROP_STATUS,
 	PROP_PERCENTAGE,
 	PROP_DAEMON_VERSION,
+	PROP_TAINTED,
 	PROP_LAST
 };
 
@@ -131,6 +133,14 @@ fwupd_client_properties_changed_cb (GDBusProxy *proxy,
 			g_object_notify (G_OBJECT (client), "status");
 		}
 	}
+	if (g_variant_dict_contains (dict, "Tainted")) {
+		g_autoptr(GVariant) val = NULL;
+		val = g_dbus_proxy_get_cached_property (proxy, "Tainted");
+		if (val != NULL) {
+			priv->tainted = g_variant_get_boolean (val);
+			g_object_notify (G_OBJECT (client), "tainted");
+		}
+	}
 	if (g_variant_dict_contains (dict, "Percentage")) {
 		g_autoptr(GVariant) val = NULL;
 		val = g_dbus_proxy_get_cached_property (proxy, "Percentage");
@@ -203,6 +213,7 @@ fwupd_client_connect (FwupdClient *client, GCancellable *cancellable, GError **e
 {
 	FwupdClientPrivate *priv = GET_PRIVATE (client);
 	g_autoptr(GVariant) val = NULL;
+	g_autoptr(GVariant) val2 = NULL;
 
 	g_return_val_if_fail (FWUPD_IS_CLIENT (client), FALSE);
 	g_return_val_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable), FALSE);
@@ -235,6 +246,9 @@ fwupd_client_connect (FwupdClient *client, GCancellable *cancellable, GError **e
 	val = g_dbus_proxy_get_cached_property (priv->proxy, "DaemonVersion");
 	if (val != NULL)
 		fwupd_client_set_daemon_version (client, g_variant_get_string (val, NULL));
+	val2 = g_dbus_proxy_get_cached_property (priv->proxy, "Tainted");
+	if (val2 != NULL)
+		priv->tainted = g_variant_get_boolean (val2);
 	return TRUE;
 }
 
@@ -1138,6 +1152,24 @@ fwupd_client_get_status (FwupdClient *client)
 	return priv->status;
 }
 
+/**
+ * fwupd_client_get_tainted:
+ * @client: A #FwupdClient
+ *
+ * Gets if the daemon has been tainted by 3rd party code.
+ *
+ * Returns: %TRUE if the daemon is unsupported
+ *
+ * Since: 1.2.4
+ **/
+gboolean
+fwupd_client_get_tainted (FwupdClient *client)
+{
+	FwupdClientPrivate *priv = GET_PRIVATE (client);
+	g_return_val_if_fail (FWUPD_IS_CLIENT (client), FALSE);
+	return priv->tainted;
+}
+
 /**
  * fwupd_client_update_metadata:
  * @client: A #FwupdClient
@@ -1461,6 +1493,9 @@ fwupd_client_get_property (GObject *object, guint prop_id,
 	case PROP_STATUS:
 		g_value_set_uint (value, priv->status);
 		break;
+	case PROP_TAINTED:
+		g_value_set_boolean (value, priv->tainted);
+		break;
 	case PROP_PERCENTAGE:
 		g_value_set_uint (value, priv->percentage);
 		break;
@@ -1598,6 +1633,17 @@ fwupd_client_class_init (FwupdClientClass *klass)
 				   G_PARAM_READWRITE | G_PARAM_STATIC_NAME);
 	g_object_class_install_property (object_class, PROP_STATUS, pspec);
 
+	/**
+	 * FwupdClient:tainted:
+	 *
+	 * If the daemon is tainted by 3rd party code.
+	 *
+	 * Since: 1.2.4
+	 */
+	pspec = g_param_spec_boolean ("tainted", NULL, NULL, FALSE,
+				      G_PARAM_READABLE | G_PARAM_STATIC_NAME);
+	g_object_class_install_property (object_class, PROP_TAINTED, pspec);
+
 	/**
 	 * FwupdClient:percentage:
 	 *
diff --git a/libfwupd/fwupd-client.h b/libfwupd/fwupd-client.h
index f33011faf..29bc13d54 100644
--- a/libfwupd/fwupd-client.h
+++ b/libfwupd/fwupd-client.h
@@ -116,6 +116,7 @@ gboolean	 fwupd_client_modify_device		(FwupdClient	*client,
 							 GCancellable	*cancellable,
 							 GError		**error);
 FwupdStatus	 fwupd_client_get_status		(FwupdClient	*client);
+gboolean	 fwupd_client_get_tainted		(FwupdClient	*client);
 guint		 fwupd_client_get_percentage		(FwupdClient	*client);
 const gchar	*fwupd_client_get_daemon_version	(FwupdClient	*client);
 
diff --git a/libfwupd/fwupd.map b/libfwupd/fwupd.map
index 929764fbd..3bedb8cb7 100644
--- a/libfwupd/fwupd.map
+++ b/libfwupd/fwupd.map
@@ -293,3 +293,9 @@ LIBFWUPD_1.2.2 {
     fwupd_release_set_protocol;
   local: *;
 } LIBFWUPD_1.2.1;
+
+LIBFWUPD_1.2.4 {
+  global:
+    fwupd_client_get_tainted;
+  local: *;
+} LIBFWUPD_1.2.2;
diff --git a/meson.build b/meson.build
index b95d7b042..aea7a545b 100644
--- a/meson.build
+++ b/meson.build
@@ -193,6 +193,7 @@ if libgcab.version().version_compare('>= 1.0')
 endif
 gcab = find_program('gcab', required : true)
 bashcomp = dependency('bash-completion', required: false)
+python3 = find_program('python3')
 
 if valgrind.found()
   conf.set('HAVE_VALGRIND', '1')
@@ -237,8 +238,6 @@ if get_option('plugin_uefi')
           gnu_efi_arch = ''
   endif
   conf.set_quoted('EFI_MACHINE_TYPE_NAME', EFI_MACHINE_TYPE_NAME)
-
-  python3 = find_program('python3')
   r = run_command([python3, 'po/test-deps'])
   if r.returncode() != 0
     error(r.stdout())
diff --git a/plugins/altos/fu-plugin-altos.c b/plugins/altos/fu-plugin-altos.c
index 8cfdc3bd6..9ddcd460e 100644
--- a/plugins/altos/fu-plugin-altos.c
+++ b/plugins/altos/fu-plugin-altos.c
@@ -13,6 +13,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "org.altusmetrum.altos");
 }
diff --git a/plugins/altos/meson.build b/plugins/altos/meson.build
index bd0d8e47c..55cb3d841 100644
--- a/plugins/altos/meson.build
+++ b/plugins/altos/meson.build
@@ -5,6 +5,7 @@ install_data(['altos.quirk'],
 )
 
 shared_module('fu_plugin_altos',
+  fu_hash,
   sources : [
     'fu-altos-device.c',
     'fu-altos-firmware.c',
diff --git a/plugins/amt/fu-plugin-amt.c b/plugins/amt/fu-plugin-amt.c
index ff5ea0e96..44bdf85be 100644
--- a/plugins/amt/fu-plugin-amt.c
+++ b/plugins/amt/fu-plugin-amt.c
@@ -539,6 +539,12 @@ fu_plugin_amt_create_device (GError **error)
 	return g_steal_pointer (&dev);
 }
 
+void
+fu_plugin_init (FuPlugin *plugin)
+{
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
+}
+
 gboolean
 fu_plugin_coldplug (FuPlugin *plugin, GError **error)
 {
diff --git a/plugins/amt/meson.build b/plugins/amt/meson.build
index 18ddedb35..0904a2ace 100644
--- a/plugins/amt/meson.build
+++ b/plugins/amt/meson.build
@@ -1,6 +1,7 @@
 cargs = ['-DG_LOG_DOMAIN="FuPluginAmt"']
 
 shared_module('fu_plugin_amt',
+  fu_hash,
   sources : [
     'fu-plugin-amt.c',
   ],
diff --git a/plugins/colorhug/fu-plugin-colorhug.c b/plugins/colorhug/fu-plugin-colorhug.c
index 1fc356627..9aefa6233 100644
--- a/plugins/colorhug/fu-plugin-colorhug.c
+++ b/plugins/colorhug/fu-plugin-colorhug.c
@@ -13,6 +13,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.hughski.colorhug");
 }
diff --git a/plugins/colorhug/meson.build b/plugins/colorhug/meson.build
index 3b6b86279..4894f2954 100644
--- a/plugins/colorhug/meson.build
+++ b/plugins/colorhug/meson.build
@@ -7,6 +7,7 @@ install_data([
 )
 
 shared_module('fu_plugin_colorhug',
+  fu_hash,
   sources : [
     'fu-colorhug-common.c',
     'fu-colorhug-device.c',
diff --git a/plugins/csr/fu-plugin-csr.c b/plugins/csr/fu-plugin-csr.c
index aaedbbca2..c6e14cea4 100644
--- a/plugins/csr/fu-plugin-csr.c
+++ b/plugins/csr/fu-plugin-csr.c
@@ -13,6 +13,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.qualcomm.dfu");
 }
diff --git a/plugins/csr/meson.build b/plugins/csr/meson.build
index 3748c18e3..09595c860 100644
--- a/plugins/csr/meson.build
+++ b/plugins/csr/meson.build
@@ -5,6 +5,7 @@ install_data(['csr-aiaiai.quirk'],
 )
 
 shared_module('fu_plugin_csr',
+  fu_hash,
   sources : [
     'fu-csr-device.c',
     'fu-plugin-csr.c',
diff --git a/plugins/dell-dock/fu-plugin-dell-dock.c b/plugins/dell-dock/fu-plugin-dell-dock.c
index eca46ed57..6992add1a 100644
--- a/plugins/dell-dock/fu-plugin-dell-dock.c
+++ b/plugins/dell-dock/fu-plugin-dell-dock.c
@@ -21,8 +21,11 @@
 
 #include "fu-dell-dock-common.h"
 
-void fu_plugin_init (FuPlugin *plugin)
+void
+fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
+
 	/* allow these to be built by quirks */
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	g_type_ensure (FU_TYPE_DELL_DOCK_STATUS);
diff --git a/plugins/dell-dock/meson.build b/plugins/dell-dock/meson.build
index a03a60760..9e719680a 100644
--- a/plugins/dell-dock/meson.build
+++ b/plugins/dell-dock/meson.build
@@ -5,6 +5,7 @@ install_data(['dell-dock.quirk'],
 )
 
 shared_module('fu_plugin_dell_dock',
+  fu_hash,
   sources : [
     'fu-plugin-dell-dock.c',
     'fu-dell-dock-common.c',
diff --git a/plugins/dell-esrt/fu-plugin-dell-esrt.c b/plugins/dell-esrt/fu-plugin-dell-esrt.c
index 7e32b4f59..a13d78c70 100644
--- a/plugins/dell-esrt/fu-plugin-dell-esrt.c
+++ b/plugins/dell-esrt/fu-plugin-dell-esrt.c
@@ -83,6 +83,12 @@ fu_plugin_dell_esrt_admin_password_present (gboolean *password_present, GError *
 	return TRUE;
 }
 
+void
+fu_plugin_init (FuPlugin *plugin)
+{
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
+}
+
 gboolean
 fu_plugin_startup (FuPlugin *plugin, GError **error)
 {
diff --git a/plugins/dell-esrt/meson.build b/plugins/dell-esrt/meson.build
index 39fae22d3..45af43ee3 100644
--- a/plugins/dell-esrt/meson.build
+++ b/plugins/dell-esrt/meson.build
@@ -1,6 +1,7 @@
 cargs = ['-DG_LOG_DOMAIN="FuPluginDellEsrt"']
 
 shared_module('fu_plugin_dell_esrt',
+  fu_hash,
   sources : [
     'fu-plugin-dell-esrt.c',
   ],
diff --git a/plugins/dell/fu-plugin-dell.c b/plugins/dell/fu-plugin-dell.c
index b7bd83f57..42cff459c 100644
--- a/plugins/dell/fu-plugin-dell.c
+++ b/plugins/dell/fu-plugin-dell.c
@@ -786,6 +786,7 @@ fu_plugin_init (FuPlugin *plugin)
 	FuPluginData *data = fu_plugin_alloc_data (plugin, sizeof (FuPluginData));
 	g_autofree gchar *tmp = NULL;
 
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	tmp = g_strdup_printf ("%d.%d",
 			       smbios_get_library_version_major(),
 			       smbios_get_library_version_minor());
diff --git a/plugins/dell/meson.build b/plugins/dell/meson.build
index 67683591b..df96b979f 100644
--- a/plugins/dell/meson.build
+++ b/plugins/dell/meson.build
@@ -5,6 +5,7 @@ install_data(['dell.quirk'],
 )
 
 shared_module('fu_plugin_dell',
+  fu_hash,
   sources : [
     'fu-plugin-dell.c',
     'fu-dell-smi.c',
diff --git a/plugins/dfu/fu-plugin-dfu.c b/plugins/dfu/fu-plugin-dfu.c
index 18060a7c4..4b0682edc 100644
--- a/plugins/dfu/fu-plugin-dfu.c
+++ b/plugins/dfu/fu-plugin-dfu.c
@@ -13,6 +13,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "org.usb.dfu");
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.st.dfuse");
diff --git a/plugins/dfu/meson.build b/plugins/dfu/meson.build
index 404b7bbb7..f9d9632dc 100644
--- a/plugins/dfu/meson.build
+++ b/plugins/dfu/meson.build
@@ -43,6 +43,7 @@ dfu = static_library(
 )
 
 shared_module('fu_plugin_dfu',
+  fu_hash,
   sources : [
     'fu-plugin-dfu.c',
   ],
@@ -65,6 +66,7 @@ shared_module('fu_plugin_dfu',
 
 dfu_tool = executable(
   'dfu-tool',
+  fu_hash,
   sources : [
     'dfu-tool.c',
   ],
diff --git a/plugins/ebitdo/fu-plugin-ebitdo.c b/plugins/ebitdo/fu-plugin-ebitdo.c
index 60a6581e0..12b6691b1 100644
--- a/plugins/ebitdo/fu-plugin-ebitdo.c
+++ b/plugins/ebitdo/fu-plugin-ebitdo.c
@@ -13,6 +13,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.8bitdo");
 }
diff --git a/plugins/ebitdo/meson.build b/plugins/ebitdo/meson.build
index 4d438a740..99c507581 100644
--- a/plugins/ebitdo/meson.build
+++ b/plugins/ebitdo/meson.build
@@ -5,6 +5,7 @@ install_data(['ebitdo.quirk'],
 )
 
 shared_module('fu_plugin_ebitdo',
+  fu_hash,
   sources : [
     'fu-plugin-ebitdo.c',
     'fu-ebitdo-common.c',
diff --git a/plugins/fastboot/fu-plugin-fastboot.c b/plugins/fastboot/fu-plugin-fastboot.c
index b2ad4023f..0d372fc08 100644
--- a/plugins/fastboot/fu-plugin-fastboot.c
+++ b/plugins/fastboot/fu-plugin-fastboot.c
@@ -13,6 +13,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.google.fastboot");
 }
diff --git a/plugins/fastboot/meson.build b/plugins/fastboot/meson.build
index 3afa16734..09d9b2323 100644
--- a/plugins/fastboot/meson.build
+++ b/plugins/fastboot/meson.build
@@ -5,6 +5,7 @@ install_data(['fastboot.quirk'],
 )
 
 shared_module('fu_plugin_fastboot',
+  fu_hash,
   sources : [
     'fu-plugin-fastboot.c',
     'fu-fastboot-device.c',
diff --git a/plugins/flashrom/fu-plugin-flashrom.c b/plugins/flashrom/fu-plugin-flashrom.c
index 11866d059..9085ffe50 100644
--- a/plugins/flashrom/fu-plugin-flashrom.c
+++ b/plugins/flashrom/fu-plugin-flashrom.c
@@ -32,6 +32,7 @@ struct FuPluginData {
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_alloc_data (plugin, sizeof (FuPluginData));
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "org.flashrom");
 }
diff --git a/plugins/flashrom/meson.build b/plugins/flashrom/meson.build
index ee048e51f..222b5f88f 100644
--- a/plugins/flashrom/meson.build
+++ b/plugins/flashrom/meson.build
@@ -5,6 +5,7 @@ install_data(['flashrom.quirk'],
 )
 
 shared_module('fu_plugin_flashrom',
+  fu_hash,
   sources : [
     'fu-plugin-flashrom.c',
   ],
diff --git a/plugins/nitrokey/fu-plugin-nitrokey.c b/plugins/nitrokey/fu-plugin-nitrokey.c
index 9c4e23609..4061ee7d7 100644
--- a/plugins/nitrokey/fu-plugin-nitrokey.c
+++ b/plugins/nitrokey/fu-plugin-nitrokey.c
@@ -14,6 +14,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 }
 
diff --git a/plugins/nitrokey/meson.build b/plugins/nitrokey/meson.build
index 1aee68859..469114985 100644
--- a/plugins/nitrokey/meson.build
+++ b/plugins/nitrokey/meson.build
@@ -5,6 +5,7 @@ install_data(['nitrokey.quirk'],
 )
 
 shared_module('fu_plugin_nitrokey',
+  fu_hash,
   sources : [
     'fu-nitrokey-device.c',
     'fu-nitrokey-common.c',
diff --git a/plugins/nvme/fu-plugin-nvme.c b/plugins/nvme/fu-plugin-nvme.c
index b1f89d7ce..25ef68f9f 100644
--- a/plugins/nvme/fu-plugin-nvme.c
+++ b/plugins/nvme/fu-plugin-nvme.c
@@ -31,6 +31,7 @@ fu_plugin_udev_device_added (FuPlugin *plugin, FuUdevDevice *device, GError **er
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_udev_subsystem (plugin, "nvme");
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "org.nvmexpress");
 }
diff --git a/plugins/nvme/meson.build b/plugins/nvme/meson.build
index 66ecb2878..614ccc609 100644
--- a/plugins/nvme/meson.build
+++ b/plugins/nvme/meson.build
@@ -7,6 +7,7 @@ install_data([
 )
 
 shared_module('fu_plugin_nvme',
+  fu_hash,
   sources : [
     'fu-plugin-nvme.c',
     'fu-nvme-common.c',
diff --git a/plugins/redfish/fu-plugin-redfish.c b/plugins/redfish/fu-plugin-redfish.c
index 918801acd..60ca60ece 100644
--- a/plugins/redfish/fu-plugin-redfish.c
+++ b/plugins/redfish/fu-plugin-redfish.c
@@ -118,6 +118,7 @@ fu_plugin_init (FuPlugin *plugin)
 	FuPluginData *data = fu_plugin_alloc_data (plugin, sizeof (FuPluginData));
 	data->client = fu_redfish_client_new ();
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "org.dmtf.redfish");
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 }
 
 void
diff --git a/plugins/redfish/meson.build b/plugins/redfish/meson.build
index ef07bd819..a4cdba558 100644
--- a/plugins/redfish/meson.build
+++ b/plugins/redfish/meson.build
@@ -1,6 +1,7 @@
 cargs = ['-DG_LOG_DOMAIN="FuPluginRedfish"']
 
 shared_module('fu_plugin_redfish',
+  fu_hash,
   sources : [
     'fu-plugin-redfish.c',
     'fu-redfish-client.c',
diff --git a/plugins/rts54hid/fu-plugin-rts54hid.c b/plugins/rts54hid/fu-plugin-rts54hid.c
index 00621399d..af6699567 100644
--- a/plugins/rts54hid/fu-plugin-rts54hid.c
+++ b/plugins/rts54hid/fu-plugin-rts54hid.c
@@ -14,6 +14,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.realtek.rts54");
 
diff --git a/plugins/rts54hid/meson.build b/plugins/rts54hid/meson.build
index ff6c621de..809a11349 100644
--- a/plugins/rts54hid/meson.build
+++ b/plugins/rts54hid/meson.build
@@ -7,6 +7,7 @@ install_data([
 )
 
 shared_module('fu_plugin_rts54hid',
+  fu_hash,
   sources : [
     'fu-rts54hid-device.c',
     'fu-rts54hid-module.c',
diff --git a/plugins/rts54hub/fu-plugin-rts54hub.c b/plugins/rts54hub/fu-plugin-rts54hub.c
index d0c7eb294..7300f2d1d 100644
--- a/plugins/rts54hub/fu-plugin-rts54hub.c
+++ b/plugins/rts54hub/fu-plugin-rts54hub.c
@@ -13,6 +13,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.realtek.rts54");
 }
diff --git a/plugins/rts54hub/meson.build b/plugins/rts54hub/meson.build
index 3f525b4ad..46b220382 100644
--- a/plugins/rts54hub/meson.build
+++ b/plugins/rts54hub/meson.build
@@ -7,6 +7,7 @@ install_data([
 )
 
 shared_module('fu_plugin_rts54hub',
+  fu_hash,
   sources : [
     'fu-rts54hub-device.c',
     'fu-plugin-rts54hub.c',
diff --git a/plugins/steelseries/fu-plugin-steelseries.c b/plugins/steelseries/fu-plugin-steelseries.c
index f5479c47a..78395cf48 100644
--- a/plugins/steelseries/fu-plugin-steelseries.c
+++ b/plugins/steelseries/fu-plugin-steelseries.c
@@ -13,6 +13,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 }
 
diff --git a/plugins/steelseries/meson.build b/plugins/steelseries/meson.build
index e2fc45ea5..68fe15abc 100644
--- a/plugins/steelseries/meson.build
+++ b/plugins/steelseries/meson.build
@@ -5,6 +5,7 @@ install_data(['steelseries.quirk'],
 )
 
 shared_module('fu_plugin_steelseries',
+  fu_hash,
   sources : [
     'fu-plugin-steelseries.c',
     'fu-steelseries-device.c',
diff --git a/plugins/superio/fu-plugin-superio.c b/plugins/superio/fu-plugin-superio.c
index 2135cd551..a8f1ed24e 100644
--- a/plugins/superio/fu-plugin-superio.c
+++ b/plugins/superio/fu-plugin-superio.c
@@ -62,6 +62,12 @@ fu_plugin_superio_coldplug_chipsets (FuPlugin *plugin, const gchar *str, GError
 	return TRUE;
 }
 
+void
+fu_plugin_init (FuPlugin *plugin)
+{
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
+}
+
 gboolean
 fu_plugin_coldplug (FuPlugin *plugin, GError **error)
 {
diff --git a/plugins/superio/meson.build b/plugins/superio/meson.build
index ecbfad547..06b094342 100644
--- a/plugins/superio/meson.build
+++ b/plugins/superio/meson.build
@@ -5,6 +5,7 @@ install_data(['superio.quirk'],
 )
 
 shared_module('fu_plugin_superio',
+  fu_hash,
   sources : [
     'fu-plugin-superio.c',
     'fu-superio-device.c',
diff --git a/plugins/synapticsmst/fu-plugin-synapticsmst.c b/plugins/synapticsmst/fu-plugin-synapticsmst.c
index 1ceb540d4..fa86b2144 100644
--- a/plugins/synapticsmst/fu-plugin-synapticsmst.c
+++ b/plugins/synapticsmst/fu-plugin-synapticsmst.c
@@ -472,4 +472,5 @@ fu_plugin_init (FuPlugin *plugin)
 	/* make sure dell is already coldplugged */
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_RUN_AFTER, "dell");
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.synaptics.mst");
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 }
diff --git a/plugins/synapticsmst/meson.build b/plugins/synapticsmst/meson.build
index 197620b77..0f1abdbf0 100644
--- a/plugins/synapticsmst/meson.build
+++ b/plugins/synapticsmst/meson.build
@@ -5,6 +5,7 @@ install_data(['synapticsmst.quirk'],
 )
 
 shared_module('fu_plugin_synapticsmst',
+  fu_hash,
   sources : [
     'fu-plugin-synapticsmst.c',
     'synapticsmst-common.c',
diff --git a/plugins/test/fu-plugin-test.c b/plugins/test/fu-plugin-test.c
index 34023a6d9..9c7875943 100644
--- a/plugins/test/fu-plugin-test.c
+++ b/plugins/test/fu-plugin-test.c
@@ -15,6 +15,10 @@ struct FuPluginData {
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	if (g_strcmp0 (g_getenv ("FWUPD_PLUGIN_TEST"), "build-hash") == 0)
+		fu_plugin_set_build_hash (plugin, "invalid");
+	else
+		fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.acme.test");
 	fu_plugin_alloc_data (plugin, sizeof (FuPluginData));
 	g_debug ("init");
diff --git a/plugins/test/meson.build b/plugins/test/meson.build
index 2dfdd91e9..c473ab4b0 100644
--- a/plugins/test/meson.build
+++ b/plugins/test/meson.build
@@ -6,6 +6,7 @@ if get_option('plugin_dummy')
 endif
 
 shared_module('fu_plugin_test',
+  fu_hash,
   sources : [
     'fu-plugin-test.c',
   ],
diff --git a/plugins/thunderbolt-power/fu-plugin-thunderbolt-power.c b/plugins/thunderbolt-power/fu-plugin-thunderbolt-power.c
index 4430347af..4bfc20d01 100644
--- a/plugins/thunderbolt-power/fu-plugin-thunderbolt-power.c
+++ b/plugins/thunderbolt-power/fu-plugin-thunderbolt-power.c
@@ -320,6 +320,7 @@ fu_plugin_init (FuPlugin *plugin)
 
 	/* make sure it's tried to coldplug */
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_RUN_AFTER, "thunderbolt");
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 }
 
 void
diff --git a/plugins/thunderbolt-power/meson.build b/plugins/thunderbolt-power/meson.build
index 1df672074..747c782f6 100644
--- a/plugins/thunderbolt-power/meson.build
+++ b/plugins/thunderbolt-power/meson.build
@@ -1,6 +1,7 @@
 cargs = ['-DG_LOG_DOMAIN="FuPluginThunderbolt"']
 
 fu_plugin_thunderbolt_power = shared_module('fu_plugin_thunderbolt_power',
+  fu_hash,
   sources : [
     'fu-plugin-thunderbolt-power.c',
   ],
diff --git a/plugins/thunderbolt/fu-plugin-thunderbolt.c b/plugins/thunderbolt/fu-plugin-thunderbolt.c
index db18b5516..3fa0613ce 100644
--- a/plugins/thunderbolt/fu-plugin-thunderbolt.c
+++ b/plugins/thunderbolt/fu-plugin-thunderbolt.c
@@ -585,6 +585,7 @@ fu_plugin_init (FuPlugin *plugin)
 	FuPluginData *data = fu_plugin_alloc_data (plugin, sizeof (FuPluginData));
 	const gchar *subsystems[] = { "thunderbolt", NULL };
 
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.intel.thunderbolt");
 	data->udev = g_udev_client_new (subsystems);
 	g_signal_connect (data->udev, "uevent",
diff --git a/plugins/thunderbolt/meson.build b/plugins/thunderbolt/meson.build
index ce7c76203..10991e5b1 100644
--- a/plugins/thunderbolt/meson.build
+++ b/plugins/thunderbolt/meson.build
@@ -1,6 +1,7 @@
 cargs = ['-DG_LOG_DOMAIN="FuPluginThunderbolt"']
 
 fu_plugin_thunderbolt = shared_module('fu_plugin_thunderbolt',
+  fu_hash,
   sources : [
     'fu-plugin-thunderbolt.c',
     'fu-thunderbolt-image.c',
diff --git a/plugins/udev/fu-plugin-udev.c b/plugins/udev/fu-plugin-udev.c
index 07f64cdf9..af48b1add 100644
--- a/plugins/udev/fu-plugin-udev.c
+++ b/plugins/udev/fu-plugin-udev.c
@@ -13,6 +13,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_udev_subsystem (plugin, "pci");
 }
 
diff --git a/plugins/udev/meson.build b/plugins/udev/meson.build
index 33eb149ca..334b14a87 100644
--- a/plugins/udev/meson.build
+++ b/plugins/udev/meson.build
@@ -1,6 +1,7 @@
 cargs = ['-DG_LOG_DOMAIN="FuPluginUdev"']
 
 shared_module('fu_plugin_udev',
+  fu_hash,
   sources : [
     'fu-plugin-udev.c',
     'fu-rom.c',
@@ -23,6 +24,7 @@ shared_module('fu_plugin_udev',
 
 executable(
   'fu-rom-tool',
+  fu_hash,
   sources : [
     'fu-rom-tool.c',
     'fu-rom.c',
diff --git a/plugins/uefi/fu-plugin-uefi.c b/plugins/uefi/fu-plugin-uefi.c
index 9c7d37f48..26a0b053f 100644
--- a/plugins/uefi/fu-plugin-uefi.c
+++ b/plugins/uefi/fu-plugin-uefi.c
@@ -40,6 +40,7 @@ fu_plugin_init (FuPlugin *plugin)
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_RUN_AFTER, "upower");
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "org.uefi.capsule");
 	fu_plugin_add_compile_version (plugin, "com.redhat.efivar", EFIVAR_LIBRARY_VERSION);
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 }
 
 void
diff --git a/plugins/uefi/meson.build b/plugins/uefi/meson.build
index 09ebdf82d..2492e4eca 100644
--- a/plugins/uefi/meson.build
+++ b/plugins/uefi/meson.build
@@ -8,6 +8,7 @@ install_data(['uefi.quirk'],
 )
 
 shared_module('fu_plugin_uefi',
+  fu_hash,
   sources : [
     'fu-plugin-uefi.c',
     'fu-uefi-bgrt.c',
@@ -40,6 +41,7 @@ shared_module('fu_plugin_uefi',
 executable(
   'fwupdate',
   resources_src,
+  fu_hash,
   sources : [
     'fu-uefi-tool.c',
     'fu-uefi-bgrt.c',
diff --git a/plugins/unifying/fu-plugin-unifying.c b/plugins/unifying/fu-plugin-unifying.c
index dd9adbcd1..58af46f35 100644
--- a/plugins/unifying/fu-plugin-unifying.c
+++ b/plugins/unifying/fu-plugin-unifying.c
@@ -173,6 +173,7 @@ fu_plugin_startup (FuPlugin *plugin, GError **error)
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.logitech.unifying");
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.logitech.unifyingsigned");
diff --git a/plugins/unifying/meson.build b/plugins/unifying/meson.build
index f3ab13008..7e04912db 100644
--- a/plugins/unifying/meson.build
+++ b/plugins/unifying/meson.build
@@ -8,6 +8,7 @@ install_data([
 
 
 shared_module('fu_plugin_unifying',
+  fu_hash,
   sources : [
     'fu-plugin-unifying.c',
     'fu-unifying-bootloader.c',
diff --git a/plugins/upower/fu-plugin-upower.c b/plugins/upower/fu-plugin-upower.c
index 3b6d7ed3c..780cf34f5 100644
--- a/plugins/upower/fu-plugin-upower.c
+++ b/plugins/upower/fu-plugin-upower.c
@@ -18,6 +18,7 @@ struct FuPluginData {
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_alloc_data (plugin, sizeof (FuPluginData));
 }
 
diff --git a/plugins/upower/meson.build b/plugins/upower/meson.build
index 62c7dfc54..0f9e0dc3a 100644
--- a/plugins/upower/meson.build
+++ b/plugins/upower/meson.build
@@ -1,6 +1,7 @@
 cargs = ['-DG_LOG_DOMAIN="FuPluginUpower"']
 
 shared_module('fu_plugin_upower',
+  fu_hash,
   sources : [
     'fu-plugin-upower.c',
   ],
diff --git a/plugins/wacom-usb/fu-plugin-wacom-usb.c b/plugins/wacom-usb/fu-plugin-wacom-usb.c
index 4617ce482..af82ae425 100644
--- a/plugins/wacom-usb/fu-plugin-wacom-usb.c
+++ b/plugins/wacom-usb/fu-plugin-wacom-usb.c
@@ -13,6 +13,7 @@
 void
 fu_plugin_init (FuPlugin *plugin)
 {
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_REQUIRES_QUIRK, FU_QUIRKS_PLUGIN);
 	fu_plugin_add_rule (plugin, FU_PLUGIN_RULE_SUPPORTS_PROTOCOL, "com.wacom.usb");
 }
diff --git a/plugins/wacom-usb/meson.build b/plugins/wacom-usb/meson.build
index 44374232c..e2a3f37c7 100644
--- a/plugins/wacom-usb/meson.build
+++ b/plugins/wacom-usb/meson.build
@@ -5,6 +5,7 @@ install_data(['wacom-usb.quirk'],
 )
 
 shared_module('fu_plugin_wacom_usb',
+  fu_hash,
   sources : [
     'fu-wac-common.c',
     'fu-wac-device.c',
diff --git a/src/fu-engine.c b/src/fu-engine.c
index dfe755e0d..8b7c2559a 100644
--- a/src/fu-engine.c
+++ b/src/fu-engine.c
@@ -34,6 +34,7 @@
 #include "fu-hwids.h"
 #include "fu-idle.h"
 #include "fu-keyring-utils.h"
+#include "fu-hash.h"
 #include "fu-history.h"
 #include "fu-mutex.h"
 #include "fu-plugin.h"
@@ -55,6 +56,7 @@ struct _FuEngine
 	FuConfig		*config;
 	FuDeviceList		*device_list;
 	FwupdStatus		 status;
+	gboolean		 tainted;
 	guint			 percentage;
 	FuHistory		*history;
 	FuIdle			*idle;
@@ -3376,10 +3378,22 @@ fu_engine_plugin_set_coldplug_delay_cb (FuPlugin *plugin, guint duration, FuEngi
 		 duration, self->coldplug_delay);
 }
 
-/* for the self tests to use */
+/* this is called by the self tests as well */
 void
 fu_engine_add_plugin (FuEngine *self, FuPlugin *plugin)
 {
+	/* plugin does not match built version */
+	if (fu_plugin_get_build_hash (plugin) == NULL) {
+		const gchar *name = fu_plugin_get_name (plugin);
+		g_warning ("%s should call fu_plugin_set_build_hash()", name);
+		self->tainted = TRUE;
+	} else if (g_strcmp0 (fu_plugin_get_build_hash (plugin), FU_BUILD_HASH) != 0) {
+		const gchar *name = fu_plugin_get_name (plugin);
+		g_warning ("%s has incorrect built version %s",
+				name, fu_plugin_get_build_hash (plugin));
+		self->tainted = TRUE;
+	}
+
 	fu_plugin_list_add (self->plugin_list, plugin);
 }
 
@@ -3428,6 +3442,12 @@ fu_engine_plugin_check_supported_cb (FuPlugin *plugin, const gchar *guid, FuEngi
 	return n != NULL;
 }
 
+gboolean
+fu_engine_get_tainted (FuEngine *self)
+{
+	return self->tainted;
+}
+
 gboolean
 fu_engine_load_plugins (FuEngine *self, GError **error)
 {
@@ -3516,7 +3536,7 @@ fu_engine_load_plugins (FuEngine *self, GError **error)
 				  self);
 
 		/* add */
-		fu_plugin_list_add (self->plugin_list, plugin);
+		fu_engine_add_plugin (self, plugin);
 	}
 
 	/* depsolve into the correct order */
diff --git a/src/fu-engine.h b/src/fu-engine.h
index 67376e652..a15d117d0 100644
--- a/src/fu-engine.h
+++ b/src/fu-engine.h
@@ -30,6 +30,7 @@ gboolean	 fu_engine_load				(FuEngine	*self,
 							 GError		**error);
 gboolean	 fu_engine_load_plugins			(FuEngine	*self,
 							 GError		**error);
+gboolean	 fu_engine_get_tainted			(FuEngine	*self);
 FwupdStatus	 fu_engine_get_status			(FuEngine	*self);
 XbSilo		*fu_engine_get_silo_from_blob		(FuEngine	*self,
 							 GBytes		*blob_cab,
diff --git a/src/fu-hash.py b/src/fu-hash.py
new file mode 100644
index 000000000..11cb1d731
--- /dev/null
+++ b/src/fu-hash.py
@@ -0,0 +1,33 @@
+#!/usr/bin/python3
+""" Builds a header for the plugins to include """
+
+# pylint: disable=invalid-name,wrong-import-position,pointless-string-statement
+
+"""
+SPDX-License-Identifier: LGPL-2.1+
+"""
+
+import sys
+import hashlib
+
+def usage(return_code):
+    """ print usage and exit with the supplied return code """
+    if return_code == 0:
+        out = sys.stdout
+    else:
+        out = sys.stderr
+    out.write("usage: fu-hash.py <DAEMON> <HEADER>")
+    sys.exit(return_code)
+
+if __name__ == '__main__':
+    if {'-?', '--help', '--usage'}.intersection(set(sys.argv)):
+        usage(0)
+    if len(sys.argv) != 3:
+        usage(1)
+    with open(sys.argv[1], 'rb') as f:
+        buf = f.read()
+        csum = hashlib.sha256(buf).hexdigest()
+        with open(sys.argv[2], 'w') as f2:
+            f2.write('#ifndef FU_BUILD_HASH\n')
+            f2.write('#define FU_BUILD_HASH "%s"\n' % csum)
+            f2.write('#endif /* FU_BUILD_HASH */\n')
diff --git a/src/fu-main.c b/src/fu-main.c
index 98efb4135..0a2b9dae4 100644
--- a/src/fu-main.c
+++ b/src/fu-main.c
@@ -1115,6 +1115,9 @@ fu_main_daemon_get_property (GDBusConnection *connection_, const gchar *sender,
 	if (g_strcmp0 (property_name, "DaemonVersion") == 0)
 		return g_variant_new_string (VERSION);
 
+	if (g_strcmp0 (property_name, "Tainted") == 0)
+		return g_variant_new_boolean (fu_engine_get_tainted (priv->engine));
+
 	if (g_strcmp0 (property_name, "Status") == 0)
 		return g_variant_new_uint32 (fu_engine_get_status (priv->engine));
 
diff --git a/src/fu-plugin-private.h b/src/fu-plugin-private.h
index bb40c3ca4..cb25b4976 100644
--- a/src/fu-plugin-private.h
+++ b/src/fu-plugin-private.h
@@ -38,6 +38,7 @@ void		 fu_plugin_set_priority			(FuPlugin	*self,
 							 guint		 priority);
 void		 fu_plugin_set_name			(FuPlugin	*self,
 							 const gchar 	*name);
+const gchar	*fu_plugin_get_build_hash		(FuPlugin	*self);
 GPtrArray	*fu_plugin_get_rules			(FuPlugin	*self,
 							 FuPluginRule	 rule);
 gboolean	 fu_plugin_has_rule			(FuPlugin	*self,
diff --git a/src/fu-plugin-vfuncs.h b/src/fu-plugin-vfuncs.h
index 52dc368e0..63289355f 100644
--- a/src/fu-plugin-vfuncs.h
+++ b/src/fu-plugin-vfuncs.h
@@ -9,6 +9,7 @@
 
 #include "fu-plugin.h"
 #include "fu-device.h"
+#include "fu-hash.h"
 
 G_BEGIN_DECLS
 
diff --git a/src/fu-plugin.c b/src/fu-plugin.c
index 882918b83..b403d9538 100644
--- a/src/fu-plugin.c
+++ b/src/fu-plugin.c
@@ -44,6 +44,7 @@ typedef struct {
 	guint			 priority;
 	GPtrArray		*rules[FU_PLUGIN_RULE_LAST];
 	gchar			*name;
+	gchar			*build_hash;
 	FuHwids			*hwids;
 	FuQuirks		*quirks;
 	GHashTable		*runtime_versions;
@@ -132,6 +133,34 @@ fu_plugin_set_name (FuPlugin *self, const gchar *name)
 	priv->name = g_strdup (name);
 }
 
+/**
+ * fu_plugin_set_build_hash:
+ * @self: A #FuPlugin
+ * @build_hash: A checksum
+ *
+ * Sets the plugin build hash, typically a SHA256 checksum. All plugins must
+ * set the correct checksum to avoid the daemon being marked as tainted.
+ *
+ * Since: 1.2.4
+ **/
+void
+fu_plugin_set_build_hash (FuPlugin *self, const gchar *build_hash)
+{
+	FuPluginPrivate *priv = GET_PRIVATE (self);
+	g_return_if_fail (FU_IS_PLUGIN (self));
+	g_return_if_fail (build_hash != NULL);
+	g_free (priv->build_hash);
+	priv->build_hash = g_strdup (build_hash);
+}
+
+const gchar *
+fu_plugin_get_build_hash (FuPlugin *self)
+{
+	FuPluginPrivate *priv = GET_PRIVATE (self);
+	g_return_val_if_fail (FU_IS_PLUGIN (self), NULL);
+	return priv->build_hash;
+}
+
 /**
  * fu_plugin_cache_lookup:
  * @self: A #FuPlugin
diff --git a/src/fu-plugin.h b/src/fu-plugin.h
index a7655f9db..bc738951c 100644
--- a/src/fu-plugin.h
+++ b/src/fu-plugin.h
@@ -97,6 +97,8 @@ FuPluginData	*fu_plugin_alloc_data			(FuPlugin	*self,
 gboolean	 fu_plugin_get_enabled			(FuPlugin	*self);
 void		 fu_plugin_set_enabled			(FuPlugin	*self,
 							 gboolean	 enabled);
+void		 fu_plugin_set_build_hash		(FuPlugin	*self,
+							 const gchar	*build_hash);
 GUsbContext	*fu_plugin_get_usb_context		(FuPlugin	*self);
 void		 fu_plugin_device_add			(FuPlugin	*self,
 							 FuDevice	*device);
diff --git a/src/fu-self-test.c b/src/fu-self-test.c
index 090d46375..7a28a33e6 100644
--- a/src/fu-self-test.c
+++ b/src/fu-self-test.c
@@ -31,6 +31,7 @@
 #include "fu-plugin-private.h"
 #include "fu-plugin-list.h"
 #include "fu-progressbar.h"
+#include "fu-hash.h"
 #include "fu-hwids.h"
 #include "fu-smbios.h"
 #include "fu-test.h"
@@ -455,6 +456,7 @@ fu_engine_partial_hash_func (void)
 
 	/* set up dummy plugin */
 	fu_plugin_set_name (plugin, "test");
+	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_engine_add_plugin (engine, plugin);
 
 	/* add two dummy devices */
@@ -1800,6 +1802,34 @@ fu_plugin_quirks_device_func (void)
 	g_assert (fu_device_has_flag (device_tmp, FWUPD_DEVICE_FLAG_UPDATABLE));
 }
 
+static void
+fu_plugin_hash_func (void)
+{
+	GError *error = NULL;
+	g_autoptr(FuEngine) engine = fu_engine_new (FU_APP_FLAGS_NONE);
+	g_autoptr(FuPlugin) plugin = fu_plugin_new ();
+	gboolean ret = FALSE;
+
+	ret = fu_engine_load (engine, &error);
+	g_assert_no_error (error);
+	g_assert (ret);
+
+	/* make sure not tainted */
+	ret = fu_engine_get_tainted (engine);
+	g_assert_false (ret);
+
+	/* create a tainted plugin */
+	g_setenv ("FWUPD_PLUGIN_TEST", "build-hash", TRUE);
+	ret = fu_plugin_open (plugin, PLUGINBUILDDIR "/libfu_plugin_test.so", &error);
+	g_assert_no_error (error);
+
+	/* make sure it tainted now */
+	g_test_expect_message ("FuEngine", G_LOG_LEVEL_WARNING, "* has incorrect built version*");
+	fu_engine_add_plugin (engine, plugin);
+	ret = fu_engine_get_tainted (engine);
+	g_assert_true (ret);
+}
+
 static void
 fu_plugin_module_func (void)
 {
@@ -3301,6 +3331,7 @@ main (int argc, char **argv)
 	g_test_add_func ("/fwupd/plugin{composite}", fu_plugin_composite_func);
 	g_test_add_func ("/fwupd/keyring{gpg}", fu_keyring_gpg_func);
 	g_test_add_func ("/fwupd/keyring{pkcs7}", fu_keyring_pkcs7_func);
+	g_test_add_func ("/fwupd/plugin{build-hash}", fu_plugin_hash_func);
 	g_test_add_func ("/fwupd/chunk", fu_chunk_func);
 	g_test_add_func ("/fwupd/common{version-guess-format}", fu_common_version_guess_format_func);
 	g_test_add_func ("/fwupd/common{guid}", fu_common_guid_func);
diff --git a/src/fu-tool.c b/src/fu-tool.c
index dfd4193ae..c40e992db 100644
--- a/src/fu-tool.c
+++ b/src/fu-tool.c
@@ -120,7 +120,13 @@ fu_util_start_engine (FuUtilPrivate *priv, GError **error)
 		}
 	}
 
-	return fu_engine_load (priv->engine, error);
+	if (!fu_engine_load (priv->engine, error))
+		return FALSE;
+	if (fu_engine_get_tainted (priv->engine)) {
+		g_printerr ("WARNING: This tool has loaded 3rd party code and "
+			    "is no longer supported by the upstream developers!\n");
+	}
+	return TRUE;
 }
 
 static gint
diff --git a/src/fu-util.c b/src/fu-util.c
index 339418808..cbe189814 100644
--- a/src/fu-util.c
+++ b/src/fu-util.c
@@ -2609,6 +2609,17 @@ main (int argc, char *argv[])
 		return EXIT_SUCCESS;
 	}
 
+	/* show a warning if the daemon is tainted */
+	if (!fwupd_client_connect (priv->client, priv->cancellable, &error)) {
+		g_printerr ("Failed to connect to daemon: %s\n",
+			    error->message);
+		return EXIT_FAILURE;
+	}
+	if (fwupd_client_get_tainted (priv->client)) {
+		g_printerr ("WARNING: The daemon has loaded 3rd party code and "
+			    "is no longer supported by the upstream developers!\n");
+	}
+
 	/* run the specified command */
 	ret = fu_util_run (priv, argv[1], (gchar**) &argv[2], &error);
 	if (!ret) {
diff --git a/src/meson.build b/src/meson.build
index 99e16a4d0..286d4fc14 100644
--- a/src/meson.build
+++ b/src/meson.build
@@ -107,9 +107,19 @@ resources_src = gnome.compile_resources(
   c_name : 'fu'
 )
 
+fu_hash = custom_target(
+  'fu-hash.h',
+  input : libfwupdprivate,
+  output : 'fu-hash.h',
+  command : [python3.path(),
+             join_paths(meson.current_source_dir(), 'fu-hash.py'),
+             '@INPUT@', '@OUTPUT@']
+)
+
 fwupdtool = executable(
   'fwupdtool',
   resources_src,
+  fu_hash,
   sources : [
     'fu-tool.c',
     keyring_src,
@@ -251,6 +261,7 @@ executable(
   install : true,
   install_dir : join_paths(libexecdir, 'fwupd')
 )
+
 endif
 
 if get_option('tests')
@@ -265,6 +276,7 @@ if get_option('tests')
     hwid_test_firmware,
     noreqs_test_firmware,
     test_deps,
+    fu_hash,
     sources : [
       keyring_src,
       'fu-self-test.c',
diff --git a/src/org.freedesktop.fwupd.xml b/src/org.freedesktop.fwupd.xml
index 7d06d97f3..23ff36a5c 100644
--- a/src/org.freedesktop.fwupd.xml
+++ b/src/org.freedesktop.fwupd.xml
@@ -22,6 +22,17 @@
       </doc:doc>
     </property>
 
+    <!--***********************************************************-->
+    <property name='Tainted' type='b' access='read'>
+      <doc:doc>
+        <doc:description>
+          <doc:para>
+            If the daemon has been tainted with a 3rd party plugin.
+          </doc:para>
+        </doc:description>
+      </doc:doc>
+    </property>
+
     <!--***********************************************************-->
     <property name='Status' type='u' access='read'>
       <doc:doc>
