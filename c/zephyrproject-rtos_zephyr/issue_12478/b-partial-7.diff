diff --git a/drivers/console/ipm_console_receiver.c b/drivers/console/ipm_console_receiver.c
index e2056b7a127c..38765bd7a163 100644
--- a/drivers/console/ipm_console_receiver.c
+++ b/drivers/console/ipm_console_receiver.c
@@ -20,7 +20,8 @@ static void ipm_console_thread(void *arg1, void *arg2, void *arg3)
 {
 	u8_t size32;
 	u16_t type;
-	int ret, key;
+	int ret;
+	k_spinlock_key_t key;
 	struct device *d;
 	const struct ipm_console_receiver_config_info *config_info;
 	struct ipm_console_receiver_runtime_data *driver_data;
@@ -36,9 +37,11 @@ static void ipm_console_thread(void *arg1, void *arg2, void *arg3)
 	while (1) {
 		k_sem_take(&driver_data->sem, K_FOREVER);
 
+		key = k_spin_lock(&driver_data->rb_spinlock);
 		ret = ring_buf_item_get(&driver_data->rb, &type,
 					(u8_t *)&config_info->line_buf[pos],
 					NULL, &size32);
+		k_spin_unlock(&driver_data->rb_spinlock, key);
 		if (ret) {
 			/* Shouldn't ever happen... */
 			printk("ipm console ring buffer error: %d\n", ret);
@@ -66,21 +69,6 @@ static void ipm_console_thread(void *arg1, void *arg2, void *arg3)
 			++pos;
 		}
 
-		/* ISR may have disabled the channel due to full buffer at
-		 * some point. If that happened and there is now room,
-		 * re-enable it.
-		 *
-		 * Lock interrupts to avoid pathological scenario where
-		 * the buffer fills up in between enabling the channel and
-		 * clearing the channel_disabled flag.
-		 */
-		if (driver_data->channel_disabled &&
-		    ring_buf_space_get(&driver_data->rb)) {
-			key = irq_lock();
-			ipm_set_enabled(driver_data->ipm_device, 1);
-			driver_data->channel_disabled = 0;
-			irq_unlock(key);
-		}
 	}
 }
 
@@ -90,27 +78,20 @@ static void ipm_console_receive_callback(void *context, u32_t id,
 	struct device *d;
 	struct ipm_console_receiver_runtime_data *driver_data;
 	int ret;
+	k_spinlock_key_t key;
 
 	ARG_UNUSED(data);
 	d = context;
 	driver_data = d->driver_data;
 
+	key = k_spin_lock(&driver_data->rb_spinlock);
 	/* Should always be at least one free buffer slot */
 	ret = ring_buf_item_put(&driver_data->rb, 0, id, NULL, 0);
+	k_spin_unlock(&driver_data->rb_spinlock, key);
 	__ASSERT(ret == 0, "Failed to insert data into ring buffer");
 	k_sem_give(&driver_data->sem);
 
-	/* If the buffer is now full, disable future interrupts for this channel
-	 * until the thread has a chance to consume characters.
-	 *
-	 * This works without losing data if the sending side tries to send
-	 * more characters because the sending side is making an ipm_send()
-	 * call with the wait flag enabled.  It blocks until the receiver side
-	 * re-enables the channel and consumes the data.
-	 */
-	if (ring_buf_space_get(&driver_data->rb) == 0) {
-		ipm_set_enabled(driver_data->ipm_device, 0);
-		driver_data->channel_disabled = 1;
+	while (ring_buf_space_get(&driver_data->rb) == 0) {
 	}
 }
 
diff --git a/include/drivers/console/ipm_console.h b/include/drivers/console/ipm_console.h
index 11782cb34c44..7f53d18996fd 100644
--- a/include/drivers/console/ipm_console.h
+++ b/include/drivers/console/ipm_console.h
@@ -12,6 +12,7 @@
 #include <kernel.h>
 #include <device.h>
 #include <ring_buffer.h>
+#include <spinlock.h>
 
 #ifdef __cplusplus
 extern "C" {
@@ -80,6 +81,11 @@ struct ipm_console_receiver_runtime_data {
 
 	/** Receiver worker thread */
 	struct k_thread rx_thread;
+
+	/**
+	 * Ring buffer spinlock
+	 */
+	struct k_spinlock rb_spinlock;
 };
 
 struct ipm_console_sender_config_info {
diff --git a/include/ring_buffer.h b/include/ring_buffer.h
index 351a1922d73c..417ecae1ed36 100644
--- a/include/ring_buffer.h
+++ b/include/ring_buffer.h
@@ -25,8 +25,8 @@ extern "C" {
  * @brief A structure to represent a ring buffer
  */
 struct ring_buf {
-	u32_t head;	 /**< Index in buf for the head element */
-	u32_t tail;	 /**< Index in buf for the tail element */
+	volatile u32_t head;	 /**< Index in buf for the head element */
+	volatile u32_t tail;	 /**< Index in buf for the tail element */
 	union ring_buf_misc {
 		struct ring_buf_misc_item_mode {
 			u32_t dropped_put_count; /**< Running tally of the
@@ -39,7 +39,7 @@ struct ring_buf {
 			u32_t tmp_head;
 		} byte_mode;
 	} misc;
-	u32_t size;   /**< Size of buf in 32-bit chunks */
+	volatile u32_t size;   /**< Size of buf in 32-bit chunks */
 
 	union ring_buf_buffer {
 		u32_t *buf32;	 /**< Memory region for stored entries */
diff --git a/include/spinlock.h b/include/spinlock.h
index 5161443eca36..d583db6a4093 100644
--- a/include/spinlock.h
+++ b/include/spinlock.h
@@ -32,6 +32,7 @@ static inline void z_arch_irq_unlock(int key)
  * fallback.  There is a DT_FLASH_SIZE parameter too, but that seems
  * even more poorly supported.
  */
+#ifndef CONFIG_SMP
 #if (CONFIG_FLASH_SIZE == 0) || (CONFIG_FLASH_SIZE > 32)
 #if defined(CONFIG_ASSERT) && (CONFIG_MP_NUM_CPUS < 4)
 #include <misc/__assert.h>
@@ -42,6 +43,7 @@ void z_spin_lock_set_owner(struct k_spinlock *l);
 #define SPIN_VALIDATE
 #endif
 #endif
+#endif
 
 struct k_spinlock_key {
 	int key;
diff --git a/tests/drivers/ipm/src/ipm_dummy.c b/tests/drivers/ipm/src/ipm_dummy.c
index 256295468adf..5812d90b2cf4 100644
--- a/tests/drivers/ipm/src/ipm_dummy.c
+++ b/tests/drivers/ipm/src/ipm_dummy.c
@@ -69,13 +69,8 @@ static int ipm_dummy_send(struct device *d, int wait, u32_t id,
 	driver_data->regs.id = id;
 	driver_data->regs.busy = 1U;
 
-	irq_offload(ipm_dummy_isr, d);
+	ipm_dummy_isr(d);
 
-	if (wait) {
-		while (driver_data->regs.busy) {
-			/* busy-wait */
-		}
-	}
 	return 0;
 }
 
diff --git a/tests/drivers/ipm/src/main.c b/tests/drivers/ipm/src/main.c
index 8fcfd0dc4c2d..bbf0987ad235 100644
--- a/tests/drivers/ipm/src/main.c
+++ b/tests/drivers/ipm/src/main.c
@@ -10,6 +10,7 @@
 #include <device.h>
 #include <init.h>
 #include <stdio.h>
+#include <kernel.h>
 
 #include <tc_util.h>
 #include "ipm_dummy.h"
@@ -25,6 +26,7 @@
 #endif
 
 #define INIT_PRIO_IPM_SEND 50
+#define DELAY_MS (1000 * 1000)
 
 extern struct ipm_driver_api ipm_dummy_api;
 
@@ -102,6 +104,9 @@ void main(void)
 	 * automation purposes?
 	 */
 
+#ifdef CONFIG_SMP
+	k_sleep(DELAY_MS);
+#endif
 	rv = TC_PASS;
 	TC_END_RESULT(rv);
 	TC_END_REPORT(rv);
diff --git a/tests/drivers/ipm/testcase.yaml b/tests/drivers/ipm/testcase.yaml
index 469feaf29cff..0095e7c8ea42 100644
--- a/tests/drivers/ipm/testcase.yaml
+++ b/tests/drivers/ipm/testcase.yaml
@@ -2,5 +2,4 @@ tests:
   peripheral.mailbox:
     filter: not CONFIG_SOC_QUARK_SE_C1000_SS
     arch_exclude: posix xtensa
-    platform_exclude: qemu_x86_64 # see issue #12478
     tags: drivers ipc
