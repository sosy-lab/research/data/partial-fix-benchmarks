diff --git a/libr/anal/Makefile b/libr/anal/Makefile
index ad199025984..76ecb6da0d9 100644
--- a/libr/anal/Makefile
+++ b/libr/anal/Makefile
@@ -29,7 +29,7 @@ include ${STATIC_ANAL_PLUGINS}
 
 STATIC_OBJS=$(addprefix $(LTOP)/anal/p/,$(STATIC_OBJ))
 OBJLIBS=meta.o reflines.o op.o fcn.o bb.o var.o
-OBJLIBS+=cond.o value.o cc.o class.o diff.o
+OBJLIBS+=cond.o value.o cc.o class.o diff.o type.o
 OBJLIBS+=hint.o anal.o data.o xrefs.o esil.o sign.o
 OBJLIBS+=anal_ex.o switch.o state.o cycles.o
 OBJLIBS+=esil_sources.o esil_interrupt.o
diff --git a/libr/anal/fcn.c b/libr/anal/fcn.c
index aa7e50fa74b..aae74ab0401 100644
--- a/libr/anal/fcn.c
+++ b/libr/anal/fcn.c
@@ -1,6 +1,7 @@
 /* radare - LGPL - Copyright 2010-2019 - nibble, alvaro, pancake */
 
 #include <r_anal.h>
+#include <r_parse.h>
 #include <r_util.h>
 #include <r_list.h>
 
@@ -2191,29 +2192,19 @@ R_API char *r_anal_fcn_to_string(RAnal *a, RAnalFunction *fs) {
 	return NULL;
 }
 
-// TODO: This function is not fully implemented
 /* set function signature from string */
 R_API int r_anal_str_to_fcn(RAnal *a, RAnalFunction *f, const char *sig) {
-	int length = 0;
-	if (!a || !f || !sig) {
-		eprintf ("r_anal_str_to_fcn: No function received\n");
-		return false;
+	r_return_val_if_fail (!a || !f || !sig, false);
+	char *error_msg = NULL;
+	const char *out = r_parse_c_string (a, sig, &error_msg);
+	if (out) {
+		r_anal_save_parsed_type (a, out);
 	}
-	length = strlen (sig) + 10;
-	/* Add 'function' keyword */
-	char *str = calloc (1, length);
-	if (!str) {
-		eprintf ("Cannot allocate %d byte(s)\n", length);
-		return false;
+	if (error_msg) {
+		fprintf (stderr, "%s", error_msg);
+		free (error_msg);
 	}
-	strcpy (str, "function ");
-	strcat (str, sig);
-
-	/* TODO: improve arguments parsing */
-	/* TODO: implement parser */
-	/* TODO: simplify this complex api usage */
 
-	free (str);
 	return true;
 }
 
diff --git a/libr/anal/meson.build b/libr/anal/meson.build
index 4857a9bdf47..9c0d093113b 100644
--- a/libr/anal/meson.build
+++ b/libr/anal/meson.build
@@ -29,6 +29,7 @@ r_anal_sources = [
   'sign.c',
   'state.c',
   'switch.c',
+  'type.c',
   'value.c',
   'var.c',
   'vtable.c',
diff --git a/libr/anal/type.c b/libr/anal/type.c
new file mode 100644
index 00000000000..ded661b5af2
--- /dev/null
+++ b/libr/anal/type.c
@@ -0,0 +1,107 @@
+/* radare - LGPL - Copyright 2019 - pancake, oddcoder, Anton Kochkov */
+
+#include <string.h>
+#include <r_anal.h>
+#include "sdb/sdb.h"
+
+/*!
+ * \brief Save the size of the given datatype in sdb
+ * \param sdb_types pointer to the sdb for types
+ * \param name the datatype whose size if to be stored
+ */
+static void save_type_size(Sdb *sdb_types, char *name) {
+	char *type = NULL;
+	r_return_if_fail (sdb_types && name);
+	if (!sdb_exists (sdb_types, name) || !(type = sdb_get (sdb_types, name, 0))) {
+		return;
+	}
+	char *type_name_size = r_str_newf ("%s.%s.%s", type, name, "size");
+	r_return_if_fail (!type_name_size);
+	int size = r_type_get_bitsize (sdb_types, name);
+	sdb_set (sdb_types, type_name_size, sdb_fmt ("%d", size), 0);
+	free (type);
+	free (type_name_size);
+}
+
+/*!
+ * \brief Save the sizes of the datatypes which have been parsed
+ * \param core pointer to radare2 core
+ * \param parsed the parsed c string in sdb format
+ */
+static void save_parsed_type_size(RAnal *anal, const char *parsed) {
+	r_return_if_fail (anal && parsed);
+	char *str = strdup (parsed);
+	if (str) {
+		char *ptr = NULL;
+		int offset = 0;
+		while ((ptr = strstr (str + offset, "=struct\n")) ||
+				(ptr = strstr (str + offset, "=union\n"))) {
+			*ptr = 0;
+			if (str + offset == ptr) {
+				break;
+			}
+			char *name = ptr - 1;
+			while (name > str && *name != '\n') {
+				name--;
+			}
+			if (*name == '\n') {
+				name++;
+			}
+			save_type_size (anal->sdb_types, name);
+			*ptr = '=';
+			offset = ptr + 1 - str;
+		}
+		free (str);
+	}
+}
+
+R_API void r_anal_remove_parsed_type(RAnal *anal, const char *name) {
+	r_return_if_fail (anal && name);
+	Sdb *TDB = anal->sdb_types;
+	SdbKv *kv;
+	SdbListIter *iter;
+	const char *type = sdb_const_get (TDB, name, 0);
+	r_return_if_fail (!type);
+	int tmp_len = strlen (name) + strlen (type);
+	char *tmp = malloc (tmp_len + 1);
+	r_type_del (TDB, name);
+	if (tmp) {
+		snprintf (tmp, tmp_len + 1, "%s.%s.", type, name);
+		SdbList *l = sdb_foreach_list (TDB, true);
+		ls_foreach (l, iter, kv) {
+			if (!strncmp (sdbkv_key (kv), tmp, tmp_len)) {
+				r_type_del (TDB, sdbkv_key (kv));
+			}
+		}
+		ls_free (l);
+		free (tmp);
+	}
+}
+
+R_API void r_anal_save_parsed_type(RAnal *anal, const char *parsed) {
+	r_return_if_fail (anal && parsed);
+	// First, if this exists, let's remove it.
+	char *type = strdup (parsed);
+	if (type) {
+		char *name = NULL;
+		if ((name = strstr (type, "=type")) ||
+			(name = strstr (type, "=struct")) ||
+			(name = strstr (type, "=union")) ||
+			(name = strstr (type, "=enum")) ||
+			(name = strstr (type, "=typedef")) ||
+			(name = strstr (type, "=func"))) {
+			*name = 0;
+			while (name - 1 >= type && *(name - 1) != '\n') {
+				name--;
+			}
+		}
+		if (name) {
+			r_anal_remove_parsed_type (anal, name);
+			// Now add the type to sdb.
+			sdb_query_lines (anal->sdb_types, parsed);
+			save_parsed_type_size (anal, parsed);
+		}
+		free (type);
+	}
+}
+
diff --git a/libr/core/cmd_print.c b/libr/core/cmd_print.c
index 23f313759cb..48c661268cf 100644
--- a/libr/core/cmd_print.c
+++ b/libr/core/cmd_print.c
@@ -1207,7 +1207,7 @@ static void cmd_print_format(RCore *core, const char *_input, const ut8* block,
 				char *error_msg = NULL;
 				char *out = r_parse_c_file (core->anal, path, &error_msg);
 				if (out) {
-					r_core_save_parsed_type (core, out);
+					r_anal_save_parsed_type (core->anal, out);
 					r_core_cmd0 (core, ".ts*");
 					free (out);
 				} else {
diff --git a/libr/core/cmd_type.c b/libr/core/cmd_type.c
index b571b4e008e..cb395d1b1a4 100644
--- a/libr/core/cmd_type.c
+++ b/libr/core/cmd_type.c
@@ -349,81 +349,6 @@ static void cmd_type_noreturn(RCore *core, const char *input) {
 	}
 }
 
-/*!
- * \brief Save the size of the given datatype in sdb
- * \param sdb_types pointer to the sdb for types
- * \param name the datatype whose size if to be stored
- */
-static void save_type_size(Sdb *sdb_types, char *name) {
-	char *type = NULL;
-	r_return_if_fail (sdb_types && name);
-	if (!sdb_exists (sdb_types, name) || !(type = sdb_get (sdb_types, name, 0))) {
-		return;
-	}
-	char *type_name_size = r_str_newf ("%s.%s.%s", type, name, "size");
-	if (!type_name_size) {
-		return;
-	}
-	int size = r_type_get_bitsize (sdb_types, name);
-	sdb_set (sdb_types, type_name_size, sdb_fmt ("%d", size), 0);
-	free (type);
-	free (type_name_size);
-}
-
-/*!
- * \brief Save the sizes of the datatypes which have been parsed
- * \param core pointer to radare2 core
- * \param parsed the parsed c string in sdb format
- */
-static void save_parsed_type_size(RCore *core, const char *parsed) {
-	r_return_if_fail (core && core->anal && parsed);
-	char *str = strdup (parsed);
-	if (str) {
-		char *ptr = NULL;
-		int offset = 0;
-		while ((ptr = strstr (str + offset, "=struct\n")) || (ptr = strstr (str + offset, "=union\n"))) {
-			*ptr = 0;
-			if (str + offset == ptr) {
-				break;
-			}
-			char *name = ptr - 1;
-			while (name > str && *name != '\n') {
-				name--;
-			}
-			if (*name == '\n') {
-				name++;
-			}
-			save_type_size (core->anal->sdb_types, name);
-			*ptr = '=';
-			offset = ptr + 1 - str;
-		}
-		free (str);
-	}
-}
-
-R_API void r_core_save_parsed_type(RCore *core, const char *parsed) {
-	r_return_if_fail (core && core->anal && parsed);
-	// First, if this exists, let's remove it.
-	char *type = strdup (parsed);
-	if (type) {
-		char *name = NULL;
-		if ((name = strstr (type, "=type")) || (name = strstr (type, "=struct")) || (name = strstr (type, "=union")) ||
-			(name = strstr (type, "=enum")) || (name = strstr (type, "=typedef")) || (name = strstr (type, "=func"))) {
-			*name = 0;
-			while (name - 1 >= type && *(name - 1) != '\n') {
-				name--;
-			}
-		}
-		if (name) {
-			r_core_cmdf (core, "\"t- %s\"", name);
-			// Now add the type to sdb.
-			sdb_query_lines (core->anal->sdb_types, parsed);
-			save_parsed_type_size (core, parsed);
-		}
-		free (type);
-	}
-}
-
 static Sdb *TDB_ = NULL; // HACK
 
 static int stdifstruct(void *user, const char *k, const char *v) {
@@ -1249,7 +1174,7 @@ static int cmd_type(void *data, const char *input) {
 						char *out = r_parse_c_string (core->anal, tmp, &error_msg);
 						if (out) {
 							//		r_cons_strcat (out);
-							r_core_save_parsed_type (core, out);
+							r_anal_save_parsed_type (core->anal, out);
 							free (out);
 						}
 						if (error_msg) {
@@ -1263,7 +1188,7 @@ static int cmd_type(void *data, const char *input) {
 					char *out = r_parse_c_file (core->anal, filename, &error_msg);
 					if (out) {
 						//r_cons_strcat (out);
-						r_core_save_parsed_type (core, out);
+						r_anal_save_parsed_type (core->anal, out);
 						free (out);
 					}
 					if (error_msg) {
@@ -1300,7 +1225,7 @@ static int cmd_type(void *data, const char *input) {
 			char *error_msg = NULL;
 			char *out = r_parse_c_string (core->anal, tmp, &error_msg);
 			if (out) {
-				r_core_save_parsed_type (core, out);
+				r_anal_save_parsed_type (core->anal, out);
 				free (out);
 			}
 			if (error_msg) {
@@ -1585,25 +1510,7 @@ static int cmd_type(void *data, const char *input) {
 			const char *name = input + 1;
 			while (IS_WHITESPACE (*name)) name++;
 			if (*name) {
-				SdbKv *kv;
-				SdbListIter *iter;
-				const char *type = sdb_const_get (TDB, name, 0);
-				if (!type)
-					break;
-				int tmp_len = strlen (name) + strlen (type);
-				char *tmp = malloc (tmp_len + 1);
-				r_type_del (TDB, name);
-				if (tmp) {
-					snprintf (tmp, tmp_len + 1, "%s.%s.", type, name);
-					SdbList *l = sdb_foreach_list (TDB, true);
-					ls_foreach (l, iter, kv) {
-						if (!strncmp (sdbkv_key (kv), tmp, tmp_len)) {
-							r_type_del (TDB, sdbkv_key (kv));
-						}
-					}
-					ls_free (l);
-					free (tmp);
-				}
+				r_anal_remove_parsed_type (core->anal, name);
 			} else {
 				eprintf ("Invalid use of t- . See t-? for help.\n");
 			}
diff --git a/libr/include/r_anal.h b/libr/include/r_anal.h
index 4e8b1d20f53..6332045a25b 100644
--- a/libr/include/r_anal.h
+++ b/libr/include/r_anal.h
@@ -1521,6 +1521,10 @@ R_API RList* r_anal_fcn_get_vars (RAnalFunction *anal);
 R_API RList* r_anal_fcn_get_bbs (RAnalFunction *anal);
 R_API RList* r_anal_get_fcns (RAnal *anal);
 
+/* type.c */
+R_API void r_anal_remove_parsed_type(RAnal *anal, const char *name);
+R_API void r_anal_save_parsed_type(RAnal *anal, const char *parsed);
+
 /* var.c */
 R_API void r_anal_var_access_clear (RAnal *a, ut64 var_addr, int scope, int index);
 R_API int r_anal_var_access (RAnal *a, ut64 var_addr, char kind, int scope, int index, int xs_type, ut64 xs_addr);
