diff --git a/src/postgres/src/backend/catalog/ybctype.c b/src/postgres/src/backend/catalog/ybctype.c
index 15a5f3e438f..a8ea49f42fa 100644
--- a/src/postgres/src/backend/catalog/ybctype.c
+++ b/src/postgres/src/backend/catalog/ybctype.c
@@ -197,6 +197,19 @@ Datum YBCBinaryToDatum(const void *data, int64 bytes, const YBCPgTypeAttrs *type
 	return PointerGetDatum(cstring_to_text_with_len(data, bytes));
 }
 
+/*
+ * TEXT type conversion.
+ */
+void YBCDatumToText(Datum datum, char **data, int64 *bytes) {
+	*data = VARDATA_ANY(datum);
+	*bytes = VARSIZE_ANY_EXHDR(datum);
+}
+
+Datum YBCTextToDatum(const char *data, int64 bytes, const YBCPgTypeAttrs *type_attrs) {
+	/* While reading back TEXT from storage, we don't need to check for data length. */
+	return PointerGetDatum(cstring_to_text_with_len(data, bytes));
+}
+
 /*
  * CHAR type conversion.
  */
@@ -572,12 +585,9 @@ static const YBCPgTypeEntity YBCTypeEntityTable[] = {
 		(YBCPgDatumToData)YBCDatumToOid,
 		(YBCPgDatumFromData)YBCOidToDatum },
 
-	/*
-	 * TODO(neil) We need to change TEXT to char-based datatype to be the same with PostgreSQL.
-	 */
-	{ TEXTOID, YB_YQL_DATA_TYPE_BINARY, true, -1,
-		(YBCPgDatumToData)YBCDatumToBinary,
-		(YBCPgDatumFromData)YBCBinaryToDatum },
+	{ TEXTOID, YB_YQL_DATA_TYPE_STRING, true, -1,
+		(YBCPgDatumToData)YBCDatumToText,
+		(YBCPgDatumFromData)YBCTextToDatum },
 
 	{ OIDOID, YB_YQL_DATA_TYPE_UINT32, true, sizeof(Oid),
 		(YBCPgDatumToData)YBCDatumToOid,
diff --git a/src/yb/yql/pggate/pg_dml.cc b/src/yb/yql/pggate/pg_dml.cc
index bd299d4cb99..bbd706f8879 100644
--- a/src/yb/yql/pggate/pg_dml.cc
+++ b/src/yb/yql/pggate/pg_dml.cc
@@ -123,12 +123,8 @@ Status PgDml::BindColumn(int attr_num, PgExpr *attr_value) {
   RETURN_NOT_OK(FindColumn(attr_num, &col));
 
   // Check datatype.
-  // TODO(neil) Current code combine TEXT and BINARY datatypes into ONE representation.  Once that
-  // is fixed, we can remove the special if() check for BINARY type.
-  if (col->internal_type() != InternalType::kBinaryValue) {
-    SCHECK_EQ(col->internal_type(), attr_value->internal_type(), Corruption,
-              "Attribute value type does not match column type");
-  }
+  SCHECK_EQ(col->internal_type(), attr_value->internal_type(), Corruption,
+            "Attribute value type does not match column type");
 
   // Alloc the protobuf.
   PgsqlExpressionPB *bind_pb = col->bind_pb();
@@ -182,12 +178,8 @@ Status PgDml::AssignColumn(int attr_num, PgExpr *attr_value) {
   RETURN_NOT_OK(FindColumn(attr_num, &col));
 
   // Check datatype.
-  // TODO(neil) Current code combine TEXT and BINARY datatypes into ONE representation.  Once that
-  // is fixed, we can remove the special if() check for BINARY type.
-  if (col->internal_type() != InternalType::kBinaryValue) {
-    SCHECK_EQ(col->internal_type(), attr_value->internal_type(), Corruption,
-              "Attribute value type does not match column type");
-  }
+  SCHECK_EQ(col->internal_type(), attr_value->internal_type(), Corruption,
+            "Attribute value type does not match column type");
 
   // Alloc the protobuf.
   PgsqlExpressionPB *assign_pb = col->assign_pb();
diff --git a/src/yb/yql/pggate/pg_expr.cc b/src/yb/yql/pggate/pg_expr.cc
index 9f70eae8194..ddd0820c2c5 100644
--- a/src/yb/yql/pggate/pg_expr.cc
+++ b/src/yb/yql/pggate/pg_expr.cc
@@ -117,12 +117,28 @@ void PgExpr::TranslateText(Slice *yb_cursor, const PgWireDataHeader& header, int
     return pg_tuple->WriteNull(index, header);
   }
 
-  int64_t text_size;
-  size_t read_size = PgDocData::ReadNumber(yb_cursor, &text_size);
+  // Get data from RPC buffer.
+  int64_t data_size;
+  size_t read_size = PgDocData::ReadNumber(yb_cursor, &data_size);
   yb_cursor->remove_prefix(read_size);
 
-  pg_tuple->WriteDatum(index, type_entity->yb_to_datum(yb_cursor->cdata(), text_size, type_attrs));
-  yb_cursor->remove_prefix(text_size);
+  // Expects data from DocDB matches the following format.
+  // - Right trim spaces for CHAR type. This should be done by DocDB when evaluate SELECTed or
+  //   RETURNed expression. Note that currently, Postgres layer (and not DocDB) evaluate
+  //   expressions, so DocDB doesn't trim for CHAR type.
+  // - NULL terminated string. This should be done by DocDB when serializing.
+  // - Text size == strlen(). When sending data over the network, RPC layer would use the actual
+  //   size of data being serialized including the '\0' character. This is not necessarily be the
+  //   length of a string.
+  // Find strlen() of STRING by right-trimming all '\0' characters.
+  const char* text = yb_cursor->cdata();
+  int64_t text_len = data_size - 1;
+
+  DCHECK(text_len >= 0 && text[text_len] == '\0' && (text_len == 0 || text[text_len - 1] != '\0'))
+    << "Data received from DocDB does not have expected format";
+
+  pg_tuple->WriteDatum(index, type_entity->yb_to_datum(text, text_len, type_attrs));
+  yb_cursor->remove_prefix(data_size);
 }
 
 void PgExpr::TranslateBinary(Slice *yb_cursor, const PgWireDataHeader& header, int index,
diff --git a/src/yb/yql/pggate/pg_select.cc b/src/yb/yql/pggate/pg_select.cc
index d866bd9c4f8..53321b2a433 100644
--- a/src/yb/yql/pggate/pg_select.cc
+++ b/src/yb/yql/pggate/pg_select.cc
@@ -191,12 +191,8 @@ Status PgSelect::BindIndexColumn(int attr_num, PgExpr *attr_value) {
   RETURN_NOT_OK(FindIndexColumn(attr_num, &col));
 
   // Check datatype.
-  // TODO(neil) Current code combine TEXT and BINARY datatypes into ONE representation.  Once that
-  // is fixed, we can remove the special if() check for BINARY type.
-  if (col->internal_type() != InternalType::kBinaryValue) {
-    SCHECK_EQ(col->internal_type(), attr_value->internal_type(), Corruption,
-              "Attribute value type does not match column type");
-  }
+  SCHECK_EQ(col->internal_type(), attr_value->internal_type(), Corruption,
+            "Attribute value type does not match column type");
 
   // Alloc the protobuf.
   PgsqlExpressionPB *bind_pb = col->bind_pb();
@@ -252,18 +248,14 @@ Status PgSelect::BindIntervalColumn(int attr_num, PgExpr *attr_value, PgExpr *at
   RETURN_NOT_OK(FindColumn(attr_num, &col));
 
   // Check datatype.
-  // TODO(neil) Current code combine TEXT and BINARY datatypes into ONE representation.  Once that
-  // is fixed, we can remove the special if() check for BINARY type.
-  if (col->internal_type() != InternalType::kBinaryValue) {
-    if (attr_value) {
-      SCHECK_EQ(col->internal_type(), attr_value->internal_type(), Corruption,
-          "Attribute value type does not match column type");
-    }
+  if (attr_value) {
+    SCHECK_EQ(col->internal_type(), attr_value->internal_type(), Corruption,
+              "Attribute value type does not match column type");
+  }
 
-    if (attr_value_end) {
-      SCHECK_EQ(col->internal_type(), attr_value_end->internal_type(), Corruption,
-          "Attribute value type does not match column type");
-    }
+  if (attr_value_end) {
+    SCHECK_EQ(col->internal_type(), attr_value_end->internal_type(), Corruption,
+              "Attribute value type does not match column type");
   }
 
   // Alloc the protobuf.
