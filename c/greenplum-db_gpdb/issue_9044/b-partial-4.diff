diff --git a/src/backend/utils/misc/guc.c b/src/backend/utils/misc/guc.c
index f1225336a1c..8aedf3cf62b 100644
--- a/src/backend/utils/misc/guc.c
+++ b/src/backend/utils/misc/guc.c
@@ -7735,44 +7735,69 @@ DispatchSetPGVariable(const char *name, List *args, bool is_local)
 
 		appendStringInfo(&buffer, "%s TO ", quote_identifier(name));
 
-		foreach(l, args)
+		/*
+		 * GPDB: We handle the timezone GUC specially. This is because the
+		 * timezone GUC can be set with the SET TIME ZONE .. syntax which is an
+		 * alias for SET timezone. Instead of dispatching the SET TIME ZONE ..
+		 * as a special case, we dispatch the already set time zone from the QD
+		 * with the usual SET syntax flavor (SET timezone TO <>).
+		 * Please refer to Issue: #9055 for additional detail.
+		 */
+		if (strcmp(name, "timezone") == 0)
+			appendStringInfo(&buffer, "%s",
+							 quote_literal_cstr(GetConfigOptionByName("timezone",
+																	  NULL,
+																	  false)));
+		else
 		{
-			A_Const    *arg = (A_Const *) lfirst(l);
-			char	   *val;
+			foreach(l, args)
+			{
+				Node	   *arg = (Node *) lfirst(l);
+				char	   *val;
+				A_Const	   *con;
 
-			if (l != list_head(args))
-				appendStringInfo(&buffer, ", ");
+				if (l != list_head(args))
+					appendStringInfo(&buffer, ", ");
 
-			if (!IsA(arg, A_Const))
-				elog(ERROR, "unrecognized node type: %d", (int) nodeTag(arg));
+				if (IsA(arg, TypeCast))
+				{
+					TypeCast   *tc = (TypeCast *) arg;
+					arg = tc->arg;
+				}
 
-			switch (nodeTag(&arg->val))
-			{
-				case T_Integer:
-					appendStringInfo(&buffer, "%ld", intVal(&arg->val));
-					break;
-				case T_Float:
-					/* represented as a string, so just copy it */
-					appendStringInfoString(&buffer, strVal(&arg->val));
-					break;
-				case T_String:
-					val = strVal(&arg->val);
+				con = (A_Const *) arg;
 
-					/*
-					 * Plain string literal or identifier. Quote it.
-					 */
+				if (!IsA(con, A_Const))
+					elog(ERROR, "unrecognized node type: %d", (int) nodeTag(arg));
 
-					if (val[0] != '\'')
-						appendStringInfo(&buffer, "%s", quote_literal_cstr(val));
-					else
-						appendStringInfo(&buffer, "%s",val);
+				switch (nodeTag(&con->val))
+				{
+					case T_Integer:
+						appendStringInfo(&buffer, "%ld", intVal(&con->val));
+						break;
+					case T_Float:
+						/* represented as a string, so just copy it */
+						appendStringInfoString(&buffer, strVal(&con->val));
+						break;
+					case T_String:
+						val = strVal(&con->val);
 
+						/*
+						 * Plain string literal or identifier. Quote it.
+						 */
 
-					break;
-				default:
-					elog(ERROR, "unrecognized node type: %d",
-						 (int) nodeTag(&arg->val));
-					break;
+						if (val[0] != '\'')
+							appendStringInfo(&buffer, "%s", quote_literal_cstr(val));
+						else
+							appendStringInfo(&buffer, "%s",val);
+
+
+						break;
+					default:
+						elog(ERROR, "unrecognized node type: %d",
+							 (int) nodeTag(&con->val));
+						break;
+				}
 			}
 		}
 	}
diff --git a/src/test/regress/expected/guc_gp.out b/src/test/regress/expected/guc_gp.out
index fbefde87d37..7f395e42856 100644
--- a/src/test/regress/expected/guc_gp.out
+++ b/src/test/regress/expected/guc_gp.out
@@ -108,43 +108,71 @@ WARNING:  "work_mem": setting is deprecated, and may be removed in a future rele
 -- Test if RESET timezone is dispatched to all slices
 --
 CREATE TABLE timezone_table AS SELECT * FROM (VALUES (123,1513123564),(123,1512140765),(123,1512173164),(123,1512396441)) foo(a, b) DISTRIBUTED RANDOMLY;
-SELECT DISTINCT to_timestamp(b)::date FROM timezone_table;
- to_timestamp 
---------------
- 12-01-2017
- 12-04-2017
- 12-12-2017
-(3 rows)
+SELECT to_timestamp(b)::timestamp WITH TIME ZONE AS b_ts FROM timezone_table ORDER BY b_ts;
+             b_ts             
+------------------------------
+ Fri Dec 01 07:06:05 2017 PST
+ Fri Dec 01 16:06:04 2017 PST
+ Mon Dec 04 06:07:21 2017 PST
+ Tue Dec 12 16:06:04 2017 PST
+(4 rows)
 
 SET timezone= 'America/New_York';
-SHOW timezone;
-     TimeZone     
-------------------
- America/New_York
+-- Check if it is set correctly on QD.
+SELECT to_timestamp(1613123565)::timestamp WITH TIME ZONE;
+         to_timestamp         
+------------------------------
+ Fri Feb 12 04:52:45 2021 EST
 (1 row)
 
-SELECT DISTINCT to_timestamp(b)::date FROM timezone_table;
- to_timestamp 
---------------
- 12-01-2017
- 12-04-2017
- 12-12-2017
-(3 rows)
+-- Check if it is set correctly on the QEs.
+SELECT to_timestamp(b)::timestamp WITH TIME ZONE AS b_ts FROM timezone_table ORDER BY b_ts;
+             b_ts             
+------------------------------
+ Fri Dec 01 10:06:05 2017 EST
+ Fri Dec 01 19:06:04 2017 EST
+ Mon Dec 04 09:07:21 2017 EST
+ Tue Dec 12 19:06:04 2017 EST
+(4 rows)
 
 RESET timezone;
-SHOW timezone;
- TimeZone 
-----------
- PST8PDT
+-- Check if it is reset correctly on QD.
+SELECT to_timestamp(1613123565)::timestamp WITH TIME ZONE;
+         to_timestamp         
+------------------------------
+ Fri Feb 12 01:52:45 2021 PST
 (1 row)
 
-SELECT DISTINCT to_timestamp(b)::date FROM timezone_table;
- to_timestamp 
---------------
- 12-01-2017
- 12-04-2017
- 12-12-2017
-(3 rows)
+-- Check if it is reset correctly on the QEs.
+SELECT to_timestamp(b)::timestamp WITH TIME ZONE AS b_ts FROM timezone_table ORDER BY b_ts;
+             b_ts             
+------------------------------
+ Fri Dec 01 07:06:05 2017 PST
+ Fri Dec 01 16:06:04 2017 PST
+ Mon Dec 04 06:07:21 2017 PST
+ Tue Dec 12 16:06:04 2017 PST
+(4 rows)
+
+--
+-- Test if SET TIME ZONE INTERVAL is dispatched correctly to all segments
+--
+SET TIME ZONE INTERVAL '04:30:06' HOUR TO MINUTE;
+-- Check if it is set correctly on QD.
+SELECT to_timestamp(1613123565)::timestamp WITH TIME ZONE;
+          to_timestamp           
+---------------------------------
+ Fri Feb 12 14:22:45 2021 +04:30
+(1 row)
+
+-- Check if it is set correctly on the QEs.
+SELECT to_timestamp(b)::timestamp WITH TIME ZONE AS b_ts FROM timezone_table ORDER BY b_ts;
+              b_ts               
+---------------------------------
+ Fri Dec 01 19:36:05 2017 +04:30
+ Sat Dec 02 04:36:04 2017 +04:30
+ Mon Dec 04 18:37:21 2017 +04:30
+ Wed Dec 13 04:36:04 2017 +04:30
+(4 rows)
 
 -- Test default_transaction_isolation and transaction_isolation fallback from serializable to repeatable read
 CREATE TABLE test_serializable(a int);
diff --git a/src/test/regress/sql/guc_gp.sql b/src/test/regress/sql/guc_gp.sql
index a1854ac3e3c..856f9e3fd7f 100644
--- a/src/test/regress/sql/guc_gp.sql
+++ b/src/test/regress/sql/guc_gp.sql
@@ -104,13 +104,26 @@ reset work_mem;
 --
 CREATE TABLE timezone_table AS SELECT * FROM (VALUES (123,1513123564),(123,1512140765),(123,1512173164),(123,1512396441)) foo(a, b) DISTRIBUTED RANDOMLY;
 
-SELECT DISTINCT to_timestamp(b)::date FROM timezone_table;
+SELECT to_timestamp(b)::timestamp WITH TIME ZONE AS b_ts FROM timezone_table ORDER BY b_ts;
 SET timezone= 'America/New_York';
-SHOW timezone;
-SELECT DISTINCT to_timestamp(b)::date FROM timezone_table;
+-- Check if it is set correctly on QD.
+SELECT to_timestamp(1613123565)::timestamp WITH TIME ZONE;
+-- Check if it is set correctly on the QEs.
+SELECT to_timestamp(b)::timestamp WITH TIME ZONE AS b_ts FROM timezone_table ORDER BY b_ts;
 RESET timezone;
-SHOW timezone;
-SELECT DISTINCT to_timestamp(b)::date FROM timezone_table;
+-- Check if it is reset correctly on QD.
+SELECT to_timestamp(1613123565)::timestamp WITH TIME ZONE;
+-- Check if it is reset correctly on the QEs.
+SELECT to_timestamp(b)::timestamp WITH TIME ZONE AS b_ts FROM timezone_table ORDER BY b_ts;
+
+--
+-- Test if SET TIME ZONE INTERVAL is dispatched correctly to all segments
+--
+SET TIME ZONE INTERVAL '04:30:06' HOUR TO MINUTE;
+-- Check if it is set correctly on QD.
+SELECT to_timestamp(1613123565)::timestamp WITH TIME ZONE;
+-- Check if it is set correctly on the QEs.
+SELECT to_timestamp(b)::timestamp WITH TIME ZONE AS b_ts FROM timezone_table ORDER BY b_ts;
 
 -- Test default_transaction_isolation and transaction_isolation fallback from serializable to repeatable read
 CREATE TABLE test_serializable(a int);
