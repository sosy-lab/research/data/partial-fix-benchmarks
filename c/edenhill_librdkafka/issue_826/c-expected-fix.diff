diff --git a/src-cpp/rdkafkacpp.h b/src-cpp/rdkafkacpp.h
index 305959be7..fc275400a 100644
--- a/src-cpp/rdkafkacpp.h
+++ b/src-cpp/rdkafkacpp.h
@@ -1147,11 +1147,10 @@ class RD_EXPORT Topic {
    * The offset will be committed (written) to the offset store according
    * to \p auto.commit.interval.ms.
    *
-   * @remark This API should only be used with the simple RdKafka::Consumer,
-   *         not the high-level RdKafka::KafkaConsumer.
-   * @remark \c auto.commit.enable must be set to \c false when using this API.
+   * @remark \c enable.auto.offset.store must be set to \c false when using this API.
    *
-   * @returns RdKafka::ERR_NO_ERROR on success or an error code on error.
+   * @returns RdKafka::ERR_NO_ERROR on success or an error code if none of the
+   *          offsets could be stored.
    */
   virtual ErrorCode offset_store (int32_t partition, int64_t offset) = 0;
 };
@@ -1583,6 +1582,20 @@ class RD_EXPORT KafkaConsumer : public virtual Handle {
    * @returns an ErrorCode to indicate success or failure.
    */
   virtual ErrorCode seek (const TopicPartition &partition, int timeout_ms) = 0;
+
+
+  /**
+   * @brief Store offset \p offset for topic partition \p partition.
+   * The offset will be committed (written) to the offset store according
+   * to \p auto.commit.interval.ms or the next manual offset-less commit*()
+   *
+   * Per-partition success/error status propagated through TopicPartition.err()
+   *
+   * @remark \c enable.auto.offset.store must be set to \c false when using this API.
+   *
+   * @returns RdKafka::ERR_NO_ERROR on success or an error code on error.
+   */
+  virtual ErrorCode offsets_store (std::vector<TopicPartition*> &offsets) = 0;
 };
 
 
diff --git a/src-cpp/rdkafkacpp_int.h b/src-cpp/rdkafkacpp_int.h
index 5dcc7e750..970fdb444 100644
--- a/src-cpp/rdkafkacpp_int.h
+++ b/src-cpp/rdkafkacpp_int.h
@@ -758,9 +758,20 @@ class KafkaConsumerImpl : virtual public KafkaConsumer, virtual public HandleImp
   ErrorCode committed (std::vector<TopicPartition*> &partitions, int timeout_ms);
   ErrorCode position (std::vector<TopicPartition*> &partitions);
 
+  ErrorCode close ();
+
   ErrorCode seek (const TopicPartition &partition, int timeout_ms);
 
-  ErrorCode close ();
+  ErrorCode offsets_store (std::vector<TopicPartition*> &offsets) {
+          rd_kafka_topic_partition_list_t *c_parts =
+                  partitions_to_c_parts(offsets);
+          rd_kafka_resp_err_t err =
+                  rd_kafka_offsets_store(rk_, c_parts);
+          update_partitions_from_c_parts(offsets, c_parts);
+          rd_kafka_topic_partition_list_destroy(c_parts);
+          return static_cast<ErrorCode>(err);
+  }
+
 };
 
 
diff --git a/src/rdkafka.h b/src/rdkafka.h
index 28bc5975a..9f953c20c 100644
--- a/src/rdkafka.h
+++ b/src/rdkafka.h
@@ -2222,15 +2222,34 @@ int rd_kafka_consume_callback_queue(rd_kafka_queue_t *rkqu,
  * @brief Store offset \p offset for topic \p rkt partition \p partition.
  *
  * The offset will be committed (written) to the offset store according
- * to \c `auto.commit.interval.ms`.
+ * to \c `auto.commit.interval.ms` or manual offset-less commit()
  *
- * @remark \c `auto.commit.enable` must be set to "false" when using this API.
+ * @remark \c `enable.auto.offset.store` must be set to "false" when using this API.
  *
  * @returns RD_KAFKA_RESP_ERR_NO_ERROR on success or an error code on error.
  */
 RD_EXPORT
 rd_kafka_resp_err_t rd_kafka_offset_store(rd_kafka_topic_t *rkt,
 					   int32_t partition, int64_t offset);
+
+
+/**
+ * @brief Store offsets for one or more partitions.
+ *
+ * The offset will be committed (written) to the offset store according
+ * to \c `auto.commit.interval.ms` or manual offset-less commit().
+ *
+ * Per-partition success/error status propagated through each partition's
+ * \c .err field.
+ *
+ * @remark \c `enable.auto.offset.store` must be set to "false" when using this API.
+ *
+ * @returns RD_KAFKA_RESP_ERR_NO_ERROR on success or an error code if
+ *          none of the offsets could be stored.
+ */
+RD_EXPORT rd_kafka_resp_err_t
+rd_kafka_offsets_store(rd_kafka_t *rk,
+                       rd_kafka_topic_partition_list_t *offsets);
 /**@}*/
 
 
diff --git a/src/rdkafka_offset.c b/src/rdkafka_offset.c
index 5609c67a8..cc6d84720 100644
--- a/src/rdkafka_offset.c
+++ b/src/rdkafka_offset.c
@@ -690,6 +690,36 @@ rd_kafka_resp_err_t rd_kafka_offset_store (rd_kafka_topic_t *app_rkt,
 }
 
 
+rd_kafka_resp_err_t
+rd_kafka_offsets_store (rd_kafka_t *rk,
+                        rd_kafka_topic_partition_list_t *offsets) {
+        int i;
+        int ok_cnt = 0;
+
+        for (i = 0 ; i < offsets->cnt ; i++) {
+                rd_kafka_topic_partition_t *rktpar = &offsets->elems[i];
+                shptr_rd_kafka_toppar_t *s_rktp;
+
+                s_rktp = rd_kafka_topic_partition_get_toppar(rk, rktpar);
+                if (!s_rktp) {
+                        rktpar->err = RD_KAFKA_RESP_ERR__UNKNOWN_PARTITION;
+                        continue;
+                }
+
+                rd_kafka_offset_store0(rd_kafka_toppar_s2i(s_rktp),
+                                       rktpar->offset, 1/*lock*/);
+                rd_kafka_toppar_destroy(s_rktp);
+
+                rktpar->err = RD_KAFKA_RESP_ERR_NO_ERROR;
+                ok_cnt++;
+        }
+
+        return offsets->cnt > 0 && ok_cnt < offsets->cnt ?
+                RD_KAFKA_RESP_ERR__UNKNOWN_PARTITION :
+                RD_KAFKA_RESP_ERR_NO_ERROR;
+}
+
+
 
 
 
