diff --git a/src/lck-windows.c b/src/lck-windows.c
index fe1adb38c..b1b1ca943 100644
--- a/src/lck-windows.c
+++ b/src/lck-windows.c
@@ -161,7 +161,7 @@ int mdbx_txn_lock(MDBX_env *env, bool dontwait) {
                      : (LCK_EXCLUSIVE | LCK_WAITFOR),
             LCK_BODY))
     return MDBX_SUCCESS;
-  int rc = GetLastError();
+  int rc = (int)GetLastError();
   LeaveCriticalSection(&env->me_windowsbug_lock);
   return (!dontwait || rc != ERROR_LOCK_VIOLATION) ? rc : MDBX_BUSY;
 }
@@ -172,7 +172,7 @@ void mdbx_txn_unlock(MDBX_env *env) {
                : funlock(env->me_lazy_fd, LCK_BODY);
   LeaveCriticalSection(&env->me_windowsbug_lock);
   if (!rc)
-    mdbx_panic("%s failed: err %u", __func__, GetLastError());
+    mdbx_panic("%s failed: err %u", __func__, (int)GetLastError());
 }
 
 /*----------------------------------------------------------------------------*/
@@ -197,7 +197,7 @@ MDBX_INTERNAL_FUNC int mdbx_rdt_lock(MDBX_env *env) {
       flock(env->me_lfd, LCK_EXCLUSIVE | LCK_WAITFOR, LCK_UPPER))
     return MDBX_SUCCESS;
 
-  int rc = GetLastError();
+  int rc = (int)GetLastError();
   mdbx_srwlock_ReleaseShared(&env->me_remap_guard);
   return rc;
 }
@@ -207,7 +207,7 @@ MDBX_INTERNAL_FUNC void mdbx_rdt_unlock(MDBX_env *env) {
     /* transition from S-E (locked) to S-? (used), e.g. unlock upper-part */
     if ((env->me_flags & MDBX_EXCLUSIVE) == 0 &&
         !funlock(env->me_lfd, LCK_UPPER))
-      mdbx_panic("%s failed: err %u", __func__, GetLastError());
+      mdbx_panic("%s failed: err %u", __func__, (int)GetLastError());
   }
   mdbx_srwlock_ReleaseShared(&env->me_remap_guard);
 }
@@ -218,7 +218,7 @@ MDBX_INTERNAL_FUNC int mdbx_lockfile(mdbx_filehandle_t fd, bool wait) {
                     : LCK_EXCLUSIVE | LCK_DONTWAIT,
                0, LCK_MAXLEN)
              ? MDBX_SUCCESS
-             : GetLastError();
+             : (int)GetLastError();
 }
 
 static int suspend_and_append(mdbx_handle_array_t **array,
@@ -242,10 +242,10 @@ static int suspend_and_append(mdbx_handle_array_t **array,
   HANDLE hThread = OpenThread(THREAD_SUSPEND_RESUME | THREAD_QUERY_INFORMATION,
                               FALSE, ThreadId);
   if (hThread == NULL)
-    return GetLastError();
+    return (int)GetLastError();
 
   if (SuspendThread(hThread) == -1) {
-    int err = GetLastError();
+    int err = (int)GetLastError();
     DWORD ExitCode;
     if (err == /* workaround for Win10 UCRT bug */ ERROR_ACCESS_DENIED ||
         !GetExitCodeThread(hThread, &ExitCode) || ExitCode != STILL_ACTIVE)
@@ -302,13 +302,13 @@ mdbx_suspend_threads_before_remap(MDBX_env *env, mdbx_handle_array_t **array) {
     mdbx_assert(env, env->me_flags & (MDBX_EXCLUSIVE | MDBX_RDONLY));
     const HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
     if (hSnapshot == INVALID_HANDLE_VALUE)
-      return GetLastError();
+      return (int)GetLastError();
 
     THREADENTRY32 entry;
     entry.dwSize = sizeof(THREADENTRY32);
 
     if (!Thread32First(hSnapshot, &entry)) {
-      rc = GetLastError();
+      rc = (int)GetLastError();
     bailout_toolhelp:
       CloseHandle(hSnapshot);
       (void)mdbx_resume_threads_after_remap(*array);
@@ -326,7 +326,7 @@ mdbx_suspend_threads_before_remap(MDBX_env *env, mdbx_handle_array_t **array) {
 
     } while (Thread32Next(hSnapshot, &entry));
 
-    rc = GetLastError();
+    rc = (int)GetLastError();
     if (rc != ERROR_NO_MORE_FILES)
       goto bailout_toolhelp;
     CloseHandle(hSnapshot);
@@ -341,7 +341,7 @@ mdbx_resume_threads_after_remap(mdbx_handle_array_t *array) {
   for (unsigned i = 0; i < array->count; ++i) {
     const HANDLE hThread = array->handles[i];
     if (ResumeThread(hThread) == -1) {
-      const int err = GetLastError();
+      const int err = (int)GetLastError();
       DWORD ExitCode;
       if (err != /* workaround for Win10 UCRT bug */ ERROR_ACCESS_DENIED &&
           GetExitCodeThread(hThread, &ExitCode) && ExitCode == STILL_ACTIVE)
@@ -393,7 +393,7 @@ static void lck_unlock(MDBX_env *env) {
     /* double `unlock` for robustly remove overlapped shared/exclusive locks */
     while (funlock(env->me_lfd, LCK_LOWER))
       ;
-    err = GetLastError();
+    err = (int)GetLastError();
     assert(err == ERROR_NOT_LOCKED ||
            (mdbx_RunningUnderWine() && err == ERROR_LOCK_VIOLATION));
     (void)err;
@@ -401,7 +401,7 @@ static void lck_unlock(MDBX_env *env) {
 
     while (funlock(env->me_lfd, LCK_UPPER))
       ;
-    err = GetLastError();
+    err = (int)GetLastError();
     assert(err == ERROR_NOT_LOCKED ||
            (mdbx_RunningUnderWine() && err == ERROR_LOCK_VIOLATION));
     (void)err;
@@ -413,7 +413,7 @@ static void lck_unlock(MDBX_env *env) {
      * releases such locks via deferred queues) */
     while (funlock(env->me_lazy_fd, LCK_BODY))
       ;
-    err = GetLastError();
+    err = (int)GetLastError();
     assert(err == ERROR_NOT_LOCKED ||
            (mdbx_RunningUnderWine() && err == ERROR_LOCK_VIOLATION));
     (void)err;
@@ -421,7 +421,7 @@ static void lck_unlock(MDBX_env *env) {
 
     while (funlock(env->me_lazy_fd, LCK_WHOLE))
       ;
-    err = GetLastError();
+    err = (int)GetLastError();
     assert(err == ERROR_NOT_LOCKED ||
            (mdbx_RunningUnderWine() && err == ERROR_LOCK_VIOLATION));
     (void)err;
@@ -439,7 +439,7 @@ static int internal_seize_lck(HANDLE lfd) {
   /* 1) now on ?-? (free), get ?-E (middle) */
   mdbx_jitter4testing(false);
   if (!flock(lfd, LCK_EXCLUSIVE | LCK_WAITFOR, LCK_UPPER)) {
-    rc = GetLastError() /* 2) something went wrong, give up */;
+    rc = (int)GetLastError() /* 2) something went wrong, give up */;
     mdbx_error("%s, err %u", "?-?(free) >> ?-E(middle)", rc);
     return rc;
   }
@@ -450,20 +450,20 @@ static int internal_seize_lck(HANDLE lfd) {
     return MDBX_RESULT_TRUE /* 4) got E-E (exclusive-write), done */;
 
   /* 5) still on ?-E (middle) */
-  rc = GetLastError();
+  rc = (int)GetLastError();
   mdbx_jitter4testing(false);
   if (rc != ERROR_SHARING_VIOLATION && rc != ERROR_LOCK_VIOLATION) {
     /* 6) something went wrong, give up */
     if (!funlock(lfd, LCK_UPPER))
       mdbx_panic("%s(%s) failed: err %u", __func__, "?-E(middle) >> ?-?(free)",
-                 GetLastError());
+                 (int)GetLastError());
     return rc;
   }
 
   /* 7) still on ?-E (middle), try S-E (locked) */
   mdbx_jitter4testing(false);
   rc = flock(lfd, LCK_SHARED | LCK_DONTWAIT, LCK_LOWER) ? MDBX_RESULT_FALSE
-                                                        : GetLastError();
+                                                        : (int)GetLastError();
 
   mdbx_jitter4testing(false);
   if (rc != MDBX_RESULT_FALSE)
@@ -473,7 +473,7 @@ static int internal_seize_lck(HANDLE lfd) {
    *    transition to S-? (used) or ?-? (free) */
   if (!funlock(lfd, LCK_UPPER))
     mdbx_panic("%s(%s) failed: err %u", __func__,
-               "X-E(locked/middle) >> X-?(used/free)", GetLastError());
+               "X-E(locked/middle) >> X-?(used/free)", (int)GetLastError());
 
   /* 9) now on S-? (used, DONE) or ?-? (free, FAILURE) */
   return rc;
@@ -492,7 +492,7 @@ MDBX_INTERNAL_FUNC int mdbx_lck_seize(MDBX_env *env) {
     /* LY: without-lck mode (e.g. on read-only filesystem) */
     mdbx_jitter4testing(false);
     if (!flock(env->me_lazy_fd, LCK_SHARED | LCK_DONTWAIT, LCK_WHOLE)) {
-      rc = GetLastError();
+      rc = (int)GetLastError();
       mdbx_error("%s, err %u", "without-lck", rc);
       return rc;
     }
@@ -509,7 +509,7 @@ MDBX_INTERNAL_FUNC int mdbx_lck_seize(MDBX_env *env) {
      *  - we can't lock meta-pages, otherwise other process could get an error
      *    while opening db in valid (non-conflict) mode. */
     if (!flock(env->me_lazy_fd, LCK_EXCLUSIVE | LCK_DONTWAIT, LCK_BODY)) {
-      rc = GetLastError();
+      rc = (int)GetLastError();
       mdbx_error("%s, err %u", "lock-against-without-lck", rc);
       mdbx_jitter4testing(false);
       lck_unlock(env);
@@ -517,7 +517,7 @@ MDBX_INTERNAL_FUNC int mdbx_lck_seize(MDBX_env *env) {
       mdbx_jitter4testing(false);
       if (!funlock(env->me_lazy_fd, LCK_BODY))
         mdbx_panic("%s(%s) failed: err %u", __func__,
-                   "unlock-against-without-lck", GetLastError());
+                   "unlock-against-without-lck", (int)GetLastError());
     }
   }
 
@@ -535,11 +535,11 @@ MDBX_INTERNAL_FUNC int mdbx_lck_downgrade(MDBX_env *env) {
   /* 1) now at E-E (exclusive-write), transition to ?_E (middle) */
   if (!funlock(env->me_lfd, LCK_LOWER))
     mdbx_panic("%s(%s) failed: err %u", __func__,
-               "E-E(exclusive-write) >> ?-E(middle)", GetLastError());
+               "E-E(exclusive-write) >> ?-E(middle)", (int)GetLastError());
 
   /* 2) now at ?-E (middle), transition to S-E (locked) */
   if (!flock(env->me_lfd, LCK_SHARED | LCK_DONTWAIT, LCK_LOWER)) {
-    int rc = GetLastError() /* 3) something went wrong, give up */;
+    int rc = (int)GetLastError() /* 3) something went wrong, give up */;
     mdbx_error("%s, err %u", "?-E(middle) >> S-E(locked)", rc);
     return rc;
   }
@@ -547,7 +547,7 @@ MDBX_INTERNAL_FUNC int mdbx_lck_downgrade(MDBX_env *env) {
   /* 4) got S-E (locked), continue transition to S-? (used) */
   if (!funlock(env->me_lfd, LCK_UPPER))
     mdbx_panic("%s(%s) failed: err %u", __func__, "S-E(locked) >> S-?(used)",
-               GetLastError());
+               (int)GetLastError());
 
   return MDBX_SUCCESS /* 5) now at S-? (used), done */;
 }
@@ -564,7 +564,7 @@ MDBX_INTERNAL_FUNC int mdbx_lck_upgrade(MDBX_env *env) {
   /* 1) now on S-? (used), try S-E (locked) */
   mdbx_jitter4testing(false);
   if (!flock(env->me_lfd, LCK_EXCLUSIVE | LCK_DONTWAIT, LCK_UPPER)) {
-    rc = GetLastError() /* 2) something went wrong, give up */;
+    rc = (int)GetLastError() /* 2) something went wrong, give up */;
     mdbx_verbose("%s, err %u", "S-?(used) >> S-E(locked)", rc);
     return rc;
   }
@@ -572,12 +572,12 @@ MDBX_INTERNAL_FUNC int mdbx_lck_upgrade(MDBX_env *env) {
   /* 3) now on S-E (locked), transition to ?-E (middle) */
   if (!funlock(env->me_lfd, LCK_LOWER))
     mdbx_panic("%s(%s) failed: err %u", __func__, "S-E(locked) >> ?-E(middle)",
-               GetLastError());
+               (int)GetLastError());
 
   /* 4) now on ?-E (middle), try E-E (exclusive-write) */
   mdbx_jitter4testing(false);
   if (!flock(env->me_lfd, LCK_EXCLUSIVE | LCK_DONTWAIT, LCK_LOWER)) {
-    rc = GetLastError() /* 5) something went wrong, give up */;
+    rc = (int)GetLastError() /* 5) something went wrong, give up */;
     mdbx_verbose("%s, err %u", "?-E(middle) >> E-E(exclusive-write)", rc);
     return rc;
   }
@@ -638,10 +638,10 @@ MDBX_INTERNAL_FUNC int mdbx_rpid_check(MDBX_env *env, uint32_t pid) {
   if (likely(hProcess)) {
     rc = WaitForSingleObject(hProcess, 0);
     if (unlikely(rc == WAIT_FAILED))
-      rc = GetLastError();
+      rc = (int)GetLastError();
     CloseHandle(hProcess);
   } else {
-    rc = GetLastError();
+    rc = (int)GetLastError();
   }
 
   switch (rc) {
@@ -744,7 +744,7 @@ static DWORD WINAPI stub_DiscardVirtualMemory(PVOID VirtualAddress,
                                               SIZE_T Size) {
   return VirtualAlloc(VirtualAddress, Size, MEM_RESET, PAGE_NOACCESS)
              ? ERROR_SUCCESS
-	  : GetLastError();
+	  : (int)GetLastError();
 }
 #endif /* unused for now */
 
diff --git a/src/osal.c b/src/osal.c
index 08e0d6875..38c259e55 100644
--- a/src/osal.c
+++ b/src/osal.c
@@ -25,7 +25,7 @@ static int waitstatus2errcode(DWORD result) {
   case WAIT_OBJECT_0:
     return MDBX_SUCCESS;
   case WAIT_FAILED:
-    return GetLastError();
+    return (int)GetLastError();
   case WAIT_ABANDONED:
     return ERROR_ABANDONED_WAIT_0;
   case WAIT_IO_COMPLETION:
@@ -44,7 +44,7 @@ static int ntstatus2errcode(NTSTATUS status) {
   memset(&ov, 0, sizeof(ov));
   ov.Internal = status;
   return GetOverlappedResult(NULL, &ov, &dummy, FALSE) ? MDBX_SUCCESS
-                                                       : GetLastError();
+                                                       : (int)GetLastError();
 }
 
 /* We use native NT APIs to setup the memory map, so that we can
@@ -384,17 +384,17 @@ MDBX_INTERNAL_FUNC int mdbx_condpair_init(mdbx_condpair_t *condpair) {
   memset(condpair, 0, sizeof(mdbx_condpair_t));
 #if defined(_WIN32) || defined(_WIN64)
   if ((condpair->mutex = CreateMutexW(NULL, FALSE, NULL)) == NULL) {
-    rc = GetLastError();
+    rc = (int)GetLastError();
     goto bailout_mutex;
   }
   if ((condpair->event[0] = CreateEventW(NULL, FALSE, FALSE, NULL)) == NULL) {
-    rc = GetLastError();
+    rc = (int)GetLastError();
     goto bailout_event;
   }
   if ((condpair->event[1] = CreateEventW(NULL, FALSE, FALSE, NULL)) != NULL)
     return MDBX_SUCCESS;
 
-  rc = GetLastError();
+  rc = (int)GetLastError();
   (void)CloseHandle(condpair->event[0]);
 bailout_event:
   (void)CloseHandle(condpair->mutex);
@@ -420,9 +420,9 @@ MDBX_INTERNAL_FUNC int mdbx_condpair_init(mdbx_condpair_t *condpair) {
 
 MDBX_INTERNAL_FUNC int mdbx_condpair_destroy(mdbx_condpair_t *condpair) {
 #if defined(_WIN32) || defined(_WIN64)
-  int rc = CloseHandle(condpair->mutex) ? MDBX_SUCCESS : GetLastError();
-  rc = CloseHandle(condpair->event[0]) ? rc : GetLastError();
-  rc = CloseHandle(condpair->event[1]) ? rc : GetLastError();
+  int rc = CloseHandle(condpair->mutex) ? MDBX_SUCCESS : (int)GetLastError();
+  rc = CloseHandle(condpair->event[0]) ? rc : (int)GetLastError();
+  rc = CloseHandle(condpair->event[1]) ? rc : (int)GetLastError();
 #else
   int err, rc = pthread_mutex_destroy(&condpair->mutex);
   rc = (err = pthread_cond_destroy(&condpair->cond[0])) ? err : rc;
@@ -443,7 +443,7 @@ MDBX_INTERNAL_FUNC int mdbx_condpair_lock(mdbx_condpair_t *condpair) {
 
 MDBX_INTERNAL_FUNC int mdbx_condpair_unlock(mdbx_condpair_t *condpair) {
 #if defined(_WIN32) || defined(_WIN64)
-  return ReleaseMutex(condpair->mutex) ? MDBX_SUCCESS : GetLastError();
+  return ReleaseMutex(condpair->mutex) ? MDBX_SUCCESS : (int)GetLastError();
 #else
   return pthread_mutex_unlock(&condpair->mutex);
 #endif
@@ -452,7 +452,7 @@ MDBX_INTERNAL_FUNC int mdbx_condpair_unlock(mdbx_condpair_t *condpair) {
 MDBX_INTERNAL_FUNC int mdbx_condpair_signal(mdbx_condpair_t *condpair,
                                             bool part) {
 #if defined(_WIN32) || defined(_WIN64)
-  return SetEvent(condpair->event[part]) ? MDBX_SUCCESS : GetLastError();
+  return SetEvent(condpair->event[part]) ? MDBX_SUCCESS : (int)GetLastError();
 #else
   return pthread_cond_signal(&condpair->cond[part]);
 #endif
@@ -530,7 +530,7 @@ MDBX_INTERNAL_FUNC int mdbx_removefile(const char *pathname) {
   wchar_t *const pathnameW = _alloca((wlen + 1) * sizeof(wchar_t));
   if (wlen != mbstowcs(pathnameW, pathname, wlen + 1))
     return ERROR_INVALID_NAME;
-  return DeleteFileW(pathnameW) ? MDBX_SUCCESS : GetLastError();
+  return DeleteFileW(pathnameW) ? MDBX_SUCCESS : (int)GetLastError();
 #else
   return unlink(pathname) ? errno : MDBX_SUCCESS;
 #endif
@@ -548,7 +548,7 @@ MDBX_INTERNAL_FUNC int mdbx_removedirectory(const char *pathname) {
   wchar_t *const pathnameW = _alloca((wlen + 1) * sizeof(wchar_t));
   if (wlen != mbstowcs(pathnameW, pathname, wlen + 1))
     return ERROR_INVALID_NAME;
-  return RemoveDirectoryW(pathnameW) ? MDBX_SUCCESS : GetLastError();
+  return RemoveDirectoryW(pathnameW) ? MDBX_SUCCESS : (int)GetLastError();
 #else
   return rmdir(pathname) ? errno : MDBX_SUCCESS;
 #endif
@@ -615,11 +615,11 @@ MDBX_INTERNAL_FUNC int mdbx_openfile(const enum mdbx_openfile_purpose purpose,
   *fd = CreateFileW(pathnameW, DesiredAccess, ShareMode, NULL,
                     CreationDisposition, FlagsAndAttributes, NULL);
   if (*fd == INVALID_HANDLE_VALUE)
-    return GetLastError();
+    return (int)GetLastError();
 
   BY_HANDLE_FILE_INFORMATION info;
   if (!GetFileInformationByHandle(*fd, &info)) {
-    int err = GetLastError();
+    int err = (int)GetLastError();
     CloseHandle(*fd);
     *fd = INVALID_HANDLE_VALUE;
     return err;
@@ -769,7 +769,7 @@ MDBX_INTERNAL_FUNC int mdbx_openfile(const enum mdbx_openfile_purpose purpose,
 
 MDBX_INTERNAL_FUNC int mdbx_closefile(mdbx_filehandle_t fd) {
 #if defined(_WIN32) || defined(_WIN64)
-  return CloseHandle(fd) ? MDBX_SUCCESS : GetLastError();
+  return CloseHandle(fd) ? MDBX_SUCCESS : (int)GetLastError();
 #else
   assert(fd > STDERR_FILENO);
   return (close(fd) == 0) ? MDBX_SUCCESS : errno;
@@ -788,7 +788,7 @@ MDBX_INTERNAL_FUNC int mdbx_pread(mdbx_filehandle_t fd, void *buf, size_t bytes,
 
   DWORD read = 0;
   if (unlikely(!ReadFile(fd, buf, (DWORD)bytes, &read, &ov))) {
-    int rc = GetLastError();
+    int rc = (int)GetLastError();
     return (rc == MDBX_SUCCESS) ? /* paranoia */ ERROR_READ_FAULT : rc;
   }
 #else
@@ -816,7 +816,7 @@ MDBX_INTERNAL_FUNC int mdbx_pwrite(mdbx_filehandle_t fd, const void *buf,
     if (unlikely(!WriteFile(
             fd, buf, likely(bytes <= MAX_WRITE) ? (DWORD)bytes : MAX_WRITE,
             &written, &ov)))
-      return GetLastError();
+      return (int)GetLastError();
     if (likely(bytes == written))
       return MDBX_SUCCESS;
 #else
@@ -847,7 +847,7 @@ MDBX_INTERNAL_FUNC int mdbx_write(mdbx_filehandle_t fd, const void *buf,
     if (unlikely(!WriteFile(
             fd, buf, likely(bytes <= MAX_WRITE) ? (DWORD)bytes : MAX_WRITE,
             &written, nullptr)))
-      return GetLastError();
+      return (int)GetLastError();
     if (likely(bytes == written))
       return MDBX_SUCCESS;
 #else
@@ -902,7 +902,7 @@ MDBX_INTERNAL_FUNC int mdbx_fsync(mdbx_filehandle_t fd,
                                   enum mdbx_syncmode_bits mode_bits) {
 #if defined(_WIN32) || defined(_WIN64)
   if ((mode_bits & (MDBX_SYNC_DATA | MDBX_SYNC_IODQ)) && !FlushFileBuffers(fd))
-    return GetLastError();
+    return (int)GetLastError();
   return MDBX_SUCCESS;
 #else
 
@@ -949,7 +949,7 @@ int mdbx_filesize(mdbx_filehandle_t fd, uint64_t *length) {
 #if defined(_WIN32) || defined(_WIN64)
   BY_HANDLE_FILE_INFORMATION info;
   if (!GetFileInformationByHandle(fd, &info))
-    return GetLastError();
+    return (int)GetLastError();
   *length = info.nFileSizeLow | (uint64_t)info.nFileSizeHigh << 32;
 #else
   struct stat st;
@@ -973,7 +973,7 @@ MDBX_INTERNAL_FUNC int mdbx_is_pipe(mdbx_filehandle_t fd) {
   case FILE_TYPE_PIPE:
     return MDBX_RESULT_TRUE;
   default:
-    return GetLastError();
+    return (int)GetLastError();
   }
 #else
   struct stat info;
@@ -1004,13 +1004,13 @@ MDBX_INTERNAL_FUNC int mdbx_ftruncate(mdbx_filehandle_t fd, uint64_t length) {
                                            &EndOfFileInfo,
                                            sizeof(FILE_END_OF_FILE_INFO))
                ? MDBX_SUCCESS
-               : GetLastError();
+               : (int)GetLastError();
   } else {
     LARGE_INTEGER li;
     li.QuadPart = length;
     return (SetFilePointerEx(fd, li, NULL, FILE_BEGIN) && SetEndOfFile(fd))
                ? MDBX_SUCCESS
-               : GetLastError();
+               : (int)GetLastError();
   }
 #else
   STATIC_ASSERT_MSG(sizeof(off_t) >= sizeof(size_t),
@@ -1024,7 +1024,7 @@ MDBX_INTERNAL_FUNC int mdbx_fseek(mdbx_filehandle_t fd, uint64_t pos) {
   LARGE_INTEGER li;
   li.QuadPart = pos;
   return SetFilePointerEx(fd, li, NULL, FILE_BEGIN) ? MDBX_SUCCESS
-                                                    : GetLastError();
+                                                    : (int)GetLastError();
 #else
   STATIC_ASSERT_MSG(sizeof(off_t) >= sizeof(size_t),
                     "libmdbx requires 64-bit file I/O on 64-bit systems");
@@ -1040,7 +1040,7 @@ mdbx_thread_create(mdbx_thread_t *thread,
                    void *arg) {
 #if defined(_WIN32) || defined(_WIN64)
   *thread = CreateThread(NULL, 0, start_routine, arg, 0, NULL);
-  return *thread ? MDBX_SUCCESS : GetLastError();
+  return *thread ? MDBX_SUCCESS : (int)GetLastError();
 #else
   return pthread_create(thread, NULL, start_routine, arg);
 #endif
@@ -1064,7 +1064,7 @@ MDBX_INTERNAL_FUNC int mdbx_msync(mdbx_mmap_t *map, size_t offset,
   uint8_t *ptr = (uint8_t *)map->address + offset;
 #if defined(_WIN32) || defined(_WIN64)
   if (!FlushViewOfFile(ptr, length))
-    return GetLastError();
+    return (int)GetLastError();
 #else
 #if defined(__linux__) || defined(__gnu_linux__)
   if (mode_bits == MDBX_SYNC_NONE && mdbx_linux_kernel_version > 0x02061300)
@@ -1089,7 +1089,7 @@ MDBX_INTERNAL_FUNC int mdbx_check_fs_rdonly(mdbx_filehandle_t handle,
   DWORD unused, flags;
   if (!mdbx_GetVolumeInformationByHandleW(handle, nullptr, 0, nullptr, &unused,
                                           &flags, nullptr, 0))
-    return GetLastError();
+    return (int)GetLastError();
   if ((flags & FILE_READ_ONLY_VOLUME) == 0)
     return MDBX_EACCESS;
 #else
@@ -1164,7 +1164,7 @@ static int mdbx_check_fs_local(mdbx_filehandle_t handle, int flags) {
     if (!mdbx_GetVolumeInformationByHandleW(handle, PathBuffer, INT16_MAX,
                                             &VolumeSerialNumber, NULL,
                                             &FileSystemFlags, NULL, 0)) {
-      rc = GetLastError();
+      rc = (int)GetLastError();
       goto bailout;
     }
 
@@ -1180,7 +1180,7 @@ static int mdbx_check_fs_local(mdbx_filehandle_t handle, int flags) {
     if (!mdbx_GetFinalPathNameByHandleW(handle, PathBuffer, INT16_MAX,
                                         FILE_NAME_NORMALIZED |
                                             VOLUME_NAME_NT)) {
-      rc = GetLastError();
+      rc = (int)GetLastError();
       goto bailout;
     }
 
@@ -2422,7 +2422,7 @@ __cold int mdbx_get_sysraminfo(intptr_t *page_size, intptr_t *total_pages,
   memset(&info, 0, sizeof(info));
   info.dwLength = sizeof(info);
   if (!GlobalMemoryStatusEx(&info))
-    return GetLastError();
+    return (int)GetLastError();
 #endif
 
   if (total_pages) {
diff --git a/src/osal.h b/src/osal.h
index c1a64abb3..fac58b9ad 100644
--- a/src/osal.h
+++ b/src/osal.h
@@ -148,6 +148,10 @@ __extern_C key_t ftok(const char *, int);
 #elif _WIN32_WINNT < 0x0500
 #error At least 'Windows 2000' API is required for libmdbx.
 #endif /* _WIN32_WINNT */
+#if (defined(__MINGW32__) || defined(__MINGW64__)) &&                          \
+    !defined(__USE_MINGW_ANSI_STDIO)
+#define __USE_MINGW_ANSI_STDIO 1
+#endif /* MinGW */
 #ifndef WIN32_LEAN_AND_MEAN
 #define WIN32_LEAN_AND_MEAN
 #endif /* WIN32_LEAN_AND_MEAN */
@@ -170,6 +174,11 @@ typedef struct {
 } mdbx_condpair_t;
 typedef CRITICAL_SECTION mdbx_fastmutex_t;
 
+#if !defined(_MSC_VER) && !defined(__try)
+#define __try
+#define __except (COND) if (false)
+#endif /* stub for MSVC's __try/__except */
+
 #if MDBX_WITHOUT_MSVC_CRT
 
 #ifndef mdbx_malloc
