diff --git a/README.md b/README.md
index 48e1064..a5eadaf 100644
--- a/README.md
+++ b/README.md
@@ -49,6 +49,7 @@ utf8codepoint | &#10004;
 utf8rcodepoint | &#10004;
 utf8size | &#10004;
 utf8valid | &#10004;
+utf8makevalid | &#10004;
 utf8codepointsize | &#10004;
 utf8catcodepoint | &#10004;
 utf8isupper |  ~~&#10004;~~
@@ -88,7 +89,7 @@ void *utf8cat(void *dst, const void *src);
 Append the utf8 string `src` onto the utf8 string `dst`.
 
 ```c
-void *utf8chr(const void *src, long chr);
+void *utf8chr(const void *src, utf8_int32_t chr);
 ```
 Find the first match of the utf8 codepoint `chr` in the utf8 string `src`.
 
@@ -158,7 +159,7 @@ Locates the first occurrence in the utf8 string `str` of any byte in the
 utf8 string `accept`, or 0 if no match was found.
 
 ```c
-void *utf8rchr(const void *src, int chr);
+void *utf8rchr(const void *src, utf8_int32_t chr);
 ```
 Find the last match of the utf8 codepoint `chr` in the utf8 string `src`.
 
@@ -191,25 +192,30 @@ void *utf8valid(const void *str);
 Return 0 on success, or the position of the invalid utf8 codepoint on failure.
 
 ```c
-void *utf8codepoint(const void *str, long *out_codepoint);
+int utf8makevalid(void *str, utf8_int32_t replacement);
+```
+Return 0 on success. Makes the `str` valid by replacing invalid sequences with
+the 1-byte `replacement` codepoint.
+
+```c
+void *utf8codepoint(const void *str, utf8_int32_t *out_codepoint);
 ```
 Sets out_codepoint to the current utf8 codepoint in `str`, and returns the
 address of the next utf8 codepoint after the current one in `str`.
 
 ```c
-void *utf8rcodepoint(const void *str, long *out_codepoint);
+void *utf8rcodepoint(const void *str, utf8_int32_t *out_codepoint);
 ```
 Sets out_codepoint to the current utf8 codepoint in `str`, and returns the
 address of the previous utf8 codepoint before the current one in `str`.
 
 ```c
-utf8_weak size_t utf8codepointsize(utf8_int32_t chr);
+size_t utf8codepointsize(utf8_int32_t chr);
 ```
 Returns the size of the given codepoint in bytes.
 
 ```c
-utf8_nonnull utf8_weak void *utf8catcodepoint(void *utf8_restrict str,
-                                              utf8_int32_t chr, size_t n);
+void *utf8catcodepoint(void *utf8_restrict str, utf8_int32_t chr, size_t n);
 ```
 Write a codepoint to the given string, and return the address to the next
 place after the written codepoint. Pass how many bytes left in the buffer to
@@ -217,32 +223,32 @@ n. If there is not enough space for the codepoint, this function returns
 null.
 
 ```x
-utf8_weak int utf8islower(utf8_int32_t chr);
+int utf8islower(utf8_int32_t chr);
 ```
 Returns 1 if the given character is lowercase, or 0 if it is not.
 
 ```c
-utf8_weak int utf8isupper(utf8_int32_t chr);
+int utf8isupper(utf8_int32_t chr);
 ```
 Returns 1 if the given character is uppercase, or 0 if it is not.
 
 ```c
-utf8_nonnull utf8_weak void utf8lwr(void *utf8_restrict str);
+void utf8lwr(void *utf8_restrict str);
 ```
 Transform the given string into all lowercase codepoints.
 
 ```c
-utf8_nonnull utf8_weak void utf8upr(void *utf8_restrict str);
+void utf8upr(void *utf8_restrict str);
 ```
 Transform the given string into all uppercase codepoints.
 
 ```c
-utf8_weak utf8_int32_t utf8lwrcodepoint(utf8_int32_t cp);
+utf8_int32_t utf8lwrcodepoint(utf8_int32_t cp);
 ```
 Make a codepoint lower case if possible.
 
 ```c
-utf8_weak utf8_int32_t utf8uprcodepoint(utf8_int32_t cp);
+utf8_int32_t utf8uprcodepoint(utf8_int32_t cp);
 ```
 Make a codepoint upper case if possible.
 
@@ -266,7 +272,6 @@ insensitive code:
 - Implement utf8coll (akin to strcoll).
 - Implement utf8fry (akin to strfry).
 - Investigate adding dst buffer sizes for utf8cpy and utf8cat to catch overwrites (as suggested by [@FlohOfWoe](https://twitter.com/FlohOfWoe) in https://twitter.com/FlohOfWoe/status/618669237771608064)
-- Investigate adding a utf8canon which would turn 'bad' utf8 sequences (like ASCII values encoded in 4-byte utf8 codepoints) into their 'good' equivalents (as suggested by [@KmBenzie](https://twitter.com/KmBenzie))
 
 ## License ##
 
diff --git a/test/main.c b/test/main.c
index f0b12ec..4f1de5d 100644
--- a/test/main.c
+++ b/test/main.c
@@ -1319,4 +1319,235 @@ UTEST(utf8rcodepoint, latin) {
   ASSERT_EQ(0x3B1, codepoint);
 }
 
+UTEST(utf8makevalid, a) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xf0';
+  invalid[1] = '\x8f';
+  invalid[2] = '\xbf';
+  invalid[3] = '\xbf';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '\xef');
+}
+
+UTEST(utf8makevalid, b) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xf1';
+  invalid[1] = '\x3f';
+  invalid[2] = '\xbf';
+  invalid[3] = '\xbf';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '?');
+  ASSERT_EQ(invalid[1], '?');
+  ASSERT_EQ(invalid[2], '?');
+  ASSERT_EQ(invalid[3], '?');
+  ASSERT_EQ(invalid[4], '\0');
+}
+
+UTEST(utf8makevalid, c) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xf1';
+  invalid[1] = '\xbf';
+  invalid[2] = '\x3f';
+  invalid[3] = '\xbf';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '?');
+  ASSERT_EQ(invalid[1], '?');
+  ASSERT_EQ(invalid[2], '?');
+  ASSERT_EQ(invalid[3], '?');
+  ASSERT_EQ(invalid[4], '\0');
+}
+
+UTEST(utf8makevalid, d) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xf1';
+  invalid[1] = '\xbf';
+  invalid[2] = '\xbf';
+  invalid[3] = '\x3f';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '?');
+  ASSERT_EQ(invalid[1], '?');
+  ASSERT_EQ(invalid[2], '?');
+  ASSERT_EQ(invalid[3], '?');
+  ASSERT_EQ(invalid[4], '\0');
+}
+
+UTEST(utf8makevalid, e) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xe0';
+  invalid[1] = '\x9f';
+  invalid[2] = '\xbf';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '\xdf');
+  ASSERT_EQ(invalid[1], '\xbf');
+  ASSERT_EQ(invalid[2], '\0');
+}
+
+UTEST(utf8makevalid, f) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xef';
+  invalid[1] = '\x3f';
+  invalid[2] = '\xbf';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '?');
+  ASSERT_EQ(invalid[1], '?');
+  ASSERT_EQ(invalid[2], '?');
+  ASSERT_EQ(invalid[3], '\0');
+}
+
+UTEST(utf8makevalid, g) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xef';
+  invalid[1] = '\xbf';
+  invalid[2] = '\x3f';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '?');
+  ASSERT_EQ(invalid[1], '?');
+  ASSERT_EQ(invalid[2], '?');
+  ASSERT_EQ(invalid[3], '\0');
+}
+
+UTEST(utf8makevalid, h) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xc1';
+  invalid[1] = '\xbf';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '\x7f');
+  ASSERT_EQ(invalid[1], '\0');
+}
+
+UTEST(utf8makevalid, i) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xdf';
+  invalid[1] = '\x3f';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '?');
+  ASSERT_EQ(invalid[1], '?');
+  ASSERT_EQ(invalid[2], '\0');
+}
+
+UTEST(utf8makevalid, j) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\x80';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '?');
+  ASSERT_EQ(invalid[1], '\0');
+}
+
+UTEST(utf8makevalid, k) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xf8';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '?');
+  ASSERT_EQ(invalid[1], '\0');
+}
+
+UTEST(utf8makevalid, l) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xf1';
+  invalid[1] = '\xbf';
+  invalid[2] = '\xbf';
+  invalid[3] = '\xbf';
+  invalid[4] = '\xbf';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '\xf1');
+  ASSERT_EQ(invalid[1], '\xbf');
+  ASSERT_EQ(invalid[2], '\xbf');
+  ASSERT_EQ(invalid[3], '\xbf');
+  ASSERT_EQ(invalid[4], '?');
+  ASSERT_EQ(invalid[5], '\0');
+}
+
+UTEST(utf8makevalid, m) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xef';
+  invalid[1] = '\xbf';
+  invalid[2] = '\xbf';
+  invalid[3] = '\xbf';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '\xef');
+  ASSERT_EQ(invalid[1], '\xbf');
+  ASSERT_EQ(invalid[2], '\xbf');
+  ASSERT_EQ(invalid[3], '?');
+  ASSERT_EQ(invalid[4], '\0');
+}
+
+UTEST(utf8makevalid, n) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xdf';
+  invalid[1] = '\xbf';
+  invalid[2] = '\xbf';
+
+  ASSERT_EQ(0, utf8makevalid(invalid, '?'));
+
+  ASSERT_EQ(invalid[0], '\xdf');
+  ASSERT_EQ(invalid[1], '\xbf');
+  ASSERT_EQ(invalid[2], '?');
+  ASSERT_EQ(invalid[3], '\0');
+}
+
+UTEST(utf8makevalid, invalid_replacement) {
+  char invalid[6];
+  memset(invalid, 0, 6);
+
+  invalid[0] = '\xdf';
+  invalid[1] = '\xbf';
+  invalid[2] = '\xbf';
+
+  ASSERT_NE(0, utf8makevalid(invalid, 0x80));
+}
+
 UTEST_MAIN();
diff --git a/utf8.h b/utf8.h
index 8151639..bf3afa8 100644
--- a/utf8.h
+++ b/utf8.h
@@ -185,6 +185,11 @@ utf8_nonnull utf8_pure utf8_weak void *utf8casestr(const void *haystack,
 // utf8 codepoint on failure.
 utf8_nonnull utf8_pure utf8_weak void *utf8valid(const void *str);
 
+// Given a null-terminated string, makes the string valid by replacing invalid
+// codepoints with a 1-byte replacement. Returns 0 on success.
+utf8_nonnull utf8_weak int utf8makevalid(void *str,
+                                         const utf8_int32_t replacement);
+
 // Sets out_codepoint to the current utf8 codepoint in str, and returns the
 // address of the next utf8 codepoint after the current one in str.
 utf8_nonnull utf8_weak void *
@@ -192,8 +197,7 @@ utf8codepoint(const void *utf8_restrict str,
               utf8_int32_t *utf8_restrict out_codepoint);
 
 // Calculates the size of the next utf8 codepoint in str.
-utf8_nonnull utf8_weak size_t
-utf8codepointcalcsize(const void *utf8_restrict str);
+utf8_nonnull utf8_weak size_t utf8codepointcalcsize(const void *str);
 
 // Returns the size of the given codepoint in bytes.
 utf8_weak size_t utf8codepointsize(utf8_int32_t chr);
@@ -202,8 +206,8 @@ utf8_weak size_t utf8codepointsize(utf8_int32_t chr);
 // place after the written codepoint. Pass how many bytes left in the buffer to
 // n. If there is not enough space for the codepoint, this function returns
 // null.
-utf8_nonnull utf8_weak void *utf8catcodepoint(void *utf8_restrict str,
-                                              utf8_int32_t chr, size_t n);
+utf8_nonnull utf8_weak void *utf8catcodepoint(void *str, utf8_int32_t chr,
+                                              size_t n);
 
 // Returns 1 if the given character is lowercase, or 0 if it is not.
 utf8_weak int utf8islower(utf8_int32_t chr);
@@ -988,6 +992,71 @@ void *utf8valid(const void *str) {
   return utf8_null;
 }
 
+int utf8makevalid(void *str, const utf8_int32_t replacement) {
+  char *read = (char *)str;
+  char *write = read;
+  const char r = (char)replacement;
+  utf8_int32_t codepoint;
+
+  if (replacement > 0x7f) {
+    return -1;
+  }
+
+  while ('\0' != *read) {
+    if (0xf0 == (0xf8 & *read)) {
+      // ensure each of the 3 following bytes in this 4-byte
+      // utf8 codepoint began with 0b10xxxxxx
+      if ((0x80 != (0xc0 & read[1])) || (0x80 != (0xc0 & read[2])) ||
+          (0x80 != (0xc0 & read[3]))) {
+        *write++ = r;
+        read++;
+        continue;
+      }
+
+      // 4-byte utf8 code point (began with 0b11110xxx)
+      read = (char *)utf8codepoint(read, &codepoint);
+      write = (char *)utf8catcodepoint(write, codepoint, 4);
+    } else if (0xe0 == (0xf0 & *read)) {
+      // ensure each of the 2 following bytes in this 3-byte
+      // utf8 codepoint began with 0b10xxxxxx
+      if ((0x80 != (0xc0 & read[1])) || (0x80 != (0xc0 & read[2]))) {
+        *write++ = r;
+        read++;
+        continue;
+      }
+
+      // 3-byte utf8 code point (began with 0b1110xxxx)
+      read = (char *)utf8codepoint(read, &codepoint);
+      write = (char *)utf8catcodepoint(write, codepoint, 3);
+    } else if (0xc0 == (0xe0 & *read)) {
+      // ensure the 1 following byte in this 2-byte
+      // utf8 codepoint began with 0b10xxxxxx
+      if (0x80 != (0xc0 & read[1])) {
+        *write++ = r;
+        read++;
+        continue;
+      }
+
+      // 2-byte utf8 code point (began with 0b110xxxxx)
+      read = (char *)utf8codepoint(read, &codepoint);
+      write = (char *)utf8catcodepoint(write, codepoint, 2);
+    } else if (0x00 == (0x80 & *read)) {
+      // 1-byte ascii (began with 0b0xxxxxxx)
+      read = (char *)utf8codepoint(read, &codepoint);
+      write = (char *)utf8catcodepoint(write, codepoint, 1);
+    } else {
+      // if we got here then we've got a dangling continuation (0b10xxxxxx)
+      *write++ = r;
+      read++;
+      continue;
+    }
+  }
+
+  *write = '\0';
+
+  return 0;
+}
+
 void *utf8codepoint(const void *utf8_restrict str,
                     utf8_int32_t *utf8_restrict out_codepoint) {
   const char *s = (const char *)str;
@@ -1015,7 +1084,7 @@ void *utf8codepoint(const void *utf8_restrict str,
   return (void *)s;
 }
 
-size_t utf8codepointcalcsize(const void *utf8_restrict str) {
+size_t utf8codepointcalcsize(const void *str) {
   const char *s = (const char *)str;
 
   if (0xf0 == (0xf8 & s[0])) {
@@ -1045,7 +1114,7 @@ size_t utf8codepointsize(utf8_int32_t chr) {
   }
 }
 
-void *utf8catcodepoint(void *utf8_restrict str, utf8_int32_t chr, size_t n) {
+void *utf8catcodepoint(void *str, utf8_int32_t chr, size_t n) {
   char *s = (char *)str;
 
   if (0 == ((utf8_int32_t)0xffffff80 & chr)) {
@@ -1062,7 +1131,7 @@ void *utf8catcodepoint(void *utf8_restrict str, utf8_int32_t chr, size_t n) {
     if (n < 2) {
       return utf8_null;
     }
-    s[0] = 0xc0 | (char)(chr >> 6);
+    s[0] = 0xc0 | (char)((chr >> 6) & 0x1f);
     s[1] = 0x80 | (char)(chr & 0x3f);
     s += 2;
   } else if (0 == ((utf8_int32_t)0xffff0000 & chr)) {
@@ -1071,7 +1140,7 @@ void *utf8catcodepoint(void *utf8_restrict str, utf8_int32_t chr, size_t n) {
     if (n < 3) {
       return utf8_null;
     }
-    s[0] = 0xe0 | (char)(chr >> 12);
+    s[0] = 0xe0 | (char)((chr >> 12) & 0x0f);
     s[1] = 0x80 | (char)((chr >> 6) & 0x3f);
     s[2] = 0x80 | (char)(chr & 0x3f);
     s += 3;
@@ -1081,7 +1150,7 @@ void *utf8catcodepoint(void *utf8_restrict str, utf8_int32_t chr, size_t n) {
     if (n < 4) {
       return utf8_null;
     }
-    s[0] = 0xf0 | (char)(chr >> 18);
+    s[0] = 0xf0 | (char)((chr >> 18) & 0x07);
     s[1] = 0x80 | (char)((chr >> 12) & 0x3f);
     s[2] = 0x80 | (char)((chr >> 6) & 0x3f);
     s[3] = 0x80 | (char)(chr & 0x3f);
