diff --git a/include/flecs.h b/include/flecs.h
index ad4cc0610..19e0e1812 100644
--- a/include/flecs.h
+++ b/include/flecs.h
@@ -100,7 +100,10 @@ typedef struct ecs_world_info_t {
 
 /** Action callback for systems and triggers */
 typedef void (*ecs_iter_action_t)(
-    ecs_iter_t *data);
+    ecs_iter_t *it);
+
+typedef bool (*ecs_iter_next_action_t)(
+    ecs_iter_t *it);
 
 /** Compare callback used for sorting */
 typedef int (*ecs_compare_action_t)(
@@ -1462,18 +1465,18 @@ bool ecs_query_next_worker(
  *
  * @param world The world.
  * @param query The query.
- * @param sort_component The component used to sort.
+ * @param component The component used to sort.
  * @param compare The compare function used to sort the components.
  */
 FLECS_EXPORT
-void ecs_query_sort(
+void ecs_query_order_by(
     ecs_world_t *world,
     ecs_query_t *query,
-    ecs_entity_t sort_component,
+    ecs_entity_t component,
     ecs_compare_action_t compare);
 
-/** Sort the matched types (tables) of a query.
- * Similar yo ecs_query_sort, but instead of sorting individual entities, this
+/** Group and sort matched tables.
+ * Similar yo ecs_query_order_by, but instead of sorting individual entities, this
  * operation only sorts matched tables. This can be useful of a query needs to
  * enforce a certain iteration order upon the tables it is iterating, for 
  * example by giving a certain component or tag a higher priority.
@@ -1488,14 +1491,14 @@ void ecs_query_sort(
  *
  * @param world The world.
  * @param query The query.
- * @param monitor_component A component passed to the rank function.
+ * @param component The component used to determine the group rank.
  * @param rank_action The rank action.
  */
 FLECS_EXPORT
-void ecs_query_sort_types(
+void ecs_query_group_by(
     ecs_world_t *world,
     ecs_query_t *query,
-    ecs_entity_t monitor_component,
+    ecs_entity_t component,
     ecs_rank_type_action_t rank_action);
 
 
@@ -1751,6 +1754,19 @@ ecs_iter_t ecs_scope_iter(
     ecs_world_t *world,
     ecs_entity_t parent);
 
+/** Return a filtered scope iterator.
+ * Same as ecs_scope_iter, but results will be filtered.
+ *
+ * @param world The world.
+ * @param parent The parent entity for which to iterate the children.
+ * @return The iterator.
+ */
+FLECS_EXPORT
+ecs_iter_t ecs_scope_iter_w_filter(
+    ecs_world_t *world,
+    ecs_entity_t parent,
+    ecs_filter_t *filter);
+
 /** Progress the scope iterator.
  * This operation progresses the scope iterator to the next table. The iterator
  * must have been initialized with `ecs_scope_iter`. This operation must be
diff --git a/include/flecs/support/api_defines.h b/include/flecs/support/api_defines.h
index 24973321f..62fdeb0a9 100644
--- a/include/flecs/support/api_defines.h
+++ b/include/flecs/support/api_defines.h
@@ -19,8 +19,11 @@ extern "C" {
 #include <stdlib.h>
 #include <assert.h>
 
-/* Non-standard but required. If not provided by platform, add manually. */
+/* Non-standard but required. If not provided by platform, add manually. If
+ * flecs is built by bake, stdint.h from bake is included. */
+#ifndef __BAKE__
 #include <stdint.h>
+#endif
 
 /* Contains macro's for importing / exporting symbols */
 #include "flecs/bake_config.h"
diff --git a/include/flecs/support/api_types.h b/include/flecs/support/api_types.h
index f2a0117f0..e6fcbc7b3 100644
--- a/include/flecs/support/api_types.h
+++ b/include/flecs/support/api_types.h
@@ -54,6 +54,7 @@ typedef struct ecs_entities_t {
 
 /** Scope-iterator specific data */
 typedef struct ecs_scope_iter_t {
+    ecs_filter_t filter;
     ecs_vector_t *tables;
     int32_t index;
 } ecs_scope_iter_t;
diff --git a/src/api_support.c b/src/api_support.c
index 683be0253..57c47a6b5 100644
--- a/src/api_support.c
+++ b/src/api_support.c
@@ -195,7 +195,6 @@ ecs_entity_t ecs_new_entity(
     const char *expr)
 {
     EcsType type = type_from_expr(world, name, expr);
-
     ecs_entity_t result = lookup(world, name, type.normalized);
     if (!result) {
         result = e ? e : ecs_new(world, 0);
diff --git a/src/bootstrap.c b/src/bootstrap.c
index cdc17b314..36bd6a66f 100644
--- a/src/bootstrap.c
+++ b/src/bootstrap.c
@@ -192,7 +192,9 @@ void ecs_bootstrap(
 
     /* Initialize scopes */
     ecs_set(world, EcsFlecs, EcsName, {"flecs"});
+    ecs_add_entity(world, EcsFlecs, EcsModule);
     ecs_set(world, EcsFlecsCore, EcsName, {"core"});
+    ecs_add_entity(world, EcsFlecsCore, EcsModule);
     ecs_add_entity(world, EcsFlecsCore, ECS_CHILDOF | EcsFlecs);
 
     /* Initialize EcsWorld */
@@ -205,6 +207,7 @@ void ecs_bootstrap(
     ecs_set(world, EcsSingleton, EcsName, {"$"});
     ecs_assert(ecs_get_name(world, EcsSingleton) != NULL, ECS_INTERNAL_ERROR, NULL);
     ecs_assert(ecs_lookup(world, "$") == EcsSingleton, ECS_INTERNAL_ERROR, NULL);
+    ecs_add_entity(world, EcsSingleton, ECS_CHILDOF | EcsFlecsCore);
 
     ecs_trace_1("initialize builtins");
     ecs_trace_push();
diff --git a/src/iter.c b/src/iter.c
index c88e1d14a..891b21427 100644
--- a/src/iter.c
+++ b/src/iter.c
@@ -204,6 +204,14 @@ void* ecs_table_column(
 {
     ecs_world_t *world = it->world;
     ecs_table_t *table = it->table;
+
+    ecs_assert(column_index < ecs_vector_count(table->type), 
+        ECS_INVALID_PARAMETER, NULL);
+
+    if (table->column_count < column_index) {
+        return NULL;
+    }
+
     ecs_data_t *data = ecs_table_get_data(world, table);
     ecs_column_t *column = &data->columns[column_index];
     return ecs_vector_first_t(column->data, column->size, column->alignment);
diff --git a/src/modules/pipeline.c b/src/modules/pipeline.c
index 7aae6bfc8..3ddbfa957 100644
--- a/src/modules/pipeline.c
+++ b/src/modules/pipeline.c
@@ -394,8 +394,8 @@ void EcsOnAddPipeline(
 
         /* Create the query. Sort the query by system id and phase */
         ecs_query_t *query = ecs_query_new_w_sig(world, 0, &sig);
-        ecs_query_sort(world, query, 0, compare_entity);
-        ecs_query_sort_types(world, query, pipeline, rank_phase);
+        ecs_query_order_by(world, query, 0, compare_entity);
+        ecs_query_group_by(world, query, pipeline, rank_phase);
 
         /* Build signature for pipeline build query. The build query includes
          * systems that are inactive, as an inactive system may become active as
@@ -407,8 +407,8 @@ void EcsOnAddPipeline(
 
         /* Use the same sorting functions for the build query */
         ecs_query_t *build_query = ecs_query_new_w_sig(world, 0, &sig);
-        ecs_query_sort(world, build_query, 0, compare_entity);
-        ecs_query_sort_types(world, build_query, pipeline, rank_phase);       
+        ecs_query_order_by(world, build_query, 0, compare_entity);
+        ecs_query_group_by(world, build_query, pipeline, rank_phase);       
 
         EcsPipelineQuery *pq = ecs_get_mut(
             world, pipeline, EcsPipelineQuery, NULL);
diff --git a/src/query.c b/src/query.c
index adfa32366..fe61367ba 100644
--- a/src/query.c
+++ b/src/query.c
@@ -1546,7 +1546,7 @@ bool ecs_query_next_worker(
     return true;
 }
 
-void ecs_query_sort(
+void ecs_query_order_by(
     ecs_world_t *world,
     ecs_query_t *query,
     ecs_entity_t sort_component,
@@ -1567,7 +1567,7 @@ void ecs_query_sort(
     }
 }
 
-void ecs_query_sort_types(
+void ecs_query_group_by(
     ecs_world_t *world,
     ecs_query_t *query,
     ecs_entity_t sort_component,
diff --git a/src/scope.c b/src/scope.c
index c290b1486..1f8996723 100644
--- a/src/scope.c
+++ b/src/scope.c
@@ -265,6 +265,10 @@ ecs_entity_t ecs_lookup_path_w_sep(
     ecs_entity_t cur;
     bool core_searched = false;
 
+    if (!sep) {
+        sep = ".";
+    }
+
     parent = get_parent_from_path(world, parent, &path, prefix);
 
 retry:
@@ -341,11 +345,30 @@ ecs_iter_t ecs_scope_iter(
     };
 }
 
+ecs_iter_t ecs_scope_iter_w_filter(
+    ecs_world_t *world,
+    ecs_entity_t parent,
+    ecs_filter_t *filter)
+{
+    ecs_scope_iter_t iter = {
+        .filter = *filter,
+        .tables = ecs_map_get_ptr(world->child_tables, ecs_vector_t*, parent),
+        .index = 0
+    };
+
+    return (ecs_iter_t) {
+        .world = world,
+        .iter.parent = iter
+    };
+}
+
 bool ecs_scope_next(
     ecs_iter_t *it)
 {
     ecs_scope_iter_t *iter = &it->iter.parent;
     ecs_vector_t *tables = iter->tables;
+    ecs_filter_t filter = iter->filter;
+
     int32_t count = ecs_vector_count(tables);
     int32_t i;
 
@@ -363,6 +386,12 @@ bool ecs_scope_next(
             continue;
         }
 
+        if (filter.include || filter.exclude) {
+            if (!ecs_table_match_filter(it->world, table, &filter)) {
+                continue;
+            }
+        }
+
         it->table = table;
         it->table_columns = data->columns;
         it->count = ecs_table_count(table);
diff --git a/test/api/src/Singleton.c b/test/api/src/Singleton.c
index 9ffadc07d..f294a6535 100644
--- a/test/api/src/Singleton.c
+++ b/test/api/src/Singleton.c
@@ -120,7 +120,7 @@ void Singleton_system_w_not_singleton() {
 void Singleton_lookup_singleton() {
     ecs_world_t *world = ecs_init();
 
-    ecs_entity_t e = ecs_lookup(world, "$");
+    ecs_entity_t e = ecs_lookup_fullpath(world, "$");
     test_assert(e == EcsSingleton);
 
     ecs_fini(world);
