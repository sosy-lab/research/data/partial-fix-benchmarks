diff --git a/docs/mintty.1 b/docs/mintty.1
index 0a8ba50d..89db5094 100644
--- a/docs/mintty.1
+++ b/docs/mintty.1
@@ -1918,6 +1918,20 @@ items automatically based on their icon and command line.  This can be
 overridden by setting the AppID to a custom string, in which case windows
 with the same AppID are grouped together.
 
+The AppID option supports up to 5 \fB%s\fP placeholder parameters for 
+a flexible grouping configuration, also supporting positional parameters 
+\fB%N$s\fP (N = 1..5), to be replaced with these values:
+.br
+\(en \fB%1$s\fP: system name (e.g. \fBCYGWIN\fP)
+.br
+\(en \fB%2$s\fP: system release
+.br
+\(en \fB%3$s\fP: machine type (\fBi686\fP/\fBx86_64\fP)
+.br
+\(en \fB%4$s\fP: icon name if started from shortcut
+.br
+\(en \fB%5$s\fP: WSL distribution name if selected
+
 The special value \fBAppID=@\fP causes mintty to derive an implicit AppID 
 from the WSL system name, in order to achieve WSL distribution-specific 
 taskbar grouping. This resolves taskbar grouping problems in some cases 
diff --git a/src/winmain.c b/src/winmain.c
index f5603ad3..d2e0b3e1 100644
--- a/src/winmain.c
+++ b/src/winmain.c
@@ -43,6 +43,7 @@ char * mintty_debug;
 
 #include <sys/stat.h>
 #include <fcntl.h>  // open flags
+#include <sys/utsname.h>
 
 #ifndef INT16
 #define INT16 short
@@ -111,6 +112,7 @@ static bool maxheight = false;
 static bool store_taskbar_properties = false;
 static bool prevent_pinning = false;
 bool support_wsl = false;
+wchar * wslname = 0;
 wstring wsl_basepath = W("");
 static char * wsl_guid = 0;
 static bool wsl_launch = false;
@@ -3136,7 +3138,7 @@ waccess(wstring fn, int amode)
 static int
 select_WSL(char * wsl)
 {
-  wchar * wslname = cs__mbstowcs(wsl ?: "");
+  wslname = cs__mbstowcs(wsl ?: "");
   wstring wsl_icon;
   // set --rootfs implicitly
   int err = getlxssinfo(false, wslname, &wsl_guid, &wsl_basepath, &wsl_icon);
@@ -3157,7 +3159,10 @@ select_WSL(char * wsl)
       // so an explicit config value derives AppID from wsl distro name
       set_arg_option("AppID", asform("%s.%s", APPNAME, wsl ?: "WSL"));
   }
-  free(wslname);
+  else {
+    free(wslname);
+    wslname = 0;
+  }
   return err;
 }
 
@@ -3283,54 +3288,21 @@ enum_commands(wstring commands, CMDENUMPROC cmdenum)
 
 
 static void
-configure_taskbar(void)
+configure_taskbar(wchar * app_id)
 {
   if (*cfg.task_commands) {
     enum_commands(cfg.task_commands, cmd_enum);
-    setup_jumplist(cfg.app_id, jumplist_len, jumplist_title, jumplist_cmd, jumplist_icon, jumplist_ii);
+    setup_jumplist(app_id, jumplist_len, jumplist_title, jumplist_cmd, jumplist_icon, jumplist_ii);
   }
 
 #if CYGWIN_VERSION_DLL_MAJOR >= 1007
   // initial patch (issue #471) contributed by Johannes Schindelin
-  wchar * app_id = (wchar *) cfg.app_id;
   wchar * relaunch_icon = (wchar *) cfg.icon;
   wchar * relaunch_display_name = (wchar *) cfg.app_name;
   wchar * relaunch_command = (wchar *) cfg.app_launch_cmd;
 
 #define dont_debug_properties
 
-#ifdef two_witty_ideas_with_bad_side_effects
-#warning automatic derivation of an AppId is likely not a good idea
-  // If an icon is configured but no app_id, we can derive one from the 
-  // icon in order to enable proper taskbar grouping by common icon.
-  // However, this has an undesirable side-effect if a shortcut is 
-  // pinned (presumably getting some implicit AppID from Windows) and 
-  // instances are started from there (with a different AppID...).
-  // Disabled.
-  if (relaunch_icon && *relaunch_icon && (!app_id || !*app_id)) {
-    const char * iconbasename = strrchr(cfg.icon, '/');
-    if (iconbasename)
-      iconbasename ++;
-    else {
-      iconbasename = strrchr(cfg.icon, '\\');
-      if (iconbasename)
-        iconbasename ++;
-      else
-        iconbasename = cfg.icon;
-    }
-    char * derived_app_id = malloc(strlen(iconbasename) + 7 + 1);
-    strcpy(derived_app_id, "Mintty.");
-    strcat(derived_app_id, iconbasename);
-    app_id = derived_app_id;
-  }
-  // If app_name is configured but no app_launch_cmd, we need an app_id 
-  // to make app_name effective as taskbar title, so invent one.
-  if (relaunch_display_name && *relaunch_display_name && 
-      (!app_id || !*app_id)) {
-    app_id = "Mintty.AppID";
-  }
-#endif
-
   // Set the app ID explicitly, as well as the relaunch command and display name
   if (prevent_pinning || (app_id && *app_id)) {
     HMODULE shell = load_sys_library("shell32.dll");
@@ -4175,14 +4147,44 @@ main(int argc, char *argv[])
     delete(icon_file);
   }
 
+  // Expand AppID placeholders
+  wchar * app_id = (wchar *)cfg.app_id;
+  if (wcschr(app_id, '%')) {
+    wchar * pc = app_id;
+    int pcn = 0;
+    while (*pc)
+      if (*pc++ == '%')
+        pcn++;
+    struct utsname name;
+    if (pcn <= 5 && uname(&name) >= 0) {
+      char * _ = strchr(name.sysname, '_');
+      if (_)
+        *_ = 0;
+      char * fmt = cs__wcstoutf(app_id);
+      char * icon = cs__wcstoutf(icon_is_from_shortcut ? cfg.icon : W(""));
+      char * wsln = cs__wcstoutf(wslname ?: W(""));
+      char * ai = asform(fmt,
+                         name.sysname,
+                         name.release,
+                         name.machine,
+                         icon,
+                         wsln);
+      app_id = cs__utftowcs(ai);
+      free(ai);
+      free(wsln);
+      free(icon);
+      free(fmt);
+    }
+  }
+
   // Set the AppID if specified and the required function is available.
-  if (*cfg.app_id && wcscmp(cfg.app_id, W("@")) != 0) {
+  if (*app_id && wcscmp(app_id, W("@")) != 0) {
     HMODULE shell = load_sys_library("shell32.dll");
     HRESULT (WINAPI *pSetAppID)(PCWSTR) =
       (void *)GetProcAddress(shell, "SetCurrentProcessExplicitAppUserModelID");
 
     if (pSetAppID)
-      pSetAppID(cfg.app_id);
+      pSetAppID(app_id);
   }
 
   inst = GetModuleHandle(NULL);
@@ -4464,7 +4466,7 @@ main(int argc, char *argv[])
     }
   }
 
-  configure_taskbar();
+  configure_taskbar(app_id);
 
   // The input method context.
   imc = ImmGetContext(wnd);
diff --git a/wiki/Changelog.md b/wiki/Changelog.md
index dfc25e29..b82bcddb 100644
--- a/wiki/Changelog.md
+++ b/wiki/Changelog.md
@@ -1,7 +1,7 @@
 Terminal features
   * Copy as HTML (#811).
   * Mitigate stalling on very long paste buffer lines (#810).
-  * Support DECLL (VT100, xterm) to switch keyboard LEDs (and their associated modifier function).
+  * New CSI DECLL (VT100, xterm) to switch keyboard LEDs (and their associated modifier function).
   * New CSI > 0/2 p to switch option HideMouse (xterm pointerMode).
 
 Appearance
@@ -14,6 +14,9 @@ Window handling
   * Setting MINTTY_SHORTCUT when started from a desktop shortcut.
   * Maintain proper terminal size after DPI change in DPI awareness mode V2 (#774).
 
+Configuration
+  * AppID supports placeholders for flexible customization of taskbar icon grouping behaviour (#784, mintty/wsltty#96, ?#495, ?#420, ??#801).
+
 ### 2.9.3 (4 October 2018) ###
 
 Terminal features
diff --git a/wiki/Tips.md b/wiki/Tips.md
index e1e02940..dcbda5ec 100644
--- a/wiki/Tips.md
+++ b/wiki/Tips.md
@@ -59,6 +59,8 @@ items automatically based on their icon and command line.  This can be
 overridden by setting the AppID to a custom string, in which case windows
 with the same AppID are grouped together.
 
+The AppID supports placeholder parameters for a flexible grouping 
+configuration (see manual).
 The special value `AppID=@` causes mintty to derive an implicit AppID 
 from the WSL system name, in order to achieve WSL distribution-specific 
 taskbar grouping. This resolves taskbar grouping problems in some cases 
