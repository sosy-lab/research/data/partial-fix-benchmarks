diff --git a/api/docs/release.dox b/api/docs/release.dox
index 09975dbd31..21c16a85de 100644
--- a/api/docs/release.dox
+++ b/api/docs/release.dox
@@ -152,6 +152,7 @@ Further non-compatibility-affecting changes include:
  - Added drfront_set_verbose() to obtain diagnostics from drfrontendlib.
  - Added new fields to #dr_os_version_info_t which contain the build number,
    edition, and Windows 10 release identifier.
+ - Added the function instr_is_xsave().
 
 **************************************************
 <hr>
diff --git a/core/arch/instr.h b/core/arch/instr.h
index 8744b9e48a..03ba7d301b 100644
--- a/core/arch/instr.h
+++ b/core/arch/instr.h
@@ -1759,6 +1759,13 @@ DR_API
  */
 bool
 instr_zeroes_ymmh(instr_t *instr);
+
+DR_API
+/** Returns true if \p instr's opcode is #OP_xsave32, #OP_xsaveopt32, #OP_xsave64,
+ * #OP_xsaveopt64, #OP_xsavec32 or #OP_xsavec64.
+ */
+bool
+instr_is_xsave(instr_t *instr);
 #endif
 
 /* DR_API EXPORT BEGIN */
diff --git a/core/arch/instr_shared.c b/core/arch/instr_shared.c
index c46e689f74..838e885761 100644
--- a/core/arch/instr_shared.c
+++ b/core/arch/instr_shared.c
@@ -1966,6 +1966,16 @@ instr_zeroes_ymmh(instr_t *instr)
     }
     return false;
 }
+
+bool
+instr_is_xsave(instr_t *instr)
+{
+    int opcode = instr_get_opcode(instr); /* force decode */
+    if (opcode == OP_xsave32 || opcode == OP_xsaveopt32 || opcode == OP_xsave64 ||
+        opcode == OP_xsaveopt64 || opcode == OP_xsavec32 || opcode == OP_xsavec64)
+        return true;
+    return false;
+}
 #endif /* X86 */
 
 #if defined(X64) || defined(ARM)
diff --git a/core/arch/opnd_shared.c b/core/arch/opnd_shared.c
index 97fd550689..6cd48aa28c 100644
--- a/core/arch/opnd_shared.c
+++ b/core/arch/opnd_shared.c
@@ -1577,7 +1577,8 @@ opnd_size_in_bytes(opnd_size_t size)
     case OPSZ_128: return 128;
     case OPSZ_512: return 512;
     case OPSZ_VAR_REGLIST: return 0; /* varies to match reglist operand */
-    case OPSZ_xsave: return 0;       /* > 512 bytes: use cpuid to determine */
+    case OPSZ_xsave:
+        return 0; /* > 512 bytes: client to use drutil_opnd_mem_size_in_bytes */
     default: CLIENT_ASSERT(false, "opnd_size_in_bytes: invalid opnd type"); return 0;
     }
 }
diff --git a/ext/drutil/drutil.c b/ext/drutil/drutil.c
index ffb18fa63c..cf63233804 100644
--- a/ext/drutil/drutil.c
+++ b/ext/drutil/drutil.c
@@ -58,12 +58,57 @@
 /* for inserting an app instruction, which must have a translation ("xl8") field */
 #define PREXL8 instrlist_preinsert
 
+#ifdef X86
+static uint drutil_xsave_area_size;
+#endif
+
 /***************************************************************************
  * INIT
  */
 
 static int drutil_init_count;
 
+#ifdef X86
+
+static inline void
+native_unix_cpuid(uint *eax, uint *ebx, uint *ecx, uint *edx)
+{
+#    ifdef UNIX
+    /* We need to do this ebx trick, because ebx might be used for fPIC,
+     * and gcc < 5 chokes on it. This can get removed and replaced by
+     * a "=b" constraint when moving to gcc-5.
+     */
+    asm volatile("xchgl\t%%ebx, %k1\n\t"
+                 "cpuid\n\t"
+                 "xchgl\t%%ebx, %k1\n\t"
+                 : "=a"(*eax), "=&r"(*ebx), "=c"(*ecx), "=d"(*edx)
+                 : "0"(*eax), "2"(*ecx));
+#    endif
+}
+
+static inline void
+cpuid(uint op, uint subop, uint *eax, uint *ebx, uint *ecx, uint *edx)
+{
+#    ifdef WINDOWS
+    int output[4];
+    __cpuidex(output, op, subop);
+    /* XXX i#3469: On a Windows laptop, I inspected this and it returned 1088
+     * bytes, which is a rather unexpected number. Investigate whether this is
+     * correct.
+     */
+    *eax = output[0];
+    *ebx = output[1];
+    *ecx = output[2];
+    *edx = output[3];
+#    else
+    *eax = op;
+    *ecx = subop;
+    native_unix_cpuid(eax, ebx, ecx, edx);
+#    endif
+}
+
+#endif
+
 DR_EXPORT
 bool
 drutil_init(void)
@@ -73,6 +118,15 @@ drutil_init(void)
     if (count > 1)
         return true;
 
+#ifdef X86
+    /* XXX: we may want to re-factor and move functions like this into drx and/or
+     * using pre-existing versions in clients/drcpusim/tests/cpuid.c.
+     */
+    uint eax, ecx, edx;
+    const int proc_ext_state_main_leaf = 0xd;
+    cpuid(proc_ext_state_main_leaf, 0, &eax, &drutil_xsave_area_size, &ecx, &edx);
+#endif
+
     /* nothing yet: but putting in API up front in case need later */
 
     return true;
@@ -439,6 +493,17 @@ drutil_opnd_mem_size_in_bytes(opnd_t memref, instr_t *inst)
         uint sz = opnd_size_in_bytes(opnd_get_size(instr_get_dst(inst, 1)));
         ASSERT(opnd_is_immed_int(instr_get_src(inst, 1)), "malformed OP_enter");
         return sz * extra_pushes;
+    } else if (inst != NULL && instr_is_xsave(inst)) {
+        /* See the doxygen docs. */
+        switch (instr_get_opcode(inst)) {
+        case OP_xsave32:
+        case OP_xsave64:
+        case OP_xsaveopt32:
+        case OP_xsaveopt64:
+        case OP_xsavec32:
+        case OP_xsavec64: return drutil_xsave_area_size; break;
+        default: ASSERT(false, "unknown xsave opcode"); return 0;
+        }
     } else
 #endif /* X86 */
         return opnd_size_in_bytes(opnd_get_size(memref));
diff --git a/ext/drutil/drutil.h b/ext/drutil/drutil.h
index 7c9257c655..7e2db1b457 100644
--- a/ext/drutil/drutil.h
+++ b/ext/drutil/drutil.h
@@ -110,6 +110,26 @@ DR_EXPORT
  * to be passed in.
  * For single-instruction string loops, returns the size referenced
  * by each iteration.
+ *
+ * If the instruction is part of the xsave family of instructions, this
+ * returns an incomplete computation of the xsave instruction's written
+ * xsave area's size. Specifically, it
+ *
+ * - Ignores the user state mask components set in edx:eax, because they are
+ *   dynamic values. The real output size of xsave depends on the instruction's
+ *   user state mask AND the user state mask as supported by the CPU based on
+ *   the XCR0 control register.
+ * - Ignores supervisor state component PT
+ *   (enabled/disabled by user state component mask bit 8).
+ * - Ignores the user state component PKRU state
+ *   (enabled/disabled by user state component mask bit 9).
+ * - Ignores the xsaveopt flavor of xsave.
+ * - Ignores the xsavec flavor of xsave (compacted format).
+ *
+ * It computes the expected size for the standard format of the x87 user state
+ * component (enabled/disabled by user state component mask bit 0), the SSE user
+ * state component (bit 1), the AVX user state component (bit 2), the MPX user
+ * state components (bit 2 and 3) and the AVX-512 user state component (bit 7).
  */
 uint
 drutil_opnd_mem_size_in_bytes(opnd_t memref, instr_t *inst);
diff --git a/suite/tests/client-interface/drmgr-test.c b/suite/tests/client-interface/drmgr-test.c
index 8dbebd8aaf..4e069595f4 100644
--- a/suite/tests/client-interface/drmgr-test.c
+++ b/suite/tests/client-interface/drmgr-test.c
@@ -232,6 +232,10 @@ main(int argc, char **argv)
         Sleep(0);
 
     WaitForSingleObject(hThread, INFINITE);
+
+    char ALIGN_VAR(64) buffer[2048];
+    _xsave(buffer, -1);
+
     print("All done\n");
 
     HMODULE hmod;
@@ -296,7 +300,7 @@ main(int argc, char **argv)
     char table[2] = { 'A', 'B' };
 #    ifdef X86
     char ch;
-    /* test xlat for drutil_insert_get_mem_addr,
+    /* Test xlat for drutil_insert_get_mem_addr,
      * we do not bother to run this test on Windows side.
      */
     __asm("mov %1, %%" IF_X64_ELSE("rbx", "ebx") "\n\t"
@@ -315,6 +319,19 @@ main(int argc, char **argv)
     print("%c\n", table[1]);
 #    endif
 
+#    ifdef X86
+    /* Test xsave for drutil_opnd_mem_size_in_bytes. We're assuming that
+     * xsave support is available and enabled, which should be the case
+     * on all machines we're running on.
+     */
+    char ALIGN_VAR(64) buffer[2048];
+    __asm("or $-1, %%eax\n"
+          "\txsave %0"
+          : "=m"(buffer)
+          :
+          : "eax");
+#    endif
+
     intervals = 10;
     /* Initialize the lock on pi */
     pthread_mutex_init(&pi_lock, NULL);
diff --git a/suite/tests/client-interface/drutil-test.dll.c b/suite/tests/client-interface/drutil-test.dll.c
index 31ccffba09..0056ec739d 100644
--- a/suite/tests/client-interface/drutil-test.dll.c
+++ b/suite/tests/client-interface/drutil-test.dll.c
@@ -204,6 +204,17 @@ event_bb_insert(void *drcontext, void *tag, instrlist_t *bb, instr_t *instr,
                 dr_restore_reg(drcontext, bb, instr, reg1, SPILL_SLOT_1);
             }
         }
+#ifdef X86
+        if (instr_is_xsave(instr)) {
+            ushort size =
+                (ushort)drutil_opnd_mem_size_in_bytes(instr_get_dst(instr, 0), instr);
+            /* We're checking for a reasonable xsave area size which is at least 576
+             * bytes for the x87 + SSE user state components, or up to 1536 if AVX-512
+             * is enabled.
+             */
+            CHECK(size >= 576 && size <= 1536, "xsave area size unexpected");
+        }
+#endif
     }
     check_label_data(bb);
     return DR_EMIT_DEFAULT;
