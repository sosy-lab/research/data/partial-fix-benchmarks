diff --git a/docs/02.API-REFERENCE.md b/docs/02.API-REFERENCE.md
index 01f04aafb9..e06eeb35f4 100644
--- a/docs/02.API-REFERENCE.md
+++ b/docs/02.API-REFERENCE.md
@@ -7551,6 +7551,105 @@ main (void)
 - [jerry_create_external_function](#jerry_create_external_function)
 
 
+## jerry_get_resurce_name
+
+**Summary**
+
+Get the resource name (usually a file name) of the currently executed script or the given function object.
+
+This function is typically called from native callbacks.
+
+*Notes*:
+- Returned value must be freed with [jerry_release_value](#jerry_release_value) when it
+is no longer needed.
+- This feature depends on build option (`JERRY_LINE_INFO`) and can be checked
+  in runtime with the `JERRY_FEATURE_LINE_INFO` feature enum value,
+  see: [jerry_is_feature_enabled](#jerry_is_feature_enabled).
+
+**Prototype**
+
+```c
+jerry_value_t
+jerry_get_resurce_name (void);
+```
+
+- return string value constructed from
+  - the currently executed function object's resource name, if the given value is undefined
+  - resource name of the function object, if the given value is a function object
+  - "<anonymous>", otherwise
+
+**Example**
+
+[doctest]: # (name="02.API-REFERENCE-jsresourcename.c")
+
+```c
+#include <stdio.h>
+#include <string.h>
+#include "jerryscript.h"
+
+static jerry_value_t
+resource_name_handler (const jerry_value_t function_obj,
+                       const jerry_value_t this_val,
+                       const jerry_value_t args_p[],
+                       const jerry_length_t args_count)
+{
+  jerry_value_t undefined_value = jerry_create_undefined ();
+  jerry_value_t resource_name = jerry_get_resource_name (args_count > 0 ? args_p[0] : undefined_value);
+  jerry_release_value (undefined_value);
+
+  return resource_name;
+} /* resource_name_handler */
+
+int
+main (void)
+{
+  jerry_init (JERRY_INIT_EMPTY);
+
+  jerry_value_t global = jerry_get_global_object ();
+
+  /* Register the "resourceName" method. */
+  {
+    jerry_value_t func = jerry_create_external_function (resource_name_handler);
+    jerry_value_t name = jerry_create_string ((const jerry_char_t *) "resourceName");
+    jerry_value_t result = jerry_set_property (global, name, func);
+    jerry_release_value (result);
+    jerry_release_value (name);
+    jerry_release_value (func);
+  }
+
+  jerry_release_value (global);
+
+  const jerry_char_t source[] = "function myFunction() { return resourceName() }; myFunction()";
+  const jerry_char_t resource[] = "demo.js";
+
+  jerry_value_t program = jerry_parse (resource,
+                                       sizeof (resource) - 1,
+                                       source,
+                                       sizeof (source) - 1,
+                                       JERRY_PARSE_NO_OPTS);
+
+  if (!jerry_value_is_error (program))
+  {
+    /* `run_result` contains "demo.js" */
+    jerry_value_t run_result = jerry_run (program);
+
+    /* usage of `run_result` */
+
+    jerry_release_value (run_result);
+  }
+
+  jerry_release_value (program);
+  jerry_cleanup ();
+
+  return 0;
+}
+```
+
+**See also**
+
+- [jerry_create_external_function](#jerry_create_external_function)
+
+
 # ArrayBuffer and TypedArray functions
 
 These APIs all depend on the ES2015-subset profile.
diff --git a/jerry-core/api/jerry.c b/jerry-core/api/jerry.c
index c7e28e39c9..fe4abec5fe 100644
--- a/jerry-core/api/jerry.c
+++ b/jerry-core/api/jerry.c
@@ -3339,6 +3339,47 @@ jerry_get_backtrace (uint32_t max_depth) /**< depth limit of the backtrace */
   return vm_get_backtrace (max_depth);
 } /* jerry_get_backtrace */
 
+/**
+ * Get the resource name (usually a file name) of the currently executed script or the given function object
+ *
+ * Note: returned value must be freed with jerry_release_value, when it is no longer needed
+ *
+ * @return JS string constructed from
+ *         - the currently executed function object's resource name, if the given value is undefined
+ *         - resource name of the function object, if the given value is a function object
+ *         - "<anonymous>", otherwise
+ */
+jerry_value_t
+jerry_get_resource_name (const jerry_value_t value) /**< jerry api value */
+{
+#if ENABLED (JERRY_LINE_INFO)
+  if (ecma_is_value_undefined (value))
+  {
+    if (!ecma_is_value_undefined (JERRY_CONTEXT (resource_name)))
+    {
+      return ecma_copy_value (JERRY_CONTEXT (resource_name));
+    }
+  }
+  else if (ecma_is_value_object (value))
+  {
+    ecma_object_t *obj_p = ecma_get_object_from_value (value);
+
+    if (ecma_get_object_type (obj_p) == ECMA_OBJECT_TYPE_FUNCTION
+        && !ecma_get_object_is_builtin (obj_p))
+    {
+      ecma_extended_object_t *ext_func_p = (ecma_extended_object_t *) obj_p;
+
+      const ecma_compiled_code_t *bytecode_data_p = ecma_op_function_get_compiled_code (ext_func_p);
+
+      return ecma_copy_value (ecma_op_resource_name (bytecode_data_p));
+    }
+  }
+#endif /* ENABLED (JERRY_LINE_INFO) */
+
+  JERRY_UNUSED (value);
+  return ecma_make_magic_string_value (LIT_MAGIC_STRING_RESOURCE_ANON);
+} /* jerry_get_resource_name */
+
 /**
  * Check if the given value is an ArrayBuffer object.
  *
diff --git a/jerry-core/ecma/operations/ecma-function-object.c b/jerry-core/ecma/operations/ecma-function-object.c
index 6c5152b2f7..4594f5da51 100644
--- a/jerry-core/ecma/operations/ecma-function-object.c
+++ b/jerry-core/ecma/operations/ecma-function-object.c
@@ -34,6 +34,45 @@
  * @{
  */
 
+#if ENABLED (JERRY_LINE_INFO)
+/**
+ * Get the resource name from the compiled code header
+ *
+ * @return resource name as ecma-string
+ */
+ecma_value_t
+ecma_op_resource_name (const ecma_compiled_code_t *bytecode_header_p)
+{
+  JERRY_ASSERT (bytecode_header_p != NULL);
+
+  ecma_length_t formal_params_number = 0;
+
+  if (CBC_NON_STRICT_ARGUMENTS_NEEDED (bytecode_header_p))
+  {
+    if (bytecode_header_p->status_flags & CBC_CODE_FLAGS_UINT16_ARGUMENTS)
+    {
+      cbc_uint16_arguments_t *args_p = (cbc_uint16_arguments_t *) bytecode_header_p;
+
+      formal_params_number = args_p->argument_end;
+    }
+    else
+    {
+      cbc_uint8_arguments_t *args_p = (cbc_uint8_arguments_t *) bytecode_header_p;
+
+      formal_params_number = args_p->argument_end;
+    }
+  }
+
+  uint8_t *byte_p = (uint8_t *) bytecode_header_p;
+  byte_p += ((size_t) bytecode_header_p->size) << JMEM_ALIGNMENT_LOG;
+
+  ecma_value_t *resource_name_p = (ecma_value_t *) byte_p;
+  resource_name_p -= formal_params_number;
+
+  return resource_name_p[-1];
+} /* ecma_op_resource_name */
+#endif /* ENABLED (JERRY_LINE_INFO) */
+
 /**
  * Checks whether the type is a normal or arrow function.
  *
diff --git a/jerry-core/ecma/operations/ecma-function-object.h b/jerry-core/ecma/operations/ecma-function-object.h
index 435bda1115..4348a5f649 100644
--- a/jerry-core/ecma/operations/ecma-function-object.h
+++ b/jerry-core/ecma/operations/ecma-function-object.h
@@ -28,6 +28,10 @@
 
 bool ecma_is_normal_or_arrow_function (ecma_object_type_t type);
 
+#if ENABLED (JERRY_LINE_INFO)
+ecma_value_t ecma_op_resource_name (const ecma_compiled_code_t *bytecode_header_p);
+#endif /* ENABLED (JERRY_LINE_INFO) */
+
 bool ecma_op_is_callable (ecma_value_t value);
 bool ecma_is_constructor (ecma_value_t value);
 
diff --git a/jerry-core/include/jerryscript-core.h b/jerry-core/include/jerryscript-core.h
index 218baf2843..3ee3e79bd5 100644
--- a/jerry-core/include/jerryscript-core.h
+++ b/jerry-core/include/jerryscript-core.h
@@ -600,6 +600,7 @@ jerry_context_t *jerry_create_context (uint32_t heap_size, jerry_context_alloc_t
  */
 void jerry_set_vm_exec_stop_callback (jerry_vm_exec_stop_callback_t stop_cb, void *user_p, uint32_t frequency);
 jerry_value_t jerry_get_backtrace (uint32_t max_depth);
+jerry_value_t jerry_get_resource_name (const jerry_value_t value);
 
 /**
  * Array buffer components.
diff --git a/jerry-core/lit/lit-magic-strings.inc.h b/jerry-core/lit/lit-magic-strings.inc.h
index 58cd55bdc4..9191fe8789 100644
--- a/jerry-core/lit/lit-magic-strings.inc.h
+++ b/jerry-core/lit/lit-magic-strings.inc.h
@@ -596,9 +596,7 @@ LIT_MAGIC_STRING_DEF (LIT_MAGIC_STRING_SET_UTC_DATE_UL, "setUTCDate")
 #if ENABLED (JERRY_BUILTIN_STRING) && ENABLED (JERRY_ES2015_BUILTIN)
 LIT_MAGIC_STRING_DEF (LIT_MAGIC_STRING_STARTS_WITH, "startsWith")
 #endif
-#if ENABLED (JERRY_LINE_INFO) || ENABLED (JERRY_ERROR_MESSAGES) || ENABLED (JERRY_ES2015_MODULE_SYSTEM)
 LIT_MAGIC_STRING_DEF (LIT_MAGIC_STRING_RESOURCE_ANON, "<anonymous>")
-#endif
 #if ENABLED (JERRY_ES2015_BUILTIN_DATAVIEW) \
 || ENABLED (JERRY_ES2015_BUILTIN_TYPEDARRAY)
 LIT_MAGIC_STRING_DEF (LIT_MAGIC_STRING_ARRAY_BUFFER_UL, "ArrayBuffer")
@@ -825,18 +823,7 @@ LIT_MAGIC_STRING_FIRST_STRING_WITH_SIZE (10, LIT_MAGIC_STRING_COPY_WITHIN)
 #else
 LIT_MAGIC_STRING_FIRST_STRING_WITH_SIZE (10, LIT_MAGIC_STRING_ENUMERABLE)
 #endif
-#if ENABLED (JERRY_LINE_INFO) || ENABLED (JERRY_ERROR_MESSAGES) || ENABLED (JERRY_ES2015_MODULE_SYSTEM)
 LIT_MAGIC_STRING_FIRST_STRING_WITH_SIZE (11, LIT_MAGIC_STRING_RESOURCE_ANON)
-#elif ENABLED (JERRY_ES2015_BUILTIN_DATAVIEW) \
-|| ENABLED (JERRY_ES2015_BUILTIN_TYPEDARRAY)
-LIT_MAGIC_STRING_FIRST_STRING_WITH_SIZE (11, LIT_MAGIC_STRING_ARRAY_BUFFER_UL)
-#elif ENABLED (JERRY_BUILTIN_ERRORS)
-LIT_MAGIC_STRING_FIRST_STRING_WITH_SIZE (11, LIT_MAGIC_STRING_SYNTAX_ERROR_UL)
-#elif ENABLED (JERRY_ES2015_BUILTIN_TYPEDARRAY)
-LIT_MAGIC_STRING_FIRST_STRING_WITH_SIZE (11, LIT_MAGIC_STRING_UINT16_ARRAY_UL)
-#else
-LIT_MAGIC_STRING_FIRST_STRING_WITH_SIZE (11, LIT_MAGIC_STRING_CONSTRUCTOR)
-#endif
 #if ENABLED (JERRY_ES2015_BUILTIN_TYPEDARRAY)
 LIT_MAGIC_STRING_FIRST_STRING_WITH_SIZE (12, LIT_MAGIC_STRING_FLOAT32_ARRAY_UL)
 #elif ENABLED (JERRY_ES2015_BUILTIN_TYPEDARRAY) && ENABLED (JERRY_NUMBER_TYPE_FLOAT64)
diff --git a/jerry-core/vm/vm.c b/jerry-core/vm/vm.c
index 78595bf2ed..13445e4588 100644
--- a/jerry-core/vm/vm.c
+++ b/jerry-core/vm/vm.c
@@ -3351,31 +3351,7 @@ vm_loop (vm_frame_ctx_t *frame_ctx_p) /**< frame context */
 #if ENABLED (JERRY_LINE_INFO)
         case VM_OC_RESOURCE_NAME:
         {
-          ecma_length_t formal_params_number = 0;
-
-          if (CBC_NON_STRICT_ARGUMENTS_NEEDED (bytecode_header_p))
-          {
-            if (bytecode_header_p->status_flags & CBC_CODE_FLAGS_UINT16_ARGUMENTS)
-            {
-              cbc_uint16_arguments_t *args_p = (cbc_uint16_arguments_t *) bytecode_header_p;
-
-              formal_params_number = args_p->argument_end;
-            }
-            else
-            {
-              cbc_uint8_arguments_t *args_p = (cbc_uint8_arguments_t *) bytecode_header_p;
-
-              formal_params_number = args_p->argument_end;
-            }
-          }
-
-          uint8_t *byte_p = (uint8_t *) bytecode_header_p;
-          byte_p += ((size_t) bytecode_header_p->size) << JMEM_ALIGNMENT_LOG;
-
-          ecma_value_t *resource_name_p = (ecma_value_t *) byte_p;
-          resource_name_p -= formal_params_number;
-
-          frame_ctx_p->resource_name = resource_name_p[-1];
+          frame_ctx_p->resource_name = ecma_op_resource_name (bytecode_header_p);
           continue;
         }
         case VM_OC_LINE:
diff --git a/jerry-ext/handler/handler-resource-name.c b/jerry-ext/handler/handler-resource-name.c
new file mode 100644
index 0000000000..947044374d
--- /dev/null
+++ b/jerry-ext/handler/handler-resource-name.c
@@ -0,0 +1,42 @@
+/* Copyright JS Foundation and other contributors, http://js.foundation
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *     http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+#include "jerryscript-ext/handler.h"
+
+/**
+ * Get the resource name (usually a file name) of the currently executed script or the given function object
+ *
+ * Note: returned value must be freed with jerry_release_value, when it is no longer needed
+ *
+ * @return JS string constructed from
+ *         - the currently executed function object's resource name, if the given value is undefined
+ *         - resource name of the function object, if the given value is a function object
+ *         - "<anonymous>", otherwise
+ */
+jerry_value_t
+jerryx_handler_resource_name (const jerry_value_t func_obj_val, /**< function object */
+                              const jerry_value_t this_p, /**< this arg */
+                              const jerry_value_t args_p[], /**< function arguments */
+                              const jerry_length_t args_cnt) /**< number of function arguments */
+{
+  (void) func_obj_val; /* unused */
+  (void) this_p; /* unused */
+
+  jerry_value_t undefined_value = jerry_create_undefined ();
+  jerry_value_t resource_name = jerry_get_resource_name (args_cnt > 0 ? args_p[0] : undefined_value);
+  jerry_release_value (undefined_value);
+
+  return resource_name;
+} /* jerryx_handler_resource_name */
diff --git a/jerry-ext/include/jerryscript-ext/handler.h b/jerry-ext/include/jerryscript-ext/handler.h
index 41bfeeb885..c36b6b8717 100644
--- a/jerry-ext/include/jerryscript-ext/handler.h
+++ b/jerry-ext/include/jerryscript-ext/handler.h
@@ -44,6 +44,8 @@ jerry_value_t jerryx_handler_gc (const jerry_value_t func_obj_val, const jerry_v
                                  const jerry_value_t args_p[], const jerry_length_t args_cnt);
 jerry_value_t jerryx_handler_print (const jerry_value_t func_obj_val, const jerry_value_t this_p,
                                     const jerry_value_t args_p[], const jerry_length_t args_cnt);
+jerry_value_t jerryx_handler_resource_name (const jerry_value_t func_obj_val, const jerry_value_t this_p,
+                                            const jerry_value_t args_p[], const jerry_length_t args_cnt);
 
 /**
  * Struct used by the `jerryx_set_functions` method to
diff --git a/jerry-main/main-unix.c b/jerry-main/main-unix.c
index 61f1073a5d..6bd4ae0b9e 100644
--- a/jerry-main/main-unix.c
+++ b/jerry-main/main-unix.c
@@ -459,6 +459,7 @@ init_engine (jerry_init_flag_t flags, /**< initialized flags for the engine */
   register_js_function ("assert", jerryx_handler_assert);
   register_js_function ("gc", jerryx_handler_gc);
   register_js_function ("print", jerryx_handler_print);
+  register_js_function ("resourceName", jerryx_handler_resource_name);
 } /* init_engine */
 
 int
diff --git a/tests/debugger/do_variables.expected b/tests/debugger/do_variables.expected
index 06f67fc7ff..10833ea82c 100644
--- a/tests/debugger/do_variables.expected
+++ b/tests/debugger/do_variables.expected
@@ -4,25 +4,27 @@ Stopped at tests/debugger/do_variables.js:15
 level | type   
 0     | global 
 (jerry-debugger) variables
-name   | type      | value     
-f      | Function  |           
-addX   | Function  |           
-z      | undefined | undefined 
-c      | undefined | undefined 
-print  | Function  |           
-gc     | Function  |           
-assert | Function  |           
+name         | type      | value     
+f            | Function  |           
+addX         | Function  |           
+z            | undefined | undefined 
+c            | undefined | undefined 
+resourceName | Function  |           
+print        | Function  |           
+gc           | Function  |           
+assert       | Function  |           
 (jerry-debugger) variables 1
 name | type | value 
 (jerry-debugger) variables 0
-name   | type      | value     
-f      | Function  |           
-addX   | Function  |           
-z      | undefined | undefined 
-c      | undefined | undefined 
-print  | Function  |           
-gc     | Function  |           
-assert | Function  |           
+name         | type      | value     
+f            | Function  |           
+addX         | Function  |           
+z            | undefined | undefined 
+c            | undefined | undefined 
+resourceName | Function  |           
+print        | Function  |           
+gc           | Function  |           
+assert       | Function  |           
 (jerry-debugger) b tests/debugger/do_variables.js:20
 Breakpoint 1 at tests/debugger/do_variables.js:20 (in function() at line:19, col:10)
 (jerry-debugger) c
@@ -40,15 +42,16 @@ n    | Number    | 9
 name | type   | value 
 x    | Number | 3     
 (jerry-debugger) variables 2
-name     | type     | value 
-addThree | Function |       
-f        | Function |       
-addX     | Function |       
-z        | Number   | 5     
-c        | Number   | 4     
-print    | Function |       
-gc       | Function |       
-assert   | Function |       
+name         | type     | value 
+addThree     | Function |       
+f            | Function |       
+addX         | Function |       
+z            | Number   | 5     
+c            | Number   | 4     
+resourceName | Function |       
+print        | Function |       
+gc           | Function |       
+assert       | Function |       
 (jerry-debugger) b tests/debugger/do_variables.js:30
 Breakpoint 2 at tests/debugger/do_variables.js:30 (in f() at line:28, col:1)
 (jerry-debugger) c
@@ -58,16 +61,17 @@ level | type
 0     | local  
 1     | global 
 (jerry-debugger) variables 1
-name     | type     | value 
-d        | Number   | 12    
-addThree | Function |       
-f        | Function |       
-addX     | Function |       
-z        | Number   | 5     
-c        | Number   | 4     
-print    | Function |       
-gc       | Function |       
-assert   | Function |       
+name         | type     | value 
+d            | Number   | 12    
+addThree     | Function |       
+f            | Function |       
+addX         | Function |       
+z            | Number   | 5     
+c            | Number   | 4     
+resourceName | Function |       
+print        | Function |       
+gc           | Function |       
+assert       | Function |       
 (jerry-debugger) variables 0
 name | type      | value     
 b    | undefined | undefined 
diff --git a/tests/jerry/regression-test-issue-2105.js b/tests/jerry/regression-test-issue-2105.js
index 4cff41b2ff..184566e4a5 100644
--- a/tests/jerry/regression-test-issue-2105.js
+++ b/tests/jerry/regression-test-issue-2105.js
@@ -51,4 +51,4 @@ try {
 /* Check properties of a */
 assert(Object.keys(a) == "one,two");
 /* Check properties of global object */
-assert(Object.keys(this) == "assert,gc,print,a,fail,fail_two");
+assert(Object.keys(this) == "assert,gc,print,resourceName,a,fail,fail_two");
diff --git a/tests/unit-core/test-resource-name.c b/tests/unit-core/test-resource-name.c
new file mode 100644
index 0000000000..ebbfa4a820
--- /dev/null
+++ b/tests/unit-core/test-resource-name.c
@@ -0,0 +1,127 @@
+/* Copyright JS Foundation and other contributors, http://js.foundation
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *     http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+#include "config.h"
+#include "jerryscript.h"
+#include "test-common.h"
+
+static jerry_value_t
+resource_name_handler (const jerry_value_t function_obj, /**< function object */
+                       const jerry_value_t this_val, /**< this value */
+                       const jerry_value_t args_p[], /**< argument list */
+                       const jerry_length_t args_count) /**< argument count */
+{
+  (void) function_obj;
+  (void) this_val;
+
+  jerry_value_t undefined_value = jerry_create_undefined ();
+  jerry_value_t resource_name = jerry_get_resource_name (args_count > 0 ? args_p[0] : undefined_value);
+  jerry_release_value (undefined_value);
+
+  return resource_name;
+} /* resource_name_handler */
+
+int
+main (void)
+{
+  TEST_INIT ();
+
+  if (!jerry_is_feature_enabled (JERRY_FEATURE_LINE_INFO))
+  {
+    jerry_port_log (JERRY_LOG_LEVEL_ERROR, "Line info support is disabled!\n");
+    return 0;
+  }
+
+  jerry_init (JERRY_INIT_EMPTY);
+
+  jerry_value_t global = jerry_get_global_object ();
+
+  /* Register the "resourceName" method. */
+  {
+    jerry_value_t func = jerry_create_external_function (resource_name_handler);
+    jerry_value_t name = jerry_create_string ((const jerry_char_t *) "resourceName");
+    jerry_value_t result = jerry_set_property (global, name, func);
+    jerry_release_value (result);
+    jerry_release_value (name);
+    jerry_release_value (func);
+  }
+
+  jerry_release_value (global);
+
+  const char *source_1 = ("function f1 () {\n"
+                          "  if (resourceName() !== 'demo1.js') return false; \n"
+                          "  if (resourceName(f1) !== 'demo1.js') return false; \n"
+                          "  if (resourceName(5) !== '<anonymous>') return false; \n"
+                          "  return f1; \n"
+                          "} \n"
+                          "f1();");
+  const char *resource_1 = "demo1.js";
+
+  jerry_value_t program = jerry_parse ((const jerry_char_t *) resource_1,
+                                       strlen (resource_1),
+                                       (const jerry_char_t *) source_1,
+                                       strlen (source_1),
+                                       JERRY_PARSE_NO_OPTS);
+  TEST_ASSERT (!jerry_value_is_error (program));
+
+  jerry_value_t run_result = jerry_run (program);
+  TEST_ASSERT (!jerry_value_is_error (run_result));
+  TEST_ASSERT (jerry_value_is_object (run_result));
+
+  jerry_value_t resource_value = jerry_get_resource_name (run_result);
+  jerry_value_t resource1_name_value = jerry_create_string ((const jerry_char_t *) resource_1);
+  TEST_ASSERT (jerry_binary_operation (JERRY_BIN_OP_STRICT_EQUAL, resource_value, resource1_name_value));
+  jerry_release_value (resource1_name_value);
+  jerry_release_value (resource_value);
+
+  jerry_release_value (run_result);
+  jerry_release_value (program);
+
+  const char *source_2 = ("function f2 () { \n"
+                          "  if (resourceName() !== 'demo2.js') return false; \n"
+                          "  if (resourceName(f2) !== 'demo2.js') return false; \n"
+                          "  if (resourceName(f1) !== 'demo1.js') return false; \n"
+                          "  if (resourceName(Object.prototype) !== '<anonymous>') return false; \n"
+                          "  if (resourceName(Function) !== '<anonymous>') return false; \n"
+                          "  return f2; \n"
+                          "} \n"
+                          "f2(); \n");
+  const char *resource_2 = "demo2.js";
+
+
+  program = jerry_parse ((const jerry_char_t *) resource_2,
+                         strlen (resource_2),
+                         (const jerry_char_t *) source_2,
+                         strlen (source_2),
+                         JERRY_PARSE_NO_OPTS);
+  TEST_ASSERT (!jerry_value_is_error (program));
+
+  run_result = jerry_run (program);
+  TEST_ASSERT (!jerry_value_is_error (run_result));
+  TEST_ASSERT (jerry_value_is_object (run_result));
+
+  resource_value = jerry_get_resource_name (run_result);
+  jerry_value_t resource2_name_value = jerry_create_string ((const jerry_char_t *) resource_2);
+  TEST_ASSERT (jerry_binary_operation (JERRY_BIN_OP_STRICT_EQUAL, resource_value, resource2_name_value));
+  jerry_release_value (resource2_name_value);
+  jerry_release_value (resource_value);
+
+  jerry_release_value (run_result);
+  jerry_release_value (program);
+
+  jerry_cleanup ();
+
+  return 0;
+} /* main */
