diff --git a/parsers/sql.c b/parsers/sql.c
index 7d6b23995f..04b7c9d8f5 100644
--- a/parsers/sql.c
+++ b/parsers/sql.c
@@ -44,6 +44,7 @@
  */
 #define isType(token,t)		(bool) ((token)->type == (t))
 #define isKeyword(token,k)	(bool) ((token)->keyword == (k))
+#define isReservedWord(token) ((token)->reserved_word)
 #define isIdentChar1(c) \
 	/*
 	 * Other databases are less restrictive on the first character of
@@ -174,6 +175,7 @@ typedef enum eTokenType {
 typedef struct sTokenInfoSQL {
 	tokenType	type;
 	keywordId	keyword;
+	bool        reserved_word;
 	vString *	string;
 	vString *	scope;
 	int         scopeKind;
@@ -241,85 +243,111 @@ static kindDefinition SqlKinds [] = {
 	{ true,  'z', "mlprop",		  "MobiLink Properties "   }
 };
 
+/* Injecting a bit representing whether a keyword is "reserved word" or not.
+ * "reserved word" cannot be used as an name.
+ * See https://dev.mysql.com/doc/refman/8.0/en/keywords.html about the
+ * difference between keywords and the reserved words.
+ *
+ * We will mark a keyword as a reserved word only if all the SQL dialects
+ * specify it as a reserved word.
+ *
+ * MYSQL
+ * => https://dev.mysql.com/doc/refman/8.0/en/keywords.html
+ * POSTGRESQL,SQL2016,SQL2011,SQL92
+ * => https://www.postgresql.org/docs/12/sql-keywords-appendix.html
+ * ORACLE11g, PLSQL
+ * => https://docs.oracle.com/cd/B28359_01/appdev.111/b31231/appb.htm#CJHIIICD
+ * SQLANYWERE
+ * => http://dcx.sap.com/1200/en/dbreference/alhakeywords.html
+ */
+#define RESERVED_WORD_BITS 1
+#define RESERVED_WORD_MASK 0x1
+#define KEYWORD(K,IS_RESERVED_WORD) (((KEYWORD_##K) << RESERVED_WORD_BITS) | (IS_RESERVED_WORD))
+
 static const keywordTable SqlKeywordTable [] = {
-	/* keyword		keyword ID */
-	{ "as",								KEYWORD_is				      },
-	{ "at",								KEYWORD_at				      },
-	{ "begin",							KEYWORD_begin			      },
-	{ "body",							KEYWORD_body			      },
-	{ "call",							KEYWORD_call			      },
-	{ "case",							KEYWORD_case			      },
-	{ "check",							KEYWORD_check			      },
-	{ "comment",						KEYWORD_comment			      },
-	{ "constraint",						KEYWORD_constraint		      },
-	{ "create",							KEYWORD_create				  },
-	{ "cursor",							KEYWORD_cursor			      },
-	{ "datatype",						KEYWORD_datatype		      },
-	{ "declare",						KEYWORD_declare			      },
-	{ "do",								KEYWORD_do				      },
-	{ "domain",							KEYWORD_domain				  },
-	{ "drop",							KEYWORD_drop			      },
-	{ "else",							KEYWORD_else			      },
-	{ "elseif",							KEYWORD_elseif			      },
-	{ "end",							KEYWORD_end				      },
-	{ "endif",							KEYWORD_endif			      },
-	{ "event",							KEYWORD_event			      },
-	{ "exception",						KEYWORD_exception		      },
-	{ "external",						KEYWORD_external		      },
-	{ "for",							KEYWORD_for				      },
-	{ "foreign",						KEYWORD_foreign			      },
-	{ "from",							KEYWORD_from			      },
-	{ "function",						KEYWORD_function		      },
-	{ "go",								KEYWORD_go				      },
-	{ "handler",						KEYWORD_handler			      },
-	{ "if",								KEYWORD_if				      },
-	{ "index",							KEYWORD_index			      },
-	{ "internal",						KEYWORD_internal		      },
-	{ "is",								KEYWORD_is				      },
-	{ "local",							KEYWORD_local			      },
-	{ "loop",							KEYWORD_loop			      },
-	{ "ml_add_connection_script",		KEYWORD_ml_conn			      },
-	{ "ml_add_dnet_connection_script",	KEYWORD_ml_conn_dnet	      },
-	{ "ml_add_dnet_table_script",		KEYWORD_ml_table_dnet	      },
-	{ "ml_add_java_connection_script",	KEYWORD_ml_conn_java	      },
-	{ "ml_add_java_table_script",		KEYWORD_ml_table_java	      },
-	{ "ml_add_lang_conn_script_chk",	KEYWORD_ml_conn_chk 	      },
-	{ "ml_add_lang_connection_script",	KEYWORD_ml_conn_lang	      },
-	{ "ml_add_lang_table_script",		KEYWORD_ml_table_lang	      },
-	{ "ml_add_lang_table_script_chk",	KEYWORD_ml_table_chk	      },
-	{ "ml_add_property",				KEYWORD_ml_prop		 	      },
-	{ "ml_add_table_script",			KEYWORD_ml_table		      },
-	{ "object",							KEYWORD_object			      },
-	{ "on",								KEYWORD_on				      },
-	{ "package",						KEYWORD_package			      },
-	{ "pragma",							KEYWORD_pragma			      },
-	{ "primary",						KEYWORD_primary			      },
-	{ "procedure",						KEYWORD_procedure		      },
-	{ "publication",					KEYWORD_publication		      },
-	{ "record",							KEYWORD_record			      },
-	{ "ref",							KEYWORD_ref				      },
-	{ "references",						KEYWORD_references		      },
-	{ "rem",							KEYWORD_rem				      },
-	{ "result",							KEYWORD_result			      },
-	{ "return",							KEYWORD_return			      },
-	{ "returns",						KEYWORD_returns			      },
-	{ "select",							KEYWORD_select			      },
-	{ "service",						KEYWORD_service			      },
-	{ "subtype",						KEYWORD_subtype			      },
-	{ "synonym",						KEYWORD_synonym			      },
-	{ "table",							KEYWORD_table			      },
-	{ "temporary",						KEYWORD_temporary		      },
-	{ "then",							KEYWORD_then			      },
-	{ "trigger",						KEYWORD_trigger			      },
-	{ "type",							KEYWORD_type			      },
-	{ "unique",							KEYWORD_unique			      },
-	{ "url",							KEYWORD_url				      },
-	{ "variable",						KEYWORD_variable		      },
-	{ "view",							KEYWORD_view			      },
-	{ "when",							KEYWORD_when			      },
-	{ "while",							KEYWORD_while			      },
-	{ "with",							KEYWORD_with			      },
-	{ "without",						KEYWORD_without			      },
+    /* keyword                          keyword ID (with "reserved word" bit)
+	 *
+	 * RESERVED_BIT: MYSQL & POSTGRESQL&SQL2016&SQL2011&SQL92 & ORACLE11g&PLSQL & SQLANYWERE
+	 *
+	 */
+
+    { "as",                             KEYWORD(is,           1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "at",                             KEYWORD(at,           0 & 0&1&1&1 & 0&1 & 0 ) },
+    { "begin",                          KEYWORD(begin,        0 & 0&1&1&1 & 0&1 & 1 ) },
+    { "body",                           KEYWORD(body,         0 & 0&0&0&0 & 0&1 & 0 ) },
+    { "call",                           KEYWORD(call,         1 & 0&1&1&0 & 0&0 & 1 ) },
+    { "case",                           KEYWORD(case,         1 & 1&1&1&1 & 0&1 & 1 ) },
+    { "check",                          KEYWORD(check,        1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "comment",                        KEYWORD(comment,      0 & 0&0&0&0 & 1&1 & 1 ) },
+    { "constraint",                     KEYWORD(constraint,   1 & 1&1&1&1 & 0&1 & 1 ) },
+    { "create",                         KEYWORD(create,       1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "cursor",                         KEYWORD(cursor,       1 & 0&1&1&1 & 0&1 & 1 ) },
+    { "datatype",                       KEYWORD(datatype,     0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "declare",                        KEYWORD(declare,      1 & 0&1&1&1 & 0&1 & 1 ) },
+    { "do",                             KEYWORD(do,           0 & 1&0&0&0 & 0&1 & 1 ) },
+    { "domain",                         KEYWORD(domain,       0 & 0&0&0&1 & 0&0 & 0 ) },
+    { "drop",                           KEYWORD(drop,         1 & 0&1&1&1 & 1&1 & 1 ) },
+    { "else",                           KEYWORD(else,         1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "elseif",                         KEYWORD(elseif,       1 & 0&0&0&0 & 0&0 & 1 ) },
+    { "end",                            KEYWORD(end,          0 & 1&1&1&1 & 0&1 & 1 ) },
+    { "endif",                          KEYWORD(endif,        0 & 0&0&0&0 & 0&0 & 1 ) },
+    { "event",                          KEYWORD(event,        0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "exception",                      KEYWORD(exception,    0 & 0&0&0&1 & 0&1 & 1 ) },
+    { "external",                       KEYWORD(external,     0 & 0&1&1&1 & 0&0 & 0 ) },
+    { "for",                            KEYWORD(for,          1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "foreign",                        KEYWORD(foreign,      1 & 1&1&1&1 & 0&0 & 1 ) },
+    { "from",                           KEYWORD(from,         1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "function",                       KEYWORD(function,     1 & 0&1&1&0 & 0&1 & 0 ) },
+    { "go",                             KEYWORD(go,           0 & 0&0&0&1 & 0&0 & 0 ) },
+    { "handler",                        KEYWORD(handler,      0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "if",                             KEYWORD(if,           1 & 0&0&0&0 & 0&1 & 1 ) },
+    { "index",                          KEYWORD(index,        1 & 0&0&0&0 & 1&1 & 1 ) },
+    { "internal",                       KEYWORD(internal,     1 & 0&1&1&0 & 0&0 & 0 ) },
+    { "is",                             KEYWORD(is,           1 & 0&1&1&1 & 1&1 & 1 ) },
+    { "local",                          KEYWORD(local,        0 & 0&1&1&1 & 0&0 & 0 ) },
+    { "loop",                           KEYWORD(loop,         1 & 1&1&1&1 & 0&1 & 0 ) },
+    { "ml_add_connection_script",       KEYWORD(ml_conn,      0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "ml_add_dnet_connection_script",  KEYWORD(ml_conn_dnet, 0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "ml_add_dnet_table_script",       KEYWORD(ml_table_dnet,0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "ml_add_java_connection_script",  KEYWORD(ml_conn_java, 0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "ml_add_java_table_script",       KEYWORD(ml_table_java,0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "ml_add_lang_conn_script_chk",    KEYWORD(ml_conn_chk,  0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "ml_add_lang_connection_script",  KEYWORD(ml_conn_lang, 0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "ml_add_lang_table_script",       KEYWORD(ml_table_lang,0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "ml_add_lang_table_script_chk",   KEYWORD(ml_table_chk, 0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "ml_add_property",                KEYWORD(ml_prop,      0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "ml_add_table_script",            KEYWORD(ml_table,     0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "object",                         KEYWORD(object,       0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "on",                             KEYWORD(on,           1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "package",                        KEYWORD(package,      0 & 0&0&0&0 & 0&1 & 0 ) },
+    { "pragma",                         KEYWORD(pragma,       0 & 0&0&0&0 & 0&1 & 0 ) },
+    { "primary",                        KEYWORD(primary,      1 & 1&1&1&1 & 0&0 & 1 ) },
+    { "procedure",                      KEYWORD(procedure,    1 & 0&0&0&0 & 0&1 & 1 ) },
+    { "publication",                    KEYWORD(publication,  0 & 0&0&0&0 & 0&0 & 1 ) },
+    { "record",                         KEYWORD(record,       0 & 0&0&0&0 & 0&1 & 0 ) },
+    { "ref",                            KEYWORD(ref,          0 & 0&1&1&0 & 0&0 & 0 ) },
+    { "references",                     KEYWORD(references,   1 & 1&1&1&1 & 0&0 & 1 ) },
+    { "rem",                            KEYWORD(rem,          0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "result",                         KEYWORD(result,       0 & 0&1&1&0 & 0&0 & 0 ) },
+    { "return",                         KEYWORD(return,       1 & 0&1&1&0 & 0&1 & 1 ) },
+    { "returns",                        KEYWORD(returns,      0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "select",                         KEYWORD(select,       1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "service",                        KEYWORD(service,      0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "subtype",                        KEYWORD(subtype,      0 & 0&0&0&0 & 0&1 & 0 ) },
+    { "synonym",                        KEYWORD(synonym,      0 & 0&0&0&0 & 1&0 & 0 ) },
+    { "table",                          KEYWORD(table,        1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "temporary",                      KEYWORD(temporary,    0 & 0&0&0&1 & 0&0 & 1 ) },
+    { "then",                           KEYWORD(then,         1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "trigger",                        KEYWORD(trigger,      1 & 0&1&1&0 & 1&0 & 1 ) },
+    { "type",                           KEYWORD(type,         0 & 0&0&0&0 & 0&1 & 0 ) },
+    { "unique",                         KEYWORD(unique,       1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "url",                            KEYWORD(url,          0 & 0&0&0&0 & 0&0 & 0 ) },
+    { "variable",                       KEYWORD(variable,     0 & 0&0&0&0 & 0&0 & 1 ) },
+    { "view",                           KEYWORD(view,         0 & 0&0&0&1 & 1&1 & 1 ) },
+    { "when",                           KEYWORD(when,         1 & 1&1&1&1 & 0&1 & 1 ) },
+    { "while",                          KEYWORD(while,        1 & 0&0&0&0 & 0&1 & 1 ) },
+    { "with",                           KEYWORD(with,         1 & 1&1&1&1 & 1&1 & 1 ) },
+    { "without",                        KEYWORD(without,      0 & 0&1&1&0 & 0&0 & 0 ) },
 };
 
 /*
@@ -400,6 +428,7 @@ static tokenInfo *newToken (void)
 
 	token->type               = TOKEN_UNDEFINED;
 	token->keyword            = KEYWORD_NONE;
+	token->reserved_word      = false;
 	token->string             = vStringNew ();
 	token->scope              = vStringNew ();
 	token->scopeKind          = SQLTAG_COUNT;
@@ -558,6 +587,7 @@ static void readToken (tokenInfo *const token)
 
 	token->type			= TOKEN_UNDEFINED;
 	token->keyword		= KEYWORD_NONE;
+	token->reserved_word = false;
 	vStringClear (token->string);
 
 getNextChar:
@@ -685,7 +715,18 @@ static void readToken (tokenInfo *const token)
 					  parseIdentifier (token->string, c);
 					  token->lineNumber = getInputLineNumber ();
 					  token->filePosition = getInputFilePosition ();
-					  token->keyword = lookupCaseKeyword (vStringValue (token->string), Lang_sql);
+					  int k = lookupCaseKeyword (vStringValue (token->string), Lang_sql);
+					  if (k == KEYWORD_NONE)
+					  {
+						  token->keyword = KEYWORD_NONE;
+						  token->reserved_word =false;
+					  }
+					  else
+					  {
+						  token->keyword = (k >> RESERVED_WORD_BITS);
+						  token->reserved_word = (bool)(k & RESERVED_WORD_MASK);
+					  }
+
 					  if (isKeyword (token, KEYWORD_rem))
 					  {
 						  vStringClear (token->string);
@@ -857,6 +898,7 @@ static void copyToken (tokenInfo *const dest, tokenInfo *const src)
 	dest->filePosition = src->filePosition;
 	dest->type = src->type;
 	dest->keyword = src->keyword;
+	dest->reserved_word = src->reserved_word;
 	vStringCopy(dest->string, src->string);
 	vStringCopy(dest->scope, src->scope);
 	dest->scopeKind = src->scopeKind;
