diff --git a/CHANGES.md b/CHANGES.md
index c8f8e503ee1d..def93b8ff57f 100644
--- a/CHANGES.md
+++ b/CHANGES.md
@@ -22,8 +22,12 @@ OpenSSL 3.0
 -----------
 
 ### Changes between 1.1.1 and 3.0 [xx XXX xxxx]
+ * The deprecated function EVP_PKEY_get0() now returns NULL being called for a
+   provided key.
 
- * The deprecated functions EVP_PKEY_get0(), EVP_PKEY_get0_RSA(),
+   *Dmitry Belyavskiy*
+
+ * The deprecated functions EVP_PKEY_get0_RSA(),
    EVP_PKEY_get0_DSA(), EVP_PKEY_get0_EC_KEY(), EVP_PKEY_get0_DH(),
    EVP_PKEY_get0_hmac(), EVP_PKEY_get0_poly1305() and EVP_PKEY_get0_siphash() as
    well as the similarly named "get1" functions behave slightly differently in
@@ -41,7 +45,7 @@ OpenSSL 3.0
 
    For the above reasons the keys returned from these functions should typically
    be treated as read-only. To emphasise this the value returned from
-   EVP_PKEY_get0(), EVP_PKEY_get0_RSA(), EVP_PKEY_get0_DSA(),
+   EVP_PKEY_get0_RSA(), EVP_PKEY_get0_DSA(),
    EVP_PKEY_get0_EC_KEY() and EVP_PKEY_get0_DH() has been made const. This may
    break some existing code. Applications broken by this change should be
    modified. The preferred solution is to refactor the code to avoid the use of
diff --git a/crypto/evp/p_lib.c b/crypto/evp/p_lib.c
index 21fbc2ea4c88..30ba8d64283c 100644
--- a/crypto/evp/p_lib.c
+++ b/crypto/evp/p_lib.c
@@ -740,12 +740,15 @@ int EVP_PKEY_assign(EVP_PKEY *pkey, int type, void *key)
 }
 # endif
 
-const void *EVP_PKEY_get0(const EVP_PKEY *pkey)
+void *EVP_PKEY_get0(const EVP_PKEY *pkey)
 {
     if (pkey == NULL)
         return NULL;
 
-    return evp_pkey_get_legacy((EVP_PKEY *)pkey);
+    if (!evp_pkey_is_provided(pkey))
+        return pkey->pkey.ptr;
+
+    return NULL;
 }
 
 const unsigned char *EVP_PKEY_get0_hmac(const EVP_PKEY *pkey, size_t *len)
@@ -755,9 +758,12 @@ const unsigned char *EVP_PKEY_get0_hmac(const EVP_PKEY *pkey, size_t *len)
         ERR_raise(ERR_LIB_EVP, EVP_R_EXPECTING_AN_HMAC_KEY);
         return NULL;
     }
-    os = EVP_PKEY_get0(pkey);
-    *len = os->length;
-    return os->data;
+    os = evp_pkey_get_legacy((EVP_PKEY *)pkey);
+    if (os != NULL) {
+        *len = os->length;
+        return os->data;
+    }
+    return NULL;
 }
 
 # ifndef OPENSSL_NO_POLY1305
@@ -768,9 +774,12 @@ const unsigned char *EVP_PKEY_get0_poly1305(const EVP_PKEY *pkey, size_t *len)
         ERR_raise(ERR_LIB_EVP, EVP_R_EXPECTING_A_POLY1305_KEY);
         return NULL;
     }
-    os = EVP_PKEY_get0(pkey);
-    *len = os->length;
-    return os->data;
+    os = evp_pkey_get_legacy((EVP_PKEY *)pkey);
+    if (os != NULL) {
+        *len = os->length;
+        return os->data;
+    }
+    return NULL;
 }
 # endif
 
@@ -783,9 +792,12 @@ const unsigned char *EVP_PKEY_get0_siphash(const EVP_PKEY *pkey, size_t *len)
         ERR_raise(ERR_LIB_EVP, EVP_R_EXPECTING_A_SIPHASH_KEY);
         return NULL;
     }
-    os = EVP_PKEY_get0(pkey);
-    *len = os->length;
-    return os->data;
+    os = evp_pkey_get_legacy((EVP_PKEY *)pkey);
+    if (os != NULL) {
+        *len = os->length;
+        return os->data;
+    }
+    return NULL;
 }
 # endif
 
diff --git a/doc/man3/EVP_PKEY_set1_RSA.pod b/doc/man3/EVP_PKEY_set1_RSA.pod
index 64760b2923a9..68e13d3480ef 100644
--- a/doc/man3/EVP_PKEY_set1_RSA.pod
+++ b/doc/man3/EVP_PKEY_set1_RSA.pod
@@ -8,8 +8,9 @@ EVP_PKEY_get0_RSA, EVP_PKEY_get0_DSA, EVP_PKEY_get0_DH, EVP_PKEY_get0_EC_KEY,
 EVP_PKEY_assign_RSA, EVP_PKEY_assign_DSA, EVP_PKEY_assign_DH,
 EVP_PKEY_assign_EC_KEY, EVP_PKEY_assign_POLY1305, EVP_PKEY_assign_SIPHASH,
 EVP_PKEY_get0_hmac, EVP_PKEY_get0_poly1305, EVP_PKEY_get0_siphash,
-EVP_PKEY_type, EVP_PKEY_id, EVP_PKEY_base_id, EVP_PKEY_set_alias_type,
-EVP_PKEY_set1_engine, EVP_PKEY_get0_engine - EVP_PKEY assignment functions
+EVP_PKEY_get0, EVP_PKEY_type, EVP_PKEY_id, EVP_PKEY_base_id,
+EVP_PKEY_set_alias_type, EVP_PKEY_set1_engine, EVP_PKEY_get0_engine -
+EVP_PKEY assignment functions
 
 =head1 SYNOPSIS
 
@@ -42,6 +43,7 @@ L<openssl_user_macros(7)>:
  const DSA *EVP_PKEY_get0_DSA(const EVP_PKEY *pkey);
  const DH *EVP_PKEY_get0_DH(const EVP_PKEY *pkey);
  const EC_KEY *EVP_PKEY_get0_EC_KEY(const EVP_PKEY *pkey);
+ void *EVP_PKEY_get0(const EVP_PKEY *pkey);
 
  int EVP_PKEY_assign_RSA(EVP_PKEY *pkey, RSA *key);
  int EVP_PKEY_assign_DSA(EVP_PKEY *pkey, DSA *key);
@@ -100,7 +102,8 @@ are deprecated. Applications should instead use the EVP_PKEY directly where
 possible. If access to the low level key parameters is required then
 applications should use L<EVP_PKEY_get_params(3)> and other similar functions.
 To write an EVP_PKEY out use the OSSL_ENCODER APIs (see
-L<OSSL_ENCODER_CTX_new_for_pkey(3)>).
+L<OSSL_ENCODER_CTX_new_for_pkey(3)>). EVP_PKEY_get0() returns a pointer to the
+legacy key or NULL if the key is not legacy.
 
 Note that if an EVP_PKEY was not constructed using one of the deprecated
 functions such as EVP_PKEY_set1_RSA(), EVP_PKEY_set1_DSA(), EVP_PKEY_set1_DH()
diff --git a/include/openssl/evp.h b/include/openssl/evp.h
index ec8503e7d887..9bd8d85a3e66 100644
--- a/include/openssl/evp.h
+++ b/include/openssl/evp.h
@@ -1249,7 +1249,7 @@ ENGINE *EVP_PKEY_get0_engine(const EVP_PKEY *pkey);
 OSSL_DEPRECATEDIN_3_0
 int EVP_PKEY_assign(EVP_PKEY *pkey, int type, void *key);
 OSSL_DEPRECATEDIN_3_0
-const void *EVP_PKEY_get0(const EVP_PKEY *pkey);
+void *EVP_PKEY_get0(const EVP_PKEY *pkey);
 OSSL_DEPRECATEDIN_3_0
 const unsigned char *EVP_PKEY_get0_hmac(const EVP_PKEY *pkey, size_t *len);
 #  ifndef OPENSSL_NO_POLY1305
diff --git a/test/threadstest.c b/test/threadstest.c
index 1967ec6dadb0..5b6424688188 100644
--- a/test/threadstest.c
+++ b/test/threadstest.c
@@ -411,7 +411,7 @@ static void thread_downgrade_shared_evp_pkey(void)
      * This test is only relevant for deprecated functions that perform
      * downgrading
      */
-    if (EVP_PKEY_get0(shared_evp_pkey) == NULL)
+    if (EVP_PKEY_get0_RSA(shared_evp_pkey) == NULL)
         multi_success = 0;
 #else
     /* Shouldn't ever get here */
diff --git a/util/missingcrypto.txt b/util/missingcrypto.txt
index 60d2572bb25e..d062ff03c01e 100644
--- a/util/missingcrypto.txt
+++ b/util/missingcrypto.txt
@@ -675,7 +675,6 @@ EVP_PKEY_assign(3)
 EVP_PKEY_decrypt_old(3)
 EVP_PKEY_delete_attr(3)
 EVP_PKEY_encrypt_old(3)
-EVP_PKEY_get0(3)
 EVP_PKEY_get_attr(3)
 EVP_PKEY_get_attr_by_NID(3)
 EVP_PKEY_get_attr_by_OBJ(3)
