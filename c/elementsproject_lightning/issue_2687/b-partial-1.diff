diff --git a/lightningd/Makefile b/lightningd/Makefile
index 1a054346ca7..932d8a46cfc 100644
--- a/lightningd/Makefile
+++ b/lightningd/Makefile
@@ -69,6 +69,7 @@ LIGHTNINGD_SRC :=				\
 	lightningd/hsm_control.c		\
 	lightningd/htlc_end.c			\
 	lightningd/invoice.c			\
+	lightningd/io_loop_with_timers.c	\
 	lightningd/json.c			\
 	lightningd/json_stream.c		\
 	lightningd/jsonrpc.c			\
diff --git a/lightningd/chaintopology.c b/lightningd/chaintopology.c
index 63340568182..4bf3025469d 100644
--- a/lightningd/chaintopology.c
+++ b/lightningd/chaintopology.c
@@ -22,6 +22,7 @@
 #include <inttypes.h>
 #include <lightningd/channel_control.h>
 #include <lightningd/gossip_control.h>
+#include <lightningd/io_loop_with_timers.h>
 
 /* Mutual recursion via timer. */
 static void try_extend_tip(struct chain_topology *topo);
@@ -894,7 +895,7 @@ void setup_topology(struct chain_topology *topo,
 	start_fee_estimate(topo);
 
 	/* Once it gets initial block, it calls io_break() and we return. */
-	io_loop(NULL, NULL);
+	io_loop_with_timers(topo->ld);
 }
 
 void begin_topology(struct chain_topology *topo)
diff --git a/lightningd/io_loop_with_timers.c b/lightningd/io_loop_with_timers.c
new file mode 100644
index 00000000000..be781d355d0
--- /dev/null
+++ b/lightningd/io_loop_with_timers.c
@@ -0,0 +1,36 @@
+#include "lightningd/io_loop_with_timers.h"
+
+#include <ccan/io/io.h>
+#include <ccan/timer/timer.h>
+#include <common/timeout.h>
+#include <lightningd/lightningd.h>
+#include <wallet/db.h>
+#include <wallet/wallet.h>
+
+void *io_loop_with_timers(struct lightningd *ld)
+{
+	void *retval = NULL;
+	struct timer *expired;
+
+	while (!retval) {
+		/* ~ccan/io's io_loop() continuously calls
+		 * io_poll_lightningd() for all file descriptors registered
+		 * with it, then calls their callbacks or closes them if they
+		 * fail, as appropriate.
+		 *
+		 * It will only exit if there's an expired timer, *or* someone
+		 * calls io_break, or if there are no more file descriptors
+		 * (which never happens in our code). */
+		retval = io_loop(&ld->timers, &expired);
+
+		/*~ Notice that timers are called here in the event loop like
+		 * anything else, so there are no weird concurrency issues. */
+		if (expired) {
+			db_begin_transaction(ld->wallet->db);
+			timer_expired(ld, expired);
+			db_commit_transaction(ld->wallet->db);
+		}
+	}
+
+	return retval;
+}
diff --git a/lightningd/io_loop_with_timers.h b/lightningd/io_loop_with_timers.h
new file mode 100644
index 00000000000..cc8e85bd470
--- /dev/null
+++ b/lightningd/io_loop_with_timers.h
@@ -0,0 +1,10 @@
+#ifndef LIGHTNING_LIGHTNINGD_IO_LOOP_WITH_TIMERS_H
+#define LIGHTNING_LIGHTNINGD_IO_LOOP_WITH_TIMERS_H
+
+#include "config.h"
+
+struct lightningd;
+
+void *io_loop_with_timers(struct lightningd *ld);
+
+#endif /* LIGHTNING_LIGHTNINGD_IO_LOOP_WITH_TIMERS_H */
diff --git a/lightningd/lightningd.c b/lightningd/lightningd.c
index c85d7df4de9..1b9c3a44583 100644
--- a/lightningd/lightningd.c
+++ b/lightningd/lightningd.c
@@ -70,6 +70,7 @@
 #include <lightningd/channel_control.h>
 #include <lightningd/connect_control.h>
 #include <lightningd/invoice.h>
+#include <lightningd/io_loop_with_timers.h>
 #include <lightningd/jsonrpc.h>
 #include <lightningd/log.h>
 #include <lightningd/onchain_control.h>
@@ -810,30 +811,12 @@ int main(int argc, char *argv[])
 
 	/*~ The root of every backtrace (almost).  This is our main event
 	 *  loop. */
-	for (;;) {
-		/* ~ccan/io's io_loop() continuously calls
-		 * io_poll_lightningd() for all file descriptors registered
-		 * with it, then calls their callbacks or closes them if they
-		 * fail, as appropriate.
-		 *
-		 * It will only exit if there's an expired timer, *or* someone
-		 * calls io_break, or if there are no more file descriptors
-		 * (which never happens in our code). */
-		struct timer *expired;
-		void *v = io_loop(&ld->timers, &expired);
-
-		/*~ We use io_break(ld) to shut down. */
-		if (v == ld)
-			break;
-
-		/*~ Notice that timers are called here in the event loop like
-		 * anything else, so there are no weird concurrency issues. */
-		if (expired) {
-			db_begin_transaction(ld->wallet->db);
-			timer_expired(ld, expired);
-			db_commit_transaction(ld->wallet->db);
-		}
-	}
+	void *io_loop_ret = io_loop_with_timers(ld);
+	/*~ io_loop_with_timers will only exit if we call io_break.
+	 *  At this point in code, we should use io_break(ld) to
+	 *  shut down.
+	 */
+	assert(io_loop_ret == ld);
 
 	shutdown_subdaemons(ld);
 
