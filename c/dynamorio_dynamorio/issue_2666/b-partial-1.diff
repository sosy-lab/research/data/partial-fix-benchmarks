diff --git a/core/win32/module_shared.c b/core/win32/module_shared.c
index 085f6dcf6b..a1ea936d49 100644
--- a/core/win32/module_shared.c
+++ b/core/win32/module_shared.c
@@ -1200,6 +1200,28 @@ free_library_64(HANDLE lib)
     return (res >= 0);
 }
 
+size_t
+nt_get_context64_size(void)
+{
+    static size_t context64_size;
+    if (context64_size == 0) {
+        uint64 ntdll64_RtlGetExtendedContextLength;
+        int len = 0;
+        uint64 len_param = (uint64)&len;
+        uint64 ntdll64 = get_module_handle_64(L"ntdll.dll");
+        ASSERT(ntdll64 != 0);
+        ntdll64_RtlGetExtendedContextLength =
+            get_proc_address_64(ntdll64, "RtlGetExtendedContextLength");
+        invoke_func64_t args = { ntdll64_RtlGetExtendedContextLength, CONTEXT_XSTATE,
+                                 len_param };
+        NTSTATUS res = switch_modes_and_call(&args);
+        ASSERT(NT_SUCCESS(res));
+        /* Add 16 so we can align it forward to 16. */
+        context64_size = len + 16;
+    }
+    return context64_size;
+}
+
 bool
 thread_get_context_64(HANDLE thread, CONTEXT_64 *cxt64)
 {
diff --git a/core/win32/ntdll.c b/core/win32/ntdll.c
index 7ec7da6810..950c03b586 100644
--- a/core/win32/ntdll.c
+++ b/core/win32/ntdll.c
@@ -5299,12 +5299,29 @@ initialize_known_SID(PSID_IDENTIFIER_AUTHORITY IdentifierAuthority, ULONG SubAut
     pSid->SubAuthority[0] = SubAuthority0;
 }
 
+/* Use nt_get_context64_size() from 32-bit for the 64-bit max size. */
+size_t
+nt_get_context_size(DWORD flags)
+{
+    /* Moved out of nt_initialize_context():
+     *   8d450c          lea     eax,[ebp+0Ch]
+     *   50              push    eax
+     *   57              push    edi
+     *   ff15b0007a76    call    dword ptr [_imp__RtlGetExtendedContextLength]
+     */
+    int len;
+    int res = ntdll_RtlGetExtendedContextLength(flags, &len);
+    ASSERT(res >= 0);
+    /* Add 16 so we can align it forward to 16. */
+    return len + 16;
+}
+
 /* Initialize the buffer as CONTEXT with extension and return the pointer
  * pointing to the start of CONTEXT.
- * Assume buffer size is MAX_CONTEXT_SIZE;
+ * Normally buf_len would come from nt_get_context_size(flags).
  */
 CONTEXT *
-nt_initialize_context(char *buf, DWORD flags)
+nt_initialize_context(char *buf, size_t buf_len, DWORD flags)
 {
     /* Ideally, kernel32!InitializeContext is used to setup context.
      * However, DR should NEVER use kernel32. DR never uses anything in
@@ -5313,15 +5330,8 @@ nt_initialize_context(char *buf, DWORD flags)
     CONTEXT *cxt;
     if (TESTALL(CONTEXT_XSTATE, flags)) {
         context_ex_t *cxt_ex;
-        int len, res;
+        int res;
         ASSERT(proc_avx_enabled());
-        /* 8d450c          lea     eax,[ebp+0Ch]
-         * 50              push    eax
-         * 57              push    edi
-         * ff15b0007a76    call    dword ptr [_imp__RtlGetExtendedContextLength]
-         */
-        res = ntdll_RtlGetExtendedContextLength(flags, &len);
-        ASSERT(res >= 0 && len <= MAX_CONTEXT_SIZE);
         /* 8d45fc          lea     eax,[ebp-4]
          * 50              push    eax
          * 57              push    edi
@@ -5337,7 +5347,7 @@ nt_initialize_context(char *buf, DWORD flags)
         cxt = (CONTEXT *)ntdll_RtlLocateLegacyContext(cxt_ex, 0);
         ASSERT(context_check_extended_sizes(cxt_ex, flags));
         ASSERT(cxt != NULL && (char *)cxt >= buf &&
-               (char *)cxt + cxt_ex->all.length < buf + MAX_CONTEXT_SIZE);
+               (char *)cxt + cxt_ex->all.length < buf + buf_len);
     } else {
         /* make it 16-byte aligned */
         cxt = (CONTEXT *)(ALIGN_FORWARD(buf, 0x10));
diff --git a/core/win32/os.c b/core/win32/os.c
index 65e094603b..4fa3a39f9e 100644
--- a/core/win32/os.c
+++ b/core/win32/os.c
@@ -1175,6 +1175,9 @@ d_r_os_init(void)
         os_user_directory_supports_ownership();
     is_wow64_process(NT_CURRENT_PROCESS);
     is_in_ntdll(get_ntdll_base());
+#    ifndef X64
+    nt_get_context64_size();
+#    endif
 
     os_take_over_init();
 
@@ -1882,7 +1885,7 @@ takeover_table_entry_free(dcontext_t *dcontext, void *e)
         close_handle(data->thread_handle);
     if (data->cxt64_alloc != NULL) {
         global_heap_free(data->cxt64_alloc,
-                         MAX_CONTEXT_64_SIZE HEAPACCT(ACCT_THREAD_MGT));
+                         nt_get_context64_size() HEAPACCT(ACCT_THREAD_MGT));
     }
 #    endif
     global_heap_free(data, sizeof(*data) HEAPACCT(ACCT_THREAD_MGT));
@@ -2092,7 +2095,9 @@ os_take_over_exit(void)
      * waiting for these threads prior to detach is not guaranteed, so instead we
      * just revert the attach.
      */
-    char buf[MAX_CONTEXT_SIZE];
+    DWORD cxt_flags = CONTEXT_DR_STATE;
+    size_t bufsz = nt_get_context_size(cxt_flags);
+    char *buf = (char *)global_heap_alloc(bufsz HEAPACCT(ACCT_THREAD_MGT));
     TABLE_RWLOCK(takeover_table, write, lock);
     int iter = 0;
     takeover_data_t *data;
@@ -2102,7 +2107,7 @@ os_take_over_exit(void)
                                          (void **)&data);
         if (iter < 0)
             break;
-        CONTEXT *cxt = nt_initialize_context(buf, CONTEXT_DR_STATE);
+        CONTEXT *cxt = nt_initialize_context(buf, bufsz, cxt_flags);
         HANDLE handle = thread_handle_from_id(data->tid);
         LOG(GLOBAL, LOG_THREADS, 1,
             "Reverting attached-but-never-scheduled thread " TIDFMT "\n", data->tid);
@@ -2124,6 +2129,7 @@ os_take_over_exit(void)
     TABLE_RWLOCK(takeover_table, write, unlock);
     generic_hash_destroy(GLOBAL_DCONTEXT, takeover_table);
     takeover_table = NULL;
+    global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
 }
 
 #    ifndef X64
@@ -2430,14 +2436,14 @@ os_take_over_wow64_extra(takeover_data_t *data, HANDLE hthread, thread_id_t tid,
     /* WOW64 context setting is fragile: we need the raw x64 context as well.
      * We can't easily use nt_initialize_context so we manually set the flags.
      */
-    buf = (byte *)global_heap_alloc(MAX_CONTEXT_64_SIZE HEAPACCT(ACCT_THREAD_MGT));
+    buf = (byte *)global_heap_alloc(nt_get_context64_size() HEAPACCT(ACCT_THREAD_MGT));
     cxt64 = (CONTEXT_64 *)ALIGN_FORWARD(buf, 0x10);
     cxt64->ContextFlags = CONTEXT_CONTROL | CONTEXT_INTEGER;
     if (!thread_get_context_64(hthread, cxt64)) {
         LOG(GLOBAL, LOG_THREADS, 1, "\tfailed to get x64 cxt for thread " TIDFMT "\n",
             tid);
         ASSERT_NOT_REACHED();
-        global_heap_free(buf, MAX_CONTEXT_64_SIZE HEAPACCT(ACCT_THREAD_MGT));
+        global_heap_free(buf, nt_get_context64_size() HEAPACCT(ACCT_THREAD_MGT));
         return;
     }
     LOG(GLOBAL, LOG_THREADS, 2,
@@ -2453,7 +2459,7 @@ os_take_over_wow64_extra(takeover_data_t *data, HANDLE hthread, thread_id_t tid,
         /* In x86 mode, so not inside the wow64 layer.  Context setting should
          * work fine.
          */
-        global_heap_free(buf, MAX_CONTEXT_64_SIZE HEAPACCT(ACCT_THREAD_MGT));
+        global_heap_free(buf, nt_get_context64_size() HEAPACCT(ACCT_THREAD_MGT));
         return;
     }
     /* Could be in ntdll or user32 or anywhere a syscall is made, so we don't
@@ -2485,7 +2491,7 @@ os_take_over_wow64_extra(takeover_data_t *data, HANDLE hthread, thread_id_t tid,
         data->cxt64 = cxt64;
         data->cxt64_alloc = buf;
     } else {
-        global_heap_free(buf, MAX_CONTEXT_64_SIZE HEAPACCT(ACCT_THREAD_MGT));
+        global_heap_free(buf, nt_get_context64_size() HEAPACCT(ACCT_THREAD_MGT));
     }
 }
 #    endif
@@ -2495,8 +2501,10 @@ static bool
 os_take_over_thread(dcontext_t *dcontext, HANDLE hthread, thread_id_t tid, bool suspended)
 {
     bool success = true;
-    char buf[MAX_CONTEXT_SIZE];
-    CONTEXT *cxt = nt_initialize_context(buf, CONTEXT_DR_STATE);
+    DWORD cxt_flags = CONTEXT_DR_STATE;
+    size_t bufsz = nt_get_context_size(cxt_flags);
+    char *buf = (char *)global_heap_alloc(bufsz HEAPACCT(ACCT_THREAD_MGT));
+    CONTEXT *cxt = nt_initialize_context(buf, bufsz, cxt_flags);
     ASSERT(tid == thread_id_from_handle(hthread));
     if ((suspended || nt_thread_suspend(hthread, NULL)) &&
         NT_SUCCESS(nt_get_context(hthread, cxt))) {
@@ -2523,6 +2531,7 @@ os_take_over_thread(dcontext_t *dcontext, HANDLE hthread, thread_id_t tid, bool
         if (is_in_dynamo_dll((app_pc)cxt->CXT_XIP) ||
             new_thread_is_waiting_for_dr_init(tid, (app_pc)cxt->CXT_XIP)) {
             LOG(GLOBAL, LOG_THREADS, 1, "\tthread " TIDFMT " is already waiting\n", tid);
+            global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
             return true; /* it's waiting for us to take it over */
         }
         /* Avoid double-takeover.
@@ -2558,8 +2567,10 @@ os_take_over_thread(dcontext_t *dcontext, HANDLE hthread, thread_id_t tid, bool
                  * know if it does.
                  */
                 ASSERT_CURIOSITY(false && "thread takeover context reverted!");
-            } else
+            } else {
+                global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
                 return true;
+            }
         } else {
             thread_record_t *tr = thread_lookup(tid);
             data = (takeover_data_t *)global_heap_alloc(sizeof(*data)
@@ -2603,6 +2614,7 @@ os_take_over_thread(dcontext_t *dcontext, HANDLE hthread, thread_id_t tid, bool
         LOG(GLOBAL, LOG_THREADS, 1, "\tfailed to suspend/query thread " TIDFMT "\n", tid);
         success = false;
     }
+    global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
     return success;
 }
 
@@ -5145,26 +5157,34 @@ os_thread_terminate(thread_record_t *tr)
 bool
 thread_get_mcontext(thread_record_t *tr, priv_mcontext_t *mc)
 {
-    char buf[MAX_CONTEXT_SIZE];
-    CONTEXT *cxt = nt_initialize_context(buf, CONTEXT_DR_STATE);
+    DWORD cxt_flags = CONTEXT_DR_STATE;
+    size_t bufsz = nt_get_context_size(cxt_flags);
+    char *buf = (char *)global_heap_alloc(bufsz HEAPACCT(ACCT_THREAD_MGT));
+    CONTEXT *cxt = nt_initialize_context(buf, bufsz, cxt_flags);
+    bool res = false;
     if (thread_get_context(tr, cxt)) {
         context_to_mcontext(mc, cxt);
-        return true;
+        res = true;
     }
-    return false;
+    global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
+    return res;
 }
 
 bool
 thread_set_mcontext(thread_record_t *tr, priv_mcontext_t *mc)
 {
-    char buf[MAX_CONTEXT_SIZE];
-    CONTEXT *cxt = nt_initialize_context(buf, CONTEXT_DR_STATE);
+    DWORD cxt_flags = CONTEXT_DR_STATE;
+    size_t bufsz = nt_get_context_size(cxt_flags);
+    char *buf = (char *)global_heap_alloc(bufsz HEAPACCT(ACCT_THREAD_MGT));
+    CONTEXT *cxt = nt_initialize_context(buf, bufsz, cxt_flags);
     /* i#1033: get the context from the dst thread to make sure
      * segments are correctly set.
      */
     thread_get_context(tr, cxt);
     mcontext_to_context(cxt, mc, false /* !set_cur_seg */);
-    return thread_set_context(tr, cxt);
+    bool res = thread_set_context(tr, cxt);
+    global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
+    return res;
 }
 
 bool
@@ -5192,8 +5212,24 @@ thread_set_self_context(void *cxt)
 void
 thread_set_self_mcontext(priv_mcontext_t *mc)
 {
-    char buf[MAX_CONTEXT_SIZE];
-    CONTEXT *cxt = nt_initialize_context(buf, CONTEXT_DR_STATE);
+    /* We can't use heap for our CONTEXT as we have no opportunity to free it.
+     * We assume call paths can handle a large stack buffer as size something
+     * larger than the largest Win10 x64 CONTEXT at this time, which is 3375 bytes.
+     */
+    char buf[4096];
+    DWORD cxt_flags = CONTEXT_DR_STATE;
+    size_t bufsz = nt_get_context_size(cxt_flags);
+    CONTEXT *cxt;
+    if (bufsz > sizeof(buf)) {
+        /* Fallback: leak memory rather than failing.
+         * We could conceivably store it in the dcontext for freeing later.
+         */
+        SYSLOG_INTERNAL_WARNING_ONCE("CONTEXT stack buffer too small in %s",
+                                     __FUNCTION__);
+        char *lost = (char *)global_heap_alloc(bufsz HEAPACCT(ACCT_THREAD_MGT));
+        cxt = nt_initialize_context(lost, bufsz, cxt_flags);
+    } else
+        cxt = nt_initialize_context(buf, bufsz, cxt_flags);
     /* need ss and cs for setting my own context */
     mcontext_to_context(cxt, mc, true /* set_cur_seg */);
     thread_set_self_context(cxt);
diff --git a/core/win32/os_private.h b/core/win32/os_private.h
index aa1c647613..85072d1ebc 100644
--- a/core/win32/os_private.h
+++ b/core/win32/os_private.h
@@ -472,12 +472,6 @@ extern uint context_xstate;
 
 #define XSTATE_HEADER_SIZE 0x40 /* 512 bits */
 #define YMMH_AREA(ymmh_area, i) (((dr_xmm_t *)ymmh_area)[i])
-#define MAX_CONTEXT_64_SIZE 0xd2f /* as observed on win10-x64 */
-#ifdef X64
-#    define MAX_CONTEXT_SIZE MAX_CONTEXT_64_SIZE
-#else
-#    define MAX_CONTEXT_SIZE 0xb23 /* as observed on win10-x64 */
-#endif
 #define CONTEXT_DYNAMICALLY_LAID_OUT(flags) (TESTALL(CONTEXT_XSTATE, flags))
 
 enum {
@@ -647,6 +641,9 @@ typedef struct DECLSPEC_ALIGN(16) _CONTEXT_64 {
 
 /* in module_shared.c */
 #ifndef X64
+size_t
+nt_get_context64_size(void);
+
 bool
 thread_get_context_64(HANDLE thread, CONTEXT_64 *cxt64);
 
@@ -935,8 +932,14 @@ bool
 convert_NT_to_Dos_path(OUT wchar_t *buf, IN const wchar_t *fname,
                        IN size_t buf_len /*# elements*/);
 
+size_t
+nt_get_context_size(DWORD flags);
+
+size_t
+nt_get_max_context_size(void);
+
 CONTEXT *
-nt_initialize_context(char *buf, DWORD flags);
+nt_initialize_context(char *buf, size_t buf_len, DWORD flags);
 
 byte *
 context_ymmh_saved_area(CONTEXT *cxt);
diff --git a/core/win32/syscall.c b/core/win32/syscall.c
index d7d3e31c0f..2d77e70786 100644
--- a/core/win32/syscall.c
+++ b/core/win32/syscall.c
@@ -1724,7 +1724,6 @@ add_dr_env_vars(dcontext_t *dcontext, HANDLE phandle, uint64 env_ptr, bool peb_i
 static bool
 not_first_thread_in_new_process(HANDLE process_handle, HANDLE thread_handle)
 {
-    char buf[MAX_CONTEXT_SIZE];
 #ifndef X64
     bool peb_is_32 = is_32bit_process(process_handle);
     if (!peb_is_32) {
@@ -1738,10 +1737,15 @@ not_first_thread_in_new_process(HANDLE process_handle, HANDLE thread_handle)
                                     "32-bit parent's 64-bit child not supported on XP");
     }
 #endif
-    CONTEXT *cxt = nt_initialize_context(buf, CONTEXT_DR_STATE);
+    DWORD cxt_flags = CONTEXT_DR_STATE;
+    size_t bufsz = nt_get_context_size(cxt_flags);
+    char *buf = (char *)global_heap_alloc(bufsz HEAPACCT(ACCT_THREAD_MGT));
+    CONTEXT *cxt = nt_initialize_context(buf, bufsz, cxt_flags);
+    bool res = false;
     if (NT_SUCCESS(nt_get_context(thread_handle, cxt)))
-        return !is_first_thread_in_new_process(process_handle, cxt);
-    return false;
+        res = !is_first_thread_in_new_process(process_handle, cxt);
+    global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
+    return res;
 }
 
 /* The caller should already have checked should_inject_into_process().
@@ -2085,8 +2089,10 @@ presys_SetContextThread(dcontext_t *dcontext, reg_t *param_base)
              * FIXME: this isn't transparent as we have to clobber
              * fields in the app cxt: should restore in post-syscall.
              */
-            char buf[MAX_CONTEXT_SIZE];
-            CONTEXT *alt_cxt = nt_initialize_context(buf, CONTEXT_DR_STATE);
+            DWORD cxt_flags = CONTEXT_DR_STATE;
+            size_t bufsz = nt_get_context_size(cxt_flags);
+            char *buf = (char *)global_heap_alloc(bufsz HEAPACCT(ACCT_THREAD_MGT));
+            CONTEXT *alt_cxt = nt_initialize_context(buf, bufsz, cxt_flags);
             STATS_INC(num_app_setcontext_no_control);
             if (thread_get_context(tr, alt_cxt) &&
                 translate_context(tr, alt_cxt, true /*set memory*/)) {
@@ -2113,6 +2119,7 @@ presys_SetContextThread(dcontext_t *dcontext, reg_t *param_base)
                 intercept = false;
                 ASSERT_NOT_REACHED();
             }
+            global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
         }
         if (intercept) {
             /* modify the being-set cxt so that we retain control */
@@ -3260,7 +3267,9 @@ postsys_CreateUserProcess(dcontext_t *dcontext, reg_t *param_base, bool success)
                                     get_application_pid(),
                                     "Child thread not created suspended");
     }
-    char buf[MAX_CONTEXT_SIZE];
+    DWORD cxt_flags = CONTEXT_DR_STATE;
+    size_t bufsz = nt_get_context_size(cxt_flags);
+    char *buf = (char *)global_heap_alloc(bufsz HEAPACCT(ACCT_THREAD_MGT));
     CONTEXT *context;
     CONTEXT *cxt = NULL;
     int res;
@@ -3272,7 +3281,7 @@ postsys_CreateUserProcess(dcontext_t *dcontext, reg_t *param_base, bool success)
         /* If no early injection we have to do thread injection, and
          * on Vista+ we don't see the NtCreateThread so we do it here.  PR 215423.
          */
-        context = nt_initialize_context(buf, CONTEXT_DR_STATE);
+        context = nt_initialize_context(buf, bufsz, cxt_flags);
         res = nt_get_context(thread_handle, context);
         if (NT_SUCCESS(res))
             cxt = context;
@@ -3300,8 +3309,11 @@ postsys_CreateUserProcess(dcontext_t *dcontext, reg_t *param_base, bool success)
     }
     ASSERT(cxt != NULL || DYNAMO_OPTION(early_inject)); /* Else, exited above. */
     /* Do the actual injection. */
-    if (!maybe_inject_into_process(dcontext, proc_handle, thread_handle, cxt))
+    if (!maybe_inject_into_process(dcontext, proc_handle, thread_handle, cxt)) {
+        global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
         return;
+    }
+    global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
     propagate_options_via_env_vars(dcontext, proc_handle, thread_handle);
     if (cxt != NULL) {
         /* injection routine is assuming doesn't have to install cxt */
@@ -3327,7 +3339,6 @@ postsys_GetContextThread(dcontext_t *dcontext, reg_t *param_base, bool success)
     CONTEXT *cxt = (CONTEXT *)postsys_param(dcontext, param_base, 1);
     thread_record_t *trec;
     thread_id_t tid = thread_handle_to_tid(thread_handle);
-    char buf[MAX_CONTEXT_SIZE];
     CONTEXT *alt_cxt;
     CONTEXT *xlate_cxt;
     LOG(THREAD, LOG_SYSCALLS | LOG_THREADS, 1,
@@ -3337,6 +3348,10 @@ postsys_GetContextThread(dcontext_t *dcontext, reg_t *param_base, bool success)
     if (!success)
         return;
 
+    DWORD cxt_flags = CONTEXT_DR_STATE;
+    size_t bufsz = nt_get_context_size(cxt_flags);
+    char *buf = (char *)global_heap_alloc(bufsz HEAPACCT(ACCT_THREAD_MGT));
+
     /* FIXME : we are going to read/write the context argument which is
      * potentially unsafe, since success it must have been readable when
      * at the os call, but there could always be multi-thread races */
@@ -3381,7 +3396,7 @@ postsys_GetContextThread(dcontext_t *dcontext, reg_t *param_base, bool success)
              * no further permissions are needed to acquire them so we
              * get our own context w/ them.
              */
-            alt_cxt = nt_initialize_context(buf, CONTEXT_DR_STATE);
+            alt_cxt = nt_initialize_context(buf, bufsz, cxt_flags);
             /* if asking for own context, thread_get_context() will point at
              * dynamorio_syscall_* and we'll fail to translate so we special-case
              */
@@ -3397,6 +3412,8 @@ postsys_GetContextThread(dcontext_t *dcontext, reg_t *param_base, bool success)
                 /* FIXME: just don't translate -- right now won't hurt us since
                  * we don't translate other than the pc anyway.
                  */
+                d_r_mutex_unlock(&thread_initexit_lock);
+                global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
                 return;
             }
             xlate_cxt = alt_cxt;
@@ -3466,6 +3483,7 @@ postsys_GetContextThread(dcontext_t *dcontext, reg_t *param_base, bool success)
         SELF_PROTECT_LOCAL(trec->dcontext, READONLY);
     }
     d_r_mutex_unlock(&thread_initexit_lock);
+    global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
 }
 
 /* NtSuspendThread */
@@ -3503,8 +3521,10 @@ postsys_SuspendThread(dcontext_t *dcontext, reg_t *param_base, bool success)
      * synch, use trylocks in case suspended thread is holding any locks */
     if (d_r_mutex_trylock(&thread_initexit_lock)) {
         if (!mutex_testlock(&all_threads_lock)) {
-            char buf[MAX_CONTEXT_SIZE];
-            CONTEXT *cxt = nt_initialize_context(buf, CONTEXT_DR_STATE);
+            DWORD cxt_flags = CONTEXT_DR_STATE;
+            size_t bufsz = nt_get_context_size(cxt_flags);
+            char *buf = (char *)global_heap_alloc(bufsz HEAPACCT(ACCT_THREAD_MGT));
+            CONTEXT *cxt = nt_initialize_context(buf, bufsz, cxt_flags);
             thread_record_t *tr;
             /* know thread isn't holding any of the locks we will need */
             LOG(THREAD, LOG_SYNCH, 2,
@@ -3527,10 +3547,12 @@ postsys_SuspendThread(dcontext_t *dcontext, reg_t *param_base, bool success)
                     LOG(THREAD, LOG_SYNCH, 2,
                         "SuspendThread suspended thread " TIDFMT " at good place\n", tid);
                     SELF_PROTECT_LOCAL(tr->dcontext, READONLY);
+                    global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
                     return;
                 }
                 SELF_PROTECT_LOCAL(tr->dcontext, READONLY);
             }
+            global_heap_free(buf, bufsz HEAPACCT(ACCT_THREAD_MGT));
         } else {
             LOG(THREAD, LOG_SYNCH, 2,
                 "SuspendThread couldn't get all_threads_lock to test if thread " TIDFMT
