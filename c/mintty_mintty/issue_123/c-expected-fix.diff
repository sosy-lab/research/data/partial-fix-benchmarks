diff --git a/src/charset.h b/src/charset.h
index f0f1893b..a79606a9 100644
--- a/src/charset.h
+++ b/src/charset.h
@@ -68,6 +68,7 @@ extern int xcwidth(xchar c);
 extern bool indicwide(xchar c);
 extern bool extrawide(xchar c);
 extern bool combiningdouble(xchar c);
+extern bool widerange(xchar c);
 
 #endif
 
diff --git a/src/mcwidth.c b/src/mcwidth.c
index 02b4a625..3ac622a1 100644
--- a/src/mcwidth.c
+++ b/src/mcwidth.c
@@ -32,6 +32,25 @@ bisearch(xchar c, const interval table[], int len)
 }
 
 
+/* sorted list of non-overlapping intervals of East Asian wide characters */
+static const interval wide[] = {
+  { 0x1100, 0x115F }, /* Hangul Jamo init. consonants */
+  { 0x2329, 0x232A }, /* angle brackets */
+  { 0x2E80, 0x303E }, /* CJK symbols and punctuation */
+  { 0x3040, 0xA4CF }, /* CJK ... Yi */
+  { 0xA960, 0xA97F }, /* Hangul Jamo Extended-A */
+  { 0xAC00, 0xD7A3 }, /* Hangul Syllables */
+  { 0xF900, 0xFAFF }, /* CJK Compatibility Ideographs */
+  { 0xFE10, 0xFE19 }, /* Vertical forms */
+  { 0xFE30, 0xFE6F }, /* CJK Compatibility Forms */
+  { 0xFF00, 0xFF60 }, /* Fullwidth Forms */
+  { 0xFFE0, 0xFFE6 }, /* fullwidth symbols */
+  { 0x1B000, 0x1B0FF }, /* Kana Supplement */
+  { 0x1F200, 0x1F2FF }, /* Enclosed Ideographic Supplement */
+  { 0x20000, 0x2FFFD }, /* CJK Extensions and Supplement */
+  { 0x30000, 0x3FFFD }
+};
+
 #if !HAS_LOCALES
 
 /*
@@ -142,25 +161,6 @@ static const interval combining[] =
 static const interval ambiguous[] =
 #include "ambiguous.t"
 
-/* sorted list of non-overlapping intervals of East Asian wide characters */
-static const interval wide[] = {
-  { 0x1100, 0x115F }, /* Hangul Jamo init. consonants */
-  { 0x2329, 0x232A }, /* angle brackets */
-  { 0x2E80, 0x303E }, /* CJK symbols and punctuation */
-  { 0x3040, 0xA4CF }, /* CJK ... Yi */
-  { 0xA960, 0xA97F }, /* Hangul Jamo Extended-A */
-  { 0xAC00, 0xD7A3 }, /* Hangul Syllables */
-  { 0xF900, 0xFAFF }, /* CJK Compatibility Ideographs */
-  { 0xFE10, 0xFE19 }, /* Vertical forms */
-  { 0xFE30, 0xFE6F }, /* CJK Compatibility Forms */
-  { 0xFF00, 0xFF60 }, /* Fullwidth Forms */
-  { 0xFFE0, 0xFFE6 }, /* fullwidth symbols */
-  { 0x1B000, 0x1B0FF }, /* Kana Supplement */
-  { 0x1F200, 0x1F2FF }, /* Enclosed Ideographic Supplement */
-  { 0x20000, 0x2FFFD }, /* CJK Extensions and Supplement */
-  { 0x30000, 0x3FFFD }
-};
-
 int
 xcwidth(xchar c)
 {
@@ -194,6 +194,12 @@ xcwidth(xchar c)
 
 #endif
 
+bool
+widerange(xchar c)
+{
+  return bisearch(c, wide, lengthof(wide));
+}
+
 static const interval indic[] = {
 #include "indicwide.t"
 };
diff --git a/src/term.c b/src/term.c
index e123c531..3df28e7d 100644
--- a/src/term.c
+++ b/src/term.c
@@ -1002,11 +1002,15 @@ term_paint(void)
           tattr.attr != (dispchars[j].attr.attr & ~(ATTR_NARROW | DATTR_MASK))) {
         if ((tattr.attr & ATTR_WIDE) == 0 && win_char_width(tchar) == 2)
           tattr.attr |= ATTR_NARROW;
-#ifdef fix_123_spoil_CJK_570
-#warning Windows may report width 1 for double-width characters↯
-        else if (tattr.attr & ATTR_WIDE && win_char_width(tchar) == 1)
+        else if (tattr.attr & ATTR_WIDE
+                 // guard character expanding properly to avoid 
+                 // false hits as reported for CJK in #570,
+                 // considering that Windows may report width 1 
+                 // for double-width characters 
+                 // (if double-width by font substitution)
+                 && cs_ambig_wide && !font_ambig_wide
+                 && win_char_width(tchar) == 1 && !widerange(tchar))
           tattr.attr |= ATTR_EXPAND;
-#endif
       }
       else if (dispchars[j].attr.attr & ATTR_NARROW)
         tattr.attr |= ATTR_NARROW;
diff --git a/src/termout.c b/src/termout.c
old mode 100644
new mode 100755
index 33e4dbbb..353736ea
--- a/src/termout.c
+++ b/src/termout.c
@@ -1522,12 +1522,17 @@ term_write(const char *buf, uint len)
           continue;
         }
 
+        unsigned long long asav = term.curs.attr.attr;
+
         // Everything else
         int width;
         if (cfg.wide_indic && wc >= 0x0900 && indicwide(wc))
           width = 2;
-        else if (cfg.wide_extra && wc >= 0x2000 && extrawide(wc))
+        else if (cfg.wide_extra && wc >= 0x2000 && extrawide(wc)) {
           width = 2;
+          if (win_char_width(wc) < 2)
+            term.curs.attr.attr |= ATTR_EXPAND;
+        }
         else
 #if HAS_LOCALES
           width = wcwidth(wc);
@@ -1535,7 +1540,6 @@ term_write(const char *buf, uint len)
           width = xcwidth(wc);
 #endif
 
-        unsigned long long asav = term.curs.attr.attr;
         switch (term.curs.csets[term.curs.g1]) {
           when CSET_LINEDRW:  // VT100 line drawing characters
             if (0x60 <= wc && wc <= 0x7E) {
diff --git a/src/wintext.c b/src/wintext.c
index c0686e7b..1691d480 100644
--- a/src/wintext.c
+++ b/src/wintext.c
@@ -464,6 +464,16 @@ win_init_fonts(int size)
   font_ambig_wide =
     greek_char_width >= latin_char_width * 1.5 ||
     line_char_width  >= latin_char_width * 1.5;
+#ifdef debug_win_char_width
+  int w_latin = win_char_width(0x0041);
+  int w_greek = win_char_width(0x03B1);
+  int w_lines = win_char_width(0x2500);
+  printf("%04X %5.2f %d\n", 0x0041, latin_char_width, w_latin);
+  printf("%04X %5.2f %d\n", 0x03B1, greek_char_width, w_greek);
+  printf("%04X %5.2f %d\n", 0x2500, line_char_width, w_lines);
+  bool faw = w_greek > w_latin || w_lines > w_latin;
+  printf("font ambig %d/%d (dual %d)\n", font_ambig_wide, faw, font_dualwidth);
+#endif
 
   // Initialise VT100 linedraw character mappings.
   // See what glyphs are available.
@@ -1339,44 +1349,63 @@ win_check_glyphs(wchar *wcs, uint num)
 #define dont_debug_win_char_width
 
 /* This function gets the actual width of a character in the normal font.
-   Usage: determine whether to trim an ambiguous wide character 
-   (of a CJK ambiguous-wide font such as BatangChe) to normal width 
-   if desired.
+   Usage:
+   * determine whether to trim an ambiguous wide character 
+     (of a CJK ambiguous-wide font such as BatangChe) to normal width 
+     if desired.
+   * also whether to expand a normal width character if expected wide
  */
 int
 win_char_width(xchar c)
 {
-  int ibuf = 0;
-
- /* If the font max is the same as the font ave width then this
-  * function is a no-op.
+ /* If the font max width is the same as the font average width
+  * then this function is a no-op.
   */
+#ifndef debug_win_char_width
   if (!font_dualwidth)
     return 1;
+#endif
 
- /* Speedup, I know of no font where ascii is the wrong width */
+ /* Speedup, I know of no font where ASCII is the wrong width */
+#ifdef debug_win_char_width
+  if (c != 'A')
+#endif
   if (c >= ' ' && c < '~')  // exclude 0x7E (overline in Shift_JIS X 0213)
     return 1;
 
+  dc = GetDC(wnd);
+#ifdef debug_win_char_width
+  bool ok0 = !!
+#endif
   SelectObject(dc, fonts[FONT_NORMAL]);
-  if (!GetCharWidth32W(dc, c, c, &ibuf))
-    return 0;
 #ifdef debug_win_char_width
-    printf("win_char_width %04X %d (cell %d)\n", c, ibuf, cell_width);
+  if (c == 0x2001)
+    win_char_width(0x5555);
+  if (!ok0)
+    printf("width %04X failed (dc %p)\n", c, dc);
+  else if (c >= '~' || c == 'A') {
+    int cw = 0;
+    BOOL ok1 = GetCharWidth32W(dc, c, c, &cw);  // "not on TrueType"
+    float cwf = 0.0;
+    BOOL ok2 = GetCharWidthFloatW(dc, c, c, &cwf);
+    ABC abc; memset(&abc, 0, sizeof abc);
+    BOOL ok3 = GetCharABCWidthsW(dc, c, c, &abc);  // only on TrueType
+    ABCFLOAT abcf; memset(&abcf, 0, sizeof abcf);
+    BOOL ok4 = GetCharABCWidthsFloatW(dc, c, c, &abcf);
+    printf("w %04X [%d] (32 %d) %d (32a %d) %d (flt %d) %.3f (abc %d) %2d %2d %2d (abcf %d) %4.1f %4.1f %4.1f\n", 
+           c, cell_width, ok1, cw, ok2, cwf, 
+           ok3, abc.abcA, abc.abcB, abc.abcC, 
+           ok4, abcf.abcfA, abcf.abcfB, abcf.abcfC);
+  }
 #endif
 
+  int ibuf = 0;
+  if (!GetCharWidth32W(dc, c, c, &ibuf))
+    return 0;
+
   // report char as wide if its width is more than 1½ cells
   ibuf += cell_width / 2 - 1;
   ibuf /= cell_width;
-#ifdef debug_win_char_width
-  if (c >= '~') {
-    float char_width;
-    GetCharWidthFloatW(dc, c, c, &char_width);
-    ABC abc;
-    GetCharABCWidthsW(dc, c, c, &abc);
-    printf("               %04X float %d %f abc %d %d %d\n", c, ibuf, char_width, abc.abcA, abc.abcB, abc.abcC);
-  }
-#endif
 
   return ibuf;
 }
