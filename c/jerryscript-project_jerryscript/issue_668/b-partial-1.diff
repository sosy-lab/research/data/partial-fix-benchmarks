diff --git a/jerry-core/ecma/base/ecma-helpers-external-pointers.c b/jerry-core/ecma/base/ecma-helpers-external-pointers.c
index ac70118f74..d6fb60854f 100644
--- a/jerry-core/ecma/base/ecma-helpers-external-pointers.c
+++ b/jerry-core/ecma/base/ecma-helpers-external-pointers.c
@@ -104,7 +104,7 @@ ecma_create_external_pointer_property (ecma_object_t *obj_p, /**< object to crea
  *         false - otherwise (value returned through out_pointer_p is NULL).
  */
 bool
-ecma_get_external_pointer_value (ecma_object_t *obj_p, /**< object to get property value from */
+ecma_get_external_pointer_value (const ecma_object_t *obj_p, /**< object to get property value from */
                                  ecma_internal_property_id_t id, /**< identifier of internal property
                                                                   *   to get value from */
                                  ecma_external_pointer_t *out_pointer_p) /**< [out] value of the external pointer */
diff --git a/jerry-core/ecma/base/ecma-helpers.c b/jerry-core/ecma/base/ecma-helpers.c
index 3aea724172..9c3397d37d 100644
--- a/jerry-core/ecma/base/ecma-helpers.c
+++ b/jerry-core/ecma/base/ecma-helpers.c
@@ -511,7 +511,7 @@ ecma_create_internal_property (ecma_object_t *object_p, /**< the object */
  *         NULL - otherwise.
  */
 ecma_property_t *
-ecma_find_internal_property (ecma_object_t *object_p, /**< object descriptor */
+ecma_find_internal_property (const ecma_object_t *object_p, /**< object descriptor */
                              ecma_internal_property_id_t property_id) /**< internal property identifier */
 {
   JERRY_ASSERT (object_p != NULL);
@@ -557,7 +557,7 @@ ecma_find_internal_property (ecma_object_t *object_p, /**< object descriptor */
  * @return pointer to the property
  */
 ecma_property_t *
-ecma_get_internal_property (ecma_object_t *object_p, /**< object descriptor */
+ecma_get_internal_property (const ecma_object_t *object_p, /**< object descriptor */
                             ecma_internal_property_id_t property_id) /**< internal property identifier */
 {
   ecma_property_t *property_p = ecma_find_internal_property (object_p, property_id);
@@ -1103,7 +1103,7 @@ ecma_set_internal_property_value (ecma_property_t *prop_p, /**< property */
  *      value previously stored in the property is freed
  */
 void
-ecma_named_data_property_assign_value (ecma_object_t *obj_p, /**< object */
+ecma_named_data_property_assign_value (const ecma_object_t *obj_p, /**< object */
                                        ecma_property_t *prop_p, /**< property */
                                        ecma_value_t value) /**< value to assign */
 {
diff --git a/jerry-core/ecma/base/ecma-helpers.h b/jerry-core/ecma/base/ecma-helpers.h
index 40807e313b..913f56f5d9 100644
--- a/jerry-core/ecma/base/ecma-helpers.h
+++ b/jerry-core/ecma/base/ecma-helpers.h
@@ -245,8 +245,8 @@ extern ecma_object_t *ecma_get_lex_env_binding_object (const ecma_object_t *) __
 extern bool ecma_get_lex_env_provide_this (const ecma_object_t *) __attr_pure___;
 
 extern ecma_property_t *ecma_create_internal_property (ecma_object_t *, ecma_internal_property_id_t);
-extern ecma_property_t *ecma_find_internal_property (ecma_object_t *, ecma_internal_property_id_t);
-extern ecma_property_t *ecma_get_internal_property (ecma_object_t *, ecma_internal_property_id_t);
+extern ecma_property_t *ecma_find_internal_property (const ecma_object_t *, ecma_internal_property_id_t);
+extern ecma_property_t *ecma_get_internal_property (const ecma_object_t *, ecma_internal_property_id_t);
 
 extern ecma_property_t *
 ecma_create_named_data_property (ecma_object_t *, ecma_string_t *, bool, bool, bool);
@@ -265,7 +265,7 @@ extern void ecma_delete_property (ecma_object_t *, ecma_property_t *);
 
 extern ecma_value_t ecma_get_named_data_property_value (const ecma_property_t *);
 extern void ecma_set_named_data_property_value (ecma_property_t *, ecma_value_t);
-extern void ecma_named_data_property_assign_value (ecma_object_t *, ecma_property_t *, ecma_value_t);
+extern void ecma_named_data_property_assign_value (const ecma_object_t *, ecma_property_t *, ecma_value_t);
 
 extern ecma_value_t ecma_get_internal_property_value (const ecma_property_t *);
 extern void ecma_set_internal_property_value (ecma_property_t *, ecma_value_t);
@@ -297,7 +297,7 @@ extern void ecma_bytecode_deref (ecma_compiled_code_t *);
 extern bool
 ecma_create_external_pointer_property (ecma_object_t *, ecma_internal_property_id_t, ecma_external_pointer_t);
 extern bool
-ecma_get_external_pointer_value (ecma_object_t *, ecma_internal_property_id_t, ecma_external_pointer_t *);
+ecma_get_external_pointer_value (const ecma_object_t *, ecma_internal_property_id_t, ecma_external_pointer_t *);
 extern void
 ecma_free_external_pointer_in_property (ecma_property_t *);
 
diff --git a/jerry-core/ecma/base/ecma-lcache.c b/jerry-core/ecma/base/ecma-lcache.c
index 70424a9fce..d41ac1f323 100644
--- a/jerry-core/ecma/base/ecma-lcache.c
+++ b/jerry-core/ecma/base/ecma-lcache.c
@@ -223,7 +223,7 @@ ecma_lcache_insert (ecma_object_t *object_p, /**< object */
  *         false - probably, not registered.
  */
 inline bool __attr_always_inline___
-ecma_lcache_lookup (ecma_object_t *object_p, /**< object */
+ecma_lcache_lookup (const ecma_object_t *object_p, /**< object */
                     const ecma_string_t *prop_name_p, /**< property's name */
                     ecma_property_t **prop_p_p) /**< [out] if return value is true,
                                                  *         then here will be pointer to property,
diff --git a/jerry-core/ecma/base/ecma-lcache.h b/jerry-core/ecma/base/ecma-lcache.h
index 6471501473..bc4cb9e359 100644
--- a/jerry-core/ecma/base/ecma-lcache.h
+++ b/jerry-core/ecma/base/ecma-lcache.h
@@ -26,7 +26,7 @@
 extern void ecma_lcache_init (void);
 extern void ecma_lcache_invalidate_all (void);
 extern void ecma_lcache_insert (ecma_object_t *, ecma_string_t *, ecma_property_t *);
-extern bool ecma_lcache_lookup (ecma_object_t *, const ecma_string_t *, ecma_property_t **);
+extern bool ecma_lcache_lookup (const ecma_object_t *, const ecma_string_t *, ecma_property_t **);
 extern void ecma_lcache_invalidate (ecma_object_t *, ecma_string_t *, ecma_property_t *);
 
 /**
diff --git a/jerry-core/ecma/operations/ecma-function-object.c b/jerry-core/ecma/operations/ecma-function-object.c
index a008629cd3..ce33b52729 100644
--- a/jerry-core/ecma/operations/ecma-function-object.c
+++ b/jerry-core/ecma/operations/ecma-function-object.c
@@ -379,7 +379,7 @@ ecma_op_function_try_lazy_instantiate_property (ecma_object_t *obj_p, /**< the f
  *         NULL (i.e. ecma-undefined) - otherwise.
  */
 ecma_property_t *
-ecma_op_function_object_get_own_property (ecma_object_t *obj_p, /**< the function object */
+ecma_op_function_object_get_own_property (const ecma_object_t *obj_p, /**< the function object */
                                           ecma_string_t *property_name_p) /**< property name */
 {
   JERRY_ASSERT (ecma_get_object_type (obj_p) == ECMA_OBJECT_TYPE_FUNCTION);
@@ -392,7 +392,7 @@ ecma_op_function_object_get_own_property (ecma_object_t *obj_p, /**< the functio
   }
   else if (!ecma_get_object_is_builtin (obj_p))
   {
-    prop_p = ecma_op_function_try_lazy_instantiate_property (obj_p, property_name_p);
+    prop_p = ecma_op_function_try_lazy_instantiate_property ((ecma_object_t *) obj_p, property_name_p);
 
     /*
      * Only non-configurable properties could be instantiated lazily in the function,
diff --git a/jerry-core/ecma/operations/ecma-function-object.h b/jerry-core/ecma/operations/ecma-function-object.h
index 016bf256a9..d249f7577c 100644
--- a/jerry-core/ecma/operations/ecma-function-object.h
+++ b/jerry-core/ecma/operations/ecma-function-object.h
@@ -45,7 +45,7 @@ ecma_op_function_call (ecma_object_t *, ecma_value_t,
                        const ecma_value_t *, ecma_length_t);
 
 extern ecma_property_t *
-ecma_op_function_object_get_own_property (ecma_object_t *, ecma_string_t *);
+ecma_op_function_object_get_own_property (const ecma_object_t *, ecma_string_t *);
 
 extern ecma_value_t
 ecma_op_function_construct (ecma_object_t *, const ecma_value_t *, ecma_length_t);
diff --git a/jerry-core/ecma/operations/ecma-objects-arguments.c b/jerry-core/ecma/operations/ecma-objects-arguments.c
index 374ae08cca..58f83defdb 100644
--- a/jerry-core/ecma/operations/ecma-objects-arguments.c
+++ b/jerry-core/ecma/operations/ecma-objects-arguments.c
@@ -311,7 +311,7 @@ ecma_arguments_get_mapped_arg_value (ecma_object_t *map_p, /**< [[ParametersMap]
  *         Returned value must be freed with ecma_free_value
  */
 ecma_value_t
-ecma_op_arguments_object_get (ecma_object_t *obj_p, /**< the object */
+ecma_op_arguments_object_get (const ecma_object_t *obj_p, /**< the object */
                               ecma_string_t *property_name_p) /**< property name */
 {
   // 1.
@@ -348,7 +348,7 @@ ecma_op_arguments_object_get (ecma_object_t *obj_p, /**< the object */
  *         Returned value must be freed with ecma_free_value
  */
 ecma_property_t *
-ecma_op_arguments_object_get_own_property (ecma_object_t *obj_p, /**< the object */
+ecma_op_arguments_object_get_own_property (const ecma_object_t *obj_p, /**< the object */
                                            ecma_string_t *property_name_p) /**< property name */
 {
   // 1.
diff --git a/jerry-core/ecma/operations/ecma-objects-arguments.h b/jerry-core/ecma/operations/ecma-objects-arguments.h
index e907b262fe..eb25493c40 100644
--- a/jerry-core/ecma/operations/ecma-objects-arguments.h
+++ b/jerry-core/ecma/operations/ecma-objects-arguments.h
@@ -24,9 +24,9 @@ ecma_op_create_arguments_object (ecma_object_t *, ecma_object_t *, const ecma_va
                                  ecma_length_t, const ecma_compiled_code_t *);
 
 extern ecma_value_t
-ecma_op_arguments_object_get (ecma_object_t *, ecma_string_t *);
+ecma_op_arguments_object_get (const ecma_object_t *, ecma_string_t *);
 extern ecma_property_t *
-ecma_op_arguments_object_get_own_property (ecma_object_t *, ecma_string_t *);
+ecma_op_arguments_object_get_own_property (const ecma_object_t *, ecma_string_t *);
 extern ecma_value_t
 ecma_op_arguments_object_delete (ecma_object_t *, ecma_string_t *, bool);
 extern ecma_value_t
diff --git a/jerry-core/ecma/operations/ecma-objects-general.c b/jerry-core/ecma/operations/ecma-objects-general.c
index 28b1f54d25..b5d4e8c531 100644
--- a/jerry-core/ecma/operations/ecma-objects-general.c
+++ b/jerry-core/ecma/operations/ecma-objects-general.c
@@ -140,7 +140,7 @@ ecma_op_create_object_object_noarg_and_set_prototype (ecma_object_t *object_prot
  *         Returned value must be freed with ecma_free_value
  */
 ecma_value_t
-ecma_op_general_object_get (ecma_object_t *obj_p, /**< the object */
+ecma_op_general_object_get (const ecma_object_t *obj_p, /**< the object */
                             ecma_string_t *property_name_p) /**< property name */
 {
   JERRY_ASSERT (obj_p != NULL
@@ -194,14 +194,14 @@ ecma_op_general_object_get (ecma_object_t *obj_p, /**< the object */
  *         NULL (i.e. ecma-undefined) - otherwise.
  */
 ecma_property_t *
-ecma_op_general_object_get_own_property (ecma_object_t *obj_p, /**< the object */
+ecma_op_general_object_get_own_property (const ecma_object_t *obj_p, /**< the object */
                                          ecma_string_t *property_name_p) /**< property name */
 {
   JERRY_ASSERT (obj_p != NULL
                 && !ecma_is_lexical_environment (obj_p));
   JERRY_ASSERT (property_name_p != NULL);
 
-  return ecma_find_named_property (obj_p, property_name_p);
+  return ecma_find_named_property ((ecma_object_t *) obj_p, property_name_p);
 } /* ecma_op_general_object_get_own_property */
 
 /**
@@ -215,7 +215,7 @@ ecma_op_general_object_get_own_property (ecma_object_t *obj_p, /**< the object *
  *         NULL (i.e. ecma-undefined) - otherwise.
  */
 ecma_property_t *
-ecma_op_general_object_get_property (ecma_object_t *obj_p, /**< the object */
+ecma_op_general_object_get_property (const ecma_object_t *obj_p, /**< the object */
                                      ecma_string_t *property_name_p) /**< property name */
 {
   JERRY_ASSERT (obj_p != NULL
diff --git a/jerry-core/ecma/operations/ecma-objects-general.h b/jerry-core/ecma/operations/ecma-objects-general.h
index b8b433b550..f55a36411f 100644
--- a/jerry-core/ecma/operations/ecma-objects-general.h
+++ b/jerry-core/ecma/operations/ecma-objects-general.h
@@ -31,9 +31,9 @@ extern ecma_object_t *ecma_op_create_object_object_noarg (void);
 extern ecma_value_t ecma_op_create_object_object_arg (ecma_value_t);
 extern ecma_object_t *ecma_op_create_object_object_noarg_and_set_prototype (ecma_object_t *);
 
-extern ecma_value_t ecma_op_general_object_get (ecma_object_t *, ecma_string_t *);
-extern ecma_property_t *ecma_op_general_object_get_own_property (ecma_object_t *, ecma_string_t *);
-extern ecma_property_t *ecma_op_general_object_get_property (ecma_object_t *, ecma_string_t *);
+extern ecma_value_t ecma_op_general_object_get (const ecma_object_t *, ecma_string_t *);
+extern ecma_property_t *ecma_op_general_object_get_own_property (const ecma_object_t *, ecma_string_t *);
+extern ecma_property_t *ecma_op_general_object_get_property (const ecma_object_t *, ecma_string_t *);
 extern ecma_value_t ecma_op_general_object_put (ecma_object_t *, ecma_string_t *, ecma_value_t, bool);
 extern bool ecma_op_general_object_can_put (ecma_object_t *, ecma_string_t *);
 extern ecma_value_t ecma_op_general_object_delete (ecma_object_t *, ecma_string_t *, bool);
diff --git a/jerry-core/ecma/operations/ecma-objects.c b/jerry-core/ecma/operations/ecma-objects.c
index e98e79152d..9134449e8b 100644
--- a/jerry-core/ecma/operations/ecma-objects.c
+++ b/jerry-core/ecma/operations/ecma-objects.c
@@ -57,7 +57,7 @@ ecma_assert_object_type_is_valid (ecma_object_type_t type) /**< object's impleme
  *         Returned value must be freed with ecma_free_value
  */
 ecma_value_t
-ecma_op_object_get (ecma_object_t *obj_p, /**< the object */
+ecma_op_object_get (const ecma_object_t *obj_p, /**< the object */
                     ecma_string_t *property_name_p) /**< property name */
 {
   JERRY_ASSERT (obj_p != NULL
@@ -98,14 +98,12 @@ ecma_op_object_get (ecma_object_t *obj_p, /**< the object */
  *         NULL (i.e. ecma-undefined) - otherwise.
  */
 static ecma_property_t * __attr_noinline___
-ecma_op_object_get_own_property_longpath (ecma_object_t *obj_p, /**< the object */
+ecma_op_object_get_own_property_longpath (const ecma_object_t *obj_p, /**< the object */
                                           ecma_string_t *property_name_p) /**< property name */
 {
   const ecma_object_type_t type = ecma_get_object_type (obj_p);
   ecma_assert_object_type_is_valid (type);
 
-  const bool is_builtin = ecma_get_object_is_builtin (obj_p);
-
   ecma_property_t *prop_p = NULL;
 
   switch (type)
@@ -145,9 +143,9 @@ ecma_op_object_get_own_property_longpath (ecma_object_t *obj_p, /**< the object
 
   if (unlikely (prop_p == NULL))
   {
-    if (is_builtin)
+    if (ecma_get_object_is_builtin (obj_p))
     {
-      prop_p = ecma_builtin_try_to_instantiate_property (obj_p, property_name_p);
+      prop_p = ecma_builtin_try_to_instantiate_property ((ecma_object_t *) obj_p, property_name_p);
     }
   }
 
@@ -164,7 +162,7 @@ ecma_op_object_get_own_property_longpath (ecma_object_t *obj_p, /**< the object
  *         NULL (i.e. ecma-undefined) - otherwise.
  */
 ecma_property_t *
-ecma_op_object_get_own_property (ecma_object_t *obj_p, /**< the object */
+ecma_op_object_get_own_property (const ecma_object_t *obj_p, /**< the object */
                                  ecma_string_t *property_name_p) /**< property name */
 {
   JERRY_ASSERT (obj_p != NULL
@@ -193,7 +191,7 @@ ecma_op_object_get_own_property (ecma_object_t *obj_p, /**< the object */
  *         NULL (i.e. ecma-undefined) - otherwise.
  */
 ecma_property_t *
-ecma_op_object_get_property (ecma_object_t *obj_p, /**< the object */
+ecma_op_object_get_property (const ecma_object_t *obj_p, /**< the object */
                              ecma_string_t *property_name_p) /**< property name */
 {
   JERRY_ASSERT (obj_p != NULL
diff --git a/jerry-core/ecma/operations/ecma-objects.h b/jerry-core/ecma/operations/ecma-objects.h
index aa6a2bc13c..162fb6d56a 100644
--- a/jerry-core/ecma/operations/ecma-objects.h
+++ b/jerry-core/ecma/operations/ecma-objects.h
@@ -26,9 +26,9 @@
  * @{
  */
 
-extern ecma_value_t ecma_op_object_get (ecma_object_t *, ecma_string_t *);
-extern ecma_property_t *ecma_op_object_get_own_property (ecma_object_t *, ecma_string_t *);
-extern ecma_property_t *ecma_op_object_get_property (ecma_object_t *, ecma_string_t *);
+extern ecma_value_t ecma_op_object_get (const ecma_object_t *, ecma_string_t *);
+extern ecma_property_t *ecma_op_object_get_own_property (const ecma_object_t *, ecma_string_t *);
+extern ecma_property_t *ecma_op_object_get_property (const ecma_object_t *, ecma_string_t *);
 extern ecma_value_t ecma_op_object_put (ecma_object_t *, ecma_string_t *, ecma_value_t, bool);
 extern bool ecma_op_object_can_put (ecma_object_t *, ecma_string_t *);
 extern ecma_value_t ecma_op_object_delete (ecma_object_t *, ecma_string_t *, bool);
diff --git a/jerry-core/ecma/operations/ecma-string-object.c b/jerry-core/ecma/operations/ecma-string-object.c
index 002687aadf..12637ebee0 100644
--- a/jerry-core/ecma/operations/ecma-string-object.c
+++ b/jerry-core/ecma/operations/ecma-string-object.c
@@ -121,7 +121,7 @@ ecma_op_create_string_object (const ecma_value_t *arguments_list_p, /**< list of
  *         Returned value must be freed with ecma_free_value
  */
 ecma_property_t *
-ecma_op_string_object_get_own_property (ecma_object_t *obj_p, /**< a String object */
+ecma_op_string_object_get_own_property (const ecma_object_t *obj_p, /**< a String object */
                                         ecma_string_t *property_name_p) /**< property name */
 {
   JERRY_ASSERT (ecma_get_object_type (obj_p) == ECMA_OBJECT_TYPE_STRING);
@@ -191,7 +191,7 @@ ecma_op_string_object_get_own_property (ecma_object_t *obj_p, /**< a String obje
     // 9.
     ecma_string_t *new_prop_str_value_p = ecma_new_ecma_string_from_code_unit (c);
 
-    new_prop_p = ecma_create_named_data_property (obj_p,
+    new_prop_p = ecma_create_named_data_property ((ecma_object_t *) obj_p,
                                                   new_prop_name_p,
                                                   false, true, false);
 
diff --git a/jerry-core/ecma/operations/ecma-string-object.h b/jerry-core/ecma/operations/ecma-string-object.h
index 55c358042b..f56386ab45 100644
--- a/jerry-core/ecma/operations/ecma-string-object.h
+++ b/jerry-core/ecma/operations/ecma-string-object.h
@@ -29,7 +29,7 @@ extern ecma_value_t
 ecma_op_create_string_object (const ecma_value_t *, ecma_length_t);
 
 extern ecma_property_t *
-ecma_op_string_object_get_own_property (ecma_object_t *, ecma_string_t *);
+ecma_op_string_object_get_own_property (const ecma_object_t *, ecma_string_t *);
 
 extern void
 ecma_op_string_list_lazy_property_names (ecma_object_t *,
diff --git a/jerry-core/jerry-api.h b/jerry-core/jerry-api.h
index b39257fc56..6f796ffe2b 100644
--- a/jerry-core/jerry-api.h
+++ b/jerry-core/jerry-api.h
@@ -284,7 +284,7 @@ jerry_api_string_t *jerry_api_create_string (const jerry_api_char_t *);
 jerry_api_string_t *jerry_api_create_string_sz (const jerry_api_char_t *, jerry_api_size_t);
 
 bool jerry_api_set_array_index_value (jerry_api_object_t *, jerry_api_length_t, jerry_api_value_t *);
-bool jerry_api_get_array_index_value (jerry_api_object_t *, jerry_api_length_t, jerry_api_value_t *);
+bool jerry_api_get_array_index_value (const jerry_api_object_t *, jerry_api_length_t, jerry_api_value_t *);
 
 jerry_api_object_t *jerry_api_create_error (jerry_api_error_t, const jerry_api_char_t *);
 jerry_api_object_t *jerry_api_create_error_sz (jerry_api_error_t, const jerry_api_char_t *, jerry_api_size_t);
@@ -297,8 +297,8 @@ bool jerry_api_add_object_field (jerry_api_object_t *, const jerry_api_char_t *,
                                  const jerry_api_value_t *, bool);
 bool jerry_api_delete_object_field (jerry_api_object_t *, const jerry_api_char_t *, jerry_api_size_t);
 
-bool jerry_api_get_object_field_value (jerry_api_object_t *, const jerry_api_char_t *, jerry_api_value_t *);
-bool jerry_api_get_object_field_value_sz (jerry_api_object_t *, const jerry_api_char_t *, jerry_api_size_t,
+bool jerry_api_get_object_field_value (const jerry_api_object_t *, const jerry_api_char_t *, jerry_api_value_t *);
+bool jerry_api_get_object_field_value_sz (const jerry_api_object_t *, const jerry_api_char_t *, jerry_api_size_t,
                                           jerry_api_value_t *);
 
 bool jerry_api_set_object_field_value (jerry_api_object_t *, const jerry_api_char_t *, const jerry_api_value_t *);
@@ -306,7 +306,7 @@ bool jerry_api_set_object_field_value_sz (jerry_api_object_t *, const jerry_api_
                                           const jerry_api_value_t *);
 
 bool jerry_api_foreach_object_field (jerry_api_object_t *, jerry_object_field_foreach_t, void *);
-bool jerry_api_get_object_native_handle (jerry_api_object_t *, uintptr_t *);
+bool jerry_api_get_object_native_handle (const jerry_api_object_t *, uintptr_t *);
 void jerry_api_set_object_native_handle (jerry_api_object_t *, uintptr_t, jerry_object_free_callback_t);
 bool jerry_api_call_function (jerry_api_object_t *, jerry_api_object_t *, jerry_api_value_t *,
                               const jerry_api_value_t[], uint16_t);
diff --git a/jerry-core/jerry.c b/jerry-core/jerry.c
index 3a33ae8e0b..902ed84944 100644
--- a/jerry-core/jerry.c
+++ b/jerry-core/jerry.c
@@ -169,9 +169,9 @@ jerry_api_value_is_boolean (const jerry_api_value_t *value_p) /**< pointer to ap
 bool
 jerry_api_value_is_number (const jerry_api_value_t *value_p) /**< pointer to api value */
 {
-  return value_p->type == JERRY_API_DATA_TYPE_FLOAT32
-  || value_p->type == JERRY_API_DATA_TYPE_FLOAT64
-  || value_p->type == JERRY_API_DATA_TYPE_UINT32;
+  return (value_p->type == JERRY_API_DATA_TYPE_FLOAT32
+          || value_p->type == JERRY_API_DATA_TYPE_FLOAT64
+          || value_p->type == JERRY_API_DATA_TYPE_UINT32);
 } /* jerry_api_value_is_number */
 
 /**
@@ -783,9 +783,9 @@ jerry_api_set_array_index_value (jerry_api_object_t *array_obj_p, /* array objec
  *         throw exception - otherwise.
  */
 bool
-jerry_api_get_array_index_value (jerry_api_object_t *array_obj_p, /* array object */
-                                 jerry_api_length_t index, /* index to be written */
-                                 jerry_api_value_t *value_p) /* output value at index */
+jerry_api_get_array_index_value (const jerry_api_object_t *array_obj_p, /**< array object */
+                                 jerry_api_length_t index, /**< index to be written */
+                                 jerry_api_value_t *value_p) /**< [out] value at index */
 {
   ecma_string_t *str_idx_p = ecma_new_ecma_string_from_uint32 ((uint32_t) index);
   ecma_value_t get_completion = ecma_op_object_get (array_obj_p, str_idx_p);
@@ -1133,7 +1133,7 @@ jerry_api_delete_object_field (jerry_api_object_t *object_p, /**< object to dele
  *                - there is field with specified name in the object;
  *         false - otherwise.
  */
-bool jerry_api_get_object_field_value (jerry_api_object_t *object_p, /**< object */
+bool jerry_api_get_object_field_value (const jerry_api_object_t *object_p, /**< object */
                                        const jerry_api_char_t *field_name_p, /**< field name */
                                        jerry_api_value_t *field_value_p) /**< [out] field value */
 {
@@ -1212,7 +1212,7 @@ jerry_api_foreach_object_field (jerry_api_object_t *object_p, /**< object */
  *         false - otherwise.
  */
 bool
-jerry_api_get_object_field_value_sz (jerry_api_object_t *object_p, /**< object */
+jerry_api_get_object_field_value_sz (const jerry_api_object_t *object_p, /**< object */
                                      const jerry_api_char_t *field_name_p, /**< name of the field */
                                      jerry_api_size_t field_name_size, /**< size of field name in bytes */
                                      jerry_api_value_t *field_value_p) /**< [out] field value, if retrieved
@@ -1314,7 +1314,7 @@ jerry_api_set_object_field_value_sz (jerry_api_object_t *object_p, /**< object *
  *         false - otherwise.
  */
 bool
-jerry_api_get_object_native_handle (jerry_api_object_t *object_p, /**< object to get handle from */
+jerry_api_get_object_native_handle (const jerry_api_object_t *object_p, /**< object to get handle from */
                                     uintptr_t *out_handle_p) /**< [out] handle value */
 {
   jerry_assert_api_available ();
