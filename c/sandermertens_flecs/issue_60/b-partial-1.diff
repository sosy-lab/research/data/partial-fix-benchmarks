diff --git a/include/flecs.h b/include/flecs.h
index 8450ac442..da01db277 100644
--- a/include/flecs.h
+++ b/include/flecs.h
@@ -663,6 +663,53 @@ ecs_entity_t _ecs_new_w_count(
 #define ecs_new_w_count(world, type, count)\
     _ecs_new_w_count(world, T##type, count)
 
+typedef void* ecs_table_columns_t;
+
+typedef struct ecs_table_data_t {
+    uint32_t row_count;
+    uint32_t column_count;
+    ecs_entity_t *entities;
+    ecs_entity_t *components;
+    ecs_table_columns_t *columns;
+} ecs_table_data_t;
+
+/** Insert data in bulk.
+ * This operation allows applications to insert data in bulk by providing the
+ * entity and component data as arrays. The data is passed in using the
+ * ecs_table_data_t type, which has to be populated with the data that has to be
+ * inserted.
+ * 
+ * The application must at least provide the row_count, column_count and 
+ * components fields. The latter is an array of component identifiers that
+ * identifies the components to be added to the entitiy.
+ *
+ * The entities array must be populated with the entity identifiers to set. If
+ * this field is left NULL, Flecs will create row_count new entities.
+ *
+ * The component data must be provided in the columns field. This is an array of
+ * component arrays. The component arrays must be provided in the same order as
+ * the components have been provided in the components array. For example, if
+ * the components array is set to {ecs_entity(Position), ecs_entity(Velocity)},
+ * the columns must first specify the Position, and then the Velocity array. If
+ * no component data is provided, the components will be left uninitialized.
+ *
+ * Both the entities array and the component data arrays in columns must contain
+ * exactly row_count elements. The columns array must contain exactly 
+ * column_count elements.
+ *
+ * The operation allows for efficient insertion of data for the same set of
+ * entities, provided that the entities are specified in the same order for
+ * every invocation of this function. After executing this operation, entities
+ * will be ordered in the same order specified in the entities array.
+ *
+ * If entities already exist in another table, they will be deleted from that
+ * table and inserted into the new table. 
+ */
+FLECS_EXPORT
+ecs_entity_t ecs_set_w_data(
+    ecs_world_t *world,
+    ecs_table_data_t *data);
+
 /** Create a new child entity.
  * Child entities are equivalent to normal entities, but can additionally be 
  * created with a container entity. Container entities allow for the creation of
diff --git a/include/flecs/util/os_api.h b/include/flecs/util/os_api.h
index db04ad8a7..2edc9afc8 100644
--- a/include/flecs/util/os_api.h
+++ b/include/flecs/util/os_api.h
@@ -200,8 +200,10 @@ void ecs_os_set_api_defaults(void);
 
 #if defined(_MSC_VER) || defined(__MINGW32__)
 #define ecs_os_alloca(type, count) _alloca(sizeof(type) * (count))
+#define _ecs_os_alloca(size, count) _alloca((size) * (count))
 #else
 #define ecs_os_alloca(type, count) alloca(sizeof(type) * (count))
+#define _ecs_os_alloca(size, count) alloca((size) * (count))
 #endif
 
 /* Threads */
diff --git a/src/entity.c b/src/entity.c
index 0a563a2f5..d2a953b1b 100644
--- a/src/entity.c
+++ b/src/entity.c
@@ -121,6 +121,7 @@ bool stage_has_entity(
     ecs_row_t *row_out)
 {
     ecs_row_t row;
+
     if (ecs_map_has(stage->entity_index, entity, &row)) {
         if (row.index) {
             *row_out = row;
@@ -1034,16 +1035,18 @@ ecs_entity_t _ecs_new(
     return entity;
 }
 
-ecs_entity_t _ecs_new_w_count(
+static
+ecs_entity_t set_w_data_intern(
     ecs_world_t *world,
     ecs_type_t type,
-    uint32_t count)
+    ecs_table_data_t *data)
 {
     ecs_assert(world != NULL, ECS_INVALID_PARAMETER, NULL);
 
     ecs_world_t *world_arg = world;
     ecs_stage_t *stage = ecs_get_stage(&world);
     ecs_entity_t result = world->last_handle + 1;
+    uint32_t count = data->row_count;
     
     world->last_handle += count;
 
@@ -1052,28 +1055,197 @@ ecs_entity_t _ecs_new_w_count(
     ecs_assert(!world->is_merging, ECS_INVALID_WHILE_MERGING, NULL);
 
     if (type) {
+        /* Get table, table columns and grow table to accomodate for new
+         * entities */
         ecs_table_t *table = ecs_world_get_table(world, stage, type);
-
         ecs_table_column_t *columns = ecs_table_get_columns(
                 world, stage, table);
         ecs_assert(columns != NULL, ECS_INTERNAL_ERROR, 0);
+        uint32_t i, start_row = 0;
 
-        uint32_t row = ecs_table_grow(world, table, columns, count, result);
-
+        /* Obtain the entity index in the current stage */
         ecs_map_t *entity_index = stage->entity_index;
 
+        /* Grow world entity index only if no entity ids are provided. If ids
+         * are provided, it is possible that they already appear in the entity
+         * index, in which case they will be overwritten. */
         uint32_t cur_index_count = ecs_map_count(entity_index);
-        ecs_map_grow(entity_index, cur_index_count + count);
+        if (!data->entities) {
+            start_row = ecs_table_grow(world, table, columns, count, result) - 1;
+            ecs_map_grow(entity_index, cur_index_count + count);
+        }
+
+        /* Obtain list of entities */
+        ecs_entity_t *entities = ecs_vector_first(columns[0].data);
+        uint32_t row_count = ecs_vector_count(columns[0].data);
+
+        /* If the entity array is NULL, we can safely allocate space for
+         * row_count number of rows, which will give a perf boost the first time
+         * the entities are inserted. */
+        if (!entities) {
+            ecs_table_dim(table, count);
+            entities = ecs_vector_first(columns[0].data);
+            ecs_assert(entities != NULL, ECS_INTERNAL_ERROR, NULL);
+        }
+
+        /* We need to commit each entity individually in order to populate
+         * the entity index */
+        for (i = 0; i < count; i ++) {
+            ecs_entity_t e;
+
+            /* If existing array with entities was provided, use entity ids from
+             * that array. Otherwise use new entity id */
+            if(data->entities) {
+                e = data->entities[i];
+
+                /* If this is not the first entity, check if the next entity in
+                 * the table is the next entity to set. If so, there is no need 
+                 * to update the entity index. This is the fast path that is
+                 * taken if all entities in the table are in the same order as
+                 * provided in the data argument. */
+                if (i) {
+                    if (entities) {
+                        if (start_row + i < row_count) {
+                            if (entities[start_row + i] == e) {
+                                continue;
+                            }
+                        }
+                    }
+                }
+
+                /* Ensure that the last issued handle will always be ahead of the
+                 * entities created by this operation */
+                if (e > world->last_handle) {
+                    world->last_handle = e + 1;
+                }                            
+            } else {
+                e = i + result;
+            }
 
-        uint64_t i, cur_row = row;
-        for (i = result; i < (result + count); i ++) {
-            /* We need to commit each entity individually in order to populate
-             * the entity index */
+            ecs_row_t *row_ptr = ecs_map_get_ptr(entity_index, e);
+            if (row_ptr) {
+                bool is_monitored = false;
+                uint32_t entity_row = row_ptr->index;
+                if (entity_row < 0) {
+                    entity_row *= -1;
+                    is_monitored = true;
+                }
 
-            ecs_row_t new_row = (ecs_row_t){.type = type, .index = cur_row};
-            ecs_map_set(entity_index, i, &new_row);
+                /* If entity exists, and this is the first entity being iterated
+                 * set the start index to the current index of the entity. The
+                 * start_index indicates the start of where the to be added
+                 * entities will be stored. 
+                 * In the ideal scenario, the subsequent entities to be added
+                 * will be provided in the same order after the first entity, so
+                 * that the entity index does not need to be updated. */
+                if (!i && row_ptr->type == type) {
+                    start_row = entity_row - 1;
+                
+                } else {
+                    /* If the entity exists but it is not the first entity, 
+                     * check if it is in the same table. If not, delete the 
+                     * entity from the other table */                        
+                    if (row_ptr->type != type) {
+                        ecs_table_t *old_table = ecs_world_get_table(
+                            world, stage, row_ptr->type);
+                        ecs_table_delete(world, old_table, entity_row);
+
+                        /* Insert new row into destination table */
+                        uint32_t row = ecs_table_insert(
+                            world, table, columns, e) - 1;
+                        if (!i) {
+                            start_row = row;
+                        }
+
+                        /* Entities array may have been reallocated */
+                        entities = ecs_vector_first(columns[0].data);
+
+                    /* If entity exists in the same table but not on the right
+                     * index (if so, we would've found out by now) we need to
+                     * move things around. This will ensure that entities are
+                     * ordered in exactly the same way as they are provided in
+                     * the data argument, so that the subsequent time this
+                     * operation is invoked with entities in the same order, 
+                     * insertion can be much faster. This also allows the data
+                     * from columns in data to be inserted with a single
+                     * memcpy per column. */
+                    } else {
+                        /* If we're not at the top of the table, simply swap the
+                         * next entity with the one that we want at this row. */
+                        if (row_count > (start_row + i)) {
+                            ecs_table_swap(stage, table, columns, 
+                                entity_row, start_row + i, row_ptr, NULL);
+
+                        /* We are at the top of the table and the entity is in
+                         * the table. This scenario is a bit nasty, since we
+                         * need to now move the added entities back one position
+                         * and swap the entity preceding them with the current
+                         * entity. This should only happen in rare cases, as any
+                         * subsequent calls with the same set of entities should
+                         * find the entities in the table to be in the right 
+                         * order. 
+                         * We could just have swapped the order in which the
+                         * entities are inserted, but then subsequent calls
+                         * would still be expensive, and we couldn't just memcpy
+                         * the component data into the columns. */
+                        } else {
+                            /* First, swap the entity preceding the start of the
+                             * added entities with the entity that we want at
+                             * the end of the block */
+                            ecs_table_swap(stage, table, columns, 
+                                entity_row, start_row - 1, row_ptr, NULL);
+
+                            /* Now move back the whole block back one position, 
+                             * while moving the entity before the start to the 
+                             * row right after the block */
+                            ecs_table_move_back_and_swap(
+                                stage, table, columns, start_row, i);
+
+                            start_row --;
+                        }
+                    }
+                }
+
+                /* Update entity index with the new table / row */
+                row_ptr->type = type;
+                row_ptr->index = start_row + i + 1;
+                
+                if (is_monitored) {
+                    row_ptr->index *= -1;
+                }
+            } else {
+                ecs_row_t new_row = (ecs_row_t){.type = type, .index = start_row + i + 1};
+                                
+                ecs_map_set(entity_index, e, &new_row);
 
-            cur_row ++;
+                if (data->entities) {
+                    ecs_table_insert(world, table, columns, e);
+
+                    /* Entities array may have been reallocated */
+                    entities = ecs_vector_first(columns[0].data);                    
+                }
+            }
+        }
+
+        /* If columns were provided, copy data from columns into table */
+        if (data->columns) {
+            uint32_t i;
+            for (i = 0; i < data->column_count; i ++) {
+                ecs_entity_t component = data->components[i];
+                int32_t column = ecs_type_index_of(type, component);
+                ecs_assert(column >= 0, ECS_INTERNAL_ERROR, NULL);
+
+                uint32_t size = columns[column + 1].size;
+                if (size) { 
+                    void *column_data = ecs_vector_first(columns[column + 1].data);
+
+                    memcpy(
+                        ECS_OFFSET(column_data, (start_row) * size),
+                        data->columns[i],
+                        data->row_count * size
+                    );
+                }
+            }
         }
 
         ecs_entity_info_t info = {
@@ -1081,7 +1253,7 @@ ecs_entity_t _ecs_new_w_count(
             .table = table, 
             .columns = columns, 
             .type = type,
-            .index = row
+            .index = start_row + 1
         };
 
         notify_after_commit(
@@ -1091,6 +1263,26 @@ ecs_entity_t _ecs_new_w_count(
     return result;
 }
 
+ecs_entity_t _ecs_new_w_count(
+    ecs_world_t *world,
+    ecs_type_t type,
+    uint32_t count)
+{
+    ecs_table_data_t table_data = {
+        .row_count = count
+    };
+
+    return set_w_data_intern(world, type, &table_data);
+}
+
+ecs_entity_t ecs_set_w_data(
+    ecs_world_t *world,
+    ecs_table_data_t *data)
+{
+    ecs_type_t type = ecs_type_find(world, data->components, data->column_count);
+    return set_w_data_intern(world, type, data);
+}
+
 ecs_entity_t _ecs_new_child(
     ecs_world_t *world,
     ecs_entity_t parent,
diff --git a/src/flecs_private.h b/src/flecs_private.h
index 5ec13049a..1b7bced0f 100644
--- a/src/flecs_private.h
+++ b/src/flecs_private.h
@@ -255,6 +255,22 @@ void ecs_table_free(
     ecs_world_t *world,
     ecs_table_t *table);
 
+void ecs_table_swap(
+    ecs_stage_t *stage,
+    ecs_table_t *table,
+    ecs_table_column_t *columns,
+    uint32_t row_1,
+    uint32_t row_2,
+    ecs_row_t *row_ptr_1,
+    ecs_row_t *row_ptr_2);
+
+void ecs_table_move_back_and_swap(
+    ecs_stage_t *stage,
+    ecs_table_t *table,
+    ecs_table_column_t *columns,
+    uint32_t row,
+    uint32_t count);
+
 /* -- System API -- */
 
 void ecs_system_init_base(
diff --git a/src/map.c b/src/map.c
index 38ac160db..b2deb2c97 100644
--- a/src/map.c
+++ b/src/map.c
@@ -143,6 +143,10 @@ void set_node_data(
     ecs_map_node_t *node,
     const void *data)
 {
+    if (!data) {
+        return;
+    }
+    
     void *node_data = get_node_data(node);
     if (data != node_data) { 
         if (data) {
diff --git a/src/table.c b/src/table.c
index 17be9a458..56c9eccb0 100644
--- a/src/table.c
+++ b/src/table.c
@@ -323,3 +323,106 @@ uint64_t ecs_table_count(
 {
     return ecs_vector_count(table->columns[0].data);
 }
+
+void ecs_table_swap(
+    ecs_stage_t *stage,
+    ecs_table_t *table,
+    ecs_table_column_t *columns,
+    uint32_t row_1,
+    uint32_t row_2,
+    ecs_row_t *row_ptr_1,
+    ecs_row_t *row_ptr_2)
+{    
+    if (row_1 == row_2) {
+        return;
+    }
+
+    ecs_entity_t *entities = ecs_vector_first(columns[0].data);
+    ecs_entity_t e1 = entities[row_1];
+    ecs_entity_t e2 = entities[row_2];
+
+    /* Get pointers to records in entity index */
+    if (!row_ptr_1) {
+        row_ptr_1 = ecs_map_get_ptr(stage->entity_index, e1);
+    }
+
+    if (!row_ptr_2) {
+        row_ptr_2 = ecs_map_get_ptr(stage->entity_index, e2);
+    }
+
+    /* Swap entities */
+    entities[row_1] = e2;
+    entities[row_2] = e1;
+    row_ptr_1->index = row_2;
+    row_ptr_2->index = row_1;
+
+    /* Swap columns */
+    uint32_t i, column_count = ecs_vector_count(table->type);
+    
+    for (i = 0; i < column_count; i ++) {
+        void *data = ecs_vector_first(columns[i + 1].data);
+        uint32_t size = columns[i + 1].size;
+
+        if (size) {
+            void *tmp = _ecs_os_alloca(size, 1);
+
+            void *el_1 = ECS_OFFSET(data, size * row_1);
+            void *el_2 = ECS_OFFSET(data, size * row_2);
+
+            memcpy(tmp, el_1, size);
+            memcpy(el_1, el_2, size);
+            memcpy(el_2, tmp, size);
+        }
+    }
+}
+
+void ecs_table_move_back_and_swap(
+    ecs_stage_t *stage,
+    ecs_table_t *table,
+    ecs_table_column_t *columns,
+    uint32_t row,
+    uint32_t count)
+{
+    ecs_entity_t *entities = ecs_vector_first(columns[0].data);
+    uint32_t i;
+
+
+    /* First move back and swap entities */
+    ecs_entity_t e = entities[row - 1];
+    for (i = 0; i < count; i ++) {
+        ecs_entity_t cur = entities[row + i];
+        entities[row + i - 1] = cur;
+
+        ecs_row_t *row_ptr = ecs_map_get_ptr(stage->entity_index, cur);
+        row_ptr->index = row + i;
+    }
+    entities[row + count - 1] = e;
+    ecs_row_t *row_ptr = ecs_map_get_ptr(stage->entity_index, e);
+    row_ptr->index = row + count - 1;
+
+    /* Move back and swap columns */
+    uint32_t column_count = ecs_vector_count(table->type);
+    
+    for (i = 0; i < column_count; i ++) {
+        void *data = ecs_vector_first(columns[i + 1].data);
+        uint32_t size = columns[i + 1].size;
+
+        if (size) {
+            /* Backup first element */
+            void *tmp = _ecs_os_alloca(size, 1);
+            void *el = ECS_OFFSET(data, size * row);
+            memcpy(tmp, el, size);
+
+            /* Move component values */
+            for (i = 0; i < count; i ++) {
+                void *dst = ECS_OFFSET(data, size * (row + i - 1));
+                void *src = ECS_OFFSET(data, size * (row + i));
+                memcpy(dst, src, size);
+            }
+
+            /* Move first element to last element */
+            void *dst = ECS_OFFSET(data, size * (row + count - 1));
+            memcpy(dst, tmp, size);
+        }
+    }
+}
diff --git a/src/vector.c b/src/vector.c
index e67e6ccb3..0052ce1f0 100644
--- a/src/vector.c
+++ b/src/vector.c
@@ -270,7 +270,7 @@ uint32_t ecs_vector_set_count(
 void* ecs_vector_first(
     const ecs_vector_t *array)
 {
-    if (array && array->count) {
+    if (array && array->size) {
         return ARRAY_BUFFER(array);
     } else {
         return NULL;
diff --git a/test/api/project.json b/test/api/project.json
index 861c67659..b6506910a 100644
--- a/test/api/project.json
+++ b/test/api/project.json
@@ -42,6 +42,18 @@
                 "type_w_2_tags",
                 "type_w_tag_mixed"
             ]
+        }, {
+            "id": "Set_w_data",
+            "testcases": [
+                "1_column_3_rows",
+                "2_columns_3_rows",
+                "1_column_3_rows_w_entities",
+                "2_columns_3_rows_w_entities",
+                "1_column_3_rows_overwrite_entities",
+                "2_columns_3_rows_overwrite_entities",
+                "overwrite_different_order",
+                "overwrite_different_type"
+            ]
         }, {
             "id": "Add",
             "testcases": [
diff --git a/test/api/src/Set_w_data.c b/test/api/src/Set_w_data.c
new file mode 100644
index 000000000..c41cc99f8
--- /dev/null
+++ b/test/api/src/Set_w_data.c
@@ -0,0 +1,412 @@
+#include <api.h>
+
+void Set_w_data_1_column_3_rows() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+
+    ecs_entity_t e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 1,
+        .row_count = 3,
+        .entities = NULL,
+        .components = (ecs_entity_t[]){ecs_entity(Position)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {10, 20},
+                {11, 21},
+                {12, 22}
+            }
+        }
+    });
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Position), 3);
+
+    int i;
+    for (i = 0; i < 3; i ++) {
+        test_assert(ecs_has(world, e + i, Position));
+
+        Position *p = ecs_get_ptr(world, e + i, Position);
+        test_assert(p != NULL);
+        test_int(p->x, 10 + i);
+        test_int(p->y, 20 + i);
+    }
+
+    ecs_fini(world);
+}
+
+void Set_w_data_2_columns_3_rows() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+    ECS_COMPONENT(world, Velocity);
+    ECS_TYPE(world, Type, Position, Velocity);
+
+    ecs_entity_t e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 2,
+        .row_count = 3,
+        .entities = NULL,
+        .components = (ecs_entity_t[]){ecs_entity(Position), ecs_entity(Velocity)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {10, 20},
+                {11, 21},
+                {12, 22}
+            },
+            (Velocity[]) {
+                {30, 40},
+                {31, 41},
+                {32, 42}
+            },
+        }
+    });
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Type), 3);
+
+    int i;
+    for (i = 0; i < 3; i ++) {
+        test_assert(ecs_has(world, e + i, Position));
+        test_assert(ecs_has(world, e + i, Velocity));
+
+        Position *p = ecs_get_ptr(world, e + i, Position);
+        test_assert(p != NULL);
+        test_int(p->x, 10 + i);
+        test_int(p->y, 20 + i);
+
+        Velocity *v = ecs_get_ptr(world, e + i, Velocity);
+        test_assert(v != NULL);
+        test_int(v->x, 30 + i);
+        test_int(v->y, 40 + i);        
+    }
+
+    ecs_fini(world);
+}
+
+void Set_w_data_1_column_3_rows_w_entities() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+
+    ecs_entity_t e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 1,
+        .row_count = 3,
+        .entities = (ecs_entity_t[]){5000, 5001, 5002},
+        .components = (ecs_entity_t[]){ecs_entity(Position)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {10, 20},
+                {11, 21},
+                {12, 22}
+            }
+        }
+    });
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Position), 3);
+
+    int i;
+    for (i = 0; i < 3; i ++) {
+        ecs_entity_t e = 5000 + i;
+        test_assert(ecs_has(world, e, Position));
+        Position *p = ecs_get_ptr(world, e, Position);
+        test_assert(p != NULL);
+        test_int(p->x, 10 + i);
+        test_int(p->y, 20 + i);
+    }
+
+    ecs_fini(world);
+}
+
+void Set_w_data_2_columns_3_rows_w_entities() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+    ECS_COMPONENT(world, Velocity);
+    ECS_TYPE(world, Type, Position, Velocity);
+
+    ecs_entity_t e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 2,
+        .row_count = 3,
+        .entities = (ecs_entity_t[]){5000, 5001, 5002},
+        .components = (ecs_entity_t[]){ecs_entity(Position), ecs_entity(Velocity)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {10, 20},
+                {11, 21},
+                {12, 22}
+            },
+            (Velocity[]) {
+                {30, 40},
+                {31, 41},
+                {32, 42}
+            },
+        }
+    });
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Type), 3);
+
+    int i;
+    for (i = 0; i < 3; i ++) {
+        test_assert(ecs_has(world, 5000 + i, Position));
+        test_assert(ecs_has(world, 5000 + i, Velocity));
+
+        Position *p = ecs_get_ptr(world, 5000 + i, Position);
+        test_assert(p != NULL);
+        test_int(p->x, 10 + i);
+        test_int(p->y, 20 + i);
+
+        Velocity *v = ecs_get_ptr(world, 5000 + i, Velocity);
+        test_assert(v != NULL);
+        test_int(v->x, 30 + i);
+        test_int(v->y, 40 + i);        
+    }
+
+    ecs_fini(world);
+}
+
+void Set_w_data_1_column_3_rows_overwrite_entities() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+
+    ecs_entity_t e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 1,
+        .row_count = 3,
+        .entities = (ecs_entity_t[]){5000, 5001, 5002},
+        .components = (ecs_entity_t[]){ecs_entity(Position)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {10, 20},
+                {11, 21},
+                {12, 22}
+            }
+        }
+    });
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Position), 3);
+
+    /* Write again */
+    e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 1,
+        .row_count = 3,
+        .entities = (ecs_entity_t[]){5000, 5001, 5002},
+        .components = (ecs_entity_t[]){ecs_entity(Position)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {30, 40},
+                {31, 41},
+                {32, 42}
+            }
+        }
+    });  
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Position), 3);
+
+    int i;
+    for (i = 0; i < 3; i ++) {
+        ecs_entity_t e = 5000 + i;
+        test_assert(ecs_has(world, e, Position));
+
+        Position *p = ecs_get_ptr(world, e, Position);
+        test_assert(p != NULL);
+        test_int(p->x, 30 + i);
+        test_int(p->y, 40 + i);
+    }
+
+    ecs_fini(world);
+}
+
+void Set_w_data_2_columns_3_rows_overwrite_entities() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+    ECS_COMPONENT(world, Velocity);
+    ECS_TYPE(world, Type, Position, Velocity);
+
+    ecs_entity_t e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 2,
+        .row_count = 3,
+        .entities = (ecs_entity_t[]){5000, 5001, 5002},
+        .components = (ecs_entity_t[]){ecs_entity(Position), ecs_entity(Velocity)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {10, 20},
+                {11, 21},
+                {12, 22}
+            },
+            (Velocity[]) {
+                {30, 40},
+                {31, 41},
+                {32, 42}
+            },
+        }
+    });
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Type), 3);
+
+    e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 2,
+        .row_count = 3,
+        .entities = (ecs_entity_t[]){5000, 5001, 5002},
+        .components = (ecs_entity_t[]){ecs_entity(Position), ecs_entity(Velocity)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {50, 60},
+                {51, 61},
+                {52, 62}
+            },
+            (Velocity[]) {
+                {70, 80},
+                {71, 81},
+                {72, 82}
+            },
+        }
+    });
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Type), 3);
+
+    int i;
+    for (i = 0; i < 3; i ++) {
+        test_assert(ecs_has(world, 5000 + i, Position));
+        test_assert(ecs_has(world, 5000 + i, Velocity));
+
+        Position *p = ecs_get_ptr(world, 5000 + i, Position);
+        test_assert(p != NULL);
+        test_int(p->x, 50 + i);
+        test_int(p->y, 60 + i);
+
+        Velocity *v = ecs_get_ptr(world, 5000 + i, Velocity);
+        test_assert(v != NULL);
+        test_int(v->x, 70 + i);
+        test_int(v->y, 80 + i);        
+    }
+
+    ecs_fini(world);
+}
+
+void Set_w_data_overwrite_different_order() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+
+    ecs_entity_t e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 1,
+        .row_count = 3,
+        .entities = (ecs_entity_t[]){5000, 5001, 5002},
+        .components = (ecs_entity_t[]){ecs_entity(Position)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {10, 20},
+                {11, 21},
+                {12, 22}
+            }
+        }
+    });
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Position), 3);
+
+    /* Write again, in different order */
+    e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 1,
+        .row_count = 3,
+        .entities = (ecs_entity_t[]){5002, 5000, 5001},
+        .components = (ecs_entity_t[]){ecs_entity(Position)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {32, 42},
+                {30, 40},
+                {31, 41}
+            }
+        }
+    });
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Position), 3);
+
+    int i;
+    for (i = 0; i < 3; i ++) {
+        ecs_entity_t e = 5000 + i;
+        test_assert(ecs_has(world, e, Position));
+
+        Position *p = ecs_get_ptr(world, e, Position);
+        test_assert(p != NULL);
+
+        test_int(p->x, 30 + i);
+        test_int(p->y, 40 + i);
+    }
+
+    ecs_fini(world);
+}
+
+void Set_w_data_overwrite_different_type() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+    ECS_COMPONENT(world, Velocity);
+    ECS_TYPE(world, Type, Position, Velocity);
+
+    ecs_entity_t e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 1,
+        .row_count = 3,
+        .entities = (ecs_entity_t[]){5000, 5001, 5002},
+        .components = (ecs_entity_t[]){ecs_entity(Position)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {10, 20},
+                {11, 21},
+                {12, 22}
+            }
+        }
+    });
+
+    test_assert(e != 0);
+    test_int(ecs_count(world, Position), 3);
+
+    e = ecs_set_w_data(world, &(ecs_table_data_t){
+        .column_count = 2,
+        .row_count = 3,
+        .entities = (ecs_entity_t[]){5000, 5001, 5002},
+        .components = (ecs_entity_t[]){ecs_entity(Position), ecs_entity(Velocity)},
+        .columns = (ecs_table_columns_t[]){
+            (Position[]) {
+                {50, 60},
+                {51, 61},
+                {52, 62}
+            },
+            (Velocity[]) {
+                {70, 80},
+                {71, 81},
+                {72, 82}
+            },
+        }
+    });
+
+    test_assert(e != 0);
+    //test_int(ecs_count(world, Position), 3);
+    test_int(ecs_count(world, Type), 3);
+
+    int i;
+    for (i = 0; i < 3; i ++) {
+        test_assert(ecs_has(world, 5000 + i, Position));
+        test_assert(ecs_has(world, 5000 + i, Velocity));
+
+        Position *p = ecs_get_ptr(world, 5000 + i, Position);
+        test_assert(p != NULL);
+        test_int(p->x, 50 + i);
+        test_int(p->y, 60 + i);
+
+        Velocity *v = ecs_get_ptr(world, 5000 + i, Velocity);
+        test_assert(v != NULL);
+        test_int(v->x, 70 + i);
+        test_int(v->y, 80 + i);        
+    }
+
+    ecs_fini(world);
+}
diff --git a/test/api/src/main.c b/test/api/src/main.c
index 35de6576c..3c38fa54b 100644
--- a/test/api/src/main.c
+++ b/test/api/src/main.c
@@ -35,6 +35,16 @@ void New_w_Count_type_w_tag(void);
 void New_w_Count_type_w_2_tags(void);
 void New_w_Count_type_w_tag_mixed(void);
 
+// Testsuite 'Set_w_data'
+void Set_w_data_1_column_3_rows(void);
+void Set_w_data_2_columns_3_rows(void);
+void Set_w_data_1_column_3_rows_w_entities(void);
+void Set_w_data_2_columns_3_rows_w_entities(void);
+void Set_w_data_1_column_3_rows_overwrite_entities(void);
+void Set_w_data_2_columns_3_rows_overwrite_entities(void);
+void Set_w_data_overwrite_different_order(void);
+void Set_w_data_overwrite_different_type(void);
+
 // Testsuite 'Add'
 void Add_zero(void);
 void Add_component(void);
@@ -834,6 +844,44 @@ static bake_test_suite suites[] = {
             }
         }
     },
+    {
+        .id = "Set_w_data",
+        .testcase_count = 8,
+        .testcases = (bake_test_case[]){
+            {
+                .id = "1_column_3_rows",
+                .function = Set_w_data_1_column_3_rows
+            },
+            {
+                .id = "2_columns_3_rows",
+                .function = Set_w_data_2_columns_3_rows
+            },
+            {
+                .id = "1_column_3_rows_w_entities",
+                .function = Set_w_data_1_column_3_rows_w_entities
+            },
+            {
+                .id = "2_columns_3_rows_w_entities",
+                .function = Set_w_data_2_columns_3_rows_w_entities
+            },
+            {
+                .id = "1_column_3_rows_overwrite_entities",
+                .function = Set_w_data_1_column_3_rows_overwrite_entities
+            },
+            {
+                .id = "2_columns_3_rows_overwrite_entities",
+                .function = Set_w_data_2_columns_3_rows_overwrite_entities
+            },
+            {
+                .id = "overwrite_different_order",
+                .function = Set_w_data_overwrite_different_order
+            },
+            {
+                .id = "overwrite_different_type",
+                .function = Set_w_data_overwrite_different_type
+            }
+        }
+    },
     {
         .id = "Add",
         .testcase_count = 29,
@@ -3545,5 +3593,5 @@ static bake_test_suite suites[] = {
 
 int main(int argc, char *argv[]) {
     ut_init(argv[0]);
-    return bake_test_run("api", argc, argv, suites, 35);
+    return bake_test_run("api", argc, argv, suites, 36);
 }
