diff --git a/plugins/imfile/imfile.c b/plugins/imfile/imfile.c
index b2ab1886fc..e80ae484ae 100644
--- a/plugins/imfile/imfile.c
+++ b/plugins/imfile/imfile.c
@@ -111,6 +111,7 @@ static struct configSettings_s {
 
 struct instanceConf_s {
 	uchar *pszFileName;
+	uchar *pszFileName_forOldStateFile; /* we unfortunately needs this to read old state files */
 	uchar *pszDirName;
 	uchar *pszFileBaseName;
 	uchar *pszTag;
@@ -303,6 +304,97 @@ static struct cnfparamblk inppblk =
 #include "im-helper.h" /* must be included AFTER the type definitions! */
 
 
+/* Support for "old cruft" state files will potentially become optional in the
+ * future (hopefully). To prepare so, we use conditional compilation with a
+ * fixed-true condition ;-) -- rgerhards, 2018-03-28
+ * reason: https://github.com/rsyslog/rsyslog/issues/2231#issuecomment-376862280
+ */
+#define ENABLE_V1_STATE_FILE_FORMAT_SUPPORT 1
+#ifdef ENABLE_V1_STATE_FILE_FORMAT_SUPPORT
+static uchar * ATTR_NONNULL(1, 2)
+OLD_getStateFileName(const instanceConf_t *const inst,
+	 	 uchar *const __restrict__ buf,
+		 const size_t lenbuf)
+{
+	DBGPRINTF("OLD_getStateFileName trying '%s'\n", inst->pszFileName_forOldStateFile);
+	snprintf((char*)buf, lenbuf - 1, "imfile-state:%s", inst->pszFileName_forOldStateFile);
+	buf[lenbuf-1] = '\0'; /* be on the safe side... */
+	uchar *p = buf;
+	for( ; *p ; ++p) {
+		if(*p == '/')
+			*p = '-';
+	}
+	return buf;
+}
+
+/* try to open an old-style state file for given file. If the state file does not
+ * exist or cannot be read, an error is returned.
+ */
+static rsRetVal ATTR_NONNULL(1)
+OLD_openFileWithStateFile(act_obj_t *const act)
+{
+	DEFiRet;
+	strm_t *psSF = NULL;
+	uchar pszSFNam[MAXFNAME];
+	size_t lenSFNam;
+	struct stat stat_buf;
+	uchar statefile[MAXFNAME];
+	const instanceConf_t *const inst = act->edge->instarr[0];// TODO: same file, multiple instances?
+
+	uchar *const statefn = OLD_getStateFileName(inst, statefile, sizeof(statefile));
+	DBGPRINTF("OLD_openFileWithStateFile: trying to open state for '%s', state file '%s'\n",
+		  act->name, statefn);
+
+	/* Get full path and file name */
+	lenSFNam = getFullStateFileName(statefn, pszSFNam, sizeof(pszSFNam));
+
+	/* check if the file exists */
+	if(stat((char*) pszSFNam, &stat_buf) == -1) {
+		if(errno == ENOENT) {
+			DBGPRINTF("OLD_openFileWithStateFile: NO state file (%s) exists for '%s'\n",
+				pszSFNam, act->name);
+			ABORT_FINALIZE(RS_RET_FILE_NOT_FOUND);
+		} else {
+			char errStr[1024];
+			rs_strerror_r(errno, errStr, sizeof(errStr));
+			DBGPRINTF("OLD_openFileWithStateFile: error trying to access state "
+				"file for '%s':%s\n", act->name, errStr);
+			ABORT_FINALIZE(RS_RET_IO_ERROR);
+		}
+	}
+
+	/* If we reach this point, we have a state file */
+
+	DBGPRINTF("old state file found - instantiating from it\n");
+	CHKiRet(strm.Construct(&psSF));
+	CHKiRet(strm.SettOperationsMode(psSF, STREAMMODE_READ));
+	CHKiRet(strm.SetsType(psSF, STREAMTYPE_FILE_SINGLE));
+	CHKiRet(strm.SetFName(psSF, pszSFNam, lenSFNam));
+	CHKiRet(strm.SetFileNotFoundError(psSF, inst->fileNotFoundError));
+	CHKiRet(strm.ConstructFinalize(psSF));
+
+	/* read back in the object */
+	CHKiRet(obj.Deserialize(&act->pStrm, (uchar*) "strm", psSF, NULL, act));
+	free(act->pStrm->pszFName);
+	CHKmalloc(act->pStrm->pszFName = ustrdup(act->name));
+
+	strm.CheckFileChange(act->pStrm);
+	CHKiRet(strm.SeekCurrOffs(act->pStrm));
+
+	/* we now persist the new state file and delete the old one, so we will
+	 * never have to deal with the old one. */
+	persistStrmState(act);
+	unlink((char*)pszSFNam);
+
+finalize_it:
+	if(psSF != NULL)
+		strm.Destruct(&psSF);
+	RETiRet;
+}
+#endif /* #ifdef ENABLE_V1_STATE_FILE_FORMAT_SUPPORT */
+
+
+
 #if 0 // Code we can potentially use for new functionality // TODO: use or remove
 //TODO add a kind of portable asprintf:
 static const char * ATTR_NONNULL()
@@ -995,7 +1087,7 @@ getFullStateFileName(const uchar *const pszstatefile, uchar *const pszout, const
 	pszworkdir = glblGetWorkDirRaw();
 
 	/* Construct file name */
-	lenout = snprintf((char*)pszout, ilenout, "%s/%s.json",
+	lenout = snprintf((char*)pszout, ilenout, "%s/%s",
 			     (char*) (pszworkdir == NULL ? "." : (char*) pszworkdir), (char*)pszstatefile);
 
 	/* return out length */
@@ -1040,16 +1132,6 @@ getStateFileName(const act_obj_t *const act,
 	snprintf((char*)buf, lenbuf - 1, "imfile-state:%lld", (long long) act->ino);
 	DBGPRINTF("getStateFileName:  stat file name now is %s\n", buf);
 	return buf;
-#if 0
-	uchar *ret;
-	//if(inst->pszStateFile == NULL) {
-		genStateFileName(act->name, buf, lenbuf);
-		ret = buf;
-	//} else {
-		//ret = inst->pszStateFile;
-	//}
-	return ret;
-#endif
 }
 
 
@@ -1127,8 +1209,10 @@ openFileWithStateFile(act_obj_t *const act)
 	const int fd = open((char*)pszSFNam, O_CLOEXEC | O_NOCTTY | O_RDONLY, 0600);
 	if(fd < 0) {
 		if(errno == ENOENT) {
-			DBGPRINTF("NO state file (%s) exists for '%s'\n", pszSFNam, act->name);
-			ABORT_FINALIZE(RS_RET_FILE_NOT_FOUND);
+			DBGPRINTF("NO state file (%s) exists for '%s' - trying to see if "
+				"old-style file exists\n", pszSFNam, act->name);
+			CHKiRet(OLD_openFileWithStateFile(act));
+			FINALIZE;
 		} else {
 			LogError(errno, RS_RET_IO_ERROR,
 				"imfile error trying to access state file for '%s'",
@@ -1188,6 +1272,7 @@ openFileWithStateFile(act_obj_t *const act)
 	CHKiRet(strm.SeekCurrOffs(act->pStrm));
 
 finalize_it:
+	dbgprintf("openStateFile returned %d\n", iRet);
 	RETiRet;
 }
 
@@ -1418,6 +1503,7 @@ checkInstance(instanceConf_t *const inst)
 	if(inst->pszFileName == NULL)
 		ABORT_FINALIZE(RS_RET_INTERNAL_ERROR);
 
+	CHKmalloc(inst->pszFileName_forOldStateFile = ustrdup(inst->pszFileName));
 	if(loadModConf->normalizePath) {
 		if(inst->pszFileName[0] == '.' && inst->pszFileName[1] == '/') {
 			DBGPRINTF("imfile: removing heading './' from name '%s'\n", inst->pszFileName);
@@ -1809,6 +1895,7 @@ CODESTARTfreeCnf
 		free(inst->pszFileName);
 		free(inst->pszTag);
 		free(inst->pszStateFile);
+		free(inst->pszFileName_forOldStateFile);
 		if(inst->startRegex != NULL) {
 			regfree(&inst->end_preg);
 			free(inst->startRegex);
diff --git a/tests/Makefile.am b/tests/Makefile.am
index e51a44349a..cc9f1f9c4c 100644
--- a/tests/Makefile.am
+++ b/tests/Makefile.am
@@ -791,6 +791,7 @@ TESTS += \
 	imfile-wildcards-dirs-multi4.sh \
 	imfile-wildcards-dirs-multi5.sh \
 	imfile-wildcards-dirs-multi5-polling.sh \
+	imfile-old-state-file.sh \
 	imfile-rename-while-stopped.sh \
 	imfile-rename.sh
 
@@ -1494,6 +1495,7 @@ EXTRA_DIST= \
 	imfile-wildcards-dirs-multi4.sh \
 	imfile-wildcards-dirs-multi5.sh \
 	imfile-wildcards-dirs-multi5-polling.sh \
+	imfile-old-state-file.sh \
 	imfile-rename-while-stopped.sh \
 	imfile-rename.sh \
 	testsuites/imfile-wildcards-simple.conf \
diff --git a/tests/imfile-old-state-file.sh b/tests/imfile-old-state-file.sh
new file mode 100755
index 0000000000..6dab0fac44
--- /dev/null
+++ b/tests/imfile-old-state-file.sh
@@ -0,0 +1,63 @@
+#!/bin/bash
+# this test checks that old (v1, pre 8.34.0) imfile state files are
+# properly read in. It is based on imfile-readmode2-with-persists.sh,
+# where the first part before the shutdown is removed, and an old state
+# file is populated. Note that in contrast to the original test the
+# initial set of lines from the input file is missing - this is
+# exactly what shall happen.
+# This is part of the rsyslog testbench, licensed under ASL 2.0
+# added 2018-03-29 by rgerhards
+. $srcdir/diag.sh init
+
+# do mock-up setup
+echo 'msgnum:0
+ msgnum:1' > rsyslog.input
+echo 'msgnum:2' >> rsyslog.input
+
+# we need to patch the state file to match the current inode number
+inode=$(ls -i rsyslog.input|awk '{print $1}')
+leninode=${#inode}
+newline="+inode:2:${leninode}:${inode}:"
+
+sed s/+inode:2:7:4464465:/${newline}/ <testsuites/imfile-old-state-file_imfile-state\:.-rsyslog.input > test-spool/imfile-state\:.-rsyslog.input
+printf "info: new input file: $(ls -i rsyslog.input)\n"
+printf "info: new inode line: ${newline}\n"
+printf "info: patched state file:\n"
+cat test-spool/imfile-state\:.-rsyslog.input
+
+. $srcdir/diag.sh startup imfile-readmode2-with-persists.conf
+
+echo 'msgnum:3
+ msgnum:4' >> rsyslog.input
+echo 'msgnum:5' >> rsyslog.input
+
+. $srcdir/diag.sh shutdown-when-empty # shut down rsyslogd when done processing messages
+. $srcdir/diag.sh wait-shutdown    # we need to wait until rsyslogd is finished!
+
+NUMLINES=$(grep -c HEADER rsyslog.out.log 2>/dev/null)
+
+if [ -z $NUMLINES ]; then
+  echo "ERROR: expecting at least a match for HEADER, maybe rsyslog.out.log wasn't even written?"
+  cat ./rsyslog.out.log
+  . $srcdir/diag.sh error-exit 1
+else
+  # note: we expect only 2 headers as the first file part if NOT processed!
+  if [ ! $NUMLINES -eq 2 ]; then
+    echo "ERROR: expecting 2 headers, got $NUMLINES"
+    cat ./rsyslog.out.log
+    . $srcdir/diag.sh error-exit 1
+  fi
+fi
+
+## check if all the data we expect to get in the file is there
+
+for i in {2..4}; do
+  grep msgnum:$i rsyslog.out.log > /dev/null 2>&1
+  if [ ! $? -eq 0 ]; then
+    echo "ERROR: expecting the string 'msgnum:$i', it's not there"
+    cat ./rsyslog.out.log
+    . $srcdir/diag.sh error-exit 1
+  fi
+done
+
+. $srcdir/diag.sh exit
