diff --git a/api/docs/release.dox b/api/docs/release.dox
index 394099d59d..794d58c635 100644
--- a/api/docs/release.dox
+++ b/api/docs/release.dox
@@ -259,6 +259,8 @@ Further non-compatibility-affecting changes include:
    lookups when internal symbols are not needed.
  - Added dr_merge_arith_flags() as a convenience routine to merge arithmetic flags
    for restoration done by outlined code.
+ - Added dr_annotation_pass_pc() to obtain the interrupted PC in an annotation
+   handler.
 
 **************************************************
 <hr>
diff --git a/core/annotations.c b/core/annotations.c
index 1723cf2a4a..6b2ecb680a 100644
--- a/core/annotations.c
+++ b/core/annotations.c
@@ -1,5 +1,5 @@
 /* ******************************************************
- * Copyright (c) 2014-2019 Google, Inc.  All rights reserved.
+ * Copyright (c) 2014-2020 Google, Inc.  All rights reserved.
  * ******************************************************/
 
 /*
@@ -609,6 +609,25 @@ dr_annotation_register_return(const char *annotation_name, void *return_value)
     return result;
 }
 
+bool
+dr_annotation_pass_pc(const char *annotation_name)
+{
+    bool result = true;
+    dr_annotation_handler_t *handler;
+
+    TABLE_RWLOCK(handlers, write, lock);
+    ASSERT_TABLE_SYNCHRONIZED(handlers, WRITE);
+    handler = (dr_annotation_handler_t *)strhash_hash_lookup(GLOBAL_DCONTEXT, handlers,
+                                                             annotation_name);
+    if (handler == NULL) {
+        result = false;
+    } else {
+        handler->pass_pc_in_slot = true;
+    }
+    TABLE_RWLOCK(handlers, write, unlock);
+    return result;
+}
+
 bool
 dr_annotation_unregister_call(const char *annotation_name, void *callee)
 {
diff --git a/core/annotations.h b/core/annotations.h
index 8ed8953fc9..337673793a 100644
--- a/core/annotations.h
+++ b/core/annotations.h
@@ -1,5 +1,5 @@
 /* ******************************************************
- * Copyright (c) 2014-2019 Google, Inc.  All rights reserved.
+ * Copyright (c) 2014-2020 Google, Inc.  All rights reserved.
  * ******************************************************/
 
 /*
@@ -198,6 +198,19 @@ DR_API
 bool
 dr_annotation_register_return(const char *annotation_name, void *return_value);
 
+DR_API
+/**
+ * Can only be called on an annotation already registered with
+ * dr_annotation_register_call().  When the annotation is encountered, the PC of
+ * the annotation interruption point will be available in DR scratch slot #SPILL_SLOT_2,
+ * which can be read with dr_read_saved_reg().
+ *
+ * @param[in] annotation_name  The name of the annotation function as it appears in the
+ *                             target app's source code (unmangled).
+ */
+bool
+dr_annotation_pass_pc(const char *annotation_name);
+
 DR_API
 /**
  * Unregister the specified handler from a DR annotation. Instances of the annotation that
@@ -327,6 +340,7 @@ typedef struct _dr_annotation_handler_t {
     uint num_args;
     opnd_t *args;
     bool is_void;
+    bool pass_pc_in_slot;
 } dr_annotation_handler_t;
 
 void
diff --git a/core/arch/x86/mangle.c b/core/arch/x86/mangle.c
index 76c0505b35..8ba00d2326 100644
--- a/core/arch/x86/mangle.c
+++ b/core/arch/x86/mangle.c
@@ -1,5 +1,5 @@
 /* ******************************************************************************
- * Copyright (c) 2010-2019 Google, Inc.  All rights reserved.
+ * Copyright (c) 2010-2020 Google, Inc.  All rights reserved.
  * Copyright (c) 2010 Massachusetts Institute of Technology  All rights reserved.
  * Copyright (c) 2000-2010 VMware, Inc.  All rights reserved.
  * ******************************************************************************/
@@ -3405,6 +3405,12 @@ mangle_annotation_helper(dcontext_t *dcontext, instr_t *label, instrlist_t *ilis
                                     UNPROTECTED);
             memcpy(args, handler->args, sizeof(opnd_t) * handler->num_args);
         }
+        if (handler->pass_pc_in_slot) {
+            app_pc pc = GET_ANNOTATION_APP_PC(label_data);
+            instrlist_insert_mov_immed_ptrsz(
+                dcontext, (ptr_int_t)pc, dr_reg_spill_slot_opnd(dcontext, SPILL_SLOT_2),
+                ilist, label, NULL, NULL);
+        }
         dr_insert_clean_call_ex_varg(dcontext, ilist, label,
                                      receiver->instrumentation.callback,
                                      receiver->save_fpstate ? DR_CLEANCALL_SAVE_FLOAT : 0,
diff --git a/suite/tests/client-interface/annotation-concurrency.c b/suite/tests/client-interface/annotation-concurrency.c
index 49c21b0e74..5e2e9160ba 100644
--- a/suite/tests/client-interface/annotation-concurrency.c
+++ b/suite/tests/client-interface/annotation-concurrency.c
@@ -277,6 +277,8 @@ main(int argc, char **argv)
         rhs_vector[i_row] = (double)(2 * row_sum) - (double)(i_row + 1);
     }
 
+    TEST_ANNOTATION_GET_PC();
+
     TEST_ANNOTATION_TEN_ARGS(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
 
     /* Initailize X[i] = B[i] */
diff --git a/suite/tests/client-interface/annotation-concurrency.dll.c b/suite/tests/client-interface/annotation-concurrency.dll.c
index e67c7ad5dd..bd588ea281 100644
--- a/suite/tests/client-interface/annotation-concurrency.dll.c
+++ b/suite/tests/client-interface/annotation-concurrency.dll.c
@@ -1,5 +1,5 @@
 /* ******************************************************
- * Copyright (c) 2015 Google, Inc.  All rights reserved.
+ * Copyright (c) 2015-2020 Google, Inc.  All rights reserved.
  * ******************************************************/
 
 /*
@@ -206,6 +206,15 @@ set_mode(uint context_id, uint new_mode)
     dr_mutex_unlock(context_lock);
 }
 
+static void
+get_pc(void)
+{
+    app_pc pc = (app_pc)dr_read_saved_reg(dr_get_current_drcontext(), SPILL_SLOT_2);
+    module_data_t *exe = dr_get_main_module();
+    ASSERT(pc >= exe->start && pc <= exe->end);
+    dr_free_module_data(exe);
+}
+
 #if !(defined(WINDOWS) && defined(X64))
 /* Identical Valgrind annotation handlers for concurrent rotation and invocation */
 static ptr_uint_t
@@ -465,6 +474,9 @@ dr_client_main(client_id_t id, int argc, const char *argv[])
                   (void *)rotate_valgrind_handler, 1);
 #endif
 
+    register_call("test_annotation_get_pc", (void *)get_pc, 0);
+    dr_annotation_pass_pc("test_annotation_get_pc");
+
     register_call("test_annotation_eight_args", (void *)test_eight_args_v1, 8);
     register_call("test_annotation_eight_args", (void *)test_eight_args_v2, 8);
     /* Test removing the last handler */
diff --git a/suite/tests/client-interface/annotation/test_mode_annotations.c b/suite/tests/client-interface/annotation/test_mode_annotations.c
index a994b8b8b8..dcd96e07b7 100644
--- a/suite/tests/client-interface/annotation/test_mode_annotations.c
+++ b/suite/tests/client-interface/annotation/test_mode_annotations.c
@@ -1,5 +1,5 @@
 /* ******************************************************
- * Copyright (c) 2014-2015 Google, Inc.  All rights reserved.
+ * Copyright (c) 2014-2020 Google, Inc.  All rights reserved.
  * ******************************************************/
 
 /*
@@ -43,6 +43,8 @@ DR_DEFINE_ANNOTATION(unsigned int, test_annotation_get_mode, (unsigned int conte
 DR_DEFINE_ANNOTATION(void, test_annotation_set_mode,
                      (unsigned int context_id, unsigned int mode), )
 
+DR_DEFINE_ANNOTATION(void, test_annotation_get_pc, (void), )
+
 DR_DEFINE_ANNOTATION(const char *, test_annotation_get_client_version, (void),
                      return NULL)
 
diff --git a/suite/tests/client-interface/annotation/test_mode_annotations.h b/suite/tests/client-interface/annotation/test_mode_annotations.h
index ec3019d53a..8aab2866cc 100644
--- a/suite/tests/client-interface/annotation/test_mode_annotations.h
+++ b/suite/tests/client-interface/annotation/test_mode_annotations.h
@@ -1,5 +1,5 @@
 /* ******************************************************
- * Copyright (c) 2014 Google, Inc.  All rights reserved.
+ * Copyright (c) 2014-2020 Google, Inc.  All rights reserved.
  * ******************************************************/
 
 /*
@@ -45,6 +45,9 @@
 #define TEST_ANNOTATION_SET_MODE(context_id, mode, native_version) \
     DR_ANNOTATION_OR_NATIVE(test_annotation_set_mode, native_version, context_id, mode)
 
+#define TEST_ANNOTATION_GET_PC(context_id, mode, native_version) \
+    DR_ANNOTATION(test_annotation_get_pc)
+
 #define TEST_ANNOTATION_GET_CLIENT_VERSION() test_annotation_get_client_version()
 
 #define TEST_ANNOTATION_ROTATE_VALGRIND_HANDLER(phase) \
@@ -64,6 +67,8 @@ DR_DECLARE_ANNOTATION(unsigned int, test_annotation_get_mode, (unsigned int cont
 DR_DECLARE_ANNOTATION(void, test_annotation_set_mode,
                       (unsigned int context_id, unsigned int mode));
 
+DR_DECLARE_ANNOTATION(void, test_annotation_get_pc, (void));
+
 DR_DECLARE_ANNOTATION(const char *, test_annotation_get_client_version, (void));
 
 DR_DECLARE_ANNOTATION(void, test_annotation_rotate_valgrind_handler, (int phase));
