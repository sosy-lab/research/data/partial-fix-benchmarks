diff --git a/include/logging/log_output.h b/include/logging/log_output.h
index b15c8c90518cd..02deb818c170c 100644
--- a/include/logging/log_output.h
+++ b/include/logging/log_output.h
@@ -45,6 +45,12 @@ extern "C" {
  */
 #define LOG_OUTPUT_FLAG_FORMAT_SYSLOG		BIT(6)
 
+#ifdef CONFIG_LOG_MIPI_SYST_ENABLE
+/** @brief Flag forcing syslog format specified in mipi sys-t
+ */
+#define LOG_OUTPUT_FLAG_FORMAT_SYST		BIT(7)
+#endif
+
 /**
  * @brief Prototype of the function processing output data.
  *
diff --git a/modules/Kconfig.syst b/modules/Kconfig.syst
new file mode 100644
index 0000000000000..afeb9d0d873ba
--- /dev/null
+++ b/modules/Kconfig.syst
@@ -0,0 +1,8 @@
+# Copyright (c) 2019 Intel Corporation
+# SPDX-License-Identifier: Apache-2.0
+
+config MIPI_SYST_LIB
+	bool "MIPI SyS-T Library Support"
+	select NEWLIB_LIBC
+	help
+	  This option enables the MIPI SyS-T Library
diff --git a/subsys/logging/CMakeLists.txt b/subsys/logging/CMakeLists.txt
index 3c8e0f289e479..d8dc7670a40d8 100644
--- a/subsys/logging/CMakeLists.txt
+++ b/subsys/logging/CMakeLists.txt
@@ -48,6 +48,11 @@ if(NOT CONFIG_LOG_MINIMAL)
     CONFIG_LOG_BACKEND_QEMU_X86_64
     log_backend_qemu_x86_64.c
   )
+
+  zephyr_sources_ifdef(
+    CONFIG_LOG_MIPI_SYST_ENABLE
+    log_output_syst.c
+  )
 else()
   zephyr_sources(log_minimal.c)
 endif()
diff --git a/subsys/logging/Kconfig b/subsys/logging/Kconfig
index ddfd5e84e5fd9..5b476dd9d717b 100644
--- a/subsys/logging/Kconfig
+++ b/subsys/logging/Kconfig
@@ -81,6 +81,12 @@ config LOG_MAX_LEVEL
 	  - 3 INFO, maximal level set to LOG_LEVEL_INFO
 	  - 4 DEBUG, maximal level set to LOG_LEVEL_DBG
 
+config LOG_MIPI_SYST_ENABLE
+	bool "Enable mipi syst format output"
+	select MIPI_SYST_LIB
+	help
+	  Enable mipi syst format output for the logger system.
+
 if !LOG_MINIMAL
 
 menu "Prepend log message with function name"
@@ -276,6 +282,13 @@ config LOG_BACKEND_UART
 	help
 	  When enabled backend is using UART to output logs.
 
+config LOG_BACKEND_UART_SYST_ENABLE
+	bool "Enable UART syst backend"
+	depends on LOG_BACKEND_UART
+	depends on LOG_MIPI_SYST_ENABLE
+	help
+	  When enabled backend is using UART to output syst format logs.
+
 config LOG_BACKEND_SWO
 	bool "Enable Serial Wire Output (SWO) backend"
 	depends on HAS_SWO
@@ -299,6 +312,12 @@ config LOG_BACKEND_SWO_FREQ_HZ
 	  reset. To ensure flawless operation the frequency configured here and
 	  by the SWO viewer program has to match.
 
+config LOG_BACKEND_SWO_SYST_ENABLE
+	bool "Enable SWO syst backend"
+	depends on LOG_MIPI_SYST_ENABLE
+	help
+	  When enabled backend is using SWO to output syst format logs.
+
 endif # LOG_BACKEND_SWO
 
 config LOG_BACKEND_RTT
@@ -331,6 +350,12 @@ config LOG_BACKEND_RTT_MODE_BLOCK
 	help
 	  Waits until there is enough space in the up-buffer for a message.
 
+config LOG_BACKEND_RTT_SYST_ENABLE
+	bool "Enable RTT syst backend"
+	depends on LOG_MIPI_SYST_ENABLE
+	help
+	  When enabled backend is using RTT to output syst format logs.
+
 endchoice
 
 if LOG_BACKEND_RTT_MODE_DROP
diff --git a/subsys/logging/log_backend_rtt.c b/subsys/logging/log_backend_rtt.c
index 9be653a200bad..eabacf81536cc 100644
--- a/subsys/logging/log_backend_rtt.c
+++ b/subsys/logging/log_backend_rtt.c
@@ -221,7 +221,12 @@ LOG_OUTPUT_DEFINE(log_output, IS_ENABLED(CONFIG_LOG_BACKEND_RTT_MODE_BLOCK) ?
 static void put(const struct log_backend *const backend,
 		struct log_msg *msg)
 {
-	log_backend_std_put(&log_output, 0, msg);
+	u32_t flag = 0;
+
+#ifdef CONFIG_LOG_BACKEND_RTT_SYST_ENABLE
+	flag |= LOG_OUTPUT_FLAG_FORMAT_SYST;
+#endif
+	log_backend_std_put(&log_output, flag, msg);
 }
 
 static void log_backend_rtt_cfg(void)
@@ -259,7 +264,12 @@ static void sync_string(const struct log_backend *const backend,
 		     struct log_msg_ids src_level, u32_t timestamp,
 		     const char *fmt, va_list ap)
 {
-	log_backend_std_sync_string(&log_output, 0, src_level,
+	u32_t flag = 0;
+
+#ifdef CONFIG_LOG_BACKEND_RTT_SYST_ENABLE
+	flag |= LOG_OUTPUT_FLAG_FORMAT_SYST;
+#endif
+	log_backend_std_sync_string(&log_output, flag, src_level,
 				    timestamp, fmt, ap);
 }
 
@@ -267,7 +277,12 @@ static void sync_hexdump(const struct log_backend *const backend,
 			 struct log_msg_ids src_level, u32_t timestamp,
 			 const char *metadata, const u8_t *data, u32_t length)
 {
-	log_backend_std_sync_hexdump(&log_output, 0, src_level,
+	u32_t flag = 0;
+
+#ifdef CONFIG_LOG_BACKEND_RTT_SYST_ENABLE
+	flag |= LOG_OUTPUT_FLAG_FORMAT_SYST;
+#endif
+	log_backend_std_sync_hexdump(&log_output, flag, src_level,
 				     timestamp, metadata, data, length);
 }
 
diff --git a/subsys/logging/log_backend_swo.c b/subsys/logging/log_backend_swo.c
index acb1af613751a..a499c132788bd 100644
--- a/subsys/logging/log_backend_swo.c
+++ b/subsys/logging/log_backend_swo.c
@@ -63,7 +63,12 @@ LOG_OUTPUT_DEFINE(log_output, char_out, buf, sizeof(buf));
 static void log_backend_swo_put(const struct log_backend *const backend,
 		struct log_msg *msg)
 {
-	log_backend_std_put(&log_output, 0, msg);
+	u32_t flag = 0;
+
+#ifdef CONFIG_LOG_BACKEND_SWO_SYST_ENABLE
+	flag |= LOG_OUTPUT_FLAG_FORMAT_SYST;
+#endif
+	log_backend_std_put(&log_output, flag, msg);
 }
 
 static void log_backend_swo_init(void)
@@ -107,7 +112,12 @@ static void log_backend_swo_sync_string(const struct log_backend *const backend,
 		struct log_msg_ids src_level, u32_t timestamp,
 		const char *fmt, va_list ap)
 {
-	log_backend_std_sync_string(&log_output, 0, src_level,
+	u32_t flag = 0;
+
+#ifdef CONFIG_LOG_BACKEND_SWO_SYST_ENABLE
+	flag |= LOG_OUTPUT_FLAG_FORMAT_SYST;
+#endif
+	log_backend_std_sync_string(&log_output, flag, src_level,
 				    timestamp, fmt, ap);
 }
 
@@ -116,7 +126,12 @@ static void log_backend_swo_sync_hexdump(
 		struct log_msg_ids src_level, u32_t timestamp,
 		const char *metadata, const u8_t *data, u32_t length)
 {
-	log_backend_std_sync_hexdump(&log_output, 0, src_level,
+	u32_t flag = 0;
+
+#ifdef CONFIG_LOG_BACKEND_SWO_SYST_ENABLE
+	flag |= LOG_OUTPUT_FLAG_FORMAT_SYST;
+#endif
+	log_backend_std_sync_hexdump(&log_output, flag, src_level,
 				     timestamp, metadata, data, length);
 }
 
diff --git a/subsys/logging/log_backend_uart.c b/subsys/logging/log_backend_uart.c
index 44e1ae4c95a52..f6c318b9498c6 100644
--- a/subsys/logging/log_backend_uart.c
+++ b/subsys/logging/log_backend_uart.c
@@ -31,7 +31,12 @@ LOG_OUTPUT_DEFINE(log_output, char_out, &buf, 1);
 static void put(const struct log_backend *const backend,
 		struct log_msg *msg)
 {
-	log_backend_std_put(&log_output, 0, msg);
+	u32_t flag = 0;
+
+#ifdef CONFIG_LOG_BACKEND_UART_SYST_ENABLE
+	flag |= LOG_OUTPUT_FLAG_FORMAT_SYST;
+#endif
+	log_backend_std_put(&log_output, flag, msg);
 }
 
 static void log_backend_uart_init(void)
@@ -60,7 +65,12 @@ static void sync_string(const struct log_backend *const backend,
 		     struct log_msg_ids src_level, u32_t timestamp,
 		     const char *fmt, va_list ap)
 {
-	log_backend_std_sync_string(&log_output, 0, src_level,
+	u32_t flag = 0;
+
+#ifdef CONFIG_LOG_BACKEND_UART_SYST_ENABLE
+	flag |= LOG_OUTPUT_FLAG_FORMAT_SYST;
+#endif
+	log_backend_std_sync_string(&log_output, flag, src_level,
 				    timestamp, fmt, ap);
 }
 
@@ -68,7 +78,12 @@ static void sync_hexdump(const struct log_backend *const backend,
 			 struct log_msg_ids src_level, u32_t timestamp,
 			 const char *metadata, const u8_t *data, u32_t length)
 {
-	log_backend_std_sync_hexdump(&log_output, 0, src_level,
+	u32_t flag = 0;
+
+#ifdef CONFIG_LOG_BACKEND_UART_SYST_ENABLE
+	flag |= LOG_OUTPUT_FLAG_FORMAT_SYST;
+#endif
+	log_backend_std_sync_hexdump(&log_output, flag, src_level,
 				     timestamp, metadata, data, length);
 }
 
diff --git a/subsys/logging/log_output.c b/subsys/logging/log_output.c
index cc32272b23dd1..8adad3057394e 100644
--- a/subsys/logging/log_output.c
+++ b/subsys/logging/log_output.c
@@ -49,6 +49,18 @@ typedef int (*out_func_t)(int c, void *ctx);
 extern int z_prf(int (*func)(), void *dest, char *format, va_list vargs);
 extern void z_vprintk(out_func_t out, void *log_output,
 		     const char *fmt, va_list ap);
+#ifdef CONFIG_LOG_MIPI_SYST_ENABLE
+extern void log_output_msg_syst_process(const struct log_output *log_output,
+				struct log_msg *msg, u32_t flag);
+
+extern void log_output_string_syst_process(const struct log_output *log_output,
+				struct log_msg_ids src_level,
+				const char *fmt, va_list ap, u32_t flag);
+
+extern void log_output_hexdump_syst_process(const struct log_output *log_output,
+				struct log_msg_ids src_level,
+				const u8_t *data, u32_t length, u32_t flag);
+#endif
 
 /* The RFC 5424 allows very flexible mapping and suggest the value 0 being the
  * highest severity and 7 to be the lowest (debugging level) severity.
@@ -506,6 +518,13 @@ void log_output_msg_process(const struct log_output *log_output,
 	bool raw_string = (level == LOG_LEVEL_INTERNAL_RAW_STRING);
 	int prefix_offset;
 
+#ifdef CONFIG_LOG_MIPI_SYST_ENABLE
+	if (flags & LOG_OUTPUT_FLAG_FORMAT_SYST) {
+		log_output_msg_syst_process(log_output, msg, flags);
+		return;
+	}
+#endif
+
 	prefix_offset = raw_string ?
 			0 : prefix_print(log_output, flags, std_msg, timestamp,
 					 level, domain_id, source_id);
@@ -547,6 +566,14 @@ void log_output_string(const struct log_output *log_output,
 	u16_t source_id = (u16_t)src_level.source_id;
 	bool raw_string = (level == LOG_LEVEL_INTERNAL_RAW_STRING);
 
+#ifdef CONFIG_LOG_MIPI_SYST_ENABLE
+	if (flags & LOG_OUTPUT_FLAG_FORMAT_SYST) {
+		log_output_string_syst_process(log_output,
+				src_level, fmt, ap, flags);
+		return;
+	}
+#endif
+
 	if (!raw_string) {
 		prefix_print(log_output, flags, true, timestamp,
 				level, domain_id, source_id);
@@ -583,6 +610,14 @@ void log_output_hexdump(const struct log_output *log_output,
 	u8_t domain_id = (u8_t)src_level.domain_id;
 	u16_t source_id = (u16_t)src_level.source_id;
 
+#ifdef CONFIG_LOG_MIPI_SYST_ENABLE
+	if (flags & LOG_OUTPUT_FLAG_FORMAT_SYST) {
+		log_output_hexdump_syst_process(log_output,
+				src_level, data, length, flags);
+		return;
+	}
+#endif
+
 	prefix_offset = prefix_print(log_output, flags, true, timestamp,
 				     level, domain_id, source_id);
 
diff --git a/subsys/logging/log_output_syst.c b/subsys/logging/log_output_syst.c
new file mode 100644
index 0000000000000..6364ae4a855c3
--- /dev/null
+++ b/subsys/logging/log_output_syst.c
@@ -0,0 +1,221 @@
+/*
+ * Copyright (c) 2019 Intel corporation
+ *
+ * SPDX-License-Identifier: Apache-2.0
+ */
+
+#include <stdio.h>
+#include <ctype.h>
+#include <assert.h>
+#include <mipi_syst.h>
+#include <logging/log.h>
+#include <logging/log_ctrl.h>
+#include <logging/log_output.h>
+
+extern struct mipi_syst_handle log_syst_handle;
+extern void update_systh_platform_data(
+		struct mipi_syst_handle *handle,
+		const struct log_output *log_output, u32_t flag);
+
+/*
+ *    0   MIPI_SYST_SEVERITY_MAX      no assigned severity
+ *    1   MIPI_SYST_SEVERITY_FATAL    critical error level
+ *    2   MIPI_SYST_SEVERITY_ERROR    error message level
+ *    3   MIPI_SYST_SEVERITY_WARNING  warning message level
+ *    4   MIPI_SYST_SEVERITY_INFO     information message level
+ *    5   MIPI_SYST_SEVERITY_USER1    user defined level 5
+ *    6   MIPI_SYST_SEVERITY_USER2    user defined level 6
+ *    7   MIPI_SYST_SEVERITY_DEBUG    debug information level
+ */
+static u32_t level_to_syst_severity(u32_t level)
+{
+	u32_t ret;
+
+	switch (level) {
+	case LOG_LEVEL_NONE:
+		ret = 0U;
+		break;
+	case LOG_LEVEL_ERR:
+		ret = 2U;
+		break;
+	case LOG_LEVEL_WRN:
+		ret = 3U;
+		break;
+	case LOG_LEVEL_INF:
+		ret = 4U;
+		break;
+	case LOG_LEVEL_DBG:
+		ret = 7U;
+		break;
+	default:
+		ret = 7U;
+		break;
+	}
+
+	return ret;
+}
+
+static void std_print(struct log_msg *msg,
+		const struct log_output *log_output)
+{
+	const char *str = log_msg_str_get(msg);
+	u32_t nargs = log_msg_nargs_get(msg);
+	u32_t *args = alloca(sizeof(u32_t)*nargs);
+	u32_t severity = level_to_syst_severity(log_msg_level_get(msg));
+
+	for (int i = 0; i < nargs; i++) {
+		args[i] = log_msg_arg_get(msg, i);
+	}
+
+	switch (log_msg_nargs_get(msg)) {
+	case 0:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str);
+		break;
+	case 1:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0]);
+		break;
+	case 2:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1]);
+		break;
+	case 3:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2]);
+		break;
+	case 4:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3]);
+		break;
+	case 5:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4]);
+		break;
+	case 6:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4], args[5]);
+		break;
+	case 7:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4], args[5],
+				args[6]);
+		break;
+	case 8:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4], args[5],
+				args[6], args[7]);
+		break;
+	case 9:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4], args[5],
+				args[6], args[7], args[8]);
+		break;
+	case 10:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4], args[5],
+				args[6], args[7], args[8], args[9]);
+		break;
+	case 11:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4], args[5],
+				args[6], args[7], args[8], args[9], args[10]);
+		break;
+	case 12:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4], args[5],
+				args[6], args[7], args[8], args[9], args[10],
+				args[11]);
+		break;
+	case 13:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4], args[5],
+				args[6], args[7], args[8], args[9], args[10],
+				args[11], args[12]);
+		break;
+	case 14:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4], args[5],
+				args[6], args[7], args[8], args[9], args[10],
+				args[11], args[12], args[13]);
+		break;
+	case 15:
+		MIPI_SYST_PRINTF(&log_syst_handle, severity, str, args[0],
+				args[1], args[2], args[3], args[4], args[5],
+				args[6], args[7], args[8], args[9], args[10],
+				args[11], args[12], args[13], args[14]);
+		break;
+	default:
+		/* Unsupported number of arguments. */
+		__ASSERT_NO_MSG(true);
+		break;
+	}
+}
+
+static void raw_string_print(struct log_msg *msg,
+			const struct log_output *log_output)
+{
+	char buf[CONFIG_LOG_STRDUP_MAX_STRING + 1];
+	size_t length = CONFIG_LOG_STRDUP_MAX_STRING;
+	u32_t severity = level_to_syst_severity(log_msg_level_get(msg));
+
+	log_msg_hexdump_data_get(msg, buf, &length, 0);
+
+	buf[length] = '\0';
+
+	MIPI_SYST_PRINTF(&log_syst_handle, severity, buf);
+}
+
+static void hexdump_print(struct log_msg *msg,
+			  const struct log_output *log_output)
+{
+	char buf[CONFIG_LOG_STRDUP_MAX_STRING + 1];
+	size_t length = CONFIG_LOG_STRDUP_MAX_STRING;
+	u32_t severity = level_to_syst_severity(log_msg_level_get(msg));
+
+	log_msg_hexdump_data_get(msg, buf, &length, 0);
+
+	MIPI_SYST_WRITE(&log_syst_handle, severity, 0x1A, buf, length);
+}
+
+void log_output_msg_syst_process(const struct log_output *log_output,
+				struct log_msg *msg, u32_t flag)
+{
+	u8_t level = (u8_t)log_msg_level_get(msg);
+	bool raw_string = (level == LOG_LEVEL_INTERNAL_RAW_STRING);
+
+	update_systh_platform_data(&log_syst_handle, log_output, flag);
+
+	if (log_msg_is_std(msg)) {
+		std_print(msg, log_output);
+	} else if (raw_string) {
+		raw_string_print(msg, log_output);
+	} else {
+		hexdump_print(msg, log_output);
+	}
+}
+
+void log_output_string_syst_process(const struct log_output *log_output,
+				struct log_msg_ids src_level,
+				const char *fmt, va_list ap, u32_t flag)
+{
+	u8_t str[CONFIG_LOG_STRDUP_MAX_STRING];
+	size_t length = CONFIG_LOG_STRDUP_MAX_STRING;
+	u32_t severity = level_to_syst_severity((u32_t)src_level.level);
+
+	length = vsnprintk(str, length, fmt, ap);
+	str[length] = '\0';
+
+	update_systh_platform_data(&log_syst_handle, log_output, flag);
+
+	MIPI_SYST_PRINTF(&log_syst_handle, severity, str);
+}
+
+void log_output_hexdump_syst_process(const struct log_output *log_output,
+				struct log_msg_ids src_level,
+				const u8_t *data, u32_t length, u32_t flag)
+{
+	u32_t severity = level_to_syst_severity((u32_t)src_level.level);
+
+	update_systh_platform_data(&log_syst_handle, log_output, flag);
+
+	MIPI_SYST_WRITE(&log_syst_handle, severity, 0x1A, data, length);
+}
diff --git a/west.yml b/west.yml
index 25d8890834d35..8fa93f5812da4 100644
--- a/west.yml
+++ b/west.yml
@@ -21,6 +21,8 @@ manifest:
   remotes:
     - name: upstream
       url-base: https://github.com/zephyrproject-rtos
+    - name: syst_upstream
+      url-base: https://github.com/wentongwu
 
   #
   # Please add items below based on alphabetical order
@@ -100,6 +102,10 @@ manifest:
     - name: littlefs
       path: modules/fs/littlefs
       revision: fe9572dd5a9fcf93a249daa4233012692bd2881d
+    - name: public-mipi-sys-t
+      remote: syst_upstream
+      path: modules/debug/public-mipi-sys-t
+      revision: a8b02f14c5e2ae2eaeb980a283f85a166e619456
 
   self:
     path: zephyr
