diff --git a/modules/shell/spawn.js b/modules/shell/spawn.js
index 229148c76b11..0940d504bc63 100644
--- a/modules/shell/spawn.js
+++ b/modules/shell/spawn.js
@@ -67,14 +67,13 @@ var cockpit = cockpit || { };
     "use strict";
 
     /* Translates HTTP error codes to Cockpit codes */
-    function ProcessError(arg0, arg1) {
+    function ProcessError(arg0, signal) {
         var status = parseInt(arg0, 10);
-        var signal = parseInt(arg1, 10);
-        if (isNaN(status) && isNaN(signal)) {
+        if (arg0 !== undefined && isNaN(status)) {
             this.problem = arg0;
             this.exit_status = NaN;
-            this.exit_signal = NaN;
-            this.message = arg1 || arg0;
+            this.exit_signal = null;
+            this.message = arg0;
         } else {
             this.exit_status = status;
             this.exit_signal = signal;
diff --git a/modules/shell/test-spawn.html b/modules/shell/test-spawn.html
index b180bd80bbb0..063ab866f08b 100644
--- a/modules/shell/test-spawn.html
+++ b/modules/shell/test-spawn.html
@@ -214,7 +214,7 @@ <h2 id="qunit-userAgent"></h2><ol id="qunit-tests"></ol>
     cockpit.spawn("/unused").
         fail(function(ex) {
             equal(ex.problem, "not-found", "with problem got problem");
-            ok(isNaN(ex.exit_signal), "with problem got no signal");
+            strictEqual(ex.exit_signal, null, "with problem got no signal");
             ok(isNaN(ex.exit_status), "with problem got no status");
         }).
         always(function() {
@@ -248,13 +248,13 @@ <h2 id="qunit-userAgent"></h2><ol id="qunit-tests"></ol>
 
     var peer = new MockPeer();
     $(peer).on("opened", function(event, channel) {
-        peer.close(channel, {"exit-signal": 15});
+        peer.close(channel, {"exit-signal": "TERM"});
     });
 
     cockpit.spawn("/unused").
         fail(function(ex) {
             equal(ex.problem, "internal-error", "with signal got problem");
-            strictEqual(ex.exit_signal, 15, "with signal got signal");
+            strictEqual(ex.exit_signal, "TERM", "with signal got signal");
             ok(isNaN(ex.exit_status), "with signal got no status");
         }).
         always(function() {
diff --git a/src/agent/cockpittextstream.c b/src/agent/cockpittextstream.c
index ab333228216c..4eb08d172b5b 100644
--- a/src/agent/cockpittextstream.c
+++ b/src/agent/cockpittextstream.c
@@ -23,10 +23,12 @@
 
 #include "common/cockpitpipe.h"
 
+#include "common/cockpitunixsignal.h"
+
 #include <gio/gunixsocketaddress.h>
 
 #include <sys/wait.h>
-
+#include <stdio.h>
 /**
  * CockpitTextStream:
  *
@@ -159,6 +161,7 @@ on_pipe_close (CockpitPipe *pipe,
   CockpitTextStream *self = user_data;
   CockpitChannel *channel = user_data;
   gint status;
+  gchar *signal;
 
   self->open = FALSE;
 
@@ -168,7 +171,11 @@ on_pipe_close (CockpitPipe *pipe,
       if (WIFEXITED (status))
         cockpit_channel_close_int_option (channel, "exit-status", WEXITSTATUS (status));
       else if (WIFSIGNALED (status))
-        cockpit_channel_close_int_option (channel, "exit-signal", WTERMSIG (status));
+        {
+          signal = cockpit_strsignal (WTERMSIG (status));
+          cockpit_channel_close_option (channel, "exit-signal", signal);
+          g_free (signal);
+        }
       else if (status)
         cockpit_channel_close_int_option (channel, "exit-status", -1);
     }
diff --git a/src/agent/test-textstream.c b/src/agent/test-textstream.c
index b974ec07b114..febabaddfa95 100644
--- a/src/agent/test-textstream.c
+++ b/src/agent/test-textstream.c
@@ -364,6 +364,7 @@ test_spawn_simple (void)
 
   g_object_unref (transport);
 }
+
 static void
 test_spawn_environ (void)
 {
@@ -468,6 +469,50 @@ test_spawn_status (void)
   g_object_unref (transport);
 }
 
+static void
+test_spawn_signal (void)
+{
+  MockTransport *transport;
+  CockpitChannel *channel;
+  gchar *problem = NULL;
+  JsonObject *options;
+  JsonArray *array;
+  JsonObject *control;
+
+  transport = g_object_new (mock_transport_get_type (), NULL);
+
+  options = json_object_new ();
+
+  array = json_array_new ();
+  json_array_add_string_element (array, "/bin/sh");
+  json_array_add_string_element (array, "-c");
+  json_array_add_string_element (array, "kill $$");
+  json_object_set_array_member (options, "spawn", array);
+
+  json_object_set_string_member (options, "payload", "text-stream");
+
+  channel = g_object_new (COCKPIT_TYPE_TEXT_STREAM,
+                          "options", options,
+                          "id", "548",
+                          "transport", transport,
+                          NULL);
+  g_signal_connect (channel, "closed", G_CALLBACK (on_closed_get_problem), &problem);
+  cockpit_channel_close (channel, NULL);
+  json_object_unref (options);
+
+  while (!problem)
+    g_main_context_iteration (NULL, TRUE);
+
+  control = mock_transport_pop_control (transport);
+  expect_control_message (control, "close", "548", "reason", "", NULL);
+  g_assert_cmpstr (json_object_get_string_member (control, "exit-signal"), ==, "TERM");
+
+  g_free (problem);
+
+  g_object_unref (channel);
+  g_object_unref (transport);
+}
+
 static void
 test_spawn_pty (void)
 {
@@ -655,6 +700,7 @@ main (int argc,
   g_test_add ("/text-stream/invalid-recv", TestCase, NULL,
               setup_channel, test_recv_invalid, teardown);
 
+  g_test_add_func ("/text-stream/spawn/signal", test_spawn_signal);
   g_test_add_func ("/text-stream/spawn/simple", test_spawn_simple);
   g_test_add_func ("/text-stream/spawn/status", test_spawn_status);
   g_test_add_func ("/text-stream/spawn/environ", test_spawn_environ);
diff --git a/src/common/Makefile-common.am b/src/common/Makefile-common.am
index 8940d93226c2..9ec01ea14ec8 100644
--- a/src/common/Makefile-common.am
+++ b/src/common/Makefile-common.am
@@ -47,6 +47,8 @@ libcockpit_common_a_SOURCES = \
 	src/common/cockpittransport.h \
 	src/common/cockpitunixfd.c \
 	src/common/cockpitunixfd.h \
+	src/common/cockpitunixsignal.c \
+	src/common/cockpitunixsignal.h \
 	$(NULL)
 
 libcockpit_common_a_CFLAGS = \
@@ -66,6 +68,7 @@ COCKPIT_CHECKS = \
 	test-json \
 	test-pipe \
 	test-transport \
+	test-unixsignal \
 	$(NULL)
 
 test_json_CFLAGS = $(libcockpit_common_a_CFLAGS)
@@ -80,5 +83,9 @@ test_transport_CFLAGS = $(libcockpit_common_a_CFLAGS)
 test_transport_SOURCES = src/common/test-transport.c
 test_transport_LDADD = $(libcockpit_common_a_LIBS)
 
+test_unixsignal_CFLAGS = $(libcockpit_common_a_CFLAGS)
+test_unixsignal_SOURCES = src/common/test-unixsignal.c
+test_unixsignal_LDADD = $(libcockpit_common_a_LIBS)
+
 noinst_PROGRAMS += $(COCKPIT_CHECKS)
 TESTS += $(COCKPIT_CHECKS)
diff --git a/src/common/cockpitunixsignal.c b/src/common/cockpitunixsignal.c
new file mode 100644
index 000000000000..b961fc6ce325
--- /dev/null
+++ b/src/common/cockpitunixsignal.c
@@ -0,0 +1,119 @@
+/*
+ * This code was copied from util-linux.
+ *
+ * Copyright (c) 1988, 1993, 1994
+ * The Regents of the University of California. All rights reserved.
+ *
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions
+ * are met:
+ * 1. Redistributions of source code must retain the above copyright
+ * notice, this list of conditions and the following disclaimer.
+ * 2. Redistributions in binary form must reproduce the above copyright
+ * notice, this list of conditions and the following disclaimer in the
+ * documentation and/or other materials provided with the distribution.
+ * 3. All advertising materials mentioning features or use of this software
+ * must display the following acknowledgment:
+ * This product includes software developed by the University of
+ * California, Berkeley and its contributors.
+ * 4. Neither the name of the University nor the names of its contributors
+ * may be used to endorse or promote products derived from this software
+ * without specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
+ * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
+ * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
+ * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
+ * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
+ * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
+ * SUCH DAMAGE.
+ */
+
+/*
+ * Oct 5 1994 -- almost entirely re-written to allow for process names.
+ * modifications (c) salvatore valente <svalente@mit.edu>
+ * may be used / modified / distributed under the same terms as the original.
+ *
+ * 1999-02-22 Arkadiusz Miśkiewicz <misiek@pld.ORG.PL>
+ * - added Native Language Support
+ *
+ * 1999-11-13 aeb Accept signal numers 128+s.
+ *
+ * Copyright (C) 2014 Sami Kerola <kerolasa@iki.fi>
+ * Copyright (C) 2014 Karel Zak <kzak@redhat.com>
+ */
+
+#include <signal.h>
+#include "cockpitunixsignal.h"
+
+struct signv {
+  const char *name;
+  int val;
+} sys_signame[] = {
+  /* POSIX signals */
+  { "HUP",      SIGHUP },       /* 1 */
+  { "INT",      SIGINT },       /* 2 */
+  { "QUIT",     SIGQUIT },      /* 3 */
+  { "ILL",      SIGILL },       /* 4 */
+  { "TRAP",     SIGTRAP },      /* 5 */
+  { "ABRT",     SIGABRT },      /* 6 */
+  { "IOT",      SIGIOT },       /* 6, same as SIGABRT */
+#ifdef SIGEMT
+  { "EMT",      SIGEMT },       /* 7 (mips,alpha,sparc*) */
+#endif
+  { "BUS",      SIGBUS },       /* 7 (arm,i386,m68k,ppc), 10 (mips,alpha,sparc*) */
+  { "FPE",      SIGFPE },       /* 8 */
+  { "KILL",     SIGKILL },      /* 9 */
+  { "USR1",     SIGUSR1 },      /* 10 (arm,i386,m68k,ppc), 30 (alpha,sparc*), 16 (mips) */
+  { "SEGV",     SIGSEGV },      /* 11 */
+  { "USR2",     SIGUSR2 },      /* 12 (arm,i386,m68k,ppc), 31 (alpha,sparc*), 17 (mips) */
+  { "PIPE",     SIGPIPE },      /* 13 */
+  { "ALRM",     SIGALRM },      /* 14 */
+  { "TERM",     SIGTERM },      /* 15 */
+  { "STKFLT",   SIGSTKFLT },    /* 16 (arm,i386,m68k,ppc) */
+  { "CHLD",     SIGCHLD },      /* 17 (arm,i386,m68k,ppc), 20 (alpha,sparc*), 18 (mips) */
+  { "CLD",      SIGCLD },       /* same as SIGCHLD (mips) */
+  { "CONT",     SIGCONT },      /* 18 (arm,i386,m68k,ppc), 19 (alpha,sparc*), 25 (mips) */
+  { "STOP",     SIGSTOP },      /* 19 (arm,i386,m68k,ppc), 17 (alpha,sparc*), 23 (mips) */
+  { "TSTP",     SIGTSTP },      /* 20 (arm,i386,m68k,ppc), 18 (alpha,sparc*), 24 (mips) */
+  { "TTIN",     SIGTTIN },      /* 21 (arm,i386,m68k,ppc,alpha,sparc*), 26 (mips) */
+  { "TTOU",     SIGTTOU },      /* 22 (arm,i386,m68k,ppc,alpha,sparc*), 27 (mips) */
+  { "URG",      SIGURG },       /* 23 (arm,i386,m68k,ppc), 16 (alpha,sparc*), 21 (mips) */
+  { "XCPU",     SIGXCPU },      /* 24 (arm,i386,m68k,ppc,alpha,sparc*), 30 (mips) */
+  { "XFSZ",     SIGXFSZ },      /* 25 (arm,i386,m68k,ppc,alpha,sparc*), 31 (mips) */
+  { "VTALRM",   SIGVTALRM },    /* 26 (arm,i386,m68k,ppc,alpha,sparc*), 28 (mips) */
+  { "PROF",     SIGPROF },      /* 27 (arm,i386,m68k,ppc,alpha,sparc*), 29 (mips) */
+  { "WINCH",    SIGWINCH },     /* 28 (arm,i386,m68k,ppc,alpha,sparc*), 20 (mips) */
+  { "IO",       SIGIO },        /* 29 (arm,i386,m68k,ppc), 23 (alpha,sparc*), 22 (mips) */
+  { "POLL",     SIGPOLL },      /* same as SIGIO */
+#ifdef SIGINFO
+  { "INFO",     SIGINFO },      /* 29 (alpha) */
+#endif
+#ifdef SIGLOST
+  { "LOST",     SIGLOST },      /* 29 (arm,i386,m68k,ppc,sparc*) */
+#endif
+  { "PWR",      SIGPWR },       /* 30 (arm,i386,m68k,ppc), 29 (alpha,sparc*), 19 (mips) */
+  { "UNUSED",   SIGUNUSED },    /* 31 (arm,i386,m68k,ppc) */
+  { "SYS",      SIGSYS },       /* 31 (mips,alpha,sparc*) */
+};
+
+gchar *
+cockpit_strsignal (int signum)
+{
+  size_t n;
+
+  for (n = 0; n < G_N_ELEMENTS (sys_signame); n++)
+    if (sys_signame[n].val == signum)
+      return g_strdup (sys_signame[n].name);
+
+#ifdef SIGRTMIN
+  if (SIGRTMIN <= signum && signum <= SIGRTMAX)
+    return g_strdup_printf ("RT%d", signum - SIGRTMIN);
+#endif
+
+  return g_strdup ("UNKNOWN");
+}
diff --git a/src/common/cockpitunixsignal.h b/src/common/cockpitunixsignal.h
new file mode 100644
index 000000000000..af2c982bed97
--- /dev/null
+++ b/src/common/cockpitunixsignal.h
@@ -0,0 +1,31 @@
+/*
+ * This file is part of Cockpit.
+ *
+ * Copyright (C) 2014 Red Hat, Inc.
+ *
+ * Cockpit is free software; you can redistribute it and/or modify it
+ * under the terms of the GNU Lesser General Public License as published by
+ * the Free Software Foundation; either version 2.1 of the License, or
+ * (at your option) any later version.
+ *
+ * Cockpit is distributed in the hope that it will be useful, but
+ * WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
+ * Lesser General Public License for more details.
+ *
+ * You should have received a copy of the GNU Lesser General Public License
+ * along with Cockpit; If not, see <http://www.gnu.org/licenses/>.
+ */
+
+#ifndef __COCKPIT_UNIX_SIGNAL_H__
+#define __COCKPIT_UNIX_SIGNAL_H__
+
+#include <glib.h>
+
+G_BEGIN_DECLS
+
+gchar *         cockpit_strsignal            (int signum);
+
+G_END_DECLS
+
+#endif
diff --git a/src/common/test-unixsignal.c b/src/common/test-unixsignal.c
new file mode 100644
index 000000000000..de91fe50e699
--- /dev/null
+++ b/src/common/test-unixsignal.c
@@ -0,0 +1,69 @@
+/*
+ * This file is part of Cockpit.
+ *
+ * Copyright (C) 2014 Red Hat, Inc.
+ *
+ * Cockpit is free software; you can redistribute it and/or modify it
+ * under the terms of the GNU Lesser General Public License as published by
+ * the Free Software Foundation; either version 2.1 of the License, or
+ * (at your option) any later version.
+ *
+ * Cockpit is distributed in the hope that it will be useful, but
+ * WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
+ * Lesser General Public License for more details.
+ *
+ * You should have received a copy of the GNU Lesser General Public License
+ * along with Cockpit; If not, see <http://www.gnu.org/licenses/>.
+ */
+
+#include "config.h"
+
+#include "cockpitunixsignal.h"
+
+#include "cockpittest.h"
+
+#include <glib.h>
+
+static void
+test_posix_signal (void)
+{
+  gchar *signal;
+
+  signal = cockpit_strsignal (SIGVTALRM);
+  g_assert_cmpstr (signal, ==, "VTALRM");
+  g_free (signal);
+}
+
+static void
+test_rt_signal (void)
+{
+  gchar *signal;
+
+  signal = cockpit_strsignal (SIGRTMIN);
+  g_assert_cmpstr (signal, ==, "RT0");
+  g_free (signal);
+}
+
+static void
+test_other_signal (void)
+{
+  gchar *signal;
+
+  signal = cockpit_strsignal (0xffffffff);
+  g_assert_cmpstr (signal, ==, "UNKNOWN");
+  g_free (signal);
+}
+
+int
+main (int argc,
+      char *argv[])
+{
+  cockpit_test_init (&argc, &argv);
+
+  g_test_add_func ("/unixsignal/posix-signal", test_posix_signal);
+  g_test_add_func ("/unixsignal/realtime-signal", test_rt_signal);
+  g_test_add_func ("/unixsignal/other-signal", test_other_signal);
+
+  return g_test_run ();
+}
