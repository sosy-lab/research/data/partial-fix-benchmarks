diff --git a/include/ewmh.h b/include/ewmh.h
index 7ed9b544a..067189c1f 100644
--- a/include/ewmh.h
+++ b/include/ewmh.h
@@ -36,6 +36,12 @@ void ewmh_update_desktop_names(void);
  */
 void ewmh_update_desktop_viewport(void);
 
+/**
+ * Updates _NET_WM_DESKTOP for the window.
+ *
+ */
+void ewmh_update_wm_desktop(Con *con);
+
 /**
  * Updates _NET_ACTIVE_WINDOW with the currently focused window.
  *
diff --git a/src/ewmh.c b/src/ewmh.c
index a5c901754..2c7a16276 100644
--- a/src/ewmh.c
+++ b/src/ewmh.c
@@ -14,32 +14,42 @@
 xcb_window_t ewmh_window;
 
 /*
- * Updates _NET_CURRENT_DESKTOP with the current desktop number.
- *
- * EWMH: The index of the current desktop. This is always an integer between 0
- * and _NET_NUMBER_OF_DESKTOPS - 1.
+ * Returns the EWMH-compliant desktop index for the given con.
  *
  */
-void ewmh_update_current_desktop(void) {
-    Con *focused_ws = con_get_workspace(focused);
+static uint32_t ewmh_get_desktop_index(Con *con) {
+    uint32_t index = 0;
+
+    Con *workspace = con_get_workspace(con);
     Con *output;
-    uint32_t idx = 0;
-    /* We count to get the index of this workspace because named workspaces
-     * don’t have the ->num property */
     TAILQ_FOREACH(output, &(croot->nodes_head), nodes) {
-        Con *ws;
-        TAILQ_FOREACH(ws, &(output_get_content(output)->nodes_head), nodes) {
-            if (STARTS_WITH(ws->name, "__"))
+        Con *current;
+        TAILQ_FOREACH(current, &(output_get_content(output)->nodes_head), nodes) {
+            if (con_is_internal(current))
                 continue;
 
-            if (ws == focused_ws) {
-                xcb_change_property(conn, XCB_PROP_MODE_REPLACE, root,
-                                    A__NET_CURRENT_DESKTOP, XCB_ATOM_CARDINAL, 32, 1, &idx);
-                return;
-            }
-            ++idx;
+            if (current == workspace)
+                return index;
+
+            ++index;
         }
     }
+
+    return -1;
+}
+
+/*
+ * Updates _NET_CURRENT_DESKTOP with the current desktop number.
+ *
+ * EWMH: The index of the current desktop. This is always an integer between 0
+ * and _NET_NUMBER_OF_DESKTOPS - 1.
+ *
+ */
+void ewmh_update_current_desktop(void) {
+    uint32_t idx = ewmh_get_desktop_index(focused);
+    if (idx >= 0) {
+        xcb_change_property(conn, XCB_PROP_MODE_REPLACE, root, A__NET_CURRENT_DESKTOP, XCB_ATOM_CARDINAL, 32, 1, &idx);
+    }
 }
 
 /*
@@ -138,6 +148,37 @@ void ewmh_update_desktop_viewport(void) {
                         A__NET_DESKTOP_VIEWPORT, XCB_ATOM_CARDINAL, 32, current_position, &viewports);
 }
 
+/*
+ * Updates _NET_WM_DESKTOP for the window.
+ *
+ */
+void ewmh_update_wm_desktop(Con *con) {
+    if (!con_has_managed_window(con))
+        return;
+
+    xcb_window_t window = con->window->id;
+    DLOG("Updating _NET_WM_DESKTOP for window = %d.\n", window);
+
+    uint32_t desktop;
+    /* Sticky windows are only actually sticky when they are floating or inside a floating container.
+     * This is technically still slightly wrong, since sticky windows will only be on all workspaces on this output, but
+     * we ignore multi-monitor situations for this since the spec isn't too precise on this anyway. */
+    if (con_is_sticky(con) && con_is_floating(con)) {
+        desktop = 0xFFFFFFFF;
+    } else {
+        desktop = ewmh_get_desktop_index(con);
+    }
+
+    if (desktop >= 0) {
+        DLOG("Setting _NET_WM_DESKTOP = %d for window %d.\n", desktop, window);
+        xcb_change_property(conn, XCB_PROP_MODE_REPLACE, window, A__NET_WM_DESKTOP, XCB_ATOM_CARDINAL, 32, 1, &desktop);
+    } else {
+        /* If we can't determine the workspace index, delete the property. We'd rather not set it than lie. */
+        ELOG("Failed to determine the proper EWMH desktop index for window %d, deleting _NET_WM_DESKTOP.\n", window);
+        xcb_delete_property(conn, window, A__NET_WM_DESKTOP);
+    }
+}
+
 /*
  * Updates _NET_ACTIVE_WINDOW with the currently focused window.
  *
diff --git a/src/manage.c b/src/manage.c
index 2bcb47f36..5ad226f4d 100644
--- a/src/manage.c
+++ b/src/manage.c
@@ -574,6 +574,9 @@ void manage_window(xcb_window_t window, xcb_get_window_attributes_cookie_t cooki
      * needs to be on the final workspace first. */
     con_set_urgency(nc, urgency_hint);
 
+    /* Update _NET_WM_DESKTOP. */
+    ewmh_update_wm_desktop(nc);
+
 geom_out:
     free(geom);
 out:
