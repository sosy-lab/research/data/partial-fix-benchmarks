diff --git a/lib/bakeware/assembler.ex b/lib/bakeware/assembler.ex
index 3f828b6..e6065b2 100644
--- a/lib/bakeware/assembler.ex
+++ b/lib/bakeware/assembler.ex
@@ -1,6 +1,7 @@
 defmodule Bakeware.Assembler do
   @moduledoc false
   defstruct [
+    :command,
     :compress?,
     :compression_level,
     :cpio,
@@ -14,6 +15,7 @@ defmodule Bakeware.Assembler do
   ]
 
   @type t() :: %__MODULE__{
+          command: binary(),
           compress?: boolean(),
           compression_level: 1..19,
           cpio: Path.t(),
@@ -69,6 +71,7 @@ defmodule Bakeware.Assembler do
     assembler
     |> create_paths()
     |> set_compression()
+    |> set_command()
     |> add_start_script()
     |> CPIO.build()
     |> build_trailer()
@@ -86,7 +89,7 @@ defmodule Bakeware.Assembler do
     if [ -z "$SELF" ]; then SELF="$0"; fi
     ROOT="$(cd "$(dirname "$SELF")" && pwd -P)"
 
-    $ROOT/#{start_script_path} start
+    $ROOT/#{start_script_path} $1
     """
 
     File.write!(start_path, script)
@@ -98,7 +101,8 @@ defmodule Bakeware.Assembler do
   defp build_trailer(assembler) do
     # maybe stream here to be more efficient
     hash = :crypto.hash(:sha, File.read!(assembler.cpio))
-    hash_padding = :binary.copy(<<0>>, 12)
+    cmd_len = byte_size(assembler.command)
+    cmd_padding = :binary.copy(<<0>>, 12 - cmd_len)
     offset = File.stat!(assembler.launcher).size
     cpio_size = File.stat!(assembler.cpio).size
 
@@ -107,8 +111,8 @@ defmodule Bakeware.Assembler do
     flags = 0
 
     trailer_bin =
-      <<hash::20-bytes, hash_padding::12-bytes, cpio_size::32, offset::32, flags::16,
-        compression::8, trailer_version::8, "BAKE">>
+      <<hash::20-bytes, assembler.command::binary, cmd_padding::binary, cpio_size::32, offset::32,
+        flags::16, compression::8, trailer_version::8, "BAKE">>
 
     File.write!(assembler.trailer, trailer_bin)
     assembler
@@ -163,4 +167,21 @@ defmodule Bakeware.Assembler do
 
     %{assembler | compression_level: compression_level, compress?: compress?}
   end
+
+  defp set_command(assembler) do
+    command = assembler.release.options[:command] || "start"
+    cmd_len = byte_size(command)
+
+    if cmd_len > 12 do
+      err = """
+      [Bakeware] invalid command. See mix release documentation for available options ¬
+
+        https://hexdocs.pm/mix/Mix.Tasks.Release.html#module-bin-release_name-commands
+      """
+
+      Mix.raise(err)
+    end
+
+    %{assembler | command: command}
+  end
 end
diff --git a/src/bakeware.h b/src/bakeware.h
index b3bb2e0..89fc5f6 100644
--- a/src/bakeware.h
+++ b/src/bakeware.h
@@ -46,6 +46,8 @@ struct bakeware_trailer
 
     uint8_t sha1[20];
     char sha1_ascii[41];
+
+    char default_command[12];
 };
 
 #define BAKEWARE_COMPRESSION_NONE 0
@@ -105,6 +107,9 @@ struct bakeware
 
     // Application invocation
     char app_path[256 + 128];
+
+    // Mix Release command
+    char *command;
 };
 
 #endif
diff --git a/src/main.c b/src/main.c
index c1cc6b9..da0325d 100644
--- a/src/main.c
+++ b/src/main.c
@@ -33,6 +33,11 @@ static void process_arguments(int argc, char *argv[])
             bw.print_info = true;
             argv[i] = "";
             break;
+        } else if (strcmp(argv[i], "--bw-command") == 0) {
+            bw.command = argv[i + 1];
+            argv[i] = "";
+            argv[i + 1] = "";
+            break;
         }
     }
 }
@@ -81,9 +86,13 @@ static void init_bk(int argc, char *argv[])
 
 static void run_application()
 {
+    if (bw.command == NULL) {
+        bw.command = bw.trailer.default_command;
+    }
+
     bw_debug("Running %s...", bw.app_path);
     update_environment(bw.argc, bw.argv);
-    execl(bw.app_path, bw.app_path, NULL);
+    execl(bw.app_path, bw.app_path, bw.command, NULL);
     bw_fatal("Failed to start application '%s'", bw.app_path);
 }
 
diff --git a/src/trailer.c b/src/trailer.c
index 238f919..44eecfd 100644
--- a/src/trailer.c
+++ b/src/trailer.c
@@ -12,6 +12,7 @@
 #define BW_TRAILER_V1_FLAGS           (BW_TRAILER_V1_LENGTH - 8)
 #define BW_TRAILER_V1_CONTENTS_OFFSET (BW_TRAILER_V1_LENGTH - 12)
 #define BW_TRAILER_V1_CONTENTS_LENGTH (BW_TRAILER_V1_LENGTH - 16)
+#define BW_TRAILER_V1_DEFAULT_COMMAND (BW_TRAILER_V1_LENGTH - 28)
 #define BW_TRAILER_V1_SHA1            (BW_TRAILER_V1_LENGTH - 48)
 
 static uint32_t read_be32(const uint8_t *buffer)
@@ -56,7 +57,7 @@ int bw_read_trailer(int fd, struct bakeware_trailer *trailer)
     trailer->contents_length = read_be32(&buffer[BW_TRAILER_V1_CONTENTS_LENGTH]);
     memcpy(trailer->sha1, &buffer[BW_TRAILER_V1_SHA1], sizeof(trailer->sha1));
     sha1_to_ascii(trailer->sha1, trailer->sha1_ascii);
+    memcpy(trailer->default_command, &buffer[BW_TRAILER_V1_DEFAULT_COMMAND], sizeof(trailer->default_command));
 
     return 0;
 }
-
diff --git a/test/bakeware_test.exs b/test/bakeware_test.exs
index 7d5ae75..aa846e0 100644
--- a/test/bakeware_test.exs
+++ b/test/bakeware_test.exs
@@ -138,4 +138,16 @@ defmodule BakewareTest do
 
     assert result =~ ~r/Trailer version: 1/
   end
+
+  @tag :tmp_dir
+  test "runs with specified mix release command", %{tmp_dir: tmp_dir} do
+    tmp_dir = fix_tmp_dir(tmp_dir)
+
+    {result, 0} =
+      System.cmd(@rel_test_binary, ["--bw-command", "version"],
+        env: [{"BAKEWARE_CACHE", Path.absname(tmp_dir)}]
+      )
+
+    assert result == "rel_test 0.1.0\n"
+  end
 end
