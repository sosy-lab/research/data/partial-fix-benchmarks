diff --git a/jerry-ext/CMakeLists.txt b/jerry-ext/CMakeLists.txt
index af26b5e375..e83b814729 100644
--- a/jerry-ext/CMakeLists.txt
+++ b/jerry-ext/CMakeLists.txt
@@ -62,7 +62,7 @@ target_link_libraries(${JERRY_EXT_NAME} jerry-core)
 
 set(JERRY_EXT_PKGCONFIG_LIBS)
 
-if(USING_MSVC AND JERRY_DEBUGGER)
+if("${PLATFORM}" STREQUAL "WINDOWS" AND JERRY_DEBUGGER)
   target_link_libraries(${JERRY_EXT_NAME} ws2_32)
   set(JERRY_EXT_PKGCONFIG_LIBS -lws2_32)
 endif()
diff --git a/jerry-ext/debugger/debugger-tcp.c b/jerry-ext/debugger/debugger-tcp.c
index d4292df5bf..c0e4f4fc3a 100644
--- a/jerry-ext/debugger/debugger-tcp.c
+++ b/jerry-ext/debugger/debugger-tcp.c
@@ -23,7 +23,6 @@
 
 #ifdef _WIN32
 #include <BaseTsd.h>
-typedef SSIZE_T ssize_t;
 #include <WS2tcpip.h>
 #include <winsock2.h>
 
@@ -33,11 +32,17 @@ typedef SSIZE_T ssize_t;
 /* On Windows the invalid socket's value of INVALID_SOCKET */
 #define JERRYX_SOCKET_INVALID INVALID_SOCKET
 
-/* On Windows sockets have a SOCKET typedef */
-typedef SOCKET jerryx_socket;
-
+/*
+ * On Windows, socket functions have the following signatures:
+ * int send(SOCKET s, const char *buf, int len, int flags);
+ * int recv(SOCKET s, char *buf, int len, int flags);
+ * int setsockopt(SOCKET s, int level, int optname, const char *optval, int optlen);
+ */
+typedef int jerryx_socket_ssize_t;
+typedef SOCKET jerryx_socket_t;
+typedef char jerryx_socket_void_t;
+typedef int jerryx_socket_size_t;
 #else /* !_WIN32 */
-
 #include <arpa/inet.h>
 #include <fcntl.h>
 #include <sys/socket.h>
@@ -49,8 +54,16 @@ typedef SOCKET jerryx_socket;
 /* On *nix the invalid socket has a value of -1 */
 #define JERRYX_SOCKET_INVALID (-1)
 
-/* On *nix the sockets are integer identifiers */
-typedef int jerryx_socket;
+/*
+ * On *nix, socket functions have the following signatures:
+ * ssize_t send(int sockfd, const void *buf, size_t len, int flags);
+ * ssize_t recv(int sockfd, void *buf, size_t len, int flags);
+ * int setsockopt(int sockfd, int level, int optname, const void *optval, socklen_t optlen);
+ */
+typedef ssize_t jerryx_socket_ssize_t;
+typedef int jerryx_socket_t;
+typedef void jerryx_socket_void_t;
+typedef size_t jerryx_socket_size_t;
 #endif /* _WIN32 */
 
 /**
@@ -59,7 +72,7 @@ typedef int jerryx_socket;
 typedef struct
 {
   jerry_debugger_transport_header_t header; /**< transport header */
-  jerryx_socket tcp_socket; /**< tcp socket */
+  jerryx_socket_t tcp_socket; /**< tcp socket */
 } jerryx_debugger_transport_tcp_t;
 
 /**
@@ -85,7 +98,7 @@ jerryx_debugger_tcp_get_errno (void)
  * Correctly close a single socket.
  */
 static inline void
-jerryx_debugger_tcp_close_socket (jerryx_socket socket_id) /**< socket to close */
+jerryx_debugger_tcp_close_socket (jerryx_socket_t socket_id) /**< socket to close */
 {
 #ifdef _WIN32
   closesocket (socket_id);
@@ -109,7 +122,7 @@ jerryx_debugger_tcp_log_error (int errno_value) /**< error value to log */
   char *error_message = NULL;
   FormatMessage (FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                  NULL,
-                 errno_value,
+                 (DWORD) errno_value,
                  MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT),
                  (LPTSTR) &error_message,
                  0,
@@ -152,6 +165,7 @@ jerryx_debugger_tcp_send (jerry_debugger_transport_header_t *header_p, /**< tcp
   JERRYX_ASSERT (jerry_debugger_transport_is_connected ());
 
   jerryx_debugger_transport_tcp_t *tcp_p = (jerryx_debugger_transport_tcp_t *) header_p;
+  jerryx_socket_size_t message_to_send_length = (jerryx_socket_size_t) message_length;
 
   do
   {
@@ -167,7 +181,8 @@ jerryx_debugger_tcp_send (jerry_debugger_transport_header_t *header_p, /**< tcp
     }
 #endif /* __linux__ */
 
-    ssize_t sent_bytes = send (tcp_p->tcp_socket, message_p, message_length, 0);
+    const jerryx_socket_void_t *message_to_send_p = (const jerryx_socket_void_t *) message_p;
+    jerryx_socket_ssize_t sent_bytes = send (tcp_p->tcp_socket, message_to_send_p, message_to_send_length, 0);
 
     if (sent_bytes < 0)
     {
@@ -184,9 +199,9 @@ jerryx_debugger_tcp_send (jerry_debugger_transport_header_t *header_p, /**< tcp
     }
 
     message_p += sent_bytes;
-    message_length -= (size_t) sent_bytes;
+    message_to_send_length -= (jerryx_socket_size_t) sent_bytes;
   }
-  while (message_length > 0);
+  while (message_to_send_length > 0);
 
   return true;
 } /* jerryx_debugger_tcp_send */
@@ -200,8 +215,10 @@ jerryx_debugger_tcp_receive (jerry_debugger_transport_header_t *header_p, /**< t
 {
   jerryx_debugger_transport_tcp_t *tcp_p = (jerryx_debugger_transport_tcp_t *) header_p;
 
-  uint8_t *buffer_p = receive_context_p->buffer_p + receive_context_p->received_length;
-  size_t buffer_size = JERRY_DEBUGGER_TRANSPORT_MAX_BUFFER_SIZE - receive_context_p->received_length;
+  jerryx_socket_void_t *buffer_p = (jerryx_socket_void_t *) (receive_context_p->buffer_p
+                                                             + receive_context_p->received_length);
+  jerryx_socket_size_t buffer_size = (jerryx_socket_size_t) (JERRY_DEBUGGER_TRANSPORT_MAX_BUFFER_SIZE
+                                                             - receive_context_p->received_length);
 
   ssize_t length = recv (tcp_p->tcp_socket, buffer_p, buffer_size, 0);
 
@@ -241,7 +258,7 @@ jerryx_debugger_tcp_receive (jerry_debugger_transport_header_t *header_p, /**< t
  *         false if there was an error
  */
 static bool
-jerryx_debugger_tcp_configure_socket (jerryx_socket server_socket, /** < socket to configure */
+jerryx_debugger_tcp_configure_socket (jerryx_socket_t server_socket, /** < socket to configure */
                                       uint16_t port) /** < port number to be used for the socket */
 {
   struct sockaddr_in addr;
@@ -250,9 +267,9 @@ jerryx_debugger_tcp_configure_socket (jerryx_socket server_socket, /** < socket
   addr.sin_port = htons (port);
   addr.sin_addr.s_addr = INADDR_ANY;
 
-  int opt_value = 1;
+  const int opt_value = 1;
 
-  if (setsockopt (server_socket, SOL_SOCKET, SO_REUSEADDR, &opt_value, sizeof (int)) != 0)
+  if (setsockopt (server_socket, SOL_SOCKET, SO_REUSEADDR, (const jerryx_socket_void_t *) &opt_value, sizeof (int)) != 0)
   {
     return false;
   }
@@ -289,7 +306,7 @@ jerryx_debugger_tcp_create (uint16_t port) /**< listening port */
   }
 #endif /* _WIN32*/
 
-  jerryx_socket server_socket = socket (AF_INET, SOCK_STREAM, 0);
+  jerryx_socket_t server_socket = socket (AF_INET, SOCK_STREAM, 0);
   if (server_socket == JERRYX_SOCKET_INVALID)
   {
     jerryx_debugger_tcp_log_error (jerryx_debugger_tcp_get_errno ());
@@ -309,7 +326,7 @@ jerryx_debugger_tcp_create (uint16_t port) /**< listening port */
   struct sockaddr_in addr;
   socklen_t sin_size = sizeof (struct sockaddr_in);
 
-  jerryx_socket tcp_socket = accept (server_socket, (struct sockaddr *) &addr, &sin_size);
+  jerryx_socket_t tcp_socket = accept (server_socket, (struct sockaddr *) &addr, &sin_size);
 
   jerryx_debugger_tcp_close_socket (server_socket);
 
@@ -322,7 +339,7 @@ jerryx_debugger_tcp_create (uint16_t port) /**< listening port */
   /* Set non-blocking mode. */
 #ifdef _WIN32
   u_long nonblocking_enabled = 1;
-  if (ioctlsocket (tcp_socket, FIONBIO, &nonblocking_enabled) != NO_ERROR)
+  if (ioctlsocket (tcp_socket, (long) FIONBIO, &nonblocking_enabled) != NO_ERROR)
   {
     jerryx_debugger_tcp_close_socket (tcp_socket);
     return false;
diff --git a/jerry-ext/handle-scope/handle-scope-allocator.c b/jerry-ext/handle-scope/handle-scope-allocator.c
index d1f5edafca..a3a0b2b9f6 100644
--- a/jerry-ext/handle-scope/handle-scope-allocator.c
+++ b/jerry-ext/handle-scope/handle-scope-allocator.c
@@ -126,7 +126,7 @@ jerryx_handle_scope_get_child (jerryx_handle_scope_t *scope)
   {
     return (jerryx_handle_scope_t *) jerryx_handle_scope_pool.start;
   }
-  long idx = JERRYX_HANDLE_SCOPE_PRELIST_IDX (scope);
+  ptrdiff_t idx = JERRYX_HANDLE_SCOPE_PRELIST_IDX (scope);
   if (idx < 0)
   {
     return NULL;
diff --git a/jerry-port/default/default-module.c b/jerry-port/default/default-module.c
index 542d192c35..00b4ab4f28 100644
--- a/jerry-port/default/default-module.c
+++ b/jerry-port/default/default-module.c
@@ -127,7 +127,7 @@ jerry_port_normalize_path (const char *in_path_p,   /**< input file path */
     char *dir_p = (char *) malloc (_MAX_DIR);
 
     _splitpath_s (base_file_p, drive, _MAX_DRIVE, dir_p, _MAX_DIR, NULL, 0, NULL, 0);
-    const size_t drive_len = strnlen (&drive, _MAX_DRIVE);
+    const size_t drive_len = strnlen (drive, _MAX_DRIVE);
     const size_t dir_len = strnlen (dir_p, _MAX_DIR);
     base_drive_dir_len = drive_len + dir_len;
     path_p = (char *) malloc (base_drive_dir_len + in_path_len + 1);
