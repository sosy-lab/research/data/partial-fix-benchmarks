diff --git a/plugins/imfile/imfile.c b/plugins/imfile/imfile.c
index 4bc6078bda..4179bbe354 100644
--- a/plugins/imfile/imfile.c
+++ b/plugins/imfile/imfile.c
@@ -66,7 +66,10 @@
 
 #include <regex.h>
 
-MODULE_TYPE_INPUT	/* must be present for input modules, do not remove */
+#define FILE_ID_HASH_SIZE 16	/* max size of a file_id hash */
+#define FILE_ID_SIZE	512	/* how many bytes are used for file-id? */
+
+MODULE_TYPE_INPUT
 MODULE_TYPE_NOKEEP
 MODULE_CNFNAME("imfile")
 
@@ -194,7 +197,8 @@ static rsRetVal ATTR_NONNULL(1) pollFile(act_obj_t *act);
 static int ATTR_NONNULL() getBasename(uchar *const __restrict__ basen, uchar *const __restrict__ path);
 static void ATTR_NONNULL() act_obj_unlink(act_obj_t *act);
 static uchar * ATTR_NONNULL(1, 2) getStateFileName(const act_obj_t *, uchar *, const size_t);
-static int ATTR_NONNULL() getFullStateFileName(const uchar *const, uchar *const pszout, const size_t ilenout);
+static int ATTR_NONNULL() getFullStateFileName(const uchar *const, const char *const,
+	uchar *const pszout, const size_t ilenout);
 
 
 #define OPMODE_POLLING 0
@@ -349,7 +353,7 @@ OLD_openFileWithStateFile(act_obj_t *const act)
 		  act->name, statefn);
 
 	/* Get full path and file name */
-	lenSFNam = getFullStateFileName(statefn, pszSFNam, sizeof(pszSFNam));
+	lenSFNam = getFullStateFileName(statefn, "", pszSFNam, sizeof(pszSFNam));
 
 	/* check if the file exists */
 	if(stat((char*) pszSFNam, &stat_buf) == -1) {
@@ -911,7 +915,7 @@ act_obj_destroy(act_obj_t *const act, const int is_deleted)
 		pollFile(act); /* get any left-over data */
 		if(inst->bRMStateOnDel) {
 			statefn = getStateFileName(act, statefile, sizeof(statefile));
-			getFullStateFileName(statefn, toDel, sizeof(toDel));
+			getFullStateFileName(statefn, "", toDel, sizeof(toDel)); // TODO: check!
 			statefn = toDel;
 		}
 		persistStrmState(act);
@@ -1159,7 +1163,10 @@ fs_node_notify_file_del(act_obj_t *const act, const char *const name)
  * open or otherwise modify disk file state.
  */
 static int ATTR_NONNULL()
-getFullStateFileName(const uchar *const pszstatefile, uchar *const pszout, const size_t ilenout)
+getFullStateFileName(const uchar *const pszstatefile,
+	const char *const file_id,
+	uchar *const pszout,
+	const size_t ilenout)
 {
 	int lenout;
 	const uchar* pszworkdir;
@@ -1168,14 +1175,64 @@ getFullStateFileName(const uchar *const pszstatefile, uchar *const pszout, const
 	pszworkdir = glblGetWorkDirRaw();
 
 	/* Construct file name */
-	lenout = snprintf((char*)pszout, ilenout, "%s/%s",
-			     (char*) (pszworkdir == NULL ? "." : (char*) pszworkdir), (char*)pszstatefile);
+	lenout = snprintf((char*)pszout, ilenout, "%s/%s%s%s",
+		(char*) (pszworkdir == NULL ? "." : (char*) pszworkdir), (char*)pszstatefile,
+		(*file_id == '\0') ? "" : ":", file_id);
 
 	/* return out length */
 	return lenout;
 }
 
 
+/* hash function for file-id
+ * Takes a block of data and returns a string with the hash value.
+ *
+ * Currently one provided by Aaaron Wiebe based on perl's hashing algorithm
+ * (so probably pretty generic). Not for excessively large strings!
+ * TODO: re-think the hash function!
+ */
+#if defined(__clang__)
+#pragma GCC diagnostic ignored "-Wunknown-attributes"
+#endif
+static void __attribute__((nonnull(1,3)))
+#if defined(__clang__)
+__attribute__((no_sanitize("unsigned-integer-overflow")))
+#endif
+get_file_id_hash(const char *data, size_t lendata,
+	char *const hash_str, const size_t len_hash_str)
+{
+	unsigned hashval = 1;
+	assert(len_hash_str >= 9); /* we always generate 8-byte strings */
+	while(lendata--) {
+		hashval = hashval * 33 + *data++;
+	}
+
+	snprintf(hash_str, len_hash_str, "%8.8x", hashval);
+}
+
+
+/* this returns the file-id for a given file
+ */
+static void ATTR_NONNULL(1, 2)
+getFileID(const act_obj_t *const act, char *const buf, const size_t lenbuf)
+{
+	*buf = '\0'; /* default: empty hash, only set if file has sufficient data */
+	const int fd = open(act->name, O_RDONLY | O_CLOEXEC);
+	if(fd >= 0) {
+		char filedata[FILE_ID_SIZE];
+		const int r = read(fd, filedata, FILE_ID_SIZE);
+		if(r == FILE_ID_SIZE) {
+			get_file_id_hash(filedata, sizeof(filedata), buf, lenbuf);
+		} else {
+			DBGPRINTF("getFileID partial or error read, ret %d\n", r);
+		}
+		close(fd);
+	} else {
+		DBGPRINTF("getFileID open %s failed\n", act->name);
+	}
+	DBGPRINTF("getFileID for '%s', file_id_hash '%s'\n", act->name, buf);
+}
+
 /* this generates a state file name suitable for the given file. To avoid
  * malloc calls, it must be passed a buffer which should be MAXFNAME large.
  * Note: the buffer is not necessarily populated ... always ONLY use the
@@ -1251,8 +1308,6 @@ enqLine(act_obj_t *const act,
 finalize_it:
 	RETiRet;
 }
-
-
 /* try to open a file which has a state file. If the state file does not
  * exist or cannot be read, an error is returned.
  */
@@ -1262,22 +1317,48 @@ openFileWithStateFile(act_obj_t *const act)
 	DEFiRet;
 	uchar pszSFNam[MAXFNAME];
 	uchar statefile[MAXFNAME];
+	char file_id[FILE_ID_HASH_SIZE];
 	int fd = -1;
 	const instanceConf_t *const inst = act->edge->instarr[0];// TODO: same file, multiple instances?
 
 	uchar *const statefn = getStateFileName(act, statefile, sizeof(statefile));
+	getFileID(act, file_id, sizeof(file_id));
 
-	getFullStateFileName(statefn, pszSFNam, sizeof(pszSFNam));
+	getFullStateFileName(statefn, file_id, pszSFNam, sizeof(pszSFNam));
 	DBGPRINTF("trying to open state for '%s', state file '%s'\n", act->name, pszSFNam);
 
 	/* check if the file exists */
 	fd = open((char*)pszSFNam, O_CLOEXEC | O_NOCTTY | O_RDONLY, 0600);
 	if(fd < 0) {
 		if(errno == ENOENT) {
-			DBGPRINTF("NO state file (%s) exists for '%s' - trying to see if "
-				"old-style file exists\n", pszSFNam, act->name);
-			CHKiRet(OLD_openFileWithStateFile(act));
-			FINALIZE;
+			if(file_id[0] != '\0') {
+				const char *pszSFNamHash = strdup((const char*)pszSFNam);
+				CHKmalloc(pszSFNamHash);
+				DBGPRINTF("state file %s for %s does not exist - trying to see if "
+					"inode-only file exists\n", pszSFNam, act->name);
+				getFullStateFileName(statefn, "", pszSFNam, sizeof(pszSFNam));
+				fd = open((char*)pszSFNam, O_CLOEXEC | O_NOCTTY | O_RDONLY, 0600);
+				if(fd >= 0) {
+					/* we now can use identify the file, so let's rename it */
+					if(rename((const char*)pszSFNam, pszSFNamHash) != 0) {
+						LogError(errno, RS_RET_IO_ERROR,
+							"imfile error trying to rename state file for '%s' - "
+							"ignoring this error, usually this means a file no "
+							"longer file is left over, but this may also cause "
+							"some real trouble. Still the best we can do ",
+							act->name);
+						free((void*) pszSFNamHash);
+						ABORT_FINALIZE(RS_RET_IO_ERROR);
+					}
+				}
+				free((void*) pszSFNamHash);
+			}
+			if(fd < 0) {
+				DBGPRINTF("state file %s for %s does not exist - trying to see if "
+					"old-style file exists\n", pszSFNam, act->name);
+				CHKiRet(OLD_openFileWithStateFile(act));
+				FINALIZE;
+			}
 		} else {
 			LogError(errno, RS_RET_IO_ERROR,
 				"imfile error trying to access state file for '%s'",
@@ -1286,6 +1367,7 @@ openFileWithStateFile(act_obj_t *const act)
 		}
 	}
 
+	DBGPRINTF("opened state file %s for %s\n", pszSFNam, act->name);
 	CHKiRet(strm.Construct(&act->pStrm));
 
 	struct json_object *jval;
@@ -2437,11 +2519,13 @@ static rsRetVal ATTR_NONNULL()
 persistStrmState(act_obj_t *const act)
 {
 	DEFiRet;
+	char file_id[FILE_ID_HASH_SIZE];
 	uchar statefile[MAXFNAME];
 	uchar statefname[MAXFNAME];
 
 	uchar *const statefn = getStateFileName(act, statefile, sizeof(statefile));
-	getFullStateFileName(statefn, statefname, sizeof(statefname));
+	getFileID(act, file_id, sizeof(file_id));
+	getFullStateFileName(statefn, file_id, statefname, sizeof(statefname));
 	DBGPRINTF("persisting state for '%s', state file '%s'\n", act->name, statefname);
 
 	struct json_object *jval = NULL;
diff --git a/tests/Makefile.am b/tests/Makefile.am
index c17613c093..3860cbce59 100644
--- a/tests/Makefile.am
+++ b/tests/Makefile.am
@@ -925,6 +925,7 @@ TESTS += \
 	imfile-rename.sh \
 	imfile-symlink.sh \
 	imfile-symlink-multi.sh \
+	imfile-growing-file-id.sh \
 	glbl-oversizeMsg-truncate-imfile.sh
 
 if HAVE_VALGRIND
@@ -1479,6 +1480,7 @@ EXTRA_DIST= \
 	imfile-rename.sh \
 	imfile-symlink.sh \
 	imfile-symlink-multi.sh \
+	imfile-growing-file-id.sh \
 	glbl-oversizeMsg-truncate-imfile.sh \
 	dynfile_invld_async.sh \
 	dynfile_invld_sync.sh \
diff --git a/tests/diag.sh b/tests/diag.sh
index cb69a03366..6fbece7ae1 100755
--- a/tests/diag.sh
+++ b/tests/diag.sh
@@ -382,7 +382,6 @@ function error_exit() {
 # $4... are just to have the abilit to pass in more options...
 # add -v to chkseq if you need more verbose output
 function seq_check() {
-	echo RSYSLOG_OUT_LOG: $RSYSLOG_OUT_LOG
 	$RS_SORTCMD -g < ${RSYSLOG_OUT_LOG} | ./chkseq -s$1 -e$2 $3 $4 $5 $6 $7
 	if [ "$?" -ne "0" ]; then
 		echo "sequence error detected in $RSYSLOG_OUT_LOG"
@@ -460,6 +459,7 @@ function exit_test() {
 	rm -f imfile-state:.-rsyslog.input
 	rm -f $RSYSLOG_DYNNAME*  # delete all of our dynamic files
 	unset TCPFLOOD_EXTRA_OPTS
+	printf "Test SUCCESFULL\n"
 	echo  -------------------------------------------------------------------------------
 }
 
diff --git a/tests/imfile-growing-file-id.sh b/tests/imfile-growing-file-id.sh
new file mode 100755
index 0000000000..0a999f29e3
--- /dev/null
+++ b/tests/imfile-growing-file-id.sh
@@ -0,0 +1,83 @@
+#!/bin/bash
+# This is part of the rsyslog testbench, licensed under GPLv3
+. $srcdir/diag.sh init
+export TESTMESSAGES=10000
+export RETRIES=10
+export TESTMESSAGESFULL=19999
+echo [imfile-rename.sh]
+. $srcdir/diag.sh check-inotify-only
+generate_conf
+add_conf '
+global(workDirectory="test-spool")
+module(load="../plugins/imfile/.libs/imfile" mode="inotify" PollingInterval="1")
+
+/* Filter out busy debug output */
+global( debug.whitelist="off"
+	debug.files=["rainerscript.c", "ratelimit.c", "ruleset.c", "main Q", "msg.c", "../action.c"])
+
+input(type="imfile" File="./rsyslog.input"
+	Tag="file:" Severity="error" Facility="local7" addMetadata="on")
+
+template(name="outfmt" type="string" string="%msg:F,58:2%\n")
+if $msg contains "msgnum:" then
+	action(type="omfile" file=`echo $RSYSLOG_OUT_LOG` template="outfmt")
+'
+
+# generate small input file - state file must be inode only
+./inputfilegen -m 1 > rsyslog.input
+ls -li rsyslog.input
+
+echo "STEP 1 - small input"
+startup
+shutdown_when_empty # shut down rsyslogd when done processing messages
+wait_shutdown	# we need to wait until rsyslogd is finished!
+
+echo "STEP 2 - still small input"
+# add a bit to input file, but state file must still be inode only
+./inputfilegen -m 1 -i1 >> rsyslog.input
+ls -li rsyslog.input*
+if [ $(ls test-spool/* | wc -l) -ne 1 ]; then
+	echo FAIL: more than one state file in work directory:
+	ls -l test-spool
+	error_exit 1
+fi
+
+startup
+shutdown_when_empty # shut down rsyslogd when done processing messages
+wait_shutdown	# we need to wait until rsyslogd is finished!
+
+echo "STEP 3 - larger input, hash shall be used"
+./inputfilegen -m 998 TESTMESSAGES -i 2 >> rsyslog.input
+ls -li rsyslog.input*
+echo ls test-spool:
+ls -l test-spool
+
+startup
+shutdown_when_empty # shut down rsyslogd when done processing messages
+wait_shutdown	# we need to wait until rsyslogd is finished!
+
+if [ $(ls test-spool/* | wc -l) -ne 1 ]; then
+	echo FAIL: more than one state file in work directory:
+	ls -l test-spool
+	error_exit 1
+fi
+
+
+echo "STEP 4 - append to larger input, hash state file must now be found"
+./inputfilegen -m 1000 TESTMESSAGES -i 1000 >> rsyslog.input
+ls -li rsyslog.input*
+echo ls test-spool:
+ls -l test-spool
+
+startup
+shutdown_when_empty # shut down rsyslogd when done processing messages
+wait_shutdown	# we need to wait until rsyslogd is finished!
+
+if [ $(ls test-spool/* | wc -l) -ne 1 ]; then
+	echo FAIL: more than one state file in work directory:
+	ls -l test-spool
+	error_exit 1
+fi
+
+seq_check 0 1999
+exit_test
diff --git a/tests/imfile-persist-state-1.sh b/tests/imfile-persist-state-1.sh
index 6462cf195b..e6a5299542 100755
--- a/tests/imfile-persist-state-1.sh
+++ b/tests/imfile-persist-state-1.sh
@@ -1,9 +1,8 @@
 #!/bin/bash
 # added 2016-11-02 by rgerhards
 # This is part of the rsyslog testbench, licensed under ASL 2.0
-echo [imfile-persist-state-1.sh]
-. $srcdir/diag.sh check-inotify
 . $srcdir/diag.sh init
+. $srcdir/diag.sh check-inotify
 generate_conf
 add_conf '
 global(workDirectory="test-spool")
