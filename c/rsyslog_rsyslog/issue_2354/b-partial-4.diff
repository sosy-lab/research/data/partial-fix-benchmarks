diff --git a/plugins/imfile/imfile.c b/plugins/imfile/imfile.c
index 6210e6aad7..950b62918b 100644
--- a/plugins/imfile/imfile.c
+++ b/plugins/imfile/imfile.c
@@ -254,8 +254,9 @@ struct dirInfo_s {
 	sbool hasWildcard;
 /* TODO: check if following property are inotify only?*/
 	int bDirType;		/* Configured or dynamic */
-	fileTable_t active; /* associated active files */
+	fileTable_t active;	/* associated active files */
 	fileTable_t configured; /* associated configured files */
+	int rdiridx;		/* Root diridx helper property */
 #if defined(OS_SOLARIS) && defined (HAVE_PORT_SOURCE_FILE)
 	struct fileinfo *pfinf;
 	sbool bPortAssociated;
@@ -390,6 +391,26 @@ wdmapInit(void)
 	RETiRet;
 }
 
+/* looks up a wdmap entry by filename and returns it's index if found
+ * or -1 if not found.
+ */
+static int
+wdmapLookupFilename(uchar *pszFileName)
+{
+	int i = 0;
+	int wd = -1;
+	/* Loop through */
+	for(i = 0 ; i < nWdmap; ++i) {
+		if (	wdmap[i].pLstn != NULL &&
+			strcmp((const char*)wdmap[i].pLstn->pszFileName, (const char*)pszFileName) == 0){
+			/* Found matching wd */
+			wd = wdmap[i].wd;
+		}
+	}
+
+	return wd;
+}
+
 /* looks up a wdmap entry by pLstn pointer and returns it's index if found
  * or -1 if not found.
  */
@@ -1716,6 +1737,10 @@ dirsAdd(const uchar *const dirName, int *const piIndex)
 	dirs[newindex].hasWildcard = containsGlobWildcard((char*)dirName);
 	CHKmalloc(dirs[newindex].dirNameBfWildCard = ustrdup(dirName));
 
+	/* Default rootidx always -1 */
+	dirs[newindex].rdiridx = -1;
+
+	/* Extrac checking for wildcard mode */
 	if (dirs[newindex].hasWildcard) {
 		// TODO: wildcard is not necessarily in last char!!!
 		// TODO: BUG: we have many more wildcards that "*" - so this check is invalid
@@ -1966,6 +1991,7 @@ static void ATTR_NONNULL(1)
 startLstnFile(lstn_t *const __restrict__ pLstn)
 {
 	rsRetVal localRet;
+	DBGPRINTF("startLstnFile for file '%s'\n", pLstn->pszFileName);
 	const int wd = inotify_add_watch(ino_fd, (char*)pLstn->pszFileName, IN_MODIFY);
 	if(wd < 0) {
 		if(pLstn->fileNotFoundError) {
@@ -1980,7 +2006,8 @@ startLstnFile(lstn_t *const __restrict__ pLstn)
 		goto done;
 	}
 	if((localRet = wdmapAdd(wd, -1, pLstn)) != RS_RET_OK) {
-		if(pLstn->fileNotFoundError) {
+		if(	localRet != RS_RET_FILE_ALREADY_IN_TABLE &&
+			pLstn->fileNotFoundError) {
 			LogError(0, NO_ERRCODE, "imfile: internal error: error %d adding file "
 					"to wdmap, ignoring file '%s'\n", localRet, pLstn->pszFileName);
 		} else {
@@ -2046,6 +2073,43 @@ in_setupFileWatchDynamic(lstn_t *pLstn,
 done:	return;
 }
 
+/* Search for matching files using glob.
+ * Create Dynamic Watch for each found file
+ */
+static void
+in_setupFileWatchGlobSearch(lstn_t *pLstn)
+{
+	int wd;
+
+	DBGPRINTF("in_setupFileWatchGlobSearch file '%s' has wildcard, doing initial expansion\n",
+		pLstn->pszFileName);
+	glob_t files;
+	const int ret = glob((char*)pLstn->pszFileName,
+				GLOB_MARK|GLOB_NOSORT|GLOB_BRACE, NULL, &files);
+	if(ret == 0) {
+		for(unsigned i = 0 ; i < files.gl_pathc ; i++) {
+			uchar basen[MAXFNAME];
+			uchar *const file = (uchar*)files.gl_pathv[i];
+
+			if(file[strlen((char*)file)-1] == '/')
+				continue;/* we cannot process subdirs! */
+
+			/* search for existing watched files !*/
+			wd = wdmapLookupFilename(file);
+			if(wd >= 0) {
+				DBGPRINTF("in_setupFileWatchGlobSearch '%s' already watched in wd %d\n",
+					file, wd);
+			} else {
+				getBasename(basen, file);
+				DBGPRINTF("in_setupFileWatchGlobSearch setup dynamic watch "
+					"for '%s : %s' \n", basen, file);
+				in_setupFileWatchDynamic(pLstn, basen, file);
+			}
+		}
+		globfree(&files);
+	}
+}
+
 /* Setup a new file watch for static (configured) files.
  * Note: we need to try to read this file, as it may already contain data this
  * needs to be processed, and we won't get an event for that as notifications
@@ -2063,26 +2127,8 @@ in_setupFileWatchStatic(lstn_t *pLstn)
 	/* perform wildcard check on static files manually */
 	hasWildcard = containsGlobWildcard((char*)pLstn->pszFileName);
 	if(hasWildcard) {
-		DBGPRINTF("in_setupFileWatchStatic file '%s' has wildcard, doing initial "
-			  "expansion\n", pLstn->pszFileName);
-		glob_t files;
-		const int ret = glob((char*)pLstn->pszFileName,
-					GLOB_MARK|runModConf->sortFiles|GLOB_BRACE, NULL, &files);
-		if(ret == 0) {
-			for(unsigned i = 0 ; i < files.gl_pathc ; i++) {
-				uchar basen[MAXFNAME];
-				uchar *const file = (uchar*)files.gl_pathv[i];
-
-				if(file[strlen((char*)file)-1] == '/')
-					continue;/* we cannot process subdirs! */
-
-				getBasename(basen, file);
-				DBGPRINTF("in_setupFileWatchStatic setup dynamic watch "
-					"for '%s : %s' \n", basen, file);
-				in_setupFileWatchDynamic(pLstn, basen, file);
-			}
-			globfree(&files);
-		}
+		/* search for matching files */
+		in_setupFileWatchGlobSearch(pLstn);
 	} else {
 		/* Duplicate static object as well, otherwise the configobject
 		 * could be deleted later! */
@@ -2206,6 +2252,8 @@ in_handleDirEventDirCREATE(struct inotify_event *ev, const int dirIdx)
 {
 	char fulldn[MAXFNAME];
 	int newdiridx;
+	int iListIdx;
+	sbool hasWildcard;
 
 	/* Combine to Full Path first */
 	in_handleDirGetFullDir(fulldn, dirIdx, (char*)ev->name);
@@ -2214,10 +2262,31 @@ in_handleDirEventDirCREATE(struct inotify_event *ev, const int dirIdx)
 	newdiridx = dirsFindDir( (uchar*)fulldn );
 	if(newdiridx == -1) {
 		/* Add dir to table and create watch */
-		DBGPRINTF("Adding new dir '%s' to dirs table \n", fulldn);
+		DBGPRINTF("in_handleDirEventDirCREATE Adding new dir '%s' to dirs table \n", fulldn);
 		dirsAdd((uchar*)fulldn, &newdiridx);
 		dirs[newdiridx].bDirType = DIR_DYNAMIC; /* Set to DYNAMIC directory! */
 		in_setupDirWatch(newdiridx);
+		/* Set propper root index for newdiridx */
+		dirs[newdiridx].rdiridx = (dirs[dirIdx].rdiridx != -1 ? dirs[dirIdx].rdiridx : dirIdx);
+
+		DBGPRINTF("in_handleDirEventDirCREATE wdentry dirIdx=%d, rdirIdx=%d, dirIdxName=%s, dir=%s)\n",
+			dirIdx, dirs[newdiridx].rdiridx, dirs[dirIdx].dirName, fulldn);
+		if (dirs[dirs[newdiridx].rdiridx].configured.currMax > 0) {
+			DBGPRINTF("in_handleDirEventDirCREATE found configured listeners\n");
+
+			/* Loop through configured Listeners and scan for dynamic files */
+			for(iListIdx = 0; iListIdx < dirs[dirs[newdiridx].rdiridx].configured.currMax; iListIdx++) {
+				hasWildcard = (	dirs[dirs[newdiridx].rdiridx].hasWildcard ||
+					dirs[dirs[newdiridx].rdiridx].configured.listeners[iListIdx].pLstn->hasWildcard
+						? TRUE : FALSE);
+				if (hasWildcard == 1){
+					DBGPRINTF("in_handleDirEventDirCREATE configured listener has Wildcard\n");
+					/* search for matching files */
+					in_setupFileWatchGlobSearch(
+						dirs[dirs[newdiridx].rdiridx].configured.listeners[iListIdx].pLstn);
+				}
+			}
+		}
 	} else {
 		DBGPRINTF("dir '%s' already exists in dirs table (Idx %d)\n", fulldn, newdiridx);
 	}
@@ -2801,7 +2870,7 @@ fen_processEventFile(struct file_obj* fobjp, lstn_t *pLstn, int revents, int dir
 	RETiRet;
 }
 
-/* function not used yet, will be needed for wildcards later */
+/* Helper function to find matching files for listener */
 rsRetVal
 fen_DirSearchFiles(lstn_t *pLstn, int dirIdx)
 {
diff --git a/tests/Makefile.am b/tests/Makefile.am
index 9567d56b83..d04a88e828 100644
--- a/tests/Makefile.am
+++ b/tests/Makefile.am
@@ -740,17 +740,17 @@ TESTS += \
 	imfile-endregex-timeout-with-shutdown-polling.sh \
 	imfile-persist-state-1.sh \
 	imfile-wildcards.sh \
+	imfile-wildcards-dirs.sh \
 	imfile-wildcards-dirs2.sh \
 	imfile-rename.sh
 
-# we TEMPORARILY disable these tests to permit "normal" CI runs
-# because we know they sometimes fail due to a confirmed bug
-# inside imfile:
-# https://github.com/rsyslog/rsyslog/issues/2271
-# rgerhards, 2017-12-30
-#	imfile-wildcards-dirs.sh \
+# TEMPORARILY disable some imfile tests because refactoring 
+# is needed to get them to work 100% all the time.
+# alorbach, 2018-01-05
 #	imfile-wildcards-dirs-multi.sh \
 #	imfile-wildcards-dirs-multi2.sh \
+#	imfile-wildcards-dirs-multi3.sh \
+#	imfile-wildcards-dirs-multi4.sh \
 #
 
 if HAVE_VALGRIND
@@ -1419,12 +1419,16 @@ EXTRA_DIST= \
 	imfile-wildcards-dirs2.sh \
 	imfile-wildcards-dirs-multi.sh \
 	imfile-wildcards-dirs-multi2.sh \
+	imfile-wildcards-dirs-multi3.sh \
+	imfile-wildcards-dirs-multi4.sh \
 	imfile-rename.sh \
 	testsuites/imfile-wildcards.conf \
 	testsuites/imfile-wildcards-simple.conf \
 	testsuites/imfile-wildcards-dirs.conf \
 	testsuites/imfile-wildcards-dirs-multi.conf \
 	testsuites/imfile-wildcards-dirs-multi2.conf \
+	testsuites/imfile-wildcards-dirs-multi3.conf \
+	testsuites/imfile-wildcards-dirs-multi4.conf \
 	dynfile_invld_async.sh \
 	dynfile_invld_sync.sh \
 	dynfile_cachemiss.sh \
diff --git a/tests/diag.sh b/tests/diag.sh
index 5942259839..422e858ac8 100755
--- a/tests/diag.sh
+++ b/tests/diag.sh
@@ -544,15 +544,38 @@ case $1 in
 		fi
 		;;
    'content-check-with-count') 
-		count=$(cat rsyslog.out.log | grep -F "$2" | wc -l)
-		if [ "x$count" == "x$3" ]; then
-		    echo content-check-with-count success, \"$2\" occured $3 times
+		# content check variables for Timeout
+		if [ "x$4" == "x" ]; then
+			timeoutend=1
 		else
-		    echo content-check-with-count failed, expected \"$2\" to occure $3 times, but found it $count times
-		    echo file rsyslog.out.log content is:
-		    cat rsyslog.out.log
-		    . $srcdir/diag.sh error-exit 1
+			timeoutend=$4
 		fi
+		timecounter=0
+
+		while [  $timecounter -lt $timeoutend ]; do
+#			echo content-check-with-count loop $timecounter
+			let timecounter=timecounter+1
+
+			count=$(cat rsyslog.out.log | grep -F "$2" | wc -l)
+
+			if [ "x$count" == "x$3" ]; then
+				echo content-check-with-count success, \"$2\" occured $3 times
+				break
+			else
+				if [ "x$timecounter" == "x$timeoutend" ]; then
+					. $srcdir/diag.sh shutdown-when-empty # shut down rsyslogd
+					. $srcdir/diag.sh wait-shutdown	# Shutdown rsyslog instance on error 
+
+					echo content-check-with-count failed, expected \"$2\" to occure $3 times, but found it $count times
+					echo file rsyslog.out.log content is:
+					cat rsyslog.out.log
+					. $srcdir/diag.sh error-exit 1
+				else
+					echo content-check-with-count failed, trying again ...
+					./msleep 1000
+				fi
+			fi
+		done
 		;;
 	 'block-stats-flush')
 		echo blocking stats flush
diff --git a/tests/imfile-wildcards-dirs-multi.sh b/tests/imfile-wildcards-dirs-multi.sh
index d2c4f13fd3..f5295beeeb 100755
--- a/tests/imfile-wildcards-dirs-multi.sh
+++ b/tests/imfile-wildcards-dirs-multi.sh
@@ -2,17 +2,15 @@
 # This is part of the rsyslog testbench, licensed under GPLv3
 export IMFILEINPUTFILES="10"
 export IMFILEINPUTFILESSTEPS="5"
-export IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $IMFILEINPUTFILESSTEPS))
-echo [imfile-wildcards-multi.sh]
-. $srcdir/diag.sh check-inotify
+#export IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $IMFILEINPUTFILESSTEPS))
+export IMFILECHECKTIMEOUT="5"
 . $srcdir/diag.sh init
+. $srcdir/diag.sh check-inotify-only
 # generate input files first. Note that rsyslog processes it as
 # soon as it start up (so the file should exist at that point).
 
 # Start rsyslog now before adding more files
 . $srcdir/diag.sh startup imfile-wildcards-dirs-multi.conf
-# sleep a little to give rsyslog a chance to begin processing
-sleep 1
 
 for j in `seq 1 $IMFILEINPUTFILESSTEPS`;
 do
@@ -21,27 +19,25 @@ do
 	for i in `seq 1 $IMFILEINPUTFILES`;
 	do
 		mkdir rsyslog.input.dir$i
-		./msleep 50
 		mkdir rsyslog.input.dir$i/dir$i
-		./msleep 50
 		./inputfilegen -m 1 > rsyslog.input.dir$i/dir$i/file.logfile
-		./msleep 100
 	done
+
 	ls -d rsyslog.input.*
+	
+	# Check correct amount of input files each time
+	let IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $j))
+	. $srcdir/diag.sh content-check-with-count "HEADER msgnum:00000000:" $IMFILEINPUTFILESALL $IMFILECHECKTIMEOUT
 
 	# Delete all but first!
 	for i in `seq 1 $IMFILEINPUTFILES`;
 	do
 		rm -rf rsyslog.input.dir$i/dir$i/file.logfile
-		./msleep 100
 		rm -rf rsyslog.input.dir$i
 	done
-done
 
-# sleep a little to give rsyslog a chance for processing
-sleep 1
+done
 
 . $srcdir/diag.sh shutdown-when-empty # shut down rsyslogd when done processing messages
 . $srcdir/diag.sh wait-shutdown	# we need to wait until rsyslogd is finished!
-. $srcdir/diag.sh content-check-with-count "HEADER msgnum:00000000:" $IMFILEINPUTFILESALL
 . $srcdir/diag.sh exit
diff --git a/tests/imfile-wildcards-dirs-multi2.sh b/tests/imfile-wildcards-dirs-multi2.sh
index e42be1e135..59d8440971 100755
--- a/tests/imfile-wildcards-dirs-multi2.sh
+++ b/tests/imfile-wildcards-dirs-multi2.sh
@@ -2,10 +2,10 @@
 # This is part of the rsyslog testbench, licensed under GPLv3
 export IMFILEINPUTFILES="1"
 export IMFILEINPUTFILESSTEPS="5"
-export IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $IMFILEINPUTFILESSTEPS))
-echo [imfile-wildcards-multi2.sh]
-. $srcdir/diag.sh check-inotify-only
+#export IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $IMFILEINPUTFILESSTEPS))
+export IMFILECHECKTIMEOUT="5"
 . $srcdir/diag.sh init
+. $srcdir/diag.sh check-inotify-only
 # generate input files first. Note that rsyslog processes it as
 # soon as it start up (so the file should exist at that point).
 
@@ -13,13 +13,10 @@ for i in `seq 1 $IMFILEINPUTFILES`;
 do
 	echo "Make rsyslog.input.dir$i"
 	mkdir rsyslog.input.dir$i
-	./msleep 100
 done
 
 # Start rsyslog now before adding more files
 . $srcdir/diag.sh startup imfile-wildcards-dirs-multi2.conf
-# sleep a little to give rsyslog a chance to begin processing
-sleep 1
 
 for j in `seq 1 $IMFILEINPUTFILESSTEPS`;
 do
@@ -28,27 +25,23 @@ do
 	do
 		echo "Make rsyslog.input.dir$i/dir$j/testdir"
 		mkdir rsyslog.input.dir$i/dir$j
-		./msleep 25
 		mkdir rsyslog.input.dir$i/dir$j/testdir
-		./msleep 25
 		./inputfilegen -m 1 > rsyslog.input.dir$i/dir$j/testdir/file.logfile
-		./msleep 50
 	done
 	ls -d rsyslog.input.*
 
+	# Check correct amount of input files each time
+	let IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $j))
+	. $srcdir/diag.sh content-check-with-count "HEADER msgnum:00000000:" $IMFILEINPUTFILESALL $IMFILECHECKTIMEOUT
+
 	# Delete all but first!
 	for i in `seq 1 $IMFILEINPUTFILES`;
 	do
 		rm -rf rsyslog.input.dir$i/dir$j/testdir/file.logfile
-		./msleep 50
 		rm -rr rsyslog.input.dir$i/dir$j
 	done
 done
 
-# sleep a little to give rsyslog a chance for processing
-sleep 1
-
 . $srcdir/diag.sh shutdown-when-empty # shut down rsyslogd when done processing messages
 . $srcdir/diag.sh wait-shutdown	# we need to wait until rsyslogd is finished!
-. $srcdir/diag.sh content-check-with-count "HEADER msgnum:00000000:" $IMFILEINPUTFILESALL
 . $srcdir/diag.sh exit
diff --git a/tests/imfile-wildcards-dirs-multi3.sh b/tests/imfile-wildcards-dirs-multi3.sh
new file mode 100755
index 0000000000..23c2139bdd
--- /dev/null
+++ b/tests/imfile-wildcards-dirs-multi3.sh
@@ -0,0 +1,50 @@
+#!/bin/bash
+# This is part of the rsyslog testbench, licensed under GPLv3
+export IMFILEINPUTFILES="1"
+export IMFILEINPUTFILESSTEPS="5"
+#export IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $IMFILEINPUTFILESSTEPS))
+export IMFILECHECKTIMEOUT="5"
+. $srcdir/diag.sh init
+. $srcdir/diag.sh check-inotify-only
+# generate input files first. Note that rsyslog processes it as
+# soon as it start up (so the file should exist at that point).
+
+for i in `seq 1 $IMFILEINPUTFILES`;
+do
+	echo "Make rsyslog.input.dir$i"
+	mkdir rsyslog.input.dir$i
+done
+
+# Start rsyslog now before adding more files
+. $srcdir/diag.sh startup imfile-wildcards-dirs-multi3.conf
+# sleep a little to give rsyslog a chance to begin processing
+sleep 2
+
+for j in `seq 1 $IMFILEINPUTFILESSTEPS`;
+do
+	echo "Loop Num $j"
+	for i in `seq 1 $IMFILEINPUTFILES`;
+	do
+		echo "Make rsyslog.input.dir$i/dir$j/testdir"
+		mkdir rsyslog.input.dir$i/dir$j
+		mkdir rsyslog.input.dir$i/dir$j/testdir
+		mkdir rsyslog.input.dir$i/dir$j/testdir/subdir$j
+		./inputfilegen -m 1 > rsyslog.input.dir$i/dir$j/testdir/subdir$j/file.logfile
+	done
+	ls -d rsyslog.input.*
+
+	# Check correct amount of input files each time
+	let IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $j))
+	. $srcdir/diag.sh content-check-with-count "HEADER msgnum:00000000:" $IMFILEINPUTFILESALL $IMFILECHECKTIMEOUT
+
+	# Delete all but first!
+	for i in `seq 1 $IMFILEINPUTFILES`;
+	do
+		rm -rf rsyslog.input.dir$i/dir$j/testdir/subdir$j/file.logfile
+		rm -rf rsyslog.input.dir$i/dir$j
+	done
+done
+
+. $srcdir/diag.sh shutdown-when-empty # shut down rsyslogd when done processing messages
+. $srcdir/diag.sh wait-shutdown	# we need to wait until rsyslogd is finished!
+. $srcdir/diag.sh exit
diff --git a/tests/imfile-wildcards-dirs-multi4.sh b/tests/imfile-wildcards-dirs-multi4.sh
new file mode 100755
index 0000000000..a8121a71bd
--- /dev/null
+++ b/tests/imfile-wildcards-dirs-multi4.sh
@@ -0,0 +1,50 @@
+#!/bin/bash
+# This is part of the rsyslog testbench, licensed under GPLv3
+export IMFILEINPUTFILES="1"
+export IMFILEINPUTFILESSTEPS="5"
+#export IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $IMFILEINPUTFILESSTEPS))
+export IMFILECHECKTIMEOUT="5"
+. $srcdir/diag.sh init
+. $srcdir/diag.sh check-inotify-only
+# generate input files first. Note that rsyslog processes it as
+# soon as it start up (so the file should exist at that point).
+
+# Start rsyslog now before adding more files
+. $srcdir/diag.sh startup imfile-wildcards-dirs-multi4.conf
+
+for i in `seq 1 $IMFILEINPUTFILES`;
+do
+	echo "Make rsyslog.input.dir$i"
+	mkdir rsyslog.input.dir$i
+done
+
+for j in `seq 1 $IMFILEINPUTFILESSTEPS`;
+do
+	echo "Loop Num $j"
+	for i in `seq 1 $IMFILEINPUTFILES`;
+	do
+		echo "Make rsyslog.input.dir$i/dir$j/testdir"
+		mkdir rsyslog.input.dir$i/dir$j
+		mkdir rsyslog.input.dir$i/dir$j/testdir
+		mkdir rsyslog.input.dir$i/dir$j/testdir/su$j
+		mkdir rsyslog.input.dir$i/dir$j/testdir/su$j/bd$j
+		mkdir rsyslog.input.dir$i/dir$j/testdir/su$j/bd$j/ir$j
+		./inputfilegen -m 1 > rsyslog.input.dir$i/dir$j/testdir/su$j/bd$j/ir$j/file.logfile
+	done
+	ls -d rsyslog.input.*
+
+	# Check correct amount of input files each time
+	let IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $j))
+	. $srcdir/diag.sh content-check-with-count "HEADER msgnum:00000000:" $IMFILEINPUTFILESALL $IMFILECHECKTIMEOUT
+
+	# Delete all but first!
+	for i in `seq 1 $IMFILEINPUTFILES`;
+	do
+		rm -rf rsyslog.input.dir$i/dir$j/testdir/su$j/bd$j/ir$j/file.logfile
+		rm -rf rsyslog.input.dir$i/dir$j
+	done
+done
+
+. $srcdir/diag.sh shutdown-when-empty # shut down rsyslogd when done processing messages
+. $srcdir/diag.sh wait-shutdown	# we need to wait until rsyslogd is finished!
+. $srcdir/diag.sh exit
diff --git a/tests/imfile-wildcards-dirs.sh b/tests/imfile-wildcards-dirs.sh
index f6c5d1a410..e86d34c9fe 100755
--- a/tests/imfile-wildcards-dirs.sh
+++ b/tests/imfile-wildcards-dirs.sh
@@ -9,21 +9,14 @@ echo [imfile-wildcards-dirs.sh]
 
 # Start rsyslog now before adding more files
 . $srcdir/diag.sh startup imfile-wildcards-dirs.conf
-# sleep a little to give rsyslog a chance to begin processing
-sleep 1
 
 for i in `seq 1 $IMFILEINPUTFILES`;
 do
 	mkdir rsyslog.input.dir$i
 	./inputfilegen -m 1 > rsyslog.input.dir$i/file.logfile
 done
-# wait for imfile to process
-./msleep 250 
 ls -d rsyslog.input.*
 
-# sleep a little to give rsyslog a chance for processing
-sleep 1
-
 . $srcdir/diag.sh shutdown-when-empty # shut down rsyslogd when done processing messages
 . $srcdir/diag.sh wait-shutdown	# we need to wait until rsyslogd is finished!
 . $srcdir/diag.sh content-check-with-count "HEADER msgnum:00000000:" $IMFILEINPUTFILES
diff --git a/tests/imfile-wildcards-dirs2.sh b/tests/imfile-wildcards-dirs2.sh
index f0b38a6f3f..0bf2907349 100755
--- a/tests/imfile-wildcards-dirs2.sh
+++ b/tests/imfile-wildcards-dirs2.sh
@@ -2,10 +2,10 @@
 # This is part of the rsyslog testbench, licensed under GPLv3
 export IMFILEINPUTFILES="10"
 export IMFILEINPUTFILESSTEPS="5"
-export IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $IMFILEINPUTFILESSTEPS))
-echo [imfile-wildcards-dirs2.sh]
-. $srcdir/diag.sh check-inotify
+#export IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $IMFILEINPUTFILESSTEPS))
+export IMFILECHECKTIMEOUT="5"
 . $srcdir/diag.sh init
+. $srcdir/diag.sh check-inotify
 # generate input files first. Note that rsyslog processes it as
 # soon as it start up (so the file should exist at that point).
 
@@ -21,10 +21,13 @@ do
 	for i in `seq 1 $IMFILEINPUTFILES`;
 	do
 		mkdir rsyslog.input.dir$i
-		./msleep 50
 		./inputfilegen -m 1 > rsyslog.input.dir$i/file.logfile
 	done
 	ls -d rsyslog.input.*
+
+	# Check correct amount of input files each time
+	let IMFILEINPUTFILESALL=$(($IMFILEINPUTFILES * $j))
+	. $srcdir/diag.sh content-check-with-count "HEADER msgnum:00000000:" $IMFILEINPUTFILESALL $IMFILECHECKTIMEOUT
 	
 	# Delete all but first!
 	for i in `seq 1 $IMFILEINPUTFILES`;
@@ -39,5 +42,4 @@ sleep 1
 
 . $srcdir/diag.sh shutdown-when-empty # shut down rsyslogd when done processing messages
 . $srcdir/diag.sh wait-shutdown	# we need to wait until rsyslogd is finished!
-. $srcdir/diag.sh content-check-with-count "HEADER msgnum:00000000:" $IMFILEINPUTFILESALL
 . $srcdir/diag.sh exit
diff --git a/tests/imfile-wildcards.sh b/tests/imfile-wildcards.sh
index b5b6ce65e5..9eb3602f33 100755
--- a/tests/imfile-wildcards.sh
+++ b/tests/imfile-wildcards.sh
@@ -12,8 +12,6 @@ imfilebefore="rsyslog.input.1.log"
 
 # Start rsyslog now before adding more files
 . $srcdir/diag.sh startup imfile-wildcards.conf
-# sleep a little to give rsyslog a chance to begin processing
-sleep 1
 
 for i in `seq 2 $IMFILEINPUTFILES`;
 do
@@ -25,9 +23,6 @@ done
 ./inputfilegen -m 3 > rsyslog.input.$((IMFILEINPUTFILES + 1)).log
 ls -l rsyslog.input.*
 
-# sleep a little to give rsyslog a chance for processing
-sleep 1
-
 . $srcdir/diag.sh shutdown-when-empty # shut down rsyslogd when done processing messages
 . $srcdir/diag.sh wait-shutdown	# we need to wait until rsyslogd is finished!
 
diff --git a/tests/testsuites/imfile-wildcards-dirs-multi3.conf b/tests/testsuites/imfile-wildcards-dirs-multi3.conf
new file mode 100644
index 0000000000..af8bef2aa5
--- /dev/null
+++ b/tests/testsuites/imfile-wildcards-dirs-multi3.conf
@@ -0,0 +1,35 @@
+$IncludeConfig diag-common.conf
+$WorkDirectory test-spool
+
+/* Filter out busy debug output, comment out if needed */
+global(
+	debug.whitelist="off"
+	debug.files=["rainerscript.c", "ratelimit.c", "ruleset.c", "main Q", "msg.c", "../action.c"]
+)
+
+module(	load="../plugins/imfile/.libs/imfile" 
+	mode="inotify" 
+	PollingInterval="1")
+
+input(type="imfile"
+	File="./rsyslog.input.*/*/testdir/*/file.logfile"
+	Tag="file:"
+	Severity="error"
+	Facility="local7"
+	addMetadata="on"
+)
+
+template(name="outfmt" type="list") {
+  constant(value="HEADER ")
+  property(name="msg" format="json")
+  constant(value="', ")
+  property(name="$!metadata!filename")
+  constant(value="\n")
+}
+
+if $msg contains "msgnum:" then
+ action(
+   type="omfile"
+   file="rsyslog.out.log"
+   template="outfmt"
+ )
diff --git a/tests/testsuites/imfile-wildcards-dirs-multi4.conf b/tests/testsuites/imfile-wildcards-dirs-multi4.conf
new file mode 100644
index 0000000000..98ac58340c
--- /dev/null
+++ b/tests/testsuites/imfile-wildcards-dirs-multi4.conf
@@ -0,0 +1,35 @@
+$IncludeConfig diag-common.conf
+$WorkDirectory test-spool
+
+/* Filter out busy debug output, comment out if needed */
+global(
+	debug.whitelist="off"
+	debug.files=["rainerscript.c", "ratelimit.c", "ruleset.c", "main Q", "msg.c", "../action.c"]
+)
+
+module(	load="../plugins/imfile/.libs/imfile" 
+	mode="inotify" 
+	PollingInterval="1")
+
+input(type="imfile"
+	File="./rsyslog.input.dir1/*/*/*/*/*/file.logfile"
+	Tag="file:"
+	Severity="error"
+	Facility="local7"
+	addMetadata="on"
+)
+
+template(name="outfmt" type="list") {
+  constant(value="HEADER ")
+  property(name="msg" format="json")
+  constant(value="', ")
+  property(name="$!metadata!filename")
+  constant(value="\n")
+}
+
+if $msg contains "msgnum:" then
+ action(
+   type="omfile"
+   file="rsyslog.out.log"
+   template="outfmt"
+ )
