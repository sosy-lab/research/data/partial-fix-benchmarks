diff --git a/sway/desktop/xdg_shell.c b/sway/desktop/xdg_shell.c
index 5b40d903fe..82db40760d 100644
--- a/sway/desktop/xdg_shell.c
+++ b/sway/desktop/xdg_shell.c
@@ -203,6 +203,29 @@ static void handle_new_popup(struct wl_listener *listener, void *data) {
 	popup_create(wlr_popup, &xdg_shell_view->view);
 }
 
+static void handle_request_fullscreen(struct wl_listener *listener, void *data) {
+	struct sway_xdg_shell_view *xdg_shell_view =
+		wl_container_of(listener, xdg_shell_view, request_fullscreen);
+	struct wlr_xdg_toplevel_set_fullscreen_event *e = data;
+	struct wlr_xdg_surface *xdg_surface =
+		xdg_shell_view->view.wlr_xdg_surface;
+	struct sway_view *view = &xdg_shell_view->view;
+
+	if (!sway_assert(xdg_surface->role == WLR_XDG_SURFACE_ROLE_TOPLEVEL,
+				"xdg_shell requested fullscreen of surface with role %i",
+				xdg_surface->role)) {
+		return;
+	}
+	if (!xdg_surface->mapped) {
+		return;
+	}
+
+	view_set_fullscreen(view, e->fullscreen);
+
+	struct sway_container *ws = container_parent(view->swayc, C_WORKSPACE);
+	arrange_and_commit(ws);
+}
+
 static void handle_unmap(struct wl_listener *listener, void *data) {
 	struct sway_xdg_shell_view *xdg_shell_view =
 		wl_container_of(listener, xdg_shell_view, unmap);
@@ -216,6 +239,7 @@ static void handle_unmap(struct wl_listener *listener, void *data) {
 
 	wl_list_remove(&xdg_shell_view->commit.link);
 	wl_list_remove(&xdg_shell_view->new_popup.link);
+	wl_list_remove(&xdg_shell_view->request_fullscreen.link);
 }
 
 static void handle_map(struct wl_listener *listener, void *data) {
@@ -248,6 +272,10 @@ static void handle_map(struct wl_listener *listener, void *data) {
 	xdg_shell_view->new_popup.notify = handle_new_popup;
 	wl_signal_add(&xdg_surface->events.new_popup,
 		&xdg_shell_view->new_popup);
+
+	xdg_shell_view->request_fullscreen.notify = handle_request_fullscreen;
+	wl_signal_add(&xdg_surface->toplevel->events.request_fullscreen,
+			&xdg_shell_view->request_fullscreen);
 }
 
 static void handle_destroy(struct wl_listener *listener, void *data) {
@@ -261,34 +289,10 @@ static void handle_destroy(struct wl_listener *listener, void *data) {
 	wl_list_remove(&xdg_shell_view->destroy.link);
 	wl_list_remove(&xdg_shell_view->map.link);
 	wl_list_remove(&xdg_shell_view->unmap.link);
-	wl_list_remove(&xdg_shell_view->request_fullscreen.link);
 	view->wlr_xdg_surface = NULL;
 	view_destroy(view);
 }
 
-static void handle_request_fullscreen(struct wl_listener *listener, void *data) {
-	struct sway_xdg_shell_view *xdg_shell_view =
-		wl_container_of(listener, xdg_shell_view, request_fullscreen);
-	struct wlr_xdg_toplevel_set_fullscreen_event *e = data;
-	struct wlr_xdg_surface *xdg_surface =
-		xdg_shell_view->view.wlr_xdg_surface;
-	struct sway_view *view = &xdg_shell_view->view;
-
-	if (!sway_assert(xdg_surface->role == WLR_XDG_SURFACE_ROLE_TOPLEVEL,
-				"xdg_shell requested fullscreen of surface with role %i",
-				xdg_surface->role)) {
-		return;
-	}
-	if (!xdg_surface->mapped) {
-		return;
-	}
-
-	view_set_fullscreen(view, e->fullscreen);
-
-	struct sway_container *ws = container_parent(view->swayc, C_WORKSPACE);
-	arrange_and_commit(ws);
-}
-
 struct sway_view *view_from_wlr_xdg_surface(
 		struct wlr_xdg_surface *xdg_surface) {
 	return xdg_surface->data;
@@ -329,9 +333,5 @@ void handle_xdg_shell_surface(struct wl_listener *listener, void *data) {
 	xdg_shell_view->destroy.notify = handle_destroy;
 	wl_signal_add(&xdg_surface->events.destroy, &xdg_shell_view->destroy);
 
-	xdg_shell_view->request_fullscreen.notify = handle_request_fullscreen;
-	wl_signal_add(&xdg_surface->toplevel->events.request_fullscreen,
-			&xdg_shell_view->request_fullscreen);
-
 	xdg_surface->data = xdg_shell_view;
 }
diff --git a/sway/desktop/xdg_shell_v6.c b/sway/desktop/xdg_shell_v6.c
index c794e23a40..0d3c164434 100644
--- a/sway/desktop/xdg_shell_v6.c
+++ b/sway/desktop/xdg_shell_v6.c
@@ -198,6 +198,29 @@ static void handle_new_popup(struct wl_listener *listener, void *data) {
 	popup_create(wlr_popup, &xdg_shell_v6_view->view);
 }
 
+static void handle_request_fullscreen(struct wl_listener *listener, void *data) {
+	struct sway_xdg_shell_v6_view *xdg_shell_v6_view =
+		wl_container_of(listener, xdg_shell_v6_view, request_fullscreen);
+	struct wlr_xdg_toplevel_v6_set_fullscreen_event *e = data;
+	struct wlr_xdg_surface_v6 *xdg_surface =
+		xdg_shell_v6_view->view.wlr_xdg_surface_v6;
+	struct sway_view *view = &xdg_shell_v6_view->view;
+
+	if (!sway_assert(xdg_surface->role == WLR_XDG_SURFACE_V6_ROLE_TOPLEVEL,
+				"xdg_shell_v6 requested fullscreen of surface with role %i",
+				xdg_surface->role)) {
+		return;
+	}
+	if (!xdg_surface->mapped) {
+		return;
+	}
+
+	view_set_fullscreen(view, e->fullscreen);
+
+	struct sway_container *ws = container_parent(view->swayc, C_WORKSPACE);
+	arrange_and_commit(ws);
+}
+
 static void handle_unmap(struct wl_listener *listener, void *data) {
 	struct sway_xdg_shell_v6_view *xdg_shell_v6_view =
 		wl_container_of(listener, xdg_shell_v6_view, unmap);
@@ -211,6 +234,7 @@ static void handle_unmap(struct wl_listener *listener, void *data) {
 
 	wl_list_remove(&xdg_shell_v6_view->commit.link);
 	wl_list_remove(&xdg_shell_v6_view->new_popup.link);
+	wl_list_remove(&xdg_shell_v6_view->request_fullscreen.link);
 }
 
 static void handle_map(struct wl_listener *listener, void *data) {
@@ -243,6 +267,10 @@ static void handle_map(struct wl_listener *listener, void *data) {
 	xdg_shell_v6_view->new_popup.notify = handle_new_popup;
 	wl_signal_add(&xdg_surface->events.new_popup,
 		&xdg_shell_v6_view->new_popup);
+
+	xdg_shell_v6_view->request_fullscreen.notify = handle_request_fullscreen;
+	wl_signal_add(&xdg_surface->toplevel->events.request_fullscreen,
+			&xdg_shell_v6_view->request_fullscreen);
 }
 
 static void handle_destroy(struct wl_listener *listener, void *data) {
@@ -252,34 +280,10 @@ static void handle_destroy(struct wl_listener *listener, void *data) {
 	wl_list_remove(&xdg_shell_v6_view->destroy.link);
 	wl_list_remove(&xdg_shell_v6_view->map.link);
 	wl_list_remove(&xdg_shell_v6_view->unmap.link);
-	wl_list_remove(&xdg_shell_v6_view->request_fullscreen.link);
 	view->wlr_xdg_surface_v6 = NULL;
 	view_destroy(view);
 }
 
-static void handle_request_fullscreen(struct wl_listener *listener, void *data) {
-	struct sway_xdg_shell_v6_view *xdg_shell_v6_view =
-		wl_container_of(listener, xdg_shell_v6_view, request_fullscreen);
-	struct wlr_xdg_toplevel_v6_set_fullscreen_event *e = data;
-	struct wlr_xdg_surface_v6 *xdg_surface =
-		xdg_shell_v6_view->view.wlr_xdg_surface_v6;
-	struct sway_view *view = &xdg_shell_v6_view->view;
-
-	if (!sway_assert(xdg_surface->role == WLR_XDG_SURFACE_V6_ROLE_TOPLEVEL,
-				"xdg_shell_v6 requested fullscreen of surface with role %i",
-				xdg_surface->role)) {
-		return;
-	}
-	if (!xdg_surface->mapped) {
-		return;
-	}
-
-	view_set_fullscreen(view, e->fullscreen);
-
-	struct sway_container *ws = container_parent(view->swayc, C_WORKSPACE);
-	arrange_and_commit(ws);
-}
-
 struct sway_view *view_from_wlr_xdg_surface_v6(
 		struct wlr_xdg_surface_v6 *xdg_surface_v6) {
        return xdg_surface_v6->data;
@@ -320,9 +324,5 @@ void handle_xdg_shell_v6_surface(struct wl_listener *listener, void *data) {
 	xdg_shell_v6_view->destroy.notify = handle_destroy;
 	wl_signal_add(&xdg_surface->events.destroy, &xdg_shell_v6_view->destroy);
 
-	xdg_shell_v6_view->request_fullscreen.notify = handle_request_fullscreen;
-	wl_signal_add(&xdg_surface->toplevel->events.request_fullscreen,
-			&xdg_shell_v6_view->request_fullscreen);
-
 	xdg_surface->data = xdg_shell_v6_view;
 }
