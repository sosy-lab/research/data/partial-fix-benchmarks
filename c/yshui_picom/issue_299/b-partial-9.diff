diff --git a/src/event.c b/src/event.c
index c6e11795..f21e3462 100644
--- a/src/event.c
+++ b/src/event.c
@@ -336,28 +336,35 @@ static inline void ev_reparent_notify(session_t *ps, xcb_reparent_notify_event_t
 		    ps->c, ev->window, XCB_CW_EVENT_MASK,
 		    (const uint32_t[]){determine_evmask(ps, ev->window, WIN_EVMODE_UNKNOWN)});
 
-		// Check if the window is an undetected client window
-		// Firstly, check if it's a known client window
-		if (!w_top) {
-			// If not, look for its frame window
+		if (!wid_has_prop(ps, ev->window, ps->atoms->aWM_STATE)) {
+			log_debug("Window %#010x doesn't have WM_STATE property, it is "
+			          "probably not a client window. But we will listen for "
+			          "property change in case it gains one.",
+			          ev->window);
+			xcb_change_window_attributes(
+			    ps->c, ev->window, XCB_CW_EVENT_MASK,
+			    (const uint32_t[]){determine_evmask(ps, ev->window, WIN_EVMODE_UNKNOWN) |
+			                       XCB_EVENT_MASK_PROPERTY_CHANGE});
+		} else {
 			auto w_real_top = find_managed_window_or_parent(ps, ev->parent);
-			// If found, and the client window has not been determined, or its
-			// frame may not have a correct client, continue
-			if (w_real_top && (!w_real_top->client_win ||
-			                   w_real_top->client_win == w_real_top->base.id)) {
-				// If it has WM_STATE, mark it the client window
-				if (wid_has_prop(ps, ev->window, ps->atoms->aWM_STATE)) {
-					w_real_top->wmwin = false;
-					win_unmark_client(ps, w_real_top);
-					win_mark_client(ps, w_real_top, ev->window);
-				}
-				// Otherwise, watch for WM_STATE on it
+			if (w_real_top && w_real_top->state != WSTATE_UNMAPPED &&
+			    w_real_top->state != WSTATE_UNMAPPING) {
+				log_debug("Mark window %#010x (%s) as having a stale "
+				          "client",
+				          w_real_top->base.id, w_real_top->name);
+				win_set_flags(w_real_top, WIN_FLAGS_CLIENT_STALE);
+				ps->pending_updates = true;
+			} else {
+				if (!w_real_top)
+					log_debug("parent %#010x not found", ev->parent);
 				else {
-					xcb_change_window_attributes(
-					    ps->c, ev->window, XCB_CW_EVENT_MASK,
-					    (const uint32_t[]){
-					        determine_evmask(ps, ev->window, WIN_EVMODE_UNKNOWN) |
-					        XCB_EVENT_MASK_PROPERTY_CHANGE});
+					// Window is not currently mapped, unmark its
+					// client to trigger a client recheck when it is
+					// mapped later.
+					win_unmark_client(ps, w_real_top);
+					log_debug("parent %#010x (%s) is in state %d",
+					          w_real_top->base.id, w_real_top->name,
+					          w_real_top->state);
 				}
 			}
 		}
@@ -451,13 +458,11 @@ static inline void ev_property_notify(session_t *ps, xcb_property_notify_event_t
 			                                 ps, ev->window, WIN_EVMODE_UNKNOWN)});
 
 			auto w_top = find_managed_window_or_parent(ps, ev->window);
-			// Initialize client_win as early as possible
-			if (w_top &&
-			    (!w_top->client_win || w_top->client_win == w_top->base.id) &&
-			    wid_has_prop(ps, ev->window, ps->atoms->aWM_STATE)) {
-				w_top->wmwin = false;
-				win_unmark_client(ps, w_top);
-				win_mark_client(ps, w_top, ev->window);
+			// ev->window might have not been managed yet, in that case w_top
+			// would be NULL.
+			if (w_top) {
+				win_set_flags(w_top, WIN_FLAGS_CLIENT_STALE);
+				ps->pending_updates = true;
 			}
 		}
 	}
diff --git a/src/win.c b/src/win.c
index 28d84ed9..10f44242 100644
--- a/src/win.c
+++ b/src/win.c
@@ -53,6 +53,8 @@ static const int WIN_GET_LEADER_MAX_RECURSION = 20;
 static const int ROUNDED_PIXELS = 1;
 static const double ROUNDED_PERCENT = 0.05;
 
+static void win_recheck_client(session_t *ps, struct managed_win *w);
+
 /// Generate a "return by value" function, from a function that returns the
 /// region via a region_t pointer argument.
 /// Function signature has to be (win *, region_t *)
@@ -323,8 +325,8 @@ void win_release_images(struct backend_base *backend, struct managed_win *w) {
 void win_process_flags(session_t *ps, struct managed_win *w) {
 	if (win_check_flags_all(w, WIN_FLAGS_MAPPED)) {
 		map_win_start(ps, w);
+		win_clear_flags(w, WIN_FLAGS_MAPPED);
 	}
-	win_clear_flags(w, WIN_FLAGS_MAPPED);
 
 	// Not a loop
 	while (win_check_flags_any(w, WIN_FLAGS_IMAGES_STALE) &&
@@ -367,7 +369,14 @@ void win_process_flags(session_t *ps, struct managed_win *w) {
 	}
 
 	// Clear stale image flags
-	win_clear_flags(w, WIN_FLAGS_IMAGES_STALE);
+	if (win_check_flags_any(w, WIN_FLAGS_IMAGES_STALE)) {
+		win_clear_flags(w, WIN_FLAGS_IMAGES_STALE);
+	}
+
+	if (win_check_flags_all(w, WIN_FLAGS_CLIENT_STALE)) {
+		win_recheck_client(ps, w);
+		win_clear_flags(w, WIN_FLAGS_CLIENT_STALE);
+	}
 }
 
 /**
@@ -2326,6 +2335,7 @@ win_is_fullscreen_xcb(xcb_connection_t *c, const struct atom *a, const xcb_windo
 
 /// Set flags on a window. Some sanity checks are performed
 void win_set_flags(struct managed_win *w, uint64_t flags) {
+	log_debug("Set flags %lu to window %#010x (%s)", flags, w->base.id, w->name);
 	if (unlikely(w->state == WSTATE_DESTROYING)) {
 		log_error("Flags set on a destroyed window %#010x (%s)", w->base.id, w->name);
 		return;
@@ -2336,6 +2346,7 @@ void win_set_flags(struct managed_win *w, uint64_t flags) {
 
 /// Clear flags on a window. Some sanity checks are performed
 void win_clear_flags(struct managed_win *w, uint64_t flags) {
+	log_debug("Clear flags %lu from window %#010x (%s)", flags, w->base.id, w->name);
 	if (unlikely(w->state == WSTATE_DESTROYING)) {
 		log_error("Flags cleared on a destroyed window %#010x (%s)", w->base.id,
 		          w->name);
diff --git a/src/win_defs.h b/src/win_defs.h
index f8494bb5..6099ac27 100644
--- a/src/win_defs.h
+++ b/src/win_defs.h
@@ -81,6 +81,8 @@ enum win_flags {
 	WIN_FLAGS_SHADOW_STALE = 8,
 	/// shadow has not been generated
 	WIN_FLAGS_SHADOW_NONE = 16,
+	/// the client window needs to be updated
+	WIN_FLAGS_CLIENT_STALE = 32,
 	/// the window is mapped by X, we need to call map_win_start for it
 	WIN_FLAGS_MAPPED = 64,
 };
diff --git a/tests/run_one_test.sh b/tests/run_one_test.sh
index 13da3df1..46193595 100755
--- a/tests/run_one_test.sh
+++ b/tests/run_one_test.sh
@@ -7,7 +7,7 @@ fi
 echo "Running test $2"
 
 # TODO keep the log file, and parse it to see if test is successful
-($1 --experimental-backends --backend dummy --log-level=debug --log-file=$PWD/log --config=$2) &
+($1 --dbus --experimental-backends --backend dummy --log-level=debug --log-file=$PWD/log --config=$2) &
 main_pid=$!
 $3
 
diff --git a/tests/testcases/common.py b/tests/testcases/common.py
index cef5ac37..4e3f6056 100644
--- a/tests/testcases/common.py
+++ b/tests/testcases/common.py
@@ -1,11 +1,27 @@
 import xcffib.xproto as xproto
 import xcffib.randr as randr
+import xcffib
 import time
 import random
 import string
 def set_window_name(conn, wid, name):
     prop_name = "_NET_WM_NAME"
-    prop_name = conn.core.InternAtom(True, len(prop_name), prop_name).reply().atom
+    prop_name = conn.core.InternAtom(False, len(prop_name), prop_name).reply().atom
+    str_type = "UTF8_STRING"
+    str_type = conn.core.InternAtom(True, len(str_type), str_type).reply().atom
+    conn.core.ChangePropertyChecked(xproto.PropMode.Replace, wid, prop_name, str_type, 8, len(name), name).check()
+    prop_name = "WM_NAME"
+    prop_name = conn.core.InternAtom(False, len(prop_name), prop_name).reply().atom
+    str_type = "STRING"
+    str_type = conn.core.InternAtom(True, len(str_type), str_type).reply().atom
+    conn.core.ChangePropertyChecked(xproto.PropMode.Replace, wid, prop_name, str_type, 8, len(name), name).check()
+
+def set_window_class(conn, wid, name):
+    if not isinstance(name, bytearray):
+        name = name.encode()
+    name = name+b"\0"+name+b"\0"
+    prop_name = "WM_CLASS"
+    prop_name = conn.core.InternAtom(False, len(prop_name), prop_name).reply().atom
     str_type = "STRING"
     str_type = conn.core.InternAtom(True, len(str_type), str_type).reply().atom
     conn.core.ChangePropertyChecked(xproto.PropMode.Replace, wid, prop_name, str_type, 8, len(name), name).check()
@@ -41,3 +57,20 @@ def trigger_root_configure(conn):
     rr.AddOutputModeChecked(output, mode).check()
     rr.SetCrtcConfig(reply.crtcs[0], reply.timestamp, reply.config_timestamp, 0, 0, mode, randr.Rotation.Rotate_0, 1, [output]).reply()
 
+def find_32bit_visual(conn):
+    setup = conn.get_setup()
+    render = conn(xcffib.render.key)
+    r = render.QueryPictFormats().reply()
+    pictfmt_ids = set()
+    for pictform in r.formats:
+        if (pictform.depth == 32 and
+            pictform.type == xcffib.render.PictType.Direct and
+            pictform.direct.alpha_mask != 0):
+            pictfmt_ids.add(pictform.id)
+    print(pictfmt_ids)
+    for screen in r.screens:
+        for depth in screen.depths:
+            for pv in depth.visuals:
+                if pv.format in pictfmt_ids:
+                    return pv.visual
+
diff --git a/tests/testcases/issue299.py b/tests/testcases/issue299.py
new file mode 100755
index 00000000..11a307aa
--- /dev/null
+++ b/tests/testcases/issue299.py
@@ -0,0 +1,87 @@
+#!/usr/bin/env python
+
+import xcffib.xproto as xproto
+import xcffib
+import time
+import os
+import subprocess
+import asyncio
+from dbus_next.aio import MessageBus
+from dbus_next.message import Message, MessageType
+from common import set_window_name, set_window_class, trigger_root_configure, find_32bit_visual
+from pydbus import SessionBus
+
+display = os.environ["DISPLAY"].replace(":", "_")
+conn = xcffib.connect()
+setup = conn.get_setup()
+root = setup.roots[0].root
+visual = setup.roots[0].root_visual
+depth = setup.roots[0].root_depth
+x = xproto.xprotoExtension(conn)
+visual32 = find_32bit_visual(conn)
+
+loop = asyncio.get_event_loop()
+bus = loop.run_until_complete(MessageBus().connect())
+
+# start i3
+i3conf = os.memfd_create("i3config", flags=0)
+os.write(i3conf, b"""bar{}
+client.focused #0000ff20 #0000ff20 #0000ff20 #0000ff20 #0000ff20
+floating_modifier Mod1
+bindsym Mod1+1      workspace 1
+for_window [class=\".*\"] border pixel 50, floating enable
+""")
+os.lseek(i3conf, 0, os.SEEK_SET)
+print("/proc/self/fd/"+str(i3conf))
+i3 = subprocess.Popen(["i3", "-c", "/proc/self/fd/"+str(i3conf)], close_fds=False)
+
+time.sleep(0.5)
+
+cmid = conn.generate_id()
+colormap = conn.core.CreateColormapChecked(xproto.ColormapAlloc._None, cmid, root, visual32).check()
+# Create windows
+wids = []
+for i in range(0,50):
+    wid = conn.generate_id()
+    print("Window : ", hex(wid))
+    conn.core.CreateWindowChecked(32, wid, root, 0, 0, 100, 100, 0,
+        xproto.WindowClass.InputOutput, visual32, xproto.CW.BorderPixel | xproto.CW.Colormap, [0, cmid]).check()
+    set_window_name(conn, wid, "Test window "+str(i))
+    set_window_class(conn, wid, "Test windows")
+    wids.append(wid)
+    conn.core.MapWindowChecked(wid).check()
+
+time.sleep(1)
+
+async def get_mode(wid):
+    message = await bus.call(Message(destination='com.github.chjj.compton.'+display, 
+        path='/',
+        interface='com.github.chjj.compton',
+        member='win_get',
+        signature='us',
+        body=[wid, 'mode']))
+    return message.body[0]
+async def get_modes(wids):
+    return [await get_mode(wid) for wid in wids]
+
+while True:
+    subprocess.run(["i3-msg", "-t", "command", "restart"])
+
+    time.sleep(1)
+
+    frames = set()
+    for wid in wids:
+        r = conn.core.QueryTree(wid).reply()
+        frames.add(r.parent)
+
+    frames = list(frames)
+
+    modes = loop.run_until_complete(get_modes(frames))
+    assert all([mode == 0 for mode in modes]), modes
+
+# Destroy the windows
+for wid in wids:
+    conn.core.DestroyWindowChecked(wid).check()
+
+i3.kill()
+
