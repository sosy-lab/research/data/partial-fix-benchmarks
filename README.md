<!--
This file is part of Partial-Fix Benchmarks,
a benchmark set for program repair based on partial fixes.
https://gitlab.com/sosy-lab/research/data/partial-fix-benchmarks/

SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# NEW: Updated and Validated Benchmark Set

A larger validated version of this benchmark set has been accepted to MSR 2024: "P3: A Dataset of Partial Program Patches".
The dataset can be found on [GitLab](https://gitlab.com/sosy-lab/research/data/partial-fix-dataset).
Its snapshot is archived at Zenodo [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10369427.svg)](https://doi.org/10.5281/zenodo.10369427).


# Benchmarks for program repair based on partial fixes

This project contains benchmark tasks for automatic program repair of partial fixes.
The benchmark tasks reflect the following scenario:

- Bug found
- Bugfix committed, but doesn't fix
- Proper bugfix committed

All benchmark tasks are stored in the following directory structure:

```
c/
|- repositoryOwner_repositoryName/
|- |- issue_number/
|- |- |- task_name.yml
```

We express each benchmark task as:

1. the base repository with the bug - stored as full repository archive.
2. a failed (or partial) fix for the bug - stored as git-diffs of all commits related to the bug.
3. a final fix for the bug - stored as a single git-diff of the final, fixing commit.

Each benchmark task is defined through a YAML file.
Example:


```yml
format_version: '1.0'

# URL of the base repository with the bug
repository_url: https://github.com/karelzak/util-linux
# URL to the issue that describes the related bug
issue_url: https://github.com/karelzak/util-linux/issues/422


input_files:
    # the base version with the original bug.
    # stored as archive of the full repository
    # (as accessed through the repository_url above)
    # in the specified commit.
    base_version:
        input_file: 'a-base.zip'
        commit-sha1: c6b0cbd
    # the first fix attempt. this is a list of
    # multiple changes that tried to fix the bug.
    fix_attempt:
        - input_file: 'b-partial-1.diff'
          commit-sha1: b6b5272


# the expected fix.
expected_fix:
    input_file: 'c-expected-fix.diff'
    commit-sha1: d8bfcb4

# arbitrary options that may be used
# to specify metadata about the tasks.
# At the moment, we only use 'language'
# to specify the code language of the benchmark task.
options:
    language: C
```
