diff --git a/src/common/dsp/FastMath.h b/src/common/dsp/FastMath.h
index 9aad033b0..bb29ca052 100644
--- a/src/common/dsp/FastMath.h
+++ b/src/common/dsp/FastMath.h
@@ -1,5 +1,7 @@
 #pragma once
 
+#include <cmath>
+
 /*
 ** Fast Math Approximations to various Functions
 **
@@ -37,5 +39,18 @@ inline float fastcos( float x ) noexcept
    return numerator / denominator;
 }
 
+/*
+** Push x into -PI, PI periodically. There is probably a faster way to do this
+*/
+inline float clampToPiRange( float x )
+{
+    if( x <= M_PI && x >= -M_PI ) return x;
+    float y = x + M_PI; // so now I am 0,2PI
+    float p = fmod( y, 2.0 * M_PI );
+    if( p < 0 )
+        p += 2.0 * M_PI;
+    return p - M_PI;
+}
+
 }
 }
diff --git a/src/common/dsp/Oscillator.cpp b/src/common/dsp/Oscillator.cpp
index 50f42686a..145f0bfee 100644
--- a/src/common/dsp/Oscillator.cpp
+++ b/src/common/dsp/Oscillator.cpp
@@ -181,10 +181,7 @@ void osc_sine::process_block(float pitch, float drift, bool stereo, bool FM, flo
              p += FMdepth.v * master_osc[k];
 
           // Unlike ::sin and ::cos we are limited in accurate range
-          while( p > M_PI )
-             p -= 2.0 * M_PI;
-          while( p < -M_PI )
-             p += 2.0 * M_PI;
+          p = Surge::DSP::clampToPiRange(p);
           
           float out_local = valueFromSinAndCos(Surge::DSP::fastsin(p), Surge::DSP::fastcos(p));
 
@@ -267,11 +264,7 @@ void osc_sine::process_block_legacy(float pitch, float drift, bool stereo, bool
                 playingramp[u] = 1;
 
              phase[u] += omega[u] + master_osc[k] * FMdepth.v;
-             // phase in range -PI to PI. Since there's FM here we can go beyond so if -> while for now. FIX for CPU
-             while (phase[u] > M_PI)
-             {
-                phase[u] -= 2.0 * M_PI;
-             }
+             phase[u] = Surge::DSP::clampToPiRange(phase[u]);
          }
 
          FMdepth.process();
diff --git a/src/headless/UnitTestsDSP.cpp b/src/headless/UnitTestsDSP.cpp
index 2c288bda5..05d4128e9 100644
--- a/src/headless/UnitTestsDSP.cpp
+++ b/src/headless/UnitTestsDSP.cpp
@@ -10,6 +10,7 @@
 #include "catch2.hpp"
 
 #include "UnitTestUtilities.h"
+#include "FastMath.h"
 
 using namespace Surge::Test;
 
@@ -345,6 +346,82 @@ TEST_CASE( "lipol_ps class", "[dsp]" )
       
 }
 
+TEST_CASE( "Check FastMath Functions", "[dsp]" )
+{
+   SECTION( "fastSin and fastCos in range -PI, PI" )
+   {
+      float sds = 0, md = 0;
+      int nsamp = 100000;
+      for( int i=0; i<nsamp; ++i )
+      {
+         float p = 2.f * M_PI * rand() / RAND_MAX - M_PI;
+         float cp = std::cosf(p);
+         float sp = std::sinf(p);
+         float fcp = Surge::DSP::fastcos(p);
+         float fsp = Surge::DSP::fastsin(p);
+
+         float cd = fabs( cp - fcp );
+         float sd = fabs( sp - fsp );
+         if ( cd > md ) md = cd;
+         if ( sd > md ) md = sd;
+         sds += cd * cd + sd * sd;
+      }
+      sds = sqrt(sds) / nsamp;
+      REQUIRE( md < 1e-4 );
+      REQUIRE( sds < 1e-6 );
+   }
+
+   SECTION( "fastSin and fastCos in range -10*PI, 10*PI with clampRange" )
+   {
+      float sds = 0, md = 0;
+      int nsamp = 100000;
+      for( int i=0; i<nsamp; ++i )
+      {
+         float p = 2.f * M_PI * rand() / RAND_MAX - M_PI;
+         p *= 10;
+         p = Surge::DSP::clampToPiRange( p );
+         float cp = std::cosf(p);
+         float sp = std::sinf(p);
+         float fcp = Surge::DSP::fastcos(p);
+         float fsp = Surge::DSP::fastsin(p);
+
+         float cd = fabs( cp - fcp );
+         float sd = fabs( sp - fsp );
+         if ( cd > md ) md = cd;
+         if ( sd > md ) md = sd;
+         sds += cd * cd + sd * sd;
+      }
+      sds = sqrt(sds) / nsamp;
+      REQUIRE( md < 1e-4 );
+      REQUIRE( sds < 1e-6 );
+   }
+
+   SECTION( "fastSin and fastCos in range -100*PI, 100*PI with clampRange" )
+   {
+      float sds = 0, md = 0;
+      int nsamp = 100000;
+      for( int i=0; i<nsamp; ++i )
+      {
+         float p = 2.f * M_PI * rand() / RAND_MAX - M_PI;
+         p *= 100;
+         p = Surge::DSP::clampToPiRange( p );
+         float cp = std::cosf(p);
+         float sp = std::sinf(p);
+         float fcp = Surge::DSP::fastcos(p);
+         float fsp = Surge::DSP::fastsin(p);
+
+         float cd = fabs( cp - fcp );
+         float sd = fabs( sp - fsp );
+         if ( cd > md ) md = cd;
+         if ( sd > md ) md = sd;
+         sds += cd * cd + sd * sd;
+      }
+      sds = sqrt(sds) / nsamp;
+      REQUIRE( md < 1e-4 );
+      REQUIRE( sds < 1e-6 );
+   }
+}
+
 // When we return to #1514 this is a good starting point
 #if 0
 TEST_CASE( "NaN Patch from Issue 1514", "[dsp]" )
