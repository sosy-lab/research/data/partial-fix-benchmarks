diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index ddea956172..c955358f91 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -57,11 +57,11 @@ set(HEADERS
   hypertable.h
   hypertable_insert.h
   indexing.h
-  parse_rewrite.h
   partitioning.h
   planner_utils.h
   process_utility.h
   scanner.h
+  hypertable_restrict_info.h
   subspace_store.h
   tablespace.h
   trigger.h
@@ -95,14 +95,13 @@ set(SOURCES
   hypertable_insert.c
   indexing.c
   init.c
-  parse_analyze.c
-  parse_rewrite.c
   partitioning.c
   planner.c
   planner_utils.c
   process_utility.c
   scanner.c
   sort_transform.c
+  hypertable_restrict_info.c
   subspace_store.c
   tablespace.c
   trigger.c
diff --git a/src/chunk.c b/src/chunk.c
index 44aa674256..454e5181fa 100644
--- a/src/chunk.c
+++ b/src/chunk.c
@@ -894,7 +894,7 @@ chunk_scan_find(int indexid,
 									nkeys,
 									chunk_tuple_found,
 									chunk,
-									num_constraints,
+									0,
 									AccessShareLock);
 
 	switch (num_found)
diff --git a/src/hypertable_restrict_info.c b/src/hypertable_restrict_info.c
new file mode 100644
index 0000000000..eb1616e667
--- /dev/null
+++ b/src/hypertable_restrict_info.c
@@ -0,0 +1,171 @@
+#include <postgres.h>
+#include <nodes/parsenodes.h>
+#include <nodes/nodeFuncs.h>
+#include <nodes/makefuncs.h>
+#include <parser/parsetree.h>
+#include <parser/parse_coerce.h>
+#include <parser/parse_func.h>
+#include <parser/parse_collate.h>
+#include <parser/parse_oper.h>
+#include <optimizer/clauses.h>
+#include <catalog/pg_type.h>
+#include <optimizer/paths.h>
+
+#include "cache.h"
+#include "hypertable.h"
+#include "partitioning.h"
+#include "compat.h"
+
+#include "hypertable_restrict_info.h"
+#include "chunk.h"
+#include "hypercube.h"
+#include "hypertable.h"
+
+static inline Dimension *
+get_dimension_for_partition_column_var(Var *var_expr, Hypertable *ht, Query *parse)
+{
+	RangeTblEntry *rte = rt_fetch(var_expr->varno, parse->rtable);
+	char	   *varname;
+
+	if (rte->relid != ht->main_table_relid)
+		return NULL;
+
+	varname = get_rte_attribute_name(rte, var_expr->varattno);
+
+	return hyperspace_get_dimension_by_name(ht->space, DIMENSION_TYPE_CLOSED, varname);
+}
+
+static Const *
+get_const_node(Node *node)
+{
+	if (IsA(node, Const))
+		return (Const *) node;
+	/* try to simplify the non-var expression */
+	node = eval_const_expressions(NULL, node);
+
+	if (IsA(node, Const))
+		return (Const *) node;
+
+	return NULL;
+}
+
+/* Return true if expr is var = const or const = var. If true, set
+ * var_node and const_node to the var and const. */
+static bool
+is_var_equal_const_opexpr(OpExpr *opexpr, Var **var_node, Const **const_node)
+{
+	Node	   *left;
+	Node	   *right;
+	Oid			eq_oid;
+
+	if (list_length(opexpr->args) != 2)
+		return false;
+
+
+	left = linitial(opexpr->args);
+	right = lsecond(opexpr->args);
+
+
+	if (IsA(left, Var))
+	{
+		*var_node = (Var *) left;
+		*const_node = get_const_node(right);
+	}
+	else if (IsA(right, Var))
+	{
+		*var_node = (Var *) right;
+		*const_node = get_const_node(left);
+	}
+	else
+	{
+		return false;
+	}
+
+	if (*const_node == NULL)
+		return false;
+
+
+	eq_oid = OpernameGetOprid(list_make2(makeString("pg_catalog"), makeString("=")), exprType(left), exprType(right));
+
+	if (eq_oid != opexpr->opno)
+		return false;
+
+	return true;
+}
+
+static HypertableRestrictInfo *
+hypertable_restrict_info_create(Dimension *dim, int32 hash_value)
+{
+	HypertableRestrictInfo *hre = palloc(sizeof(HypertableRestrictInfo));
+
+	*hre = (HypertableRestrictInfo)
+	{
+		.dim = dim,
+			.value = hash_value,
+	};
+	return hre;
+}
+
+
+HypertableRestrictInfo *
+hypertable_restrict_info_get(RestrictInfo *base_restrict_info, Query *query, Hypertable *hentry)
+{
+	Var		   *var_expr;
+	Const	   *const_expr;
+	Expr	   *node = base_restrict_info->clause;
+	OpExpr	   *opexpr;
+
+	if (!IsA(node, OpExpr))
+		return NULL;
+
+	opexpr = (OpExpr *) node;
+
+	if (is_var_equal_const_opexpr(opexpr, &var_expr, &const_expr))
+	{
+		/*
+		 * I now have a var = const. Make sure var is a partitioning column
+		 */
+		Dimension  *dim = get_dimension_for_partition_column_var(var_expr, hentry, query);
+
+		if (dim != NULL)
+			return hypertable_restrict_info_create(dim,
+												   partitioning_func_apply(dim->partitioning, const_expr->constvalue));
+	}
+	return NULL;
+}
+
+
+void
+hypertable_restrict_info_apply(HypertableRestrictInfo *spe,
+							   PlannerInfo *root,
+							   Hypertable *ht,
+							   Index main_table_child_index)
+{
+	ListCell   *lsib;
+
+	/*
+	 * Go through all the append rel list and find all children that are
+	 * chunks. Then check the chunk for exclusion and make dummy if necessary
+	 */
+	foreach(lsib, root->append_rel_list)
+	{
+		AppendRelInfo *appinfo = (AppendRelInfo *) lfirst(lsib);
+		RelOptInfo *siblingrel;
+		RangeTblEntry *siblingrte;
+		Chunk	   *chunk;
+		DimensionSlice *ds;
+
+		/* find all chunks: children that are not the main table */
+		if (appinfo->parent_reloid != ht->main_table_relid || appinfo->child_relid == main_table_child_index)
+			continue;
+
+		siblingrel = root->simple_rel_array[appinfo->child_relid];
+		siblingrte = root->simple_rte_array[appinfo->child_relid];
+		chunk = chunk_get_by_relid(siblingrte->relid, ht->space->num_dimensions, true);
+		ds = hypercube_get_slice_by_dimension_id(chunk->cube, spe->dim->fd.id);
+
+		/* If the hash value is not inside the ds, exclude the chunk */
+		if (dimension_slice_cmp_coordinate(ds, (int64) spe->value) != 0)
+			set_dummy_rel_pathlist(siblingrel);
+	}
+}
diff --git a/src/hypertable_restrict_info.h b/src/hypertable_restrict_info.h
new file mode 100644
index 0000000000..2b09b85c73
--- /dev/null
+++ b/src/hypertable_restrict_info.h
@@ -0,0 +1,21 @@
+#ifndef TIMESCALEDB_SPACE_PARTITION_EXCLUDE_H
+#define TIMESCALEDB_SPACE_PARTITION_EXCLUDE_H
+
+#include <postgres.h>
+#include "dimension.h"
+
+typedef struct HypertableRestrictInfo
+{
+	Dimension  *dim;
+	int32		value;
+} HypertableRestrictInfo;
+
+HypertableRestrictInfo *hypertable_restrict_info_get(RestrictInfo *base_restrict_info, Query *query, Hypertable *hentry);
+
+void hypertable_restrict_info_apply(HypertableRestrictInfo *spe,
+							   PlannerInfo *root,
+							   Hypertable *ht,
+							   Index main_table_child_index);
+
+
+#endif							/* TIMESCALEDB_SPACE_PARTITION_EXCLUDE_H */
diff --git a/src/init.c b/src/init.c
index 900345aa70..9ce8be1c1b 100644
--- a/src/init.c
+++ b/src/init.c
@@ -36,9 +36,6 @@ extern void _process_utility_fini(void);
 extern void _event_trigger_init(void);
 extern void _event_trigger_fini(void);
 
-extern void _parse_analyze_init(void);
-extern void _parse_analyze_fini(void);
-
 extern void PGDLLEXPORT _PG_init(void);
 extern void PGDLLEXPORT _PG_fini(void);
 
@@ -58,7 +55,6 @@ _PG_init(void)
 	_planner_init();
 	_event_trigger_init();
 	_process_utility_init();
-	_parse_analyze_init();
 	_guc_init();
 }
 
@@ -70,7 +66,6 @@ _PG_fini(void)
 	 * document any exceptions.
 	 */
 	_guc_fini();
-	_parse_analyze_fini();
 	_process_utility_fini();
 	_event_trigger_fini();
 	_planner_fini();
diff --git a/src/parse_analyze.c b/src/parse_analyze.c
deleted file mode 100644
index 457ec039fa..0000000000
--- a/src/parse_analyze.c
+++ /dev/null
@@ -1,121 +0,0 @@
-#include <postgres.h>
-#include <nodes/nodeFuncs.h>
-#include <parser/analyze.h>
-
-#include "cache.h"
-#include "hypertable.h"
-#include "hypertable_cache.h"
-#include "extension.h"
-#include "parse_rewrite.h"
-
-void		_parse_analyze_init(void);
-void		_parse_analyze_fini(void);
-
-static post_parse_analyze_hook_type prev_post_parse_analyze_hook = NULL;
-
-typedef struct HypertableQueryCtx
-{
-	Query	   *parse;
-	Query	   *parent;
-	CmdType		cmdtype;
-	Cache	   *hcache;
-	Hypertable *hentry;
-} HypertableQueryCtx;
-
-/*
- * Identify queries on a hypertable by walking the query tree. If the query is
- * indeed on a hypertable, setup the necessary state and/or make modifications
- * to the query tree.
- */
-static bool
-hypertable_query_walker(Node *node, void *context)
-{
-	if (node == NULL)
-		return false;
-
-	if (IsA(node, RangeTblEntry))
-	{
-		RangeTblEntry *rte = (RangeTblEntry *) node;
-		HypertableQueryCtx *ctx = (HypertableQueryCtx *) context;
-
-		if (rte->rtekind == RTE_RELATION)
-		{
-			Hypertable *hentry = hypertable_cache_get_entry(ctx->hcache, rte->relid);
-
-			if (hentry != NULL)
-				ctx->hentry = hentry;
-		}
-
-		return false;
-	}
-
-	if (IsA(node, Query))
-	{
-		bool		result;
-		HypertableQueryCtx *ctx = (HypertableQueryCtx *) context;
-		CmdType		old = ctx->cmdtype;
-		Query	   *query = (Query *) node;
-		Query	   *oldparent = ctx->parent;
-
-		/* adjust context */
-		ctx->cmdtype = query->commandType;
-		ctx->parent = query;
-
-		result = query_tree_walker(ctx->parent, hypertable_query_walker,
-								   context, QTW_EXAMINE_RTES);
-
-		/* restore context */
-		ctx->cmdtype = old;
-		ctx->parent = oldparent;
-
-		return result;
-	}
-
-	return expression_tree_walker(node, hypertable_query_walker, context);
-}
-
-static void
-timescaledb_post_parse_analyze(ParseState *pstate, Query *query)
-{
-	if (NULL != prev_post_parse_analyze_hook)
-		/* Call any earlier hooks */
-		prev_post_parse_analyze_hook(pstate, query);
-
-	if (extension_is_loaded())
-	{
-		HypertableQueryCtx context = {
-			.parse = query,
-			.parent = query,
-			.cmdtype = query->commandType,
-			.hcache = hypertable_cache_pin(),
-			.hentry = NULL,
-		};
-
-		/* The query for explains is in the utility statement */
-		if (query->commandType == CMD_UTILITY &&
-			IsA(query->utilityStmt, ExplainStmt))
-			query = (Query *) ((ExplainStmt *) query->utilityStmt)->query;
-
-		hypertable_query_walker((Node *) query, &context);
-
-		/* note assumes 1 hypertable per query */
-		if (NULL != context.hentry)
-			parse_rewrite_query(pstate, query, context.hentry);
-
-		cache_release(context.hcache);
-	}
-}
-
-void
-_parse_analyze_init(void)
-{
-	prev_post_parse_analyze_hook = post_parse_analyze_hook;
-	post_parse_analyze_hook = timescaledb_post_parse_analyze;
-}
-
-void
-_parse_analyze_fini(void)
-{
-	post_parse_analyze_hook = prev_post_parse_analyze_hook;
-	prev_post_parse_analyze_hook = NULL;
-}
diff --git a/src/parse_rewrite.c b/src/parse_rewrite.c
deleted file mode 100644
index 2479cdceb7..0000000000
--- a/src/parse_rewrite.c
+++ /dev/null
@@ -1,206 +0,0 @@
-#include <postgres.h>
-#include <nodes/parsenodes.h>
-#include <nodes/nodeFuncs.h>
-#include <nodes/makefuncs.h>
-#include <parser/parsetree.h>
-#include <parser/parse_coerce.h>
-#include <parser/parse_func.h>
-#include <parser/parse_collate.h>
-#include <parser/parse_oper.h>
-#include <optimizer/clauses.h>
-#include <catalog/pg_type.h>
-
-#include "cache.h"
-#include "hypertable.h"
-#include "partitioning.h"
-#include "parse_rewrite.h"
-#include "compat.h"
-
-typedef struct AddPartFuncQualCtx
-{
-	ParseState *pstate;
-	Query	   *parse;
-	Hypertable *hentry;
-} AddPartFuncQualCtx;
-
-
-/*
- * Returns the partitioning info for a var if the var is a partitioning
- * column. If the var is not a partitioning column return NULL.
- */
-static inline PartitioningInfo *
-get_partitioning_info_for_partition_column_var(Var *var_expr, AddPartFuncQualCtx *context)
-{
-	Hypertable *ht = context->hentry;
-	RangeTblEntry *rte = rt_fetch(var_expr->varno, context->parse->rtable);
-	char	   *varname;
-	Dimension  *dim;
-
-	if (rte->relid != ht->main_table_relid)
-		return NULL;
-
-	varname = get_rte_attribute_name(rte, var_expr->varattno);
-
-	dim = hyperspace_get_dimension_by_name(ht->space, DIMENSION_TYPE_CLOSED, varname);
-
-	if (dim != NULL)
-		return dim->partitioning;
-
-	return NULL;
-}
-
-/*
- * Creates an expression for partioning_func(var_expr, partitioning_mod) =
- * partitioning_func(const_expr, partitioning_mod).  This function makes a copy
- * of all nodes given in input.
- */
-static Expr *
-create_partition_func_equals_const(ParseState *pstate, PartitioningInfo *pi, Var *var_expr, Const *const_expr)
-{
-	Expr	   *op_expr;
-	List	   *func_name = partitioning_func_qualified_name(&pi->partfunc);
-	Node	   *var_node;
-	Node	   *const_node;
-	List	   *args_func_var;
-	List	   *args_func_const;
-	FuncCall   *fc_var;
-	FuncCall   *fc_const;
-	Node	   *f_var;
-	Node	   *f_const;
-
-	var_node = (Node *) copyObject(var_expr);
-	const_node = (Node *) copyObject(const_expr);
-
-	args_func_var = list_make1(var_node);
-	args_func_const = list_make1(const_node);
-
-	fc_var = makeFuncCall(func_name, args_func_var, -1);
-	fc_const = makeFuncCall(func_name, args_func_const, -1);
-
-	f_var = ParseFuncOrColumnCompat(pstate,
-									func_name,
-									args_func_var,
-									fc_var,
-									-1);
-
-	assign_expr_collations(pstate, f_var);
-
-	f_const = ParseFuncOrColumnCompat(pstate,
-									  func_name,
-									  args_func_const,
-									  fc_const,
-									  -1);
-
-	op_expr = make_op_compat(pstate,
-							 list_make2(makeString("pg_catalog"), makeString("=")),
-							 f_var,
-							 f_const,
-							 -1);
-
-	return op_expr;
-}
-
-static Node *
-add_partitioning_func_qual_mutator(Node *node, AddPartFuncQualCtx *context)
-{
-	if (node == NULL)
-		return NULL;
-
-	/*
-	 * Detect partitioning_column = const. If not fall-thru. If detected,
-	 * replace with partitioning_column = const AND
-	 * partitioning_func(partition_column) = partitioning_func(const)
-	 */
-	if (IsA(node, OpExpr))
-	{
-		OpExpr	   *exp = (OpExpr *) node;
-
-		if (list_length(exp->args) == 2)
-		{
-			/* only look at var op const or const op var; */
-			Node	   *left = (Node *) linitial(exp->args);
-			Node	   *right = (Node *) lsecond(exp->args);
-			Var		   *var_expr = NULL;
-			Node	   *other_expr = NULL;
-
-			if (IsA(left, Var))
-			{
-				var_expr = (Var *) left;
-				other_expr = right;
-			}
-			else if (IsA(right, Var))
-			{
-				var_expr = (Var *) right;
-				other_expr = left;
-			}
-
-			if (var_expr != NULL)
-			{
-				if (!IsA(other_expr, Const))
-				{
-					/* try to simplify the non-var expression */
-					other_expr = eval_const_expressions(NULL, other_expr);
-				}
-				if (IsA(other_expr, Const))
-				{
-					/* have a var and const, make sure the op is = */
-					Const	   *const_expr = (Const *) other_expr;
-					Oid			eq_oid = OpernameGetOprid(list_make2(makeString("pg_catalog"), makeString("=")), exprType(left), exprType(right));
-
-					if (eq_oid == exp->opno)
-					{
-						/*
-						 * I now have a var = const. Make sure var is a
-						 * partitioning column
-						 */
-						PartitioningInfo *pi =
-						get_partitioning_info_for_partition_column_var(var_expr,
-																	   context);
-
-						if (pi != NULL)
-						{
-							/* The var is a partitioning column */
-							Expr	   *partitioning_clause =
-							create_partition_func_equals_const(context->pstate, pi, var_expr, const_expr);
-
-							return (Node *) make_andclause(list_make2(node, partitioning_clause));
-
-						}
-					}
-				}
-			}
-		}
-	}
-
-	return expression_tree_mutator(node, add_partitioning_func_qual_mutator,
-								   (void *) context);
-}
-
-/*
- * This function does a transformation that allows postgres's native constraint
- * exclusion to exclude space partititions when the query contains equivalence
- * qualifiers on the space partition key.
- *
- * This function goes through the upper-level qual of a parse tree and finds
- * quals of the form:
- *				partitioning_column = const
- * It transforms them into the qual:
- *				partitioning_column = const AND
- *				partitioning_func(partition_column, partitioning_mod) =
- *				partitioning_func(const, partitioning_mod)
- *
- * This tranformation helps because the check constraint on a table is of the
- * form CHECK(partitioning_func(partition_column, partitioning_mod) BETWEEN X
- * AND Y).
- */
-void
-parse_rewrite_query(ParseState *pstate, Query *parse, Hypertable *ht)
-{
-	AddPartFuncQualCtx context = {
-		.pstate = pstate,
-		.parse = parse,
-		.hentry = ht,
-	};
-
-	parse->jointree->quals = add_partitioning_func_qual_mutator(parse->jointree->quals, &context);
-}
diff --git a/src/parse_rewrite.h b/src/parse_rewrite.h
deleted file mode 100644
index d3ee0d8fd4..0000000000
--- a/src/parse_rewrite.h
+++ /dev/null
@@ -1,11 +0,0 @@
-#ifndef TIMESCALEDB_PARSE_REWRITE_H
-#define TIMESCALEDB_PARSE_REWRITE_H
-
-#include <postgres.h>
-#include <nodes/parsenodes.h>
-
-typedef struct Hypertable Hypertable;
-
-extern void parse_rewrite_query(ParseState *pstate, Query *parse, Hypertable *ht);
-
-#endif							/* TIMESCALEDB_PARSE_REWRITE_H */
diff --git a/src/planner.c b/src/planner.c
index f3546fbb48..5a245a6d53 100644
--- a/src/planner.c
+++ b/src/planner.c
@@ -22,6 +22,9 @@
 #include "planner_utils.h"
 #include "hypertable_insert.h"
 #include "constraint_aware_append.h"
+#include "hypertable_restrict_info.h"
+#include "chunk.h"
+#include "hypercube.h"
 
 void		_planner_init(void);
 void		_planner_fini(void);
@@ -262,16 +265,35 @@ timescaledb_set_rel_pathlist(PlannerInfo *root,
 	else if (ht != NULL && is_append_child(rel, rte))
 	{
 		/* Otherwise, apply only to hypertables */
-
 		/*
-		 * When applying to hypertables, apply when you get the first append
-		 * relation child (indicated by RELOPT_OTHER_MEMBER_REL) which is the
-		 * main table. Then apply to all other children of that hypertable. We
-		 * can't wait to get the parent of the append relation b/c by that
-		 * time it's too late.
+		 * This is the case where we find the main table as a child of the
+		 * append relation. The order of execution of this function is:
+		 * main_table as child, chunks, main_table as parent because of the
+		 * way the recursion in the planner works.
 		 */
 		ListCell   *l;
 
+		/*
+		 * Exclude the chunks by space partition if you see column = value in
+		 * the qual (baserestrictinfo).
+		 */
+		foreach(l, rel->baserestrictinfo)
+		{
+			RestrictInfo *re = lfirst(l);
+			HypertableRestrictInfo *spe = hypertable_restrict_info_get(re, root->parse, ht);
+
+			if (NULL != spe)
+				hypertable_restrict_info_apply(spe, root, ht, rti);
+		}
+
+		/*
+		 * Apply the sort_transform_optimization which makes the planner
+		 * understand that certain functions (e.g. date_trunc, time_bucket)
+		 * preserve order in such ways that indexes on the plain column can be
+		 * reused. Note: do the transform on the chunks before they are
+		 * processed in the usual way. Thus, do the transform when you see the
+		 * main_table_child and before you see the chunks.
+		 */
 		foreach(l, root->append_rel_list)
 		{
 			AppendRelInfo *appinfo = (AppendRelInfo *) lfirst(l);
diff --git a/test/expected/append.out b/test/expected/append.out
index 0b50adf965..b63ecb50c9 100644
--- a/test/expected/append.out
+++ b/test/expected/append.out
@@ -158,7 +158,6 @@ psql:include/append.sql:68: NOTICE:  Stable function now_s() called!
 EXPLAIN (costs off)
 SELECT * FROM append_test WHERE time > now_i() - interval '2 months'
 ORDER BY time;
-psql:include/append.sql:75: NOTICE:  Immutable function now_i() called!
 psql:include/append.sql:75: NOTICE:  Immutable function now_i() called!
                                                              QUERY PLAN                                                             
 ------------------------------------------------------------------------------------------------------------------------------------
diff --git a/test/expected/append_unoptimized.out b/test/expected/append_unoptimized.out
index 795b5ec7a2..0fd676ece2 100644
--- a/test/expected/append_unoptimized.out
+++ b/test/expected/append_unoptimized.out
@@ -178,7 +178,6 @@ psql:include/append.sql:68: NOTICE:  Stable function now_s() called!
 EXPLAIN (costs off)
 SELECT * FROM append_test WHERE time > now_i() - interval '2 months'
 ORDER BY time;
-psql:include/append.sql:75: NOTICE:  Immutable function now_i() called!
 psql:include/append.sql:75: NOTICE:  Immutable function now_i() called!
                                                           QUERY PLAN                                                          
 ------------------------------------------------------------------------------------------------------------------------------
diff --git a/test/expected/append_x_diff.out b/test/expected/append_x_diff.out
index eff4fddba1..cbb8cac571 100644
--- a/test/expected/append_x_diff.out
+++ b/test/expected/append_x_diff.out
@@ -109,13 +109,13 @@
 168,169d149
 < psql:include/append.sql:68: NOTICE:  Stable function now_s() called!
 < psql:include/append.sql:68: NOTICE:  Stable function now_s() called!
-183,184c163,164
+182,183c162,163
 <                                                           QUERY PLAN                                                          
 < ------------------------------------------------------------------------------------------------------------------------------
 ---
 >                                                              QUERY PLAN                                                             
 > ------------------------------------------------------------------------------------------------------------------------------------
-187,202c167,175
+186,201c166,174
 <    ->  Append
 <          ->  Seq Scan on append_test
 <                Filter: ("time" > ('Tue Aug 22 10:00:00 2017 PDT'::timestamp with time zone - '@ 2 mons'::interval))
@@ -142,13 +142,13 @@
 >                      ->  Bitmap Index Scan on _hyper_1_3_chunk_append_test_time_idx
 >                            Index Cond: ("time" > ('Tue Aug 22 10:00:00 2017 PDT'::timestamp with time zone - '@ 2 mons'::interval))
 > (10 rows)
-211,212c184,185
+210,211c183,184
 <                             QUERY PLAN                             
 < -------------------------------------------------------------------
 ---
 >                                QUERY PLAN                                
 > -------------------------------------------------------------------------
-215,224c188,198
+214,223c187,197
 <    ->  Append
 <          ->  Seq Scan on append_test
 <                Filter: ("time" > (now_v() - '@ 2 mons'::interval))
@@ -171,10 +171,10 @@
 >                ->  Seq Scan on _hyper_1_3_chunk
 >                      Filter: ("time" > (now_v() - '@ 2 mons'::interval))
 > (12 rows)
-250,251d223
+249,250d222
 < psql:include/append.sql:94: NOTICE:  Stable function now_s() called!
 < psql:include/append.sql:94: NOTICE:  Stable function now_s() called!
-259,271c231,241
+258,270c230,240
 <                                         QUERY PLAN                                         
 < -------------------------------------------------------------------------------------------
 <  Merge Append
@@ -200,30 +200,30 @@
 >          ->  Index Scan Backward using _hyper_1_3_chunk_append_test_time_idx on _hyper_1_3_chunk
 >                Index Cond: ("time" > (now_s() - '@ 2 mons'::interval))
 > (7 rows)
-299a270
+298a269
 > psql:include/append.sql:110: NOTICE:  Stable function now_s() called!
-306c277,279
+305c276,278
 <          ->  Result
 ---
 >          ->  Custom Scan (ConstraintAwareAppend)
 >                Hypertable: append_test
 >                Chunks left after exclusion: 2
-308,311d280
+307,310d279
 <                      ->  Seq Scan on append_test
 <                            Filter: ("time" > (now_s() - '@ 4 mons'::interval))
 <                      ->  Index Scan using _hyper_1_1_chunk_append_test_time_idx on _hyper_1_1_chunk
 <                            Index Cond: ("time" > (now_s() - '@ 4 mons'::interval))
-316c285
+315c284
 < (14 rows)
 ---
 > (12 rows)
-328,329c297,298
+327,328c296,297
 <                                                                                                              QUERY PLAN                                                                                                              
 < -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---
 >                               QUERY PLAN                               
 > -----------------------------------------------------------------------
-334,338c303,306
+333,337c302,305
 <          ->  Result
 <                ->  Append
 <                      ->  Seq Scan on append_test
@@ -234,36 +234,36 @@
 >                Hypertable: append_test
 >                Chunks left after exclusion: 0
 > (7 rows)
-383,384c351,353
+382,383c350,352
 <                                               QUERY PLAN                                               
 < -------------------------------------------------------------------------------------------------------
 ---
 > psql:include/append.sql:149: NOTICE:  Stable function now_s() called!
 >                                                   QUERY PLAN                                                   
 > ---------------------------------------------------------------------------------------------------------------
-390c359,361
+389c358,360
 <            ->  Result
 ---
 >            ->  Custom Scan (ConstraintAwareAppend)
 >                  Hypertable: append_test
 >                  Chunks left after exclusion: 3
-392,394c363
+391,393c362
 <                        ->  Seq Scan on append_test
 <                              Filter: ((colorid > 0) AND ("time" > (now_s() - '@ 400 days'::interval)))
 <                        ->  Index Scan using _hyper_1_1_chunk_append_test_time_idx on _hyper_1_1_chunk
 ---
 >                        ->  Index Scan Backward using _hyper_1_1_chunk_append_test_time_idx on _hyper_1_1_chunk
-397c366
+396c365
 <                        ->  Index Scan using _hyper_1_2_chunk_append_test_time_idx on _hyper_1_2_chunk
 ---
 >                        ->  Index Scan Backward using _hyper_1_2_chunk_append_test_time_idx on _hyper_1_2_chunk
-400c369
+399c368
 <                        ->  Index Scan using _hyper_1_3_chunk_append_test_time_idx on _hyper_1_3_chunk
 ---
 >                        ->  Index Scan Backward using _hyper_1_3_chunk_append_test_time_idx on _hyper_1_3_chunk
-435a405
+434a404
 > psql:include/append.sql:166: NOTICE:  Stable function now_s() called!
-475,476c445,448
+474,475c444,447
 <                                          QUERY PLAN                                         
 < --------------------------------------------------------------------------------------------
 ---
@@ -271,7 +271,7 @@
 > psql:include/append.sql:187: NOTICE:  Stable function now_s() called!
 >                                             QUERY PLAN                                            
 > --------------------------------------------------------------------------------------------------
-479,497c451,467
+478,496c450,466
 <    ->  Append
 <          ->  Seq Scan on append_test a
 <                Filter: ("time" > (now_s() - '@ 3 hours'::interval))
diff --git a/test/expected/partitioning.out b/test/expected/partitioning.out
index d9cd947dda..dec2b8cfad 100644
--- a/test/expected/partitioning.out
+++ b/test/expected/partitioning.out
@@ -30,15 +30,15 @@ SELECT * FROM test.show_constraintsp('_timescaledb_internal._hyper_1_%_chunk');
 -- Make sure constraint exclusion works on device column
 EXPLAIN (verbose, costs off)
 SELECT * FROM part_legacy WHERE device = 1;
-                                                               QUERY PLAN                                                                
------------------------------------------------------------------------------------------------------------------------------------------
+                                       QUERY PLAN                                        
+-----------------------------------------------------------------------------------------
  Append
    ->  Seq Scan on public.part_legacy
          Output: part_legacy."time", part_legacy.temp, part_legacy.device
-         Filter: ((part_legacy.device = 1) AND (_timescaledb_internal.get_partition_for_key(part_legacy.device) = 1516350201))
+         Filter: (part_legacy.device = 1)
    ->  Seq Scan on _timescaledb_internal._hyper_1_1_chunk
          Output: _hyper_1_1_chunk."time", _hyper_1_1_chunk.temp, _hyper_1_1_chunk.device
-         Filter: ((_hyper_1_1_chunk.device = 1) AND (_timescaledb_internal.get_partition_for_key(_hyper_1_1_chunk.device) = 1516350201))
+         Filter: (_hyper_1_1_chunk.device = 1)
 (7 rows)
 
 CREATE TABLE part_new(time timestamptz, temp float, device int);
@@ -74,15 +74,15 @@ SELECT * FROM test.show_constraintsp('_timescaledb_internal._hyper_2_%_chunk');
 -- Make sure constraint exclusion works on device column
 EXPLAIN (verbose, costs off)
 SELECT * FROM part_new WHERE device = 1;
-                                                             QUERY PLAN                                                              
--------------------------------------------------------------------------------------------------------------------------------------
+                                       QUERY PLAN                                        
+-----------------------------------------------------------------------------------------
  Append
    ->  Seq Scan on public.part_new
          Output: part_new."time", part_new.temp, part_new.device
-         Filter: ((part_new.device = 1) AND (_timescaledb_internal.get_partition_hash(part_new.device) = 242423622))
+         Filter: (part_new.device = 1)
    ->  Seq Scan on _timescaledb_internal._hyper_2_3_chunk
          Output: _hyper_2_3_chunk."time", _hyper_2_3_chunk.temp, _hyper_2_3_chunk.device
-         Filter: ((_hyper_2_3_chunk.device = 1) AND (_timescaledb_internal.get_partition_hash(_hyper_2_3_chunk.device) = 242423622))
+         Filter: (_hyper_2_3_chunk.device = 1)
 (7 rows)
 
 CREATE TABLE part_new_convert1(time timestamptz, temp float8, device int);
diff --git a/test/expected/sql_query.out b/test/expected/sql_query.out
index 043400d1c0..d683ef4827 100644
--- a/test/expected/sql_query.out
+++ b/test/expected/sql_query.out
@@ -70,14 +70,13 @@ EXPLAIN (verbose ON, costs off) SELECT * FROM PUBLIC."two_Partitions" WHERE devi
  Append
    ->  Seq Scan on public."two_Partitions"
          Output: "two_Partitions"."timeCustom", "two_Partitions".device_id, "two_Partitions".series_0, "two_Partitions".series_1, "two_Partitions".series_2, "two_Partitions".series_bool
-         Filter: (("two_Partitions".device_id = 'dev2'::text) AND (_timescaledb_internal.get_partition_hash("two_Partitions".device_id) = 405750566))
+         Filter: ("two_Partitions".device_id = 'dev2'::text)
    ->  Bitmap Heap Scan on _timescaledb_internal._hyper_1_4_chunk
          Output: _hyper_1_4_chunk."timeCustom", _hyper_1_4_chunk.device_id, _hyper_1_4_chunk.series_0, _hyper_1_4_chunk.series_1, _hyper_1_4_chunk.series_2, _hyper_1_4_chunk.series_bool
          Recheck Cond: (_hyper_1_4_chunk.device_id = 'dev2'::text)
-         Filter: (_timescaledb_internal.get_partition_hash(_hyper_1_4_chunk.device_id) = 405750566)
          ->  Bitmap Index Scan on "_hyper_1_4_chunk_two_Partitions_device_id_timeCustom_idx"
                Index Cond: (_hyper_1_4_chunk.device_id = 'dev2'::text)
-(10 rows)
+(9 rows)
 
 EXPLAIN (verbose ON, costs off) SELECT * FROM PUBLIC."two_Partitions" WHERE device_id = 'dev'||'2';
                                                                                         QUERY PLAN                                                                                        
@@ -85,14 +84,13 @@ EXPLAIN (verbose ON, costs off) SELECT * FROM PUBLIC."two_Partitions" WHERE devi
  Append
    ->  Seq Scan on public."two_Partitions"
          Output: "two_Partitions"."timeCustom", "two_Partitions".device_id, "two_Partitions".series_0, "two_Partitions".series_1, "two_Partitions".series_2, "two_Partitions".series_bool
-         Filter: (("two_Partitions".device_id = 'dev2'::text) AND (_timescaledb_internal.get_partition_hash("two_Partitions".device_id) = 405750566))
+         Filter: ("two_Partitions".device_id = 'dev2'::text)
    ->  Bitmap Heap Scan on _timescaledb_internal._hyper_1_4_chunk
          Output: _hyper_1_4_chunk."timeCustom", _hyper_1_4_chunk.device_id, _hyper_1_4_chunk.series_0, _hyper_1_4_chunk.series_1, _hyper_1_4_chunk.series_2, _hyper_1_4_chunk.series_bool
          Recheck Cond: (_hyper_1_4_chunk.device_id = 'dev2'::text)
-         Filter: (_timescaledb_internal.get_partition_hash(_hyper_1_4_chunk.device_id) = 405750566)
          ->  Bitmap Index Scan on "_hyper_1_4_chunk_two_Partitions_device_id_timeCustom_idx"
                Index Cond: (_hyper_1_4_chunk.device_id = 'dev2'::text)
-(10 rows)
+(9 rows)
 
 EXPLAIN (verbose ON, costs off) SELECT * FROM PUBLIC."two_Partitions" WHERE 'dev'||'2' = device_id;
                                                                                         QUERY PLAN                                                                                        
@@ -100,14 +98,28 @@ EXPLAIN (verbose ON, costs off) SELECT * FROM PUBLIC."two_Partitions" WHERE 'dev
  Append
    ->  Seq Scan on public."two_Partitions"
          Output: "two_Partitions"."timeCustom", "two_Partitions".device_id, "two_Partitions".series_0, "two_Partitions".series_1, "two_Partitions".series_2, "two_Partitions".series_bool
-         Filter: (('dev2'::text = "two_Partitions".device_id) AND (_timescaledb_internal.get_partition_hash("two_Partitions".device_id) = 405750566))
+         Filter: ('dev2'::text = "two_Partitions".device_id)
    ->  Bitmap Heap Scan on _timescaledb_internal._hyper_1_4_chunk
          Output: _hyper_1_4_chunk."timeCustom", _hyper_1_4_chunk.device_id, _hyper_1_4_chunk.series_0, _hyper_1_4_chunk.series_1, _hyper_1_4_chunk.series_2, _hyper_1_4_chunk.series_bool
          Recheck Cond: ('dev2'::text = _hyper_1_4_chunk.device_id)
-         Filter: (_timescaledb_internal.get_partition_hash(_hyper_1_4_chunk.device_id) = 405750566)
          ->  Bitmap Index Scan on "_hyper_1_4_chunk_two_Partitions_device_id_timeCustom_idx"
                Index Cond: ('dev2'::text = _hyper_1_4_chunk.device_id)
-(10 rows)
+(9 rows)
+
+CREATE VIEW "two_Partitions_view" AS ( SELECT * FROM PUBLIC."two_Partitions");
+EXPLAIN (verbose ON, costs off) SELECT * FROM PUBLIC."two_Partitions_view" WHERE device_id = 'dev2';
+                                                                                        QUERY PLAN                                                                                        
+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
+ Append
+   ->  Seq Scan on public."two_Partitions"
+         Output: "two_Partitions"."timeCustom", "two_Partitions".device_id, "two_Partitions".series_0, "two_Partitions".series_1, "two_Partitions".series_2, "two_Partitions".series_bool
+         Filter: ("two_Partitions".device_id = 'dev2'::text)
+   ->  Bitmap Heap Scan on _timescaledb_internal._hyper_1_4_chunk
+         Output: _hyper_1_4_chunk."timeCustom", _hyper_1_4_chunk.device_id, _hyper_1_4_chunk.series_0, _hyper_1_4_chunk.series_1, _hyper_1_4_chunk.series_2, _hyper_1_4_chunk.series_bool
+         Recheck Cond: (_hyper_1_4_chunk.device_id = 'dev2'::text)
+         ->  Bitmap Index Scan on "_hyper_1_4_chunk_two_Partitions_device_id_timeCustom_idx"
+               Index Cond: (_hyper_1_4_chunk.device_id = 'dev2'::text)
+(9 rows)
 
 --test integer partition key
 CREATE TABLE "int_part"(time timestamp, object_id int, temp float);
@@ -136,19 +148,18 @@ SELECT * FROM "int_part" WHERE object_id = 1;
 
 --make sure this touches only one partititon
 EXPLAIN (verbose ON, costs off) SELECT * FROM "int_part" WHERE object_id = 1;
-                                                        QUERY PLAN                                                         
----------------------------------------------------------------------------------------------------------------------------
+                                         QUERY PLAN                                         
+--------------------------------------------------------------------------------------------
  Append
    ->  Seq Scan on public.int_part
          Output: int_part."time", int_part.object_id, int_part.temp
-         Filter: ((int_part.object_id = 1) AND (_timescaledb_internal.get_partition_hash(int_part.object_id) = 242423622))
+         Filter: (int_part.object_id = 1)
    ->  Bitmap Heap Scan on _timescaledb_internal._hyper_2_5_chunk
          Output: _hyper_2_5_chunk."time", _hyper_2_5_chunk.object_id, _hyper_2_5_chunk.temp
          Recheck Cond: (_hyper_2_5_chunk.object_id = 1)
-         Filter: (_timescaledb_internal.get_partition_hash(_hyper_2_5_chunk.object_id) = 242423622)
          ->  Bitmap Index Scan on _hyper_2_5_chunk_int_part_object_id_time_idx
                Index Cond: (_hyper_2_5_chunk.object_id = 1)
-(10 rows)
+(9 rows)
 
 --TODO: handle this later?
 --EXPLAIN (verbose ON, costs off) SELECT * FROM "two_Partitions" WHERE device_id IN ('dev2', 'dev21');
diff --git a/test/sql/sql_query.sql b/test/sql/sql_query.sql
index e715dc18b5..fe0f8cfaae 100644
--- a/test/sql/sql_query.sql
+++ b/test/sql/sql_query.sql
@@ -11,6 +11,9 @@ EXPLAIN (verbose ON, costs off) SELECT * FROM PUBLIC."two_Partitions" WHERE devi
 EXPLAIN (verbose ON, costs off) SELECT * FROM PUBLIC."two_Partitions" WHERE device_id = 'dev'||'2';
 EXPLAIN (verbose ON, costs off) SELECT * FROM PUBLIC."two_Partitions" WHERE 'dev'||'2' = device_id;
 
+CREATE VIEW "two_Partitions_view" AS ( SELECT * FROM PUBLIC."two_Partitions");
+EXPLAIN (verbose ON, costs off) SELECT * FROM PUBLIC."two_Partitions_view" WHERE device_id = 'dev2';
+
 --test integer partition key
 CREATE TABLE "int_part"(time timestamp, object_id int, temp float);
 SELECT create_hypertable('"int_part"', 'time', 'object_id', 2);
