diff --git a/src/listconf.c b/src/listconf.c
index 7a1b2dca83..def4569b8e 100644
--- a/src/listconf.c
+++ b/src/listconf.c
@@ -20,6 +20,13 @@
 #define NEED_OS_FLOCK
 #include "os.h"
 
+#include <unistd.h>
+#if HAVE_SYS_TIME_H
+#include <sys/time.h>
+#endif
+#if HAVE_SYS_TIMES_H
+#include <sys/times.h>
+#endif
 #if !AC_BUILT
  #include <string.h>
  #ifndef _MSC_VER
@@ -47,18 +54,6 @@
 #define HAVE_LIBDL 1
 #endif
 
-#include "arch.h"
-#include "simd-intrinsics.h"
-#include "jumbo.h"
-#include "params.h"
-#include "path.h"
-#include "formats.h"
-#include "options.h"
-#include "unicode.h"
-#include "dynamic.h"
-#include "dynamic_types.h"
-#include "config.h"
-
 #if HAVE_LIBGMP
 #if HAVE_GMP_GMP_H
 #include <gmp/gmp.h>
@@ -71,7 +66,25 @@
 #include <gnu/libc-version.h>
 #endif
 
+#include "arch.h"
+#include "simd-intrinsics.h"
+#include "jumbo.h"
+#include "params.h"
+#include "path.h"
+#include "formats.h"
+#include "options.h"
+#include "unicode.h"
+#include "dynamic.h"
+#include "dynamic_types.h"
+#include "config.h"
+#include "bench.h"
+#include "timer.h"
+#include "misc.h"
 #include "regex.h"
+#include "opencl_common.h"
+#include "mask_ext.h"
+#include "version.h"
+#include "listconf.h" /* must be included after version.h */
 
 #ifdef NO_JOHN_BLD
 #define JOHN_BLD "unk-build-type"
@@ -79,11 +92,6 @@
 #include "john_build_rule.h"
 #endif
 
-#include "opencl_common.h"
-#include "mask_ext.h"
-#include "version.h"
-#include "listconf.h" /* must be included after version.h */
-
 #if CPU_DETECT
 extern char CPU_req_name[];
 #endif
@@ -134,6 +142,10 @@ static void listconf_list_build_info(void)
 #ifdef __GNU_MP_VERSION
 	int gmp_major, gmp_minor, gmp_patchlevel;
 #endif
+	sTimer test;
+
+	sTimer_Init(&test);
+
 	puts("Version: " JTR_GIT_VERSION);
 	puts("Build: " JOHN_BLD _MP_VERSION DEBUG_STRING ASAN_STRING UBSAN_STRING);
 #ifdef SIMD_COEF_32
@@ -306,6 +318,22 @@ static void listconf_list_build_info(void)
 #endif
 	printf("memmem(): " memmem_func "\n");
 
+	printf("clock(3) CLOCKS_PER_SEC: %u\n", (uint32_t)CLOCKS_PER_SEC);
+	clk_tck_init();
+#if defined(_SC_CLK_TCK) || !defined(CLK_TCK)
+	printf("times(2) sysconf(_SC_CLK_TCK) is %ld\n", clk_tck);
+#else
+	printf("times(2) CLK_TCK is %ld\n", clk_tck);
+#endif
+#if defined (__MINGW32__) || defined (_MSC_VER)
+	printf("Using clock(3) for timers, claimed resolution %ss, actual seen %ss\n",
+	       human_prefix_small(1.0 / CLOCKS_PER_SEC),
+	       human_prefix_small(sm_cPrecision));
+#else
+	printf("Using times(2) for timers, resolution %ss\n", human_prefix_small(1.0 / clk_tck));
+#endif
+	printf("HR timer claimed resolution %ss, actual seen %ss\n", human_prefix_small(1.0 / sm_HRTicksPerSec), human_prefix_small(sm_hrPrecision));
+
 // OK, now append debugging options, BUT only output  something if
 // one or more of them is set. IF none set, be silent.
 #if defined (DEBUG)
diff --git a/src/misc.c b/src/misc.c
index d39e575928..3632203fe5 100644
--- a/src/misc.c
+++ b/src/misc.c
@@ -579,6 +579,25 @@ char *human_prefix(uint64_t num)
 	return out;
 }
 
+char *human_prefix_small(double num)
+{
+	char *out = mem_alloc_tiny(16, MEM_ALIGN_NONE);
+	char prefixes[] = "\0munp";
+	char *p = prefixes;
+
+	while (p[1] && num > 0 && num < 1) {
+		num *= 1000;
+		p++;
+	}
+
+	if (*p)
+		snprintf(out, 16, "%u %c", (uint32_t)num, *p);
+	else
+		snprintf(out, 16, "%u ", (uint32_t)num);
+
+	return out;
+}
+
 unsigned int lcm(unsigned int x, unsigned int y)
 {
 	unsigned int tmp, a, b;
diff --git a/src/misc.h b/src/misc.h
index 2fc1c7ad09..9cd9090b56 100644
--- a/src/misc.h
+++ b/src/misc.h
@@ -195,6 +195,12 @@ const char *jtr_ulltoa(uint64_t num, char *result, int result_len, int base);
  */
 extern char *human_prefix(uint64_t num);
 
+/*
+ * Change some tiny number to a string possibly using SI prefix
+ * eg. 0.123 -> "123 m"
+ */
+extern char *human_prefix_small(double num);
+
 /*
  * Compute the least common multiple, lowest common multiple, or smallest
  * common multiple of two integers x and y, usually denoted by LCM(x, y),
diff --git a/src/timer.h b/src/timer.h
index 5842ba77e6..af6615b971 100644
--- a/src/timer.h
+++ b/src/timer.h
@@ -44,7 +44,7 @@ typedef LARGE_INTEGER hr_timer;
 #include <sys/time.h>
 typedef struct timeval hr_timer;
 #define HRZERO(X)             (X).tv_sec = (X).tv_usec = 0
-#define HRSETCURRENT(X)       { struct timezone tz; gettimeofday (&(X), &tz); }
+#define HRSETCURRENT(X)       { gettimeofday (&(X), NULL); }
 #define HRGETTICKS(X)         ((double)(X).tv_sec * 1000000.0 +	\
                                (double)(X).tv_usec)
 #define HRGETTICKS_PER_SEC(X) (X) = 1000000.0
