diff --git a/docs/mintty.1 b/docs/mintty.1
index 239d6d20..bc9f251e 100644
--- a/docs/mintty.1
+++ b/docs/mintty.1
@@ -771,6 +771,12 @@ Clear ("5") for glass.
 If OpaqueWhenFocused is set, opaqueness is temporarily disabled to 
 provide visible feedback for the changes.
 
+.TP
+\fBUser-defined shortcuts\fP
+Function keys, modified function keys, and Ctrl+Shift+key combinations 
+can be redefined to generate user-defined input or invoke functions.
+See option \fBKeyFunctions\fP for details.
+
 .SS Embedding graphics in terminal output
 
 The SIXEL graphics support feature facilitates a range of applications 
@@ -867,7 +873,8 @@ command-line.)
 .br
 Unicode-enabled settings: BellFile, ThemeFile, Background, Title, ExitTitle, Icon, Log, 
 Language, Font, Font1..., FontSample, Printer, Answerback, SixelClipChars, 
-Class, AppID, AppName, AppLaunchCmd, DropCommands, UserCommands, SessionCommands, TaskCommands.
+Class, AppID, AppName, AppLaunchCmd, 
+DropCommands, UserCommands, SessionCommands, TaskCommands, KeyFunctions.
 
 Be careful when running multiple instances of mintty. If options are saved 
 from different instances, or the config file is edited manually, 
@@ -1937,6 +1944,107 @@ Key_Break=2 would turn the Break key into an Insert key
 .br
 Key_Break=_BRK_ would assign the simulated terminal line Break function
 
+.TP
+\fBUser-defined shortcuts (KeyFunctions=)\fP
+With this setting, function keys, modified function keys, and 
+Ctrl+Shift+key combinations can be mapped to invoke specific functions 
+or generate user-defined input.
+The value is a series of semicolon-separated, colon-combined 
+pairs of key and action descriptors
+(if a semicolon shall be embedded into any of the action descriptors, 
+a non-whitespace control character (i.e. none of \fB^I^J^K^L^M\fP) can be 
+specified as an alternative separator by starting the whole setting with it).
+
+Supported keys are described as follows:
+.br
+\(en function keys F1...F24
+.br
+\(en modified function keys, preceded with a combination of C, A, S, in this order,
+indicating Ctrl, Alt, Shift as combined with the function key, 
+attached with a "+" separator, e.g. \fBCS+F9\fP
+.br
+\(en any character reachable on the keyboard without Shift; 
+AltGr-generated characters are supported
+
+\fINote:\fP The definition is ignored if the Ctrl+Shift+key combination would 
+generate a valid input character already. This is particularly the case 
+if a valid control character is mapped to the key in the keyboard layout.
+E.g. if you add a definition for Ctrl+Shift+minus and Shift+minus is "_" 
+(underline) on the keyboard, the user-defined key function will be ignored, 
+in order to prevent that input of Ctrl+_ would be inhibited.
+
+Supported actions are described as follows:
+.br
+\(en \fB"string"\fP or \fB'string'\fP: enters the literal string
+.br
+\(en \fB`command`\fP: enters the output of the command
+.br
+\(en \fBnew\fP: opens a new terminal window when key is released
+.br
+\(en \fBoptions\fP: opens the Options dialog
+.br
+\(en \fBmenu-text (*)\fP: opens the context menu at the cursor position
+.br
+\(en \fBmenu-pointer (*)\fP: opens the context menu at the mouse position
+.br
+\(en \fBsearch\fP: opens the search bar
+.br
+\(en \fBfullscreen (*)\fP: switches to fullscreen mode
+.br
+\(en \fBdefault-size (*)\fP: switches to default window size
+.br
+\(en \fBscrollbar-outer\fP: toggles the scrollbar (xterm compatible)
+.br
+\(en \fBscrollbar-inner\fP: toggles the scrollbar (mintty legacy)
+.br
+\(en \fBcycle-pointer-style\fP: cycles the pointer style
+.br
+\(en \fBcycle-transparency-level\fP: cycles the transparency level
+.br
+\(en \fBcopy\fP: copies the selection to the clipboard
+.br
+\(en \fBpaste\fP: pastes from the clipboard
+.br
+\(en \fBcopy-paste\fP: copies and pastes
+.br
+\(en \fBselect-all\fP: selects all text including scrollback
+.br
+\(en \fBclear-scrollback\fP: clears the scrollback
+.br
+\(en \fBcopy-title\fP: copies the window title to the clipboard
+.br
+\(en \fBreset\fP: resets the terminal
+.br
+\(en \fBbreak\fP: sends a break condition to the application
+.br
+\(en \fBflipscreen\fP: switches the alternate screen
+.br
+\(en \fBopen\fP: opens the selection, e.g. a URL or filename
+.br
+\(en \fBtoggle-logging\fP: toggles logfile logging
+.br
+\(en \fBtoggle-char-info\fP: toggles character info display
+.br
+\(en \fBexport-html\fP: exports the screen as an HTML file
+
+\fINote (*):\fP The exact behaviour of some actions depends on the 
+Shift state as being mapped from.
+
+\fINote:\fP To just disable the built-in shortcut (and not override it 
+with a user-defined one), leave the action empty, but include the colon.
+
+Examples:
+.br
+\(en \fBKeyFunctions=d:`echo -n `date``;A+F4:;CA+F12:break;-:"minus"\fP
+will input the current date on Ctrl+Shift+d, disable window closing with Alt+F4,
+send a BRK on Ctrl+Alt+F12, but likely not input "minus" on Ctrl+Shift+minus 
+because Shift+minus is an underline on many keyboard layouts which has a 
+Ctrl+_ assignment already
+.br
+\(en \fBKeyFunctions=c:copy;v:paste\fP in combination with setting 
+\fB-o CtrlExchangeShift=true\fP will assign copy/paste to ^C and ^V 
+for Windows addicts
+
 .TP
 \fBUse system colours\fP (UseSystemColours=no)
 If this is set, the Windows-wide colour settings are used
diff --git a/src/config.c b/src/config.c
index 2995a953..144f70fe 100644
--- a/src/config.c
+++ b/src/config.c
@@ -97,6 +97,7 @@ const config default_cfg = {
   .key_break = "",	// VK_CANCEL
   .key_menu = "",	// VK_APPS
   .key_scrlock = "",	// VK_SCROLL
+  .key_commands = W(""),
   // Mouse
   .copy_on_select = true,
   .copy_as_rtf = true,
@@ -309,6 +310,7 @@ options[] = {
   {"Key_ScrollLock", OPT_STRING, offcfg(key_scrlock)},
   {"Break", OPT_STRING | OPT_LEGACY, offcfg(key_break)},
   {"Pause", OPT_STRING | OPT_LEGACY, offcfg(key_pause)},
+  {"KeyFunctions", OPT_WSTRING, offcfg(key_commands)},
 
   // Mouse
   {"CopyOnSelect", OPT_BOOL, offcfg(copy_on_select)},
diff --git a/src/config.h b/src/config.h
index d5d88474..802a4162 100644
--- a/src/config.h
+++ b/src/config.h
@@ -95,6 +95,7 @@ typedef struct {
   string key_break;	// VK_CANCEL
   string key_menu;	// VK_APPS
   string key_scrlock;	// VK_SCROLL
+  wstring key_commands;
   // Mouse
   bool copy_on_select;
   bool copy_as_rtf;
diff --git a/src/wininput.c b/src/wininput.c
index cfe430f4..b08d126c 100644
--- a/src/wininput.c
+++ b/src/wininput.c
@@ -15,6 +15,7 @@
 
 static HMENU ctxmenu = NULL;
 static HMENU sysmenu;
+static uint newwin_key = 0;
 static bool newwin_pending = false;
 static bool newwin_shifted = false;
 static bool newwin_home = false;
@@ -1033,6 +1034,107 @@ vk_name(uint key)
 #define trace_key(tag)	
 #endif
 
+static void
+menu_text()
+{
+  open_popup_menu(true, null, get_mods());
+}
+
+static void
+menu_pointer()
+{
+  //win_popup_menu(get_mods());
+  open_popup_menu(false, null, get_mods());
+}
+
+static void
+transparency_level()
+{
+  if (!transparency_pending) {
+    previous_transparency = cfg.transparency;
+    transparency_pending = 1;
+    transparency_tuned = false;
+  }
+  if (cfg.opaque_when_focused)
+    win_update_transparency(false);
+}
+
+static void
+newwin_begin()
+{
+  newwin_pending = true;
+  newwin_home = false; newwin_monix = 0; newwin_moniy = 0;
+}
+
+/*
+   Simplified variant of term_cmd().
+ */
+static void
+key_cmd(char * cmd)
+{
+  FILE * cmdf = popen(cmd, "r");
+  if (cmdf) {
+    char line[222];
+    while (fgets(line, sizeof line, cmdf)) {
+      child_send(line, strlen(line));
+    }
+    pclose(cmdf);
+  }
+}
+
+static struct {
+  string name;
+  union {
+    WPARAM cmd;
+    void (*fct)(void);
+  };
+} cmd_defs[] = {
+#ifdef support_sc_defs
+#warning these do not work, they crash
+  {"restore", {SC_RESTORE}},
+  {"move", {SC_MOVE}},
+  {"resize", {SC_SIZE}},
+  {"minimize", {SC_MINIMIZE}},
+  {"maximize", {SC_MAXIMIZE}},
+  {"menu", {SC_KEYMENU}},
+  {"close", {SC_CLOSE}},
+#endif
+#ifdef support_other
+#warning these do not work properly
+  {"new-window", {IDM_NEW}},
+  {"new-monitor", {IDM_NEW_MONI}},
+  {"fullscreen-zoom", {IDM_FULLSCREEN_ZOOM}},
+  {"default-size-zoom", {IDM_DEFSIZE_ZOOM}},
+#endif
+
+  {"new", {.fct = newwin_begin}},
+  {"options", {IDM_OPTIONS}},
+  {"menu-text", {.fct = menu_text}},
+  {"menu-pointer", {.fct = menu_pointer}},
+
+  {"search", {IDM_SEARCH}},
+  {"fullscreen", {IDM_FULLSCREEN}},
+  {"default-size", {IDM_DEFSIZE}},
+  {"scrollbar-outer", {IDM_SCROLLBAR}},
+  {"scrollbar-inner", {.fct = toggle_scrollbar}},
+  {"cycle-pointer-style", {.fct = cycle_pointer_style}},
+  {"cycle-transparency-level", {.fct = transparency_level}},
+
+  {"copy", {IDM_COPY}},
+  {"paste", {IDM_PASTE}},
+  {"copy-paste", {IDM_COPASTE}},
+  {"select-all", {IDM_SELALL}},
+  {"clear-scrollback", {IDM_CLRSCRLBCK}},
+  {"copy-title", {IDM_COPYTITLE}},
+  {"reset", {IDM_RESET}},
+  {"break", {IDM_BREAK}},
+  {"flipscreen", {IDM_FLIPSCREEN}},
+  {"open", {IDM_OPEN}},
+  {"toggle-logging", {IDM_TOGLOG}},
+  {"toggle-char-info", {IDM_TOGCHARINFO}},
+  {"export-html", {IDM_HTML}},
+};
+
 bool
 win_key_down(WPARAM wp, LPARAM lp)
 {
@@ -1218,6 +1320,132 @@ win_key_down(WPARAM wp, LPARAM lp)
     }
 #endif
 
+#define dont_debug_def_keys 1
+
+    // user-defined shortcuts
+    //wstring key_commands = W("-:'blabla';A+F3:;A+F5:flipscreen;A+F9:\"f9\";C+F10:\"f10\";p:paste;d:`date`;o:\"oo\";ö:\"öö\";€:\"euro\";~:'tilde';]:']]';[:'[[';}:'}}';{:'{{';\\:'\\\\';µ:'µµ';²:'²²'");
+    if (*cfg.key_commands) {
+      bool pick_key_function(char * tag)
+      {
+        char * ukey_commands = cs__wcstoutf(cfg.key_commands);
+        char * cmdp = ukey_commands;
+        char sepch = ';';
+        if ((uchar)*cmdp <= (uchar)' ')
+          sepch = *cmdp++;
+
+        char * paramp;
+        while ((paramp = strchr(cmdp, ':'))) {
+          *paramp = '\0';
+          paramp++;
+          char * sepp = strchr(paramp, sepch);
+          if (sepp)
+            *sepp = '\0';
+
+#if defined(debug_def_keys) && debug_def_keys > 1
+          printf("tag <%s>: cmd <%s> fct <%s>\n", tag, cmdp, paramp);
+#endif
+          if (!strcmp(cmdp, tag)) {
+#if defined(debug_def_keys) && debug_def_keys == 1
+            printf("tag <%s>: cmd <%s> fct <%s>\n", tag, cmdp, paramp);
+#endif
+            wchar * fct = cs__utftowcs(paramp);
+            if ((*fct == '"' && fct[wcslen(fct) - 1] == '"') ||
+                (*fct == '\'' && fct[wcslen(fct) - 1] == '\'')) {
+              child_sendw(&fct[1], wcslen(fct) - 2);
+            }
+            else if (*fct == '`' && fct[wcslen(fct) - 1] == '`') {
+              fct[wcslen(fct) - 1] = 0;
+              char * cmd = cs__wcstombs(&fct[1]);
+              key_cmd(cmd);
+              free(cmd);
+            }
+            else {
+              for (uint i = 0; i < lengthof(cmd_defs); i++)
+                if (!strcmp(paramp, cmd_defs[i].name)) {
+                  if (cmd_defs[i].cmd < 0xF000)
+                    send_syscommand(cmd_defs[i].cmd);
+                  else if (cmd_defs[i].fct == newwin_begin) {
+                    newwin_begin();
+                    newwin_key = key;
+                    if (mods & MDK_SHIFT)
+                      newwin_shifted = true;
+                    else
+                      newwin_shifted = false;
+                  }
+                  else
+                    cmd_defs[i].fct();
+                  break;
+                }
+            }
+
+            free(fct);
+            free(ukey_commands);
+            return true;
+          }
+
+          if (sepp)
+            cmdp = sepp + 1;
+          else
+            break;
+        }
+        free(ukey_commands);
+        return false;
+      }
+
+      char * tag = 0;
+      if (VK_F1 <= key && key <= VK_F24) {
+        tag = asform("%s%s%s%sF%d",
+                     ctrl ? "C" : "",
+                     alt ? "A" : "",
+                     shift ? "S" : "",
+                     mods ? "+" : "",
+                     key - VK_F1 + 1);
+      }
+      else if ((mods & ~MDK_ALT) == (cfg.ctrl_exchange_shift ? MDK_CTRL : (MDK_CTRL | MDK_SHIFT))) {
+        uchar kbd0[256];
+        GetKeyboardState(kbd0);
+        wchar wbuf[4];
+        int wlen = ToUnicode(key, scancode, kbd0, wbuf, lengthof(wbuf), 0);
+        wchar w1 = wlen > 0 ? *wbuf : 0;
+        kbd0[VK_SHIFT] = 0;
+        wlen = ToUnicode(key, scancode, kbd0, wbuf, lengthof(wbuf), 0);
+        wchar w2 = wlen > 0 ? *wbuf : 0;
+#ifdef debug_def_keys
+        printf("VK_*CONTROL %d %d/%d *ctrl %d %d/%d -> %04X; -SHIFT %04X\n",
+               is_key_down(VK_CONTROL), is_key_down(VK_LCONTROL), is_key_down(VK_RCONTROL),
+               ctrl, lctrl, rctrl,
+               w1, w2);
+#endif
+        if (!w1 || w1 == w2) {
+          kbd0[VK_SHIFT] = 0;
+          kbd0[VK_LCONTROL] = 0;
+          if (!altgr)
+            kbd0[VK_CONTROL] = 0;
+          wlen = ToUnicode(key, scancode, kbd0, wbuf, lengthof(wbuf), 0);
+#ifdef debug_def_keys
+          printf("            %d %d/%d *ctrl %d %d/%d -> %d %04X\n",
+                 is_key_down(VK_CONTROL), is_key_down(VK_LCONTROL), is_key_down(VK_RCONTROL),
+                 ctrl, lctrl, rctrl,
+                 wlen, *wbuf);
+#endif
+          if (wlen == 1 || wlen == 2) {
+            wbuf[wlen] = 0;
+            tag = cs__wcstoutf(wbuf);
+          }
+        }
+#ifdef debug_def_keys
+        printf("ctrl+shift+key %04X <%s>\n", *wbuf, tag);
+#endif
+      }
+      if (tag) {
+        if (pick_key_function(tag))
+          return true;
+      }
+#ifdef debug_def_keys
+      printf("check key <%s>\n", tag);
+#endif
+    }
+
     // Alt+Fn shortcuts
     if (cfg.alt_fn_shortcuts && alt && VK_F1 <= key && key <= VK_F24) {
       if (!ctrl) {
@@ -1225,6 +1453,7 @@ win_key_down(WPARAM wp, LPARAM lp)
           when VK_F2:
             // defer send_syscommand(IDM_NEW) until key released
             // monitor cursor keys to collect parameters meanwhile
+            newwin_key = key;
             newwin_pending = true;
             newwin_home = false; newwin_monix = 0; newwin_moniy = 0;
             if (mods & MDK_SHIFT)
@@ -1910,7 +2139,7 @@ win_key_up(WPARAM wp, LPARAM unused(lp))
   }
 
   if (newwin_pending) {
-    if ((uint)wp == VK_F2) {
+    if (key == newwin_key) {
       inline bool is_key_down(uchar vk) { return GetKeyState(vk) & 0x80; }
       if (is_key_down(VK_SHIFT))
         newwin_shifted = true;
@@ -1966,7 +2195,7 @@ win_key_up(WPARAM wp, LPARAM unused(lp))
     win_update(false);
   }
 
-  if (wp != VK_MENU)
+  if (key != VK_MENU)
     return false;
 
   if (alt_state > ALT_ALONE && alt_code) {
diff --git a/wiki/Keycodes.md b/wiki/Keycodes.md
index b55cd2e1..3fc633de 100644
--- a/wiki/Keycodes.md
+++ b/wiki/Keycodes.md
@@ -27,6 +27,10 @@ with a few minor changes and some additions.
 Below, [Caret notation](http://en.wikipedia.org/wiki/Caret_notation) 
 is used to show control characters.
 
+A number of options are available to customize the keyboard behaviour, 
+including user-defined function keys and Ctrl+Shift+key shortcuts.
+See the manual page for options and details.
+
 
 ## Alt and Meta ##
 
diff --git a/wiki/Tips.md b/wiki/Tips.md
index 78477134..33a804cc 100644
--- a/wiki/Tips.md
+++ b/wiki/Tips.md
@@ -364,6 +364,13 @@ bind-key -n User1 previous-window
 ```
 
 
+## Keyboard customization ##
+
+A number of options are available to customize the keyboard behaviour, 
+including user-defined function keys and Ctrl+Shift+key shortcuts.
+See the manual page for options and details.
+
+
 ## Compose key ##
 
 Mintty uses the Windows keyboard layout system with its “dead key” mechanism 
