diff --git a/NEWS.md b/NEWS.md
index fe20b7b1c..30cd4b84c 100644
--- a/NEWS.md
+++ b/NEWS.md
@@ -112,7 +112,7 @@
     #  0.357   0.763   0.292  # now
     ```
 
-18. New function `coalesce(...)` has been written in C, and is multithreaded for numeric and factor types. It replaces missing values according to a prioritized list of candidates (as per SQL COALESCE, `dplyr::coalesce`, and `hutils::coalesce`), [#3424](https://github.com/Rdatatable/data.table/issues/3424). It accepts any number of vectors in several forms. For example, given three vectors `x`, `y`, and `z`, where each `NA` in `x` is to be replaced by the corresponding value in `y` if that is non-NA, else the corresponding value in `z`, the following equivalent forms are all accepted: `coalesce(x,y,z)`, `coalesce(x,list(y,z))`, and `coalesce(list(x,y,z))`.
+18. New function `coalesce(...)` has been written in C, and is multithreaded for numeric, complex, and factor types. It replaces missing values according to a prioritized list of candidates (as per SQL COALESCE, `dplyr::coalesce`, and `hutils::coalesce`), [#3424](https://github.com/Rdatatable/data.table/issues/3424). It accepts any number of vectors in several forms. For example, given three vectors `x`, `y`, and `z`, where each `NA` in `x` is to be replaced by the corresponding value in `y` if that is non-NA, else the corresponding value in `z`, the following equivalent forms are all accepted: `coalesce(x,y,z)`, `coalesce(x,list(y,z))`, and `coalesce(list(x,y,z))`.
 
     ```R
     # default 4 threads on a laptop with 16GB RAM and 8 logical CPU
@@ -128,8 +128,8 @@
     identical(y1,y2) && identical(y1,y3)
     # TRUE
     ```
-    
-19. Extended support of `shift` to complex vectors, part of [#3690](https://github.com/Rdatatable/data.table/issues/3690).
+
+19. `shift` now supports complex vectors, part of [#3690](https://github.com/Rdatatable/data.table/issues/3690).
 
 #### BUG FIXES
 
diff --git a/inst/tests/tests.Rraw b/inst/tests/tests.Rraw
index 469c0a59d..4065299c7 100644
--- a/inst/tests/tests.Rraw
+++ b/inst/tests/tests.Rraw
@@ -15228,6 +15228,18 @@ setcoalesce(xx, list())
 test(2060.503, xx_addr, address(xx))
 test(2060.504, xx, x)
 test(2060.505, address(setcoalesce(xx)), xx_addr)
+# complex support for coalesce
+z1 = c(1i, NA, 1-1i, NA, 0+3i, NA)
+z2 = c(NA, 4-2i, 0+0i, NA, NA, NA)
+z3 = c(2, NA, 3+6i, 5-1i, NA, NA)
+na_idx = c(2L, 4L, 6L)
+test(2060.600, coalesce(z1, 0+0i), `[<-`(z1, na_idx, 0+0i))
+test(2060.601, coalesce(z1, z2), `[<-`(z1, na_idx, c(4-2i, NA, NA)))
+test(2060.602, coalesce(z1, z2, z3), `[<-`(z1, na_idx, c(4-2i, 5-1i, NA)))
+z_addr = address(z1)
+setcoalesce(z1, z2, z3)
+test(2060.603, address(z1), z_addr)
+test(2060.604, z1, `[<-`(z1, na_idx, c(4-2i, 5-1i, NA)))
 
 # #3650 -- ensure max nrow check on CJ is applied after unique
 l = replicate(ceiling(log10(.Machine$integer.max)), rep(1L, 10L), simplify = FALSE)
diff --git a/src/coalesce.c b/src/coalesce.c
index e57d63eb4..81e815634 100644
--- a/src/coalesce.c
+++ b/src/coalesce.c
@@ -118,6 +118,28 @@ SEXP coalesce(SEXP x, SEXP inplaceArg) {
       }
     }
   } break;
+  case CPLXSXP: {
+    Rcomplex *xP = COMPLEX(first), finalVal=NA_CPLX;
+    int k=0;
+    for (int j=0; j<nval; ++j) {
+      SEXP item = VECTOR_ELT(x, j+off);
+      if (length(item)==1) {
+        Rcomplex tt = COMPLEX(item)[0];
+        if (ISNAN(tt.r) && ISNAN(tt.i)) continue;
+        finalVal = tt;
+        break;
+      }
+      valP[k++] = COMPLEX(item);
+    }
+    const bool final = !ISNAN(finalVal.r) && !ISNAN(finalVal.i);
+    #pragma omp parallel for num_threads(getDTthreads())
+    for (int i=0; i<nrow; ++i) {
+      Rcomplex val=xP[i];
+      if (!ISNAN(val.r) && !ISNAN(val.i)) continue;
+      int j=0; while (ISNAN(val.r) && ISNAN(val.i) && j<k) val=((Rcomplex *)valP[j++])[i];
+      if (!ISNAN(val.r) || !ISNAN(val.i)) xP[i]=val; else if (final) xP[i]=finalVal;
+    }
+  } break;
   case STRSXP: {
     const SEXP *xP = STRING_PTR(first);
     SEXP finalVal=NA_STRING;
