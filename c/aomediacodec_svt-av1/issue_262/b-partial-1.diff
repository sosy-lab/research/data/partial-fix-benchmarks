diff --git a/Source/Lib/Common/ASM_AVX2/EbTransforms_Intrinsic_AVX2.c b/Source/Lib/Common/ASM_AVX2/EbTransforms_Intrinsic_AVX2.c
index 29a3dfbf64..25bd351dd3 100644
--- a/Source/Lib/Common/ASM_AVX2/EbTransforms_Intrinsic_AVX2.c
+++ b/Source/Lib/Common/ASM_AVX2/EbTransforms_Intrinsic_AVX2.c
@@ -1739,3 +1739,161 @@ void mat_mult_nxn_avx2_intrin(
     z = _mm256_sad_epu8(z, _mm256_srli_si256(z, 7));
     *nonzerocoeff = _mm_cvtsi128_si32(_mm_add_epi32(_mm256_extracti128_si256(z, 0), _mm256_extracti128_si256(z, 1)));
 }
+
+static INLINE void EnergyComputation_kernel_avx2(const int32_t *const in,
+    __m256i *const sum256)
+{
+    const __m256i zero = _mm256_setzero_si256();
+    const __m256i input = _mm256_loadu_si256((__m256i *)in);
+    const __m256i in_lo = _mm256_unpacklo_epi32(input, zero);
+    const __m256i in_hi = _mm256_unpackhi_epi32(input, zero);
+    const __m256i energy_lo = _mm256_mul_epi32(in_lo, in_lo);
+    const __m256i energy_hi = _mm256_mul_epi32(in_hi, in_hi);
+    *sum256 = _mm256_add_epi64(*sum256, energy_lo);
+    *sum256 = _mm256_add_epi64(*sum256, energy_hi);
+}
+
+static INLINE uint64_t hadd64_avx2(const __m256i sum256) {
+    const __m128i sum256_lo = _mm256_castsi256_si128(sum256);
+    const __m128i sum256_hi = _mm256_extracti128_si256(sum256, 1);
+    const __m128i sum128 = _mm_add_epi64(sum256_lo, sum256_hi);
+    const __m128i sum128_hi = _mm_srli_si128(sum128, 8);
+    const __m128i sum = _mm_add_epi64(sum128, sum128_hi);
+
+    return _mm_extract_epi64(sum, 0);
+}
+
+static INLINE uint64_t EnergyComputation_avx2(const int32_t *const in,
+    const uint32_t size)
+{
+    const __m256i zero = _mm256_setzero_si256();
+    uint32_t i = 0;
+    __m256i sum = zero;
+
+    do {
+        EnergyComputation_kernel_avx2(in + i, &sum);
+        i += 8;
+    } while (i < size);
+
+    return hadd64_avx2(sum);
+}
+
+static INLINE uint64_t EnergyComputation64_avx2(const int32_t *in,
+    const uint32_t height)
+{
+    const __m256i zero = _mm256_setzero_si256();
+    uint32_t i = height;
+    __m256i sum = zero;
+
+    do {
+        EnergyComputation_kernel_avx2(in + 0 * 8, &sum);
+        EnergyComputation_kernel_avx2(in + 1 * 8, &sum);
+        EnergyComputation_kernel_avx2(in + 2 * 8, &sum);
+        EnergyComputation_kernel_avx2(in + 3 * 8, &sum);
+        in += 64;
+    } while (--i);
+
+    return hadd64_avx2(sum);
+}
+
+static INLINE void clean_256_bytes_avx2(int32_t *buf, const uint32_t height) {
+    const __m256i zero = _mm256_setzero_si256();
+    uint32_t h = height;
+
+    do {
+        _mm256_storeu_si256((__m256i *)(buf + 0 * 8), zero);
+        _mm256_storeu_si256((__m256i *)(buf + 1 * 8), zero);
+        _mm256_storeu_si256((__m256i *)(buf + 2 * 8), zero);
+        _mm256_storeu_si256((__m256i *)(buf + 3 * 8), zero);
+        buf += 64;
+    } while (--h);
+}
+
+static INLINE void copy_32_bytes_avx2(const int32_t *src, int32_t *dst) {
+    const __m256i val = _mm256_loadu_si256((__m256i *)(src + 0 * 8));
+    _mm256_storeu_si256((__m256i *)(dst + 0 * 8), val);
+}
+
+static INLINE void copy_256x_bytes_avx2(const int32_t *src, int32_t *dst,
+    const uint32_t height) {
+    uint32_t h = height;
+
+    do {
+        copy_32_bytes_avx2(src + 0 * 8, dst + 0 * 8);
+        copy_32_bytes_avx2(src + 1 * 8, dst + 1 * 8);
+        copy_32_bytes_avx2(src + 2 * 8, dst + 2 * 8);
+        copy_32_bytes_avx2(src + 3 * 8, dst + 3 * 8);
+        src += 64;
+        dst += 32;
+    } while (--h);
+}
+
+uint64_t HandleTransform16x64_avx2(int32_t *output) {
+    //bottom 16x32 area.
+    const uint64_t three_quad_energy =
+        EnergyComputation_avx2(output + 16 * 32, 16 * 32);
+
+    // Zero out the bottom 16x32 area.
+    memset(output + 16 * 32, 0, 16 * 32 * sizeof(*output));
+
+    return three_quad_energy;
+}
+
+uint64_t HandleTransform32x64_avx2(int32_t *output) {
+    //bottom 32x32 area.
+    const uint64_t three_quad_energy =
+        EnergyComputation_avx2(output + 32 * 32, 32 * 32);
+
+    // Zero out the bottom 32x32 area.
+    memset(output + 32 * 32, 0, 32 * 32 * sizeof(*output));
+
+    return three_quad_energy;
+}
+
+uint64_t HandleTransform64x16_avx2(int32_t *output) {
+    // top - right 32x16 area.
+    const uint64_t three_quad_energy =
+        EnergyComputation64_avx2(output + 32, 16);
+
+    // Zero out right 32x16 area.
+    clean_256_bytes_avx2(output + 32, 16);
+
+    // Re-pack non-zero coeffs in the first 32x16 indices.
+    copy_256x_bytes_avx2(output + 64, output + 32, 15);
+
+    return three_quad_energy;
+}
+
+uint64_t HandleTransform64x32_avx2(int32_t *output) {
+    // top - right 32x32 area.
+    const uint64_t three_quad_energy =
+        EnergyComputation64_avx2(output + 32, 32);
+
+    // Zero out right 32x32 area.
+    clean_256_bytes_avx2(output + 32, 32);
+
+    // Re-pack non-zero coeffs in the first 32x32 indices.
+    copy_256x_bytes_avx2(output + 64, output + 32, 31);
+
+    return three_quad_energy;
+}
+
+uint64_t HandleTransform64x64_avx2(int32_t *output) {
+    uint64_t three_quad_energy;
+
+    // top - right 32x32 area.
+    three_quad_energy = EnergyComputation64_avx2(output + 32, 32);
+    //bottom 64x32 area.
+    three_quad_energy += EnergyComputation_avx2(output + 32 * 64, 64 * 32);
+
+    // Zero out top-right 32x32 area.
+    clean_256_bytes_avx2(output + 32, 32);
+
+    // Zero out the bottom 64x32 area.
+    memset(output + 32 * 64, 0, 32 * 64 * sizeof(*output));
+
+    // Re-pack non-zero coeffs in the first 32x32 indices.
+    copy_256x_bytes_avx2(output + 64, output + 32, 31);
+
+    return three_quad_energy;
+}
diff --git a/Source/Lib/Common/Codec/EbTransforms.c b/Source/Lib/Common/Codec/EbTransforms.c
index a7b872c2d4..e4408182c4 100644
--- a/Source/Lib/Common/Codec/EbTransforms.c
+++ b/Source/Lib/Common/Codec/EbTransforms.c
@@ -4319,7 +4319,7 @@ void Av1TransformConfig(
     set_fwd_txfm_non_scale_range(cfg);
 }
 
-uint64_t EnergyComputation(
+static uint64_t EnergyComputation(
     int32_t  *coeff,
     uint32_t   coeff_stride,
     uint32_t   area_width,
@@ -4343,32 +4343,24 @@ uint64_t EnergyComputation(
     return predictionDistortion;
 }
 
-uint64_t  HandleTransform64x64_c(
-    int32_t         *output,
-    uint32_t         outputStride)
-{
-    uint64_t three_quad_energy = 0;
+uint64_t HandleTransform64x64_c(int32_t *output) {
+    uint64_t three_quad_energy;
 
     // top - right 32x32 area.
-    three_quad_energy = EnergyComputation(
-        output + 32,
-        outputStride,
-        32,
-        32);
+    three_quad_energy = EnergyComputation(output + 32, 64, 32, 32);
     //bottom 64x32 area.
-    three_quad_energy += EnergyComputation(
-        output + (32 * outputStride),
-        outputStride,
-        64,
-        32);
+    three_quad_energy += EnergyComputation(output + 32 * 64, 64, 64, 32);
 
-    uint32_t row;
     // Zero out top-right 32x32 area.
-    for (row = 0; row < 32; ++row)
+    for (int32_t row = 0; row < 32; ++row)
         memset(output + row * 64 + 32, 0, 32 * sizeof(*output));
+
     // Zero out the bottom 64x32 area.
     memset(output + 32 * 64, 0, 32 * 64 * sizeof(*output));
-    //// Re-pack non-zero coeffs in the first 32x32 indices.
+
+    // Re-pack non-zero coeffs in the first 32x32 indices.
+    for (int32_t row = 1; row < 32; ++row)
+        memcpy(output + row * 32, output + row * 64, 32 * sizeof(*output));
 
     return three_quad_energy;
 }
@@ -4542,22 +4534,19 @@ void av1_fwd_txfm2d_64x32_c(
         bit_depth);
 }
 
-uint64_t  HandleTransform64x32_c(
-    int32_t         *output,
-    uint32_t         outputStride)
-{
-    uint64_t three_quad_energy = 0;
-
+uint64_t HandleTransform64x32_c(int32_t *output) {
     // top - right 32x32 area.
-    three_quad_energy = EnergyComputation(
-        output + 32,
-        outputStride,
-        32,
-        32);
+    const uint64_t three_quad_energy =
+        EnergyComputation(output + 32, 64, 32, 32);
 
     // Zero out right 32x32 area.
     for (int32_t row = 0; row < 32; ++row)
         memset(output + row * 64 + 32, 0, 32 * sizeof(*output));
+
+    // Re-pack non-zero coeffs in the first 32x32 indices.
+    for (int32_t row = 1; row < 32; ++row)
+        memcpy(output + row * 32, output + row * 64, 32 * sizeof(*output));
+
     return three_quad_energy;
 }
 
@@ -4581,18 +4570,11 @@ void av1_fwd_txfm2d_32x64_c(
         intermediateTransformBuffer,
         bit_depth);
 }
-uint64_t  HandleTransform32x64_c(
-    int32_t         *output,
-    uint32_t         outputStride)
-{
-    uint64_t three_quad_energy = 0;
 
+uint64_t HandleTransform32x64_c(int32_t *output) {
     //bottom 32x32 area.
-    three_quad_energy += EnergyComputation(
-        output + (32 * outputStride),
-        outputStride,
-        32,
-        32);
+    const uint64_t three_quad_energy =
+        EnergyComputation(output + 32 * 32, 32, 32, 32);
 
     // Zero out the bottom 32x32 area.
     memset(output + 32 * 32, 0, 32 * 32 * sizeof(*output));
@@ -4619,22 +4601,20 @@ void av1_fwd_txfm2d_64x16_c(
         intermediateTransformBuffer,
         bit_depth);
 }
-uint64_t  HandleTransform64x16_c(
-    int32_t         *output,
-    uint32_t         outputStride)
-{
-    uint64_t three_quad_energy = 0;
 
+uint64_t HandleTransform64x16_c(int32_t *output) {
     // top - right 32x16 area.
-    three_quad_energy = EnergyComputation(
-        output + 32,
-        outputStride,
-        32,
-        16);
+    const uint64_t three_quad_energy =
+        EnergyComputation(output + 32, 64, 32, 16);
 
     // Zero out right 32x16 area.
     for (int32_t row = 0; row < 16; ++row)
         memset(output + row * 64 + 32, 0, 32 * sizeof(*output));
+
+    // Re-pack non-zero coeffs in the first 32x16 indices.
+    for (int32_t row = 1; row < 16; ++row)
+        memcpy(output + row * 32, output + row * 64, 32 * sizeof(*output));
+
     return three_quad_energy;
 }
 
@@ -4659,18 +4639,10 @@ void av1_fwd_txfm2d_16x64_c(
         bit_depth);
 }
 
-uint64_t  HandleTransform16x64_c(
-    int32_t         *output,
-    uint32_t         outputStride)
-{
-    uint64_t three_quad_energy = 0;
-
+uint64_t HandleTransform16x64_c(int32_t *output) {
     //bottom 16x32 area.
-    three_quad_energy += EnergyComputation(
-        output + (32 * outputStride),
-        outputStride,
-        16,
-        32);
+    const uint64_t three_quad_energy =
+        EnergyComputation(output + 16 * 32, 16, 16, 32);
 
     // Zero out the bottom 16x32 area.
     memset(output + 16 * 32, 0, 16 * 32 * sizeof(*output));
@@ -4904,12 +4876,8 @@ EbErrorType av1_estimate_transform(
                 transform_type,
                 bit_depth);
 
-        *three_quad_energy = HandleTransform64x32_c(coeff_buffer,
-            64);
+        *three_quad_energy = HandleTransform64x32(coeff_buffer);
 
-        // Re-pack non-zero coeffs in the first 32x32 indices.
-        for (int32_t row = 1; row < 32; ++row)
-            memcpy(coeff_buffer + row * 32, coeff_buffer + row * 64, 32 * sizeof(int32_t));
         break;
 
     case TX_32X64:
@@ -4928,8 +4896,7 @@ EbErrorType av1_estimate_transform(
                 transform_type,
                 bit_depth);
 
-        *three_quad_energy = HandleTransform32x64_c(coeff_buffer,
-            32);
+        *three_quad_energy = HandleTransform32x64(coeff_buffer);
 
         break;
 
@@ -4949,12 +4916,10 @@ EbErrorType av1_estimate_transform(
                 transform_type,
                 bit_depth);
 
-        *three_quad_energy = HandleTransform64x16_c(coeff_buffer,
-            64);
-        // Re-pack non-zero coeffs in the first 32x16 indices.
-        for (int32_t row = 1; row < 16; ++row)
-            memcpy(coeff_buffer + row * 32, coeff_buffer + row * 64, 32 * sizeof(int32_t));
+        *three_quad_energy = HandleTransform64x16(coeff_buffer);
+
         break;
+
     case TX_16X64:
         if (transform_type == DCT_DCT)
             av1_fwd_txfm2d_16x64(
@@ -4970,8 +4935,8 @@ EbErrorType av1_estimate_transform(
                 residual_stride,
                 transform_type,
                 bit_depth);
-        *three_quad_energy = HandleTransform16x64_c(coeff_buffer,
-            16);
+
+        *three_quad_energy = HandleTransform16x64(coeff_buffer);
 
         break;
 
@@ -5107,13 +5072,8 @@ EbErrorType av1_estimate_transform(
             transform_type,
             bit_depth);
 
-        *three_quad_energy = HandleTransform64x64_c(coeff_buffer,
-            64);
+        *three_quad_energy = HandleTransform64x64(coeff_buffer);
 
-        uint32_t row;
-        // Re-pack non-zero coeffs in the first 32x32 indices.
-        for (row = 1; row < 32; ++row)
-            memcpy(coeff_buffer + row * 32, coeff_buffer + row * 64, 32 * sizeof(int32_t));
         break;
 
     case TX_32X32:
diff --git a/Source/Lib/Common/Codec/aom_dsp_rtcd.h b/Source/Lib/Common/Codec/aom_dsp_rtcd.h
index 919555e1a0..95935646d5 100644
--- a/Source/Lib/Common/Codec/aom_dsp_rtcd.h
+++ b/Source/Lib/Common/Codec/aom_dsp_rtcd.h
@@ -263,6 +263,26 @@ extern "C" {
     void get_proj_subspace_avx2(const uint8_t *src8, int width, int height, int src_stride, const uint8_t *dat8, int dat_stride, int use_highbitdepth, int32_t *flt0, int flt0_stride, int32_t *flt1, int flt1_stride, int *xq, const SgrParamsType *params);
     RTCD_EXTERN void(*get_proj_subspace)(const uint8_t *src8, int width, int height, int src_stride, const uint8_t *dat8, int dat_stride, int use_highbitdepth, int32_t *flt0, int flt0_stride, int32_t *flt1, int flt1_stride, int *xq, const SgrParamsType *params);
 
+    uint64_t HandleTransform16x64_c(int32_t *output);
+    uint64_t HandleTransform16x64_avx2(int32_t *output);
+    RTCD_EXTERN uint64_t(*HandleTransform16x64)(int32_t *output);
+
+    uint64_t HandleTransform32x64_c(int32_t *output);
+    uint64_t HandleTransform32x64_avx2(int32_t *output);
+    RTCD_EXTERN uint64_t(*HandleTransform32x64)(int32_t *output);
+
+    uint64_t HandleTransform64x16_c(int32_t *output);
+    uint64_t HandleTransform64x16_avx2(int32_t *output);
+    RTCD_EXTERN uint64_t(*HandleTransform64x16)(int32_t *output);
+
+    uint64_t HandleTransform64x32_c(int32_t *output);
+    uint64_t HandleTransform64x32_avx2(int32_t *output);
+    RTCD_EXTERN uint64_t(*HandleTransform64x32)(int32_t *output);
+
+    uint64_t HandleTransform64x64_c(int32_t *output);
+    uint64_t HandleTransform64x64_avx2(int32_t *output);
+    RTCD_EXTERN uint64_t(*HandleTransform64x64)(int32_t *output);
+
     uint64_t search_one_dual_c(int *lev0, int *lev1, int nb_strengths, uint64_t(**mse)[64], int sb_count, int fast, int start_gi, int end_gi);
     uint64_t search_one_dual_avx2(int *lev0, int *lev1, int nb_strengths, uint64_t(**mse)[64], int sb_count, int fast, int start_gi, int end_gi);
     RTCD_EXTERN uint64_t(*search_one_dual)(int *lev0, int *lev1, int nb_strengths, uint64_t(**mse)[64], int sb_count, int fast, int start_gi, int end_gi);
@@ -3193,6 +3213,17 @@ extern "C" {
         av1_fwd_txfm2d_4x4 = Av1TransformTwoD_4x4_c;
         if (flags & HAS_SSE4_1) av1_fwd_txfm2d_4x4 = av1_fwd_txfm2d_4x4_sse4_1;
 
+        HandleTransform16x64 = HandleTransform16x64_c;
+        if (flags & HAS_AVX2) HandleTransform16x64 = HandleTransform16x64_avx2;
+        HandleTransform32x64 = HandleTransform32x64_c;
+        if (flags & HAS_AVX2) HandleTransform32x64 = HandleTransform32x64_avx2;
+        HandleTransform64x16 = HandleTransform64x16_c;
+        if (flags & HAS_AVX2) HandleTransform64x16 = HandleTransform64x16_avx2;
+        HandleTransform64x32 = HandleTransform64x32_c;
+        if (flags & HAS_AVX2) HandleTransform64x32 = HandleTransform64x32_avx2;
+        HandleTransform64x64 = HandleTransform64x64_c;
+        if (flags & HAS_AVX2) HandleTransform64x64 = HandleTransform64x64_avx2;
+
         // aom_highbd_v_predictor
         aom_highbd_v_predictor_16x16 = aom_highbd_v_predictor_16x16_c;
         if (flags & HAS_AVX2) aom_highbd_v_predictor_16x16 = aom_highbd_v_predictor_16x16_avx2;
diff --git a/test/InvTxfm2dAsmTest.cc b/test/InvTxfm2dAsmTest.cc
index e1ca44cb61..fac8bf917b 100644
--- a/test/InvTxfm2dAsmTest.cc
+++ b/test/InvTxfm2dAsmTest.cc
@@ -33,6 +33,7 @@
 #include "util.h"
 #include "aom_dsp_rtcd.h"
 #include "EbTransforms.h"
+#include "EbUnitTestUtility.h"
 #include "TxfmCommon.h"
 #include "av1_inv_txfm_ssse3.h"
 
@@ -378,6 +379,137 @@ class InvTxfm2dAsmTest : public ::testing::TestWithParam<int> {
         }
     }
 
+    void run_HandleTransform_match_test() {
+        using FwdTxfm2dFunc = void (*)(int16_t * input,
+                                       int32_t * output,
+                                       uint32_t stride,
+                                       TxType tx_type,
+                                       uint8_t bd);
+        using HandleTxfmFunc = uint64_t (*)(int32_t * output);
+        const int num_htf_sizes = 5;
+        const TxSize htf_tx_size[num_htf_sizes] = {
+            TX_16X64, TX_32X64, TX_64X16, TX_64X32, TX_64X64};
+        const HandleTxfmFunc htf_ref_funcs[num_htf_sizes] = {
+            HandleTransform16x64_c,
+            HandleTransform32x64_c,
+            HandleTransform64x16_c,
+            HandleTransform64x32_c,
+            HandleTransform64x64_c};
+        const HandleTxfmFunc htf_asm_funcs[num_htf_sizes] = {
+            HandleTransform16x64_avx2,
+            HandleTransform32x64_avx2,
+            HandleTransform64x16_avx2,
+            HandleTransform64x32_avx2,
+            HandleTransform64x64_avx2};
+        DECLARE_ALIGNED(32, int32_t, input[MAX_TX_SQUARE]);
+
+        for (int idx = 0; idx < num_htf_sizes; ++idx) {
+            const TxSize tx_size = htf_tx_size[idx];
+
+            eb_buf_random_s32(input_, sizeof(input_) / sizeof(*input_));
+            memcpy(input, input_, sizeof(input_));
+
+            const uint64_t energy_ref = htf_ref_funcs[idx](input_);
+            const uint64_t energy_asm = htf_asm_funcs[idx](input);
+
+            ASSERT_EQ(energy_ref, energy_asm);
+
+            for (int i = 0; i < MAX_TX_SIZE; i++) {
+                for (int j = 0; j < MAX_TX_SIZE; j++) {
+                    ASSERT_EQ(input_[i * MAX_TX_SIZE + j],
+                                input[i * MAX_TX_SIZE + j])
+                        << " tx_size: " << tx_size << " " << j << " x "
+                        << i;
+                }
+            }
+        }
+    }
+
+    void run_HandleTransform_speed_test() {
+        using FwdTxfm2dFunc = void (*)(int16_t * input,
+                                       int32_t * output,
+                                       uint32_t stride,
+                                       TxType tx_type,
+                                       uint8_t bd);
+        using HandleTxfmFunc = uint64_t (*)(int32_t * output);
+        const int num_htf_sizes = 5;
+        const TxSize htf_tx_size[num_htf_sizes] = {
+            TX_16X64, TX_32X64, TX_64X16, TX_64X32, TX_64X64};
+        const int widths[num_htf_sizes] = {16, 32, 64, 64, 64};
+        const int heights[num_htf_sizes] = {64, 64, 16, 32, 64};
+        const HandleTxfmFunc htf_ref_funcs[num_htf_sizes] = {
+            HandleTransform16x64_c,
+            HandleTransform32x64_c,
+            HandleTransform64x16_c,
+            HandleTransform64x32_c,
+            HandleTransform64x64_c};
+        const HandleTxfmFunc htf_asm_funcs[num_htf_sizes] = {
+            HandleTransform16x64_avx2,
+            HandleTransform32x64_avx2,
+            HandleTransform64x16_avx2,
+            HandleTransform64x32_avx2,
+            HandleTransform64x64_avx2};
+        DECLARE_ALIGNED(32, int32_t, input[MAX_TX_SQUARE]);
+        double time_c, time_o;
+        uint64_t start_time_seconds, start_time_useconds;
+        uint64_t middle_time_seconds, middle_time_useconds;
+        uint64_t finish_time_seconds, finish_time_useconds;
+
+        for (int idx = 0; idx < num_htf_sizes; ++idx) {
+            const TxSize tx_size = htf_tx_size[idx];
+            const uint64_t num_loop = 10000000;
+            uint64_t energy_ref, energy_asm;
+
+            eb_buf_random_s32(input_, sizeof(input_) / sizeof(*input_));
+            memcpy(input, input_, sizeof(input_));
+
+            EbStartTime(&start_time_seconds, &start_time_useconds);
+
+            for (uint64_t i = 0; i < num_loop; i++)
+                energy_ref = htf_ref_funcs[idx](input_);
+
+            EbStartTime(&middle_time_seconds, &middle_time_useconds);
+
+            for (uint64_t i = 0; i < num_loop; i++)
+                energy_asm = htf_asm_funcs[idx](input);
+
+            EbStartTime(&finish_time_seconds, &finish_time_useconds);
+            EbComputeOverallElapsedTimeMs(start_time_seconds,
+                                          start_time_useconds,
+                                          middle_time_seconds,
+                                          middle_time_useconds,
+                                          &time_c);
+            EbComputeOverallElapsedTimeMs(middle_time_seconds,
+                                          middle_time_useconds,
+                                          finish_time_seconds,
+                                          finish_time_useconds,
+                                          &time_o);
+
+            ASSERT_EQ(energy_ref, energy_asm);
+
+            for (int i = 0; i < MAX_TX_SIZE; i++) {
+                for (int j = 0; j < MAX_TX_SIZE; j++) {
+                    ASSERT_EQ(input_[i * MAX_TX_SIZE + j],
+                              input[i * MAX_TX_SIZE + j])
+                        << " tx_size: " << tx_size << " " << j << " x " << i;
+                }
+            }
+
+            printf("Average Nanoseconds per Function Call\n");
+            printf("    HandleTransform%dx%d_c     : %6.2f\n",
+                   widths[idx],
+                   heights[idx],
+                   1000000 * time_c / num_loop);
+            printf(
+                "    HandleTransform%dx%d_avx2) : %6.2f   (Comparison: "
+                "%5.2fx)\n",
+                widths[idx],
+                heights[idx],
+                1000000 * time_o / num_loop,
+                time_c / time_o);
+        }
+    }
+
   private:
     // clear the coeffs according to eob position, note the coeffs are
     // linear.
@@ -423,40 +555,23 @@ class InvTxfm2dAsmTest : public ::testing::TestWithParam<int> {
         memset(lowbd_output_test_, 0, sizeof(lowbd_output_test_));
         for (int i = 0; i < height; i++) {
             for (int j = 0; j < width; j++) {
-                pixel_input_[i * stride_ + j] = s_bd_rnd_->random();
+                pixel_input_[i * stride_ + j] =
+                    static_cast<int16_t>(s_bd_rnd_->random());
                 output_ref_[i * stride_ + j] = output_test_[i * stride_ + j] =
-                    u_bd_rnd_->random();
+                    static_cast<uint16_t>(u_bd_rnd_->random());
             }
         }
 
-        fwd_txfm_func[tx_size](pixel_input_, input_, stride_, tx_type, bd_);
+        fwd_txfm_func[tx_size](pixel_input_, input_, stride_, tx_type,
+            static_cast<uint8_t>(bd_));
         // post-process, re-pack the coeffcients
         uint64_t energy = 0;
         switch (tx_size) {
-        case TX_64X64:
-            energy = HandleTransform64x64_c(input_, stride_);
-            for (int32_t row = 1; row < 32; ++row) {
-                memcpy(
-                    input_ + row * 32, input_ + row * 64, 32 * sizeof(int32_t));
-            }
-            break;
-        case TX_64X32:
-            energy = HandleTransform64x32_c(input_, stride_);
-            for (int32_t row = 1; row < 32; ++row) {
-                memcpy(
-                    input_ + row * 32, input_ + row * 64, 32 * sizeof(int32_t));
-            }
-            break;
-        case TX_32X64: energy = HandleTransform32x64_c(input_, stride_); break;
-        case TX_64X16:
-            energy = HandleTransform64x16_c(input_, stride_);
-            // Re-pack non-zero coeffs in the first 32x16 indices.
-            for (int32_t row = 1; row < 16; ++row) {
-                memcpy(
-                    input_ + row * 32, input_ + row * 64, 32 * sizeof(int32_t));
-            }
-            break;
-        case TX_16X64: energy = HandleTransform16x64_c(input_, stride_); break;
+        case TX_64X64: energy = HandleTransform64x64_c(input_); break;
+        case TX_64X32: energy = HandleTransform64x32_c(input_); break;
+        case TX_32X64: energy = HandleTransform32x64_c(input_); break;
+        case TX_64X16: energy = HandleTransform64x16_c(input_); break;
+        case TX_16X64: energy = HandleTransform16x64_c(input_); break;
         default: break;
         }
         return;
@@ -505,6 +620,14 @@ TEST_P(InvTxfm2dAsmTest, lowbd_txfm_match_test) {
     }
 }
 
+TEST_P(InvTxfm2dAsmTest, HandleTransform_match_test) {
+    run_HandleTransform_match_test();
+}
+
+TEST_P(InvTxfm2dAsmTest, HandleTransform_speed_test) {
+    run_HandleTransform_speed_test();
+}
+
 INSTANTIATE_TEST_CASE_P(TX, InvTxfm2dAsmTest,
                         ::testing::Values(static_cast<int>(AOM_BITS_8),
                                           static_cast<int>(AOM_BITS_10)));
diff --git a/test/TxfmCommon.h b/test/TxfmCommon.h
index f7b84c7270..fc5e3c467b 100644
--- a/test/TxfmCommon.h
+++ b/test/TxfmCommon.h
@@ -215,15 +215,6 @@ static INLINE bool dct_adst_combine_imp(const TxType tx_type) {
     }
 }
 
-/* Implemented in EbTransforms.c, and used by InvTxfm2dAsmtest to prepare
-   the inv_input buffer.
- */
-uint64_t HandleTransform64x64_c(int32_t *output, uint32_t outputStride);
-uint64_t HandleTransform64x16_c(int32_t *output, uint32_t outputStride);
-uint64_t HandleTransform16x64_c(int32_t *output, uint32_t outputStride);
-uint64_t HandleTransform32x64_c(int32_t *output, uint32_t outputStride);
-uint64_t HandleTransform64x32_c(int32_t *output, uint32_t outputStride);
-
 #ifdef __cplusplus
 }
 #endif
