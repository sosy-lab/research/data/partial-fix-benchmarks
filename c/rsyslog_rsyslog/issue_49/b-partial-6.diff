diff --git a/runtime/dnscache.c b/runtime/dnscache.c
index 89e7a8c07d..42fb4f5aed 100644
--- a/runtime/dnscache.c
+++ b/runtime/dnscache.c
@@ -7,7 +7,7 @@
  * In any case, even the initial implementaton is far faster than what we had
  * before. -- rgerhards, 2011-06-06
  *
- * Copyright 2011-2016 by Rainer Gerhards and Adiscon GmbH.
+ * Copyright 2011-2019 by Rainer Gerhards and Adiscon GmbH.
  *
  * This file is part of the rsyslog runtime library.
  *
@@ -52,6 +52,7 @@ struct dnscache_entry_s {
 	prop_t *fqdnLowerCase;
 	prop_t *localName; /* only local name, without domain part (if configured so) */
 	prop_t *ip;
+	time_t validUntil;
 	struct dnscache_entry_s *next;
 	unsigned nUsed;
 };
@@ -63,6 +64,8 @@ struct dnscache_s {
 };
 typedef struct dnscache_s dnscache_t;
 
+unsigned dnscacheDefaultTTL = 24 * 60 * 60; /* 24 hrs default TTL */
+
 
 /* static data */
 DEFobjStaticHelpers
@@ -73,12 +76,7 @@ static prop_t *staticErrValue;
 
 
 /* Our hash function.
- * TODO: check how well it performs on socket addresses!
  */
-#if defined(__GNUC__) && !defined(__clang__)
-#pragma GCC diagnostic push
-#pragma GCC diagnostic ignored "-Wmaybe-uninitialized" /* TODO: how can we fix these warnings? */
-#endif
 static unsigned int
 hash_from_key_fn(void *k)
 {
@@ -95,15 +93,17 @@ hash_from_key_fn(void *k)
 			len = sizeof (struct in6_addr);
 			rkey = (uchar*) &(((struct sockaddr_in6 *)k)->sin6_addr);
 			break;
+		default:
+			dbgprintf("hash_from_key_fn: unknown address family!\n");
+			len = 0;
+			rkey = NULL;
 	}
 	while(len--)
 		hashval = hashval * 33 + *rkey++;
 
 	return hashval;
 }
-#if defined(__GNUC__) && !defined(__clang__)
-#pragma GCC diagnostic pop
-#endif
+
 
 static int
 key_equals_fn(void *key1, void *key2)
@@ -180,10 +180,29 @@ dnscacheDeinit(void)
 }
 
 
-static inline dnscache_entry_t*
-findEntry(struct sockaddr_storage *addr)
+static dnscache_entry_t*
+findEntry(struct sockaddr_storage *const addr)
 {
-	return((dnscache_entry_t*) hashtable_search(dnsCache.ht, addr));
+	dnscache_entry_t *etry = hashtable_search(dnsCache.ht, addr);
+	if(etry == NULL) {
+		goto done;
+	}
+
+	if(etry->validUntil <= time(NULL)) {
+		DBGPRINTF("hashtable: entry timed out, discarding it; "
+			"valid until %lld, now %lld\n",
+			(long long) etry->validUntil, (long long) time(NULL));
+		dnscache_entry_t *const deleted = hashtable_remove(dnsCache.ht, addr);
+		if(deleted != etry) {
+			LogError(0, RS_RET_INTERNAL_ERROR, "dnscache %d: removed different "
+				"hashtable entry than expected - please report issue; "
+				"rsyslog version is %s", __LINE__, VERSION);
+		}
+		entryDestruct(etry);
+		etry = NULL;
+	}
+done:
+	return etry;
 }
 
 
@@ -320,7 +339,6 @@ resolveAddr(struct sockaddr_storage *addr, dnscache_entry_t *etry)
 			 * we got a numeric one, someone messed with DNS!
 			 */
 			if(getaddrinfo (fqdnBuf, NULL, &hints, &res) == 0) {
-				uchar szErrMsg[1024];
 				freeaddrinfo (res);
 				/* OK, we know we have evil. The question now is what to do about
 				 * it. One the one hand, the message might probably be intended
@@ -332,11 +350,10 @@ resolveAddr(struct sockaddr_storage *addr, dnscache_entry_t *etry)
 				 * is OK in any way. We do also log the error message. rgerhards, 2007-07-16
 		 		 */
 		 		if(glbl.GetDropMalPTRMsgs() == 1) {
-					snprintf((char*)szErrMsg, sizeof(szErrMsg),
+					LogError(0, RS_RET_MALICIOUS_ENTITY,
 						 "Malicious PTR record, message dropped "
 						 "IP = \"%s\" HOST = \"%s\"",
 						 szIP, fqdnBuf);
-					LogError(0, RS_RET_MALICIOUS_ENTITY, "%s", szErrMsg);
 					pthread_sigmask(SIG_SETMASK, &omask, NULL);
 					ABORT_FINALIZE(RS_RET_MALICIOUS_ENTITY);
 				}
@@ -347,11 +364,10 @@ resolveAddr(struct sockaddr_storage *addr, dnscache_entry_t *etry)
 				 * (OK, I admit this is more or less impossible, but I am paranoid...)
 				 * rgerhards, 2007-07-16
 				 */
-				snprintf((char*)szErrMsg, sizeof(szErrMsg),
+				LogError(0, NO_ERRCODE,
 					 "Malicious PTR record (message accepted, but used IP "
 					 "instead of PTR name: IP = \"%s\" HOST = \"%s\"",
 					 szIP, fqdnBuf);
-				LogError(0, NO_ERRCODE, "%s", szErrMsg);
 
 				error = 1; /* that will trigger using IP address below. */
 			} else {/* we have a valid entry, so let's create the respective properties */
@@ -397,6 +413,7 @@ addEntry(struct sockaddr_storage *const addr, dnscache_entry_t **const pEtry)
 	dnscache_entry_t *etry = NULL;
 	DEFiRet;
 
+	pthread_rwlock_unlock(&dnsCache.rwlock);
 	pthread_rwlock_wrlock(&dnsCache.rwlock);
 	/* first check, if the entry was added in the mean time */
 	etry = findEntry(addr);
@@ -410,6 +427,7 @@ addEntry(struct sockaddr_storage *const addr, dnscache_entry_t **const pEtry)
 	CHKiRet(resolveAddr(addr, etry));
 	memcpy(&etry->addr, addr, SALEN((struct sockaddr*) addr));
 	etry->nUsed = 0;
+	etry->validUntil = time(NULL) + dnscacheDefaultTTL;
 
 	memcpy(keybuf, addr, sizeof(struct sockaddr_storage));
 
@@ -421,6 +439,7 @@ addEntry(struct sockaddr_storage *const addr, dnscache_entry_t **const pEtry)
 
 finalize_it:
 	pthread_rwlock_unlock(&dnsCache.rwlock);
+	pthread_rwlock_rdlock(&dnsCache.rwlock);
 	if(iRet == RS_RET_OK) {
 		*pEtry = etry;
 	} else {
@@ -447,22 +466,20 @@ validateEntry(dnscache_entry_t __attribute__((unused)) *etry, struct sockaddr_st
  * If the entry can not be resolved, an error is reported back. If fqdn
  * or fqdnLowerCase are NULL, they are not set.
  */
-rsRetVal
-dnscacheLookup(struct sockaddr_storage *addr, prop_t **fqdn, prop_t **fqdnLowerCase,
-	       prop_t **localName, prop_t **ip)
+rsRetVal ATTR_NONNULL(1, 5)
+dnscacheLookup(struct sockaddr_storage *const addr,
+	prop_t **const fqdn, prop_t **const fqdnLowerCase,
+	prop_t **const localName, prop_t **const ip)
 {
 	dnscache_entry_t *etry;
 	DEFiRet;
 
-	pthread_rwlock_rdlock(&dnsCache.rwlock); /* TODO: optimize this! */
+	pthread_rwlock_rdlock(&dnsCache.rwlock);
 	do {
 		etry = findEntry(addr);
 		dbgprintf("dnscache: entry %p found\n", etry);
 		if(etry == NULL) {
-			pthread_rwlock_unlock(&dnsCache.rwlock);
-			iRet = addEntry(addr, &etry);
-			pthread_rwlock_rdlock(&dnsCache.rwlock); /* TODO: optimize this! */
-			CHKiRet(iRet);
+			CHKiRet(addEntry(addr, &etry));
 		} else {
 			CHKiRet(validateEntry(etry, addr));
 		}
diff --git a/runtime/dnscache.h b/runtime/dnscache.h
index 221ddbd8d8..bf30323dd4 100644
--- a/runtime/dnscache.h
+++ b/runtime/dnscache.h
@@ -1,6 +1,6 @@
 /* Definitions for dnscache module.
  *
- * Copyright 2011-2013 Adiscon GmbH.
+ * Copyright 2011-2019 Adiscon GmbH.
  *
  * This file is part of the rsyslog runtime library.
  *
@@ -24,7 +24,9 @@
 
 rsRetVal dnscacheInit(void);
 rsRetVal dnscacheDeinit(void);
-rsRetVal dnscacheLookup(struct sockaddr_storage *addr, prop_t **fqdn, prop_t **fqdnLowerCase,
-prop_t **localName, prop_t **ip);
+rsRetVal ATTR_NONNULL(1, 5) dnscacheLookup(struct sockaddr_storage *const addr,
+	prop_t **const fqdn, prop_t **const fqdnLowerCase,
+	prop_t **const localName, prop_t **const ip);
 
+extern unsigned dnscacheDefaultTTL;
 #endif /* #ifndef INCLUDED_DNSCACHE_H */
diff --git a/runtime/glbl.c b/runtime/glbl.c
index ebe6bea611..a5f27085a5 100644
--- a/runtime/glbl.c
+++ b/runtime/glbl.c
@@ -7,7 +7,7 @@
  *
  * Module begun 2008-04-16 by Rainer Gerhards
  *
- * Copyright 2008-2018 Rainer Gerhards and Adiscon GmbH.
+ * Copyright 2008-2019 Rainer Gerhards and Adiscon GmbH.
  *
  * This file is part of the rsyslog runtime library.
  *
@@ -56,6 +56,7 @@
 #include "net.h"
 #include "rsconf.h"
 #include "queue.h"
+#include "dnscache.h"
 
 #define REPORT_CHILD_PROCESS_EXITS_NONE 0
 #define REPORT_CHILD_PROCESS_EXITS_ERRORS 1
@@ -213,6 +214,7 @@ static struct cnfparamdescr cnfparamdescr[] = {
 	{ "default.action.queue.timeoutactioncompletion", eCmdHdlrInt, 0 },
 	{ "default.action.queue.timeoutenqueue", eCmdHdlrInt, 0 },
 	{ "default.action.queue.timeoutworkerthreadshutdown", eCmdHdlrInt, 0 },
+	{ "dnscache.default.ttl", eCmdHdlrNonNegInt, 0 },
 	{ "debug.files", eCmdHdlrArray, 0 },
 	{ "debug.whitelist", eCmdHdlrBinary, 0 }
 };
@@ -1464,6 +1466,8 @@ glblDoneLoadCnf(void)
 			actq_dflt_toEnq = cnfparamvals[i].val.d.n;
 		} else if(!strcmp(paramblk.descr[i].name, "default.action.queue.timeoutworkerthreadshutdown")) {
 			actq_dflt_toWrkShutdown = cnfparamvals[i].val.d.n;
+		} else if(!strcmp(paramblk.descr[i].name, "dnscache.default.ttl")) {
+			dnscacheDefaultTTL = cnfparamvals[i].val.d.n;
 		} else {
 			dbgprintf("glblDoneLoadCnf: program error, non-handled "
 				"param '%s'\n", paramblk.descr[i].name);
diff --git a/tests/Makefile.am b/tests/Makefile.am
index 4a927ee5a2..9a2932f8dc 100644
--- a/tests/Makefile.am
+++ b/tests/Makefile.am
@@ -160,6 +160,7 @@ TESTS +=  \
 	glbl_setenv_err_too_long.sh \
 	glbl_setenv.sh \
 	nested-call-shutdown.sh \
+	dnscache-TTL-0.sh \
 	invalid_nested_include.sh \
 	omfwd-keepalive.sh \
 	omfile-read-only-errmsg.sh \
@@ -402,6 +403,7 @@ endif # ENABLE_LIBGCRYPT
 if HAVE_VALGRIND
 TESTS +=  \
 	tcpflood_wrong_option_output.sh \
+	dnscache-TTL-0-vg.sh \
 	include-obj-outside-control-flow-vg.sh \
 	include-obj-in-if-vg.sh \
 	include-obj-text-vg.sh \
@@ -1374,6 +1376,8 @@ distclean-local:
 
 EXTRA_DIST= \
 	urlencode.py \
+	dnscache-TTL-0.sh \
+	dnscache-TTL-0-vg.sh \
 	operatingstate-basic.sh \
 	operatingstate-empty.sh \
 	operatingstate-unclean.sh \
diff --git a/tests/dnscache-TTL-0-vg.sh b/tests/dnscache-TTL-0-vg.sh
new file mode 100755
index 0000000000..8618bd12fa
--- /dev/null
+++ b/tests/dnscache-TTL-0-vg.sh
@@ -0,0 +1,4 @@
+#!/bin/bash
+# added 2019-04-12 by Rainer Gerhards, released under ASL 2.0
+export USE_VALGRIND="YES"
+source ${srcdir:=.}/dnscache-TTL-0.sh
diff --git a/tests/dnscache-TTL-0.sh b/tests/dnscache-TTL-0.sh
new file mode 100755
index 0000000000..3e5efcc7cf
--- /dev/null
+++ b/tests/dnscache-TTL-0.sh
@@ -0,0 +1,20 @@
+#!/bin/bash
+# addd 2019-04-12 by RGerhards, released under ASL 2.0
+. ${srcdir:=.}/diag.sh init
+export NUMMESSAGES=20 # should be sufficient to stress DNS cache
+generate_conf
+add_conf '
+global(dnscache.default.ttl="0")
+module(load="../plugins/imtcp/.libs/imtcp")
+input(type="imtcp" port="'$TCPFLOOD_PORT'")
+
+template(name="outfmt" type="string" string="%msg:F,58:2%\n")
+:msg, contains, "msgnum:" action(type="omfile" template="outfmt"
+			         file=`echo $RSYSLOG_OUT_LOG`)
+'
+startup
+tcpflood -m$NUMMESSAGES -c10 # run on 10 connections --> 10 dns cache calls
+shutdown_when_empty # shut down rsyslogd when done processing messages
+wait_shutdown
+seq_check
+exit_test
