diff --git a/bgpd/bgp_nexthop.c b/bgpd/bgp_nexthop.c
index 76bfa73feeb..8433a94d859 100644
--- a/bgpd/bgp_nexthop.c
+++ b/bgpd/bgp_nexthop.c
@@ -251,6 +251,26 @@ static void bgp_address_del(struct bgp *bgp, struct prefix *p)
 	}
 }
 
+/* Verify if the bgp router ID is in the address hash table */
+void bgp_validate_set_router_id(struct bgp *bgp, struct in_addr *new_addr)
+{
+	struct bgp_addr tmp;
+	struct bgp_addr *addr;
+
+	if (!new_addr) {
+		zlog_err("%s : invalid router ID", __func__);
+		return;
+	}
+
+	tmp.addr = bgp->router_id;
+	addr = hash_lookup(bgp->address_hash, &tmp);
+	if (addr == NULL) {
+		if (BGP_DEBUG(zebra, ZEBRA))
+			zlog_debug("RID change : vrf %d, RTR ID %s",
+				bgp->vrf_id, inet_ntoa(*new_addr));
+		bgp_router_id_set(bgp, new_addr);
+	}
+}
 
 struct bgp_connected_ref {
 	unsigned int refcnt;
diff --git a/bgpd/bgp_nexthop.h b/bgpd/bgp_nexthop.h
index a771bead230..64ce5b023f3 100644
--- a/bgpd/bgp_nexthop.h
+++ b/bgpd/bgp_nexthop.h
@@ -102,5 +102,6 @@ extern void bgp_tip_add(struct bgp *bgp, struct in_addr *tip);
 extern void bgp_tip_del(struct bgp *bgp, struct in_addr *tip);
 extern void bgp_tip_hash_init(struct bgp *bgp);
 extern void bgp_tip_hash_destroy(struct bgp *bgp);
-
+extern void bgp_validate_set_router_id(struct bgp *bgp,
+			struct in_addr *new_addr);
 #endif /* _QUAGGA_BGP_NEXTHOP_H */
diff --git a/bgpd/bgp_zebra.c b/bgpd/bgp_zebra.c
index 9591fe673f7..f3b5b4a0d9e 100644
--- a/bgpd/bgp_zebra.c
+++ b/bgpd/bgp_zebra.c
@@ -379,6 +379,8 @@ static int bgp_interface_address_delete(int command, struct zclient *zclient,
 {
 	struct connected *ifc;
 	struct bgp *bgp;
+	struct prefix *p;
+	bool update_routerid = false;
 
 	bgp = bgp_lookup_by_vrf_id(vrf_id);
 	if (!bgp)
@@ -396,12 +398,23 @@ static int bgp_interface_address_delete(int command, struct zclient *zclient,
 			   ifc->ifp->name, buf);
 	}
 
+	/* Update the router id if the interface address being deleted is
+	 * same as the bgp router id
+	 */
+	p = ifc->address;
+	if ((p->family == AF_INET) &&
+		IPV4_ADDR_SAME(&bgp->router_id, &p->u.prefix4))
+		update_routerid = true;
+
 	if (if_is_operative(ifc->ifp)) {
 		bgp_connected_delete(bgp, ifc);
 	}
 
 	connected_free(ifc);
 
+	if (update_routerid)
+		bgp_router_id_zebra_bump(vrf_id, NULL);
+
 	return 0;
 }
 
diff --git a/bgpd/bgpd.c b/bgpd/bgpd.c
index 82da0245b5c..c83e3767c27 100644
--- a/bgpd/bgpd.c
+++ b/bgpd/bgpd.c
@@ -234,7 +234,7 @@ static int bgp_config_check(struct bgp *bgp, int config)
 }
 
 /* Set BGP router identifier. */
-static int bgp_router_id_set(struct bgp *bgp, const struct in_addr *id)
+int bgp_router_id_set(struct bgp *bgp, const struct in_addr *id)
 {
 	struct peer *peer;
 	struct listnode *node, *nnode;
@@ -270,6 +270,10 @@ void bgp_router_id_zebra_bump(vrf_id_t vrf_id, const struct prefix *router_id)
 {
 	struct listnode *node, *nnode;
 	struct bgp *bgp;
+	struct in_addr *addr = NULL;
+
+	if (router_id != NULL)
+		addr = (struct in_addr *)&router_id->u.prefix4;
 
 	if (vrf_id == VRF_DEFAULT) {
 		/* Router-id change for default VRF has to also update all
@@ -278,17 +282,27 @@ void bgp_router_id_zebra_bump(vrf_id_t vrf_id, const struct prefix *router_id)
 			if (bgp->inst_type == BGP_INSTANCE_TYPE_VRF)
 				continue;
 
-			bgp->router_id_zebra = router_id->u.prefix4;
+			/* If router_id is NULL, then it is interface delete
+			 * Set bgp router ID to the value router_id_zebra
+			 */
+			if (addr)
+				bgp->router_id_zebra = *addr;
+			else
+				addr = &bgp->router_id_zebra;
+
 			if (!bgp->router_id_static.s_addr)
-				bgp_router_id_set(bgp, &router_id->u.prefix4);
+				bgp_validate_set_router_id(bgp, addr);
 		}
 	} else {
 		bgp = bgp_lookup_by_vrf_id(vrf_id);
 		if (bgp) {
-			bgp->router_id_zebra = router_id->u.prefix4;
+			if (addr)
+				bgp->router_id_zebra = *addr;
+			else
+				addr = &bgp->router_id_zebra;
 
 			if (!bgp->router_id_static.s_addr)
-				bgp_router_id_set(bgp, &router_id->u.prefix4);
+				bgp_validate_set_router_id(bgp, addr);
 		}
 	}
 }
diff --git a/bgpd/bgpd.h b/bgpd/bgpd.h
index 06eb86da955..4aba994f51d 100644
--- a/bgpd/bgpd.h
+++ b/bgpd/bgpd.h
@@ -1868,5 +1868,5 @@ extern void bgp_update_redist_vrf_bitmaps(struct bgp *, vrf_id_t);
 
 /* For benefit of rfapi */
 extern struct peer *peer_new(struct bgp *bgp);
-
+extern int bgp_router_id_set(struct bgp *bgp, const struct in_addr *id);
 #endif /* _QUAGGA_BGPD_H */
diff --git a/zebra/redistribute.c b/zebra/redistribute.c
index e3101fbe72c..f6692e92d9f 100644
--- a/zebra/redistribute.c
+++ b/zebra/redistribute.c
@@ -449,14 +449,18 @@ void zebra_interface_address_add_update(struct interface *ifp,
 
 	zebra_vxlan_add_del_gw_macip(ifp, ifc->address, 1);
 
-	router_id_add_address(ifc);
-
 	for (ALL_LIST_ELEMENTS(zebrad.client_list, node, nnode, client))
 		if (CHECK_FLAG(ifc->conf, ZEBRA_IFC_REAL)) {
 			client->connected_rt_add_cnt++;
 			zsend_interface_address(ZEBRA_INTERFACE_ADDRESS_ADD,
 						client, ifp, ifc);
 		}
+
+	/* Send ROUTER_ID update message to client after sending interface
+	 * address ADD message. This will allow the client to update the
+	 * hash table of connected address used for selecting router ID
+	 */
+	router_id_add_address(ifc);
 }
 
 /* Interface address deletion. */
@@ -478,14 +482,17 @@ void zebra_interface_address_delete_update(struct interface *ifp,
 
 	zebra_vxlan_add_del_gw_macip(ifp, ifc->address, 0);
 
-	router_id_del_address(ifc);
-
 	for (ALL_LIST_ELEMENTS(zebrad.client_list, node, nnode, client))
 		if (CHECK_FLAG(ifc->conf, ZEBRA_IFC_REAL)) {
 			client->connected_rt_del_cnt++;
 			zsend_interface_address(ZEBRA_INTERFACE_ADDRESS_DELETE,
 						client, ifp, ifc);
 		}
+	/* Send ROUTER_ID update message to client after sending interface
+	 * address DELETE message. This will allow the client to update the
+	 * hash table of connected address used for selecting router ID
+	 */
+	router_id_del_address(ifc);
 }
 
 /* Interface VRF change. May need to delete from clients not interested in
