diff --git a/i3bar/include/config.h b/i3bar/include/config.h
index 1ce5dfa6e..2a0590462 100644
--- a/i3bar/include/config.h
+++ b/i3bar/include/config.h
@@ -29,6 +29,12 @@ typedef struct binding_t {
     TAILQ_ENTRY(binding_t) bindings;
 } binding_t;
 
+typedef struct tray_output_t {
+    char *output;
+
+    TAILQ_ENTRY(tray_output_t) tray_outputs;
+} tray_output_t;
+
 typedef struct config_t {
     int modifier;
     TAILQ_HEAD(bindings_head, binding_t) bindings;
@@ -42,7 +48,7 @@ typedef struct config_t {
     char *command;
     char *fontname;
     i3String *separator_symbol;
-    char *tray_output;
+    TAILQ_HEAD(tray_outputs_head, tray_output_t) tray_outputs;
     int tray_padding;
     int num_outputs;
     char **outputs;
diff --git a/i3bar/src/config.c b/i3bar/src/config.c
index 0e2dd05a8..6c3701576 100644
--- a/i3bar/src/config.c
+++ b/i3bar/src/config.c
@@ -21,6 +21,7 @@
 
 static char *cur_key;
 static bool parsing_bindings;
+static bool parsing_tray_outputs;
 
 /*
  * Parse a key.
@@ -35,14 +36,20 @@ static int config_map_key_cb(void *params_, const unsigned char *keyVal, size_t
     strncpy(cur_key, (const char *)keyVal, keyLen);
     cur_key[keyLen] = '\0';
 
-    if (strcmp(cur_key, "bindings") == 0)
+    if (strcmp(cur_key, "bindings") == 0) {
         parsing_bindings = true;
+    }
+
+    if (strcmp(cur_key, "tray_outputs") == 0) {
+        parsing_tray_outputs = true;
+    }
 
     return 1;
 }
 
 static int config_end_array_cb(void *params_) {
     parsing_bindings = false;
+    parsing_tray_outputs = false;
     return 1;
 }
 
@@ -93,6 +100,14 @@ static int config_string_cb(void *params_, const unsigned char *val, size_t _len
         return 0;
     }
 
+    if (parsing_tray_outputs) {
+        DLOG("Adding tray_output = %.*s to the list.\n", len, val);
+        tray_output_t *tray_output = scalloc(1, sizeof(tray_output_t));
+        sasprintf(&(tray_output->output), "%.*s", len, val);
+        TAILQ_INSERT_TAIL(&(config.tray_outputs), tray_output, tray_outputs);
+        return 1;
+    }
+
     if (!strcmp(cur_key, "mode")) {
         DLOG("mode = %.*s, len = %d\n", len, val, len);
         config.hide_on_modifier = (len == 4 && !strncmp((const char *)val, "dock", strlen("dock")) ? M_DOCK
@@ -198,10 +213,13 @@ static int config_string_cb(void *params_, const unsigned char *val, size_t _len
         return 1;
     }
 
+    /* We keep the old single tray_output working for users who only restart i3bar
+     * after updating. */
     if (!strcmp(cur_key, "tray_output")) {
-        DLOG("tray_output %.*s\n", len, val);
-        FREE(config.tray_output);
-        sasprintf(&config.tray_output, "%.*s", len, val);
+        ELOG("Found deprecated key tray_output %.*s.\n", len, val);
+        tray_output_t *tray_output = scalloc(1, sizeof(tray_output_t));
+        sasprintf(&(tray_output->output), "%.*s", len, val);
+        TAILQ_INSERT_TAIL(&(config.tray_outputs), tray_output, tray_outputs);
         return 1;
     }
 
@@ -317,6 +335,7 @@ void parse_config_json(char *json) {
     handle = yajl_alloc(&outputs_callbacks, NULL, NULL);
 
     TAILQ_INIT(&(config.bindings));
+    TAILQ_INIT(&(config.tray_outputs));
 
     state = yajl_parse(handle, (const unsigned char *)json, strlen(json));
 
diff --git a/i3bar/src/xcb.c b/i3bar/src/xcb.c
index ac9b50f98..2885eeab2 100644
--- a/i3bar/src/xcb.c
+++ b/i3bar/src/xcb.c
@@ -678,25 +678,45 @@ static void handle_client_message(xcb_client_message_event_t *event) {
             }
 
             DLOG("X window %08x requested docking\n", client);
-            i3_output *walk, *output = NULL;
-            SLIST_FOREACH(walk, outputs, slist) {
-                if (!walk->active)
-                    continue;
-                if (config.tray_output) {
-                    if ((strcasecmp(walk->name, config.tray_output) != 0) &&
-                        (!walk->primary || strcasecmp("primary", config.tray_output) != 0))
+            i3_output *output = NULL;
+            i3_output *walk = NULL;
+            tray_output_t *tray_output = NULL;
+            TAILQ_FOREACH(tray_output, &(config.tray_outputs), tray_outputs) {
+                SLIST_FOREACH(walk, outputs, slist) {
+                    if (!walk->active)
                         continue;
+
+                    if (strcasecmp(walk->name, tray_output->output) == 0) {
+                        DLOG("Found tray_output assignment for output %s.\n", walk->name);
+                        output = walk;
+                        break;
+                    }
+
+                    if (walk->primary && strcasecmp("primary", tray_output->output) == 0) {
+                        DLOG("Found tray_output assignment on primary output %s.\n", walk->name);
+                        output = walk;
+                        break;
+                    }
                 }
 
-                DLOG("using output %s\n", walk->name);
-                output = walk;
-                break;
+                /* If we found an output, we're done. */
+                if (output != NULL)
+                    break;
+            }
+
+            /* Check whether any "tray_output primary" was defined for this bar. */
+            bool contains_primary = false;
+            TAILQ_FOREACH(tray_output, &(config.tray_outputs), tray_outputs) {
+                if (strcasecmp("primary", tray_output->output) == 0) {
+                    contains_primary = true;
+                    break;
+                }
             }
+
             /* In case of tray_output == primary and there is no primary output
-             * configured, we fall back to the first available output. */
-            if (output == NULL &&
-                config.tray_output &&
-                strcasecmp("primary", config.tray_output) == 0) {
+             * configured, we fall back to the first available output. We do the
+             * same if no tray_output was specified. */
+            if (output == NULL && (contains_primary || TAILQ_EMPTY(&(config.tray_outputs)))) {
                 SLIST_FOREACH(walk, outputs, slist) {
                     if (!walk->active)
                         continue;
@@ -1676,20 +1696,36 @@ void reconfig_windows(bool redraw_bars) {
                 exit(EXIT_FAILURE);
             }
 
-            const char *tray_output = (config.tray_output ? config.tray_output : SLIST_FIRST(outputs)->name);
-            if (!tray_configured && strcasecmp(tray_output, "none") != 0) {
-                /* Configuration sanity check: ensure this i3bar instance handles the output on
-                 * which the tray should appear (e.g. don’t initialize a tray if tray_output ==
-                 * VGA-1 but output == [HDMI-1]).
-                 */
-                i3_output *output;
-                SLIST_FOREACH(output, outputs, slist) {
-                    if (strcasecmp(output->name, tray_output) == 0 ||
-                        (strcasecmp(tray_output, "primary") == 0 && output->primary)) {
-                        init_tray();
-                        break;
+            /* Unless "tray_output none" was specified, we need to initialize the tray. */
+            const char *first = (TAILQ_EMPTY(&(config.tray_outputs))) ? SLIST_FIRST(outputs)->name : TAILQ_FIRST(&(config.tray_outputs))->output;
+            if (!tray_configured && strcasecmp(first, "none") != 0) {
+                /* We do a sanity check here to ensure that this i3bar instance actually handles
+                 * the output on which the tray should appear. */
+
+                /* If no tray_output was specified, we go ahead and initialize the tray as
+                 * we will be using the first available output. */
+                if (TAILQ_EMPTY(&(config.tray_outputs)))
+                    init_tray();
+
+                /* If a or many tray_output were specified, we ensure that at least one of
+                 * them is actually an output managed by this instance. */
+                tray_output_t *tray_output;
+                TAILQ_FOREACH(tray_output, &(config.tray_outputs), tray_outputs) {
+                    i3_output *output;
+                    bool found = false;
+                    SLIST_FOREACH(output, outputs, slist) {
+                        if (strcasecmp(output->name, tray_output->output) == 0 ||
+                            (strcasecmp(tray_output->output, "primary") == 0 && output->primary)) {
+                            found = true;
+                            init_tray();
+                            break;
+                        }
                     }
+
+                    if (found)
+                        break;
                 }
+
                 tray_configured = true;
             }
         } else {
diff --git a/include/config.h b/include/config.h
index fd6fe6c09..260447aa6 100644
--- a/include/config.h
+++ b/include/config.h
@@ -247,9 +247,10 @@ struct Barconfig {
      * simplicity (since we store just strings). */
     char **outputs;
 
-    /** Output on which the tray should be shown. The special value of 'no'
-     * disables the tray (it’s enabled by default). */
-    char *tray_output;
+    /* List of outputs on which the tray is allowed to be shown, in order.
+     * The special value "none" disables it (per default, it will be shown) and
+     * the special value "primary" enabled it on the primary output. */
+    TAILQ_HEAD(tray_outputs_head, tray_output_t) tray_outputs;
 
     /* Padding around the tray icons. */
     int tray_padding;
@@ -361,6 +362,12 @@ struct Barbinding {
     TAILQ_ENTRY(Barbinding) bindings;
 };
 
+struct tray_output_t {
+    char *output;
+
+    TAILQ_ENTRY(tray_output_t) tray_outputs;
+};
+
 /**
  * Finds the configuration file to use (either the one specified by
  * override_configpath), the user’s one or the system default) and calls
diff --git a/src/config.c b/src/config.c
index d8db85e63..229d71ae8 100644
--- a/src/config.c
+++ b/src/config.c
@@ -111,7 +111,6 @@ void load_configuration(xcb_connection_t *conn, const char *override_configpath,
             for (int c = 0; c < barconfig->num_outputs; c++)
                 free(barconfig->outputs[c]);
             FREE(barconfig->outputs);
-            FREE(barconfig->tray_output);
             FREE(barconfig->socket_path);
             FREE(barconfig->status_command);
             FREE(barconfig->i3bar_command);
diff --git a/src/config_directives.c b/src/config_directives.c
index cd0432fe0..9ca305c14 100644
--- a/src/config_directives.c
+++ b/src/config_directives.c
@@ -518,8 +518,9 @@ CFGFUN(bar_socket_path, const char *socket_path) {
 }
 
 CFGFUN(bar_tray_output, const char *output) {
-    FREE(current_bar.tray_output);
-    current_bar.tray_output = sstrdup(output);
+    struct tray_output_t *tray_output = scalloc(1, sizeof(struct tray_output_t));
+    tray_output->output = sstrdup(output);
+    TAILQ_INSERT_TAIL(&(current_bar.tray_outputs), tray_output, tray_outputs);
 }
 
 CFGFUN(bar_tray_padding, const long padding_px) {
@@ -554,6 +555,7 @@ CFGFUN(bar_strip_workspace_numbers, const char *value) {
 
 CFGFUN(bar_start) {
     TAILQ_INIT(&(current_bar.bar_bindings));
+    TAILQ_INIT(&(current_bar.tray_outputs));
     current_bar.tray_padding = 2;
 }
 
diff --git a/src/ipc.c b/src/ipc.c
index 1a8d28ed2..433141f7e 100644
--- a/src/ipc.c
+++ b/src/ipc.c
@@ -544,6 +544,18 @@ static void dump_bar_config(yajl_gen gen, Barconfig *config) {
         y(array_close);
     }
 
+    if (!TAILQ_EMPTY(&(config->tray_outputs))) {
+        ystr("tray_outputs");
+        y(array_open);
+
+        struct tray_output_t *tray_output;
+        TAILQ_FOREACH(tray_output, &(config->tray_outputs), tray_outputs) {
+            ystr(tray_output->output);
+        }
+
+        y(array_close);
+    }
+
 #define YSTR_IF_SET(name)       \
     do {                        \
         if (config->name) {     \
@@ -552,8 +564,6 @@ static void dump_bar_config(yajl_gen gen, Barconfig *config) {
         }                       \
     } while (0)
 
-    YSTR_IF_SET(tray_output);
-
     ystr("tray_padding");
     y(integer, config->tray_padding);
 
diff --git a/testcases/t/177-bar-config.t b/testcases/t/177-bar-config.t
index cc4826c15..3c2cc67c3 100644
--- a/testcases/t/177-bar-config.t
+++ b/testcases/t/177-bar-config.t
@@ -135,7 +135,7 @@ ok(!$bar_config->{binding_mode_indicator}, 'mode indicator disabled');
 is($bar_config->{mode}, 'dock', 'dock mode');
 is($bar_config->{position}, 'top', 'position top');
 is_deeply($bar_config->{outputs}, [ 'HDMI1', 'HDMI2' ], 'outputs ok');
-is($bar_config->{tray_output}, 'HDMI2', 'tray_output ok');
+is_deeply($bar_config->{tray_outputs}, [ 'LVDS1', 'HDMI2' ], 'tray_output ok');
 is($bar_config->{tray_padding}, 0, 'tray_padding ok');
 is($bar_config->{font}, 'Terminus', 'font ok');
 is($bar_config->{socket_path}, '/tmp/foobar', 'socket_path ok');
@@ -288,7 +288,7 @@ ok($bar_config->{binding_mode_indicator}, 'mode indicator enabled');
 is($bar_config->{mode}, 'dock', 'dock mode');
 is($bar_config->{position}, 'top', 'position top');
 is_deeply($bar_config->{outputs}, [ 'HDMI1', 'HDMI2' ], 'outputs ok');
-is($bar_config->{tray_output}, 'HDMI2', 'tray_output ok');
+is_deeply($bar_config->{tray_output}, [ 'LVDS1', 'HDMI2' ], 'tray_output ok');
 is($bar_config->{font}, 'Terminus', 'font ok');
 is($bar_config->{socket_path}, '/tmp/foobar', 'socket_path ok');
 is_deeply($bar_config->{colors},
